# Scrape.php

Scrape.php is a PHP script which is to be run from the command line. Simply pass a series of course URLs to the script and it will scrape, download, and zip up the course, suitable for local browsing.

# Scraping URLs

Pass a list of (space separated) URLs. Each URL should be the **first page of a module** only. No need to pass anything else, as the scraper will go through the entire course and download each page.


```
$ php scrape.php https://demo.coursearc.com/content/accessibility-101-public-version/accessibility/introduction/ https://demo.coursearc.com/content/world-history-public/world-war/introduction/
```

## Additional Options

Following are some additional options you can pass to the scraper.

### Custom ZIP Name

If you want to pass a custom name for the .zip file, just add the `zip=` flag with your custom name, minus the .zip extension.

```
$ php scrape.php http://demo.coursearc.com/content/algebra-i/introduction zip=myZip
```
### Debugging
If you want to do simple debugging, just pass the `debug` flag.

```
$ php scrape.php http://demo.coursearc.com/content/algebra-i/introduction debug
```

If you want to do more in-depth debugging, simply pass the `debugDeep` flag

```
$ php scrape.php http://demo.coursearc.com/content/algebra-i/introduction debugDeep
```

If you don't want to zip the course, just pass the `noZip` flag

```
$ php scrape.php http://demo.coursearc.com/content/algebra-i/introduction noZip
```


# Scraping CSV Files

If you would like to scrape a CSV file, just pass the filename as the only option. Each course will be zipped, with the zip filename being `course-name.zip`. Note that the additional options above do not work with CSV file scraping.

```
php scrape.php filename.csv
```

The CSV file must be formatted as `course-name,URL`, and each URL must be on its own line, with no trailing commas.

```
Algebra-I,http://demo.coursearc.com/content/algebra-i/introduction
Algebra-II,http://demo.coursearc.com/content/algebra-ii/introduction
Algebra-II,http://demo.coursearc.com/content/algebra-ii/lesson-overview
```

# Failed Scrapes

If there are URLs which failed to be scraped, they will be placed into a `failedScrapes` folder so that you may revisit them.

The easiest way to remedy failed scrapes is to go through the `failedScrapes` directory and fix the URLs, and then, instead of copy/pasting them into a new CSV file, just pass the `failedScrapes` directory name to the scraper and it will re-run each .txt file and scrape the URLs.

```
php scrape.php failedScrapes
```

Note that you can pass any directory name using the `dir=` flag, just make sure it's full of .txt files in the proper format. The `failedScrapes` flag above is basically an alias of `dir=failedScrapes`.

```
php scrape.php dir=myDirectory
```

# Scorm Example
php scrape.php https://demo.coursearc.review/content/adamscrapefixes/sample-lesson/introduction scorm nozip masteryscore=80 pointspossible=24 savelocation=/srv/app/accounts/DOMAIN/files/ scormSave zip=adams  

This copies the files from the ScormSample folder in this project along with scrape to build a scorm course. 
You will need all w params nozip scorm pointspossible18 masteryscore75 savelocation=your/dir/here/ to create a valid scorm pacakge.
Set the number on pointspossible to be total points for entire module. The masteryscore is the percentage to pass course. The saveLocation is where to save completed scorm package on the server


