#!/bin/bash
# This script is used with scrape.php to zip each link individually as requested by Virtual Arkansas
# create an array of url's to scrape and it will run scrape.php multiple times zipping each module individually
# **Make Sure to pass in the zip=NameOfZip to get unique names for each module so they are not overwritten**
# This will zip each course with individual zips for each module in the course

urls4=("https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/learn-it-1-production-process-monitoring zip=Learn-It-1-Production-Process-Monitoring"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/solve-it-production-process-monitoring-and-industry-wide-standards zip=Solve-It-Production-Process-Monitoring-and-Industry-Wide-Standard"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/learn-it-2-thermal-hvac-and-refrigeration-systems zip=Learn-It-2-Thermal-HVAC-and-Refrigeration-System"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/learn-it-1-physical-health-hazards zip=Learn-It-1-Physical-Health-Hazards")

for i in urls4 "${urls4[@]}"
do

   php scrape.php $i
   
done;
cd zips
zip -r Final4.zip .
mv Final4.zip ../VirtualArkansasFinished

exit


urls5=("https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/closing-lesson zip=Closing-the-Lesson-64"
"https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/learn-it-1-operations-management zip=Learn-It-1-Operations-Management-53"
"https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/learn-it-2-production-activities zip=Learn-It-2-Production-Activities-55"
"https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/lesson-overview zip=Lesson-Overview-51"
"https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/more-resources zip=More-Resources-62"
"https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/solve-it-critical-work-functions zip=Solve-it-Critical-Work-Functions-60"
"https://virtualarkansas.coursearc.com/content/5-1-critical-work-functions/watch-it-machine-operator zip=Watch-It-Machine-Operator-57"
"https://virtualarkansas.coursearc.com/content/5-2-systems/closing-lesson zip=Closing-the-Lesson-69"
"https://virtualarkansas.coursearc.com/content/5-2-systems/learn-it-1-physical-systems zip=Learn-It-1-Physical-Systems-61"
"https://virtualarkansas.coursearc.com/content/5-2-systems/learn-it-2-business-system zip=Learn-It-2-Business-as-a-System-63"
"https://virtualarkansas.coursearc.com/content/5-2-systems/learn-it-3-organizational-structure zip=Learn-It-3-Organizational-Structure-65"
"https://virtualarkansas.coursearc.com/content/5-2-systems/lesson-overview zip=Lesson-Overview-58"
"https://virtualarkansas.coursearc.com/content/5-2-systems/more-resources zip=More-Resources-68"
"https://virtualarkansas.coursearc.com/content/5-2-systems/solve-it-systems zip=Solve-It-Systems-67"
"https://virtualarkansas.coursearc.com/content/5-2-systems/watch-it-systems-engineer-lockheed-martin zip=Watch-It-Systems-Engineer---Lockheed-Martin-66"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/closing-lesson zip=Closing-the-Lesson-80"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/learn-it-1-materials zip=Learn-It-1-Materials-71"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/learn-it-2-precision-measurement zip=Learn-It-2-Precision-Measurement-72"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/lesson-overview zip=Lesson-Overview-70"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/more-resources zip=More-Resources-78"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/solve-it-materials-and-precision-measurement zip=Solve-It-Materials-and-Precision-Measurement-76"
"https://virtualarkansas.coursearc.com/content/5-3-materials-and-precision-measurement/watch-it-senior-machinist-smith-and-nephew zip=Watch-It-Senior-Machinist---Smith--Nephew-74"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/closing-lesson zip=Closing-the-Lesson-83"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/learn-it-1-manual-semi-automated-and-automated-systems zip=Learn-It-1-Manual-Semi-Automated-and-Automated-Systems-75"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/learn-it-2-statistical-process-control-and-analytical-testing zip=Learn-It-2-Statistical-Process-Control-and-Analytical-Testing-77"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/lesson-overview zip=Lesson-Overview-73"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/more-resources zip=More-Resources-82"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/solve-it-tools-equipment-and-automated-systems zip=Solve-It-Tools-Equipment-and-Automated-Systems-81"
"https://virtualarkansas.coursearc.com/content/5-4-tools-equipment-and-automated-systems/watch-it-teaching-automation-coworkers zip=Watch-It-Teaching-Automation-to-Coworkers-79"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/closing-lesson zip=Closing-the-Lesson-350"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/learn-it-1-process-applications-and-operation zip=Learn-It-1-Process-Applications-and-Operation-332"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/learn-it-2-manufacturing-types zip=Learn-It-2-Manufacturing-Types-337"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/lesson-overview zip=Lesson-Overview-326"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/more-resources zip=More-Resources-348"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/solve-it-manufacturing-types-processes-and-operations zip=Solve-It-Manufacturing-Types-Processes-and-Operations-345"
"https://virtualarkansas.coursearc.com/content/5-5-manufacturing-types-processes-and-operations/watch-it-manufacturing-engineering-technician-microport-orthopedics zip=Watch-It-Manufacturing-Engineering-Technician---MicroPort-Orthopedics-341"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/closing-lesson zip=Closing-the-Lesson-349"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/learn-it-1-industrial-process zip=Learn-It-1-Industrial-Process-327"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/learn-it-2-industrial-production zip=Learn-It-2-Industrial-Production-333"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/lesson-overview zip=Lesson-Overview-320"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/more-resources zip=More-Resources-346"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/solve-it-industrial-processes-and-production zip=Solve-It-Industrial-Processes-and-Production-342"
"https://virtualarkansas.coursearc.com/content/5-6-industrial-processes-and-production/watch-it-technicians-abbott zip=Watch-It-Technicians-at-Abbott-338"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/closing-lesson zip=Closing-the-Lesson-347"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/learn-it-1-production-process-monitoring zip=Learn-It-1-Production/Process-Monitoring-321"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/learn-it-2-industry-wide-standards zip=Learn-It-2-Industry-Wide-Standards-328"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/lesson-overview zip=Lesson-Overview-315"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/more-resources zip=More-Resources-343"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/solve-it-production-process-monitoring-and-industry-wide-standards zip=Solve-It-Production/Process-Monitoring-and-Industry-Wide-Standards-339"
"https://virtualarkansas.coursearc.com/content/5-7-production-process-monitoring-and-industry-wide-standards/watch-it-cnc-machinist-keeler-iron-works zip=Watch-It-CNC-Machinist---Keeler-Iron-Works-334"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/closing-lesson zip=Closing-the-Lesson-344"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/learn-it-1-project-management zip=Learn-It-1-Project-Management-316"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/learn-it-2-organizational-design zip=Learn-It-2-Organizational-Design-322"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/lesson-overview zip=Lesson-Overview-311"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/more-resources zip=More-Resources-340"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/solve-it-project-management-and-organizational-design zip=Solve-It-Project-Management-and-Organizational-Design-335"
"https://virtualarkansas.coursearc.com/content/5-8-project-management-and-organizational-design/watch-it-manager-technical-services-and-project-management zip=Watch-It-Manager-of-Technical-Services-and-Project-Management-329")

for i in urls5 "${urls5[@]}"
do

   php scrape.php $i
   
done;
cd zips
zip -r 5.zip .
mv 5.zip ../VirtualArkansasFinished
cd ..
rm -rf zips


urls6=("https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/closing-lesson zip=Closing-the-Lesson-94"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/learn-it-1-support-installation-customization-or-upgrading-equipment zip=Learn-It-1-Support-the-Installation-Customization-or-Upgrading-of-Equipment-85"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/learn-it-2-maintenance-and-repair zip=Learn-It-2-Maintenance-and-Repair-86"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/learn-it-3-documentation-and-communication zip=Learn-It-3-Documentation-and-Communication-87"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/lesson-overview zip=Lesson-Overview-84"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/more-resources zip=More-Resources-92"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/solve-it-understanding-critical-work-functions zip=Solve-It-Understanding-Critical-Work-Functions-90"
"https://virtualarkansas.coursearc.com/content/6-1-critical-work-functions/watch-it-condensers-explained zip=Watch-It-Condensers-Explained-88"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/closing-lesson zip=Closing-the-Lesson-336"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/learn-it-1-general-skills-part-1 zip=Learn-It-1-General-Skills-Part-1-302"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/learn-it-2-general-skills-part-2 zip=Learn-It-2-General-Skills-Part-2-307"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/learn-it-3-machining-skills zip=Learn-It-3-Machining-Skills-312"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/lesson-overview zip=Lesson-Overview-296"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/more-resources zip=More-Resources-330"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/solve-it-understanding-general-and-machining-skills zip=Solve-It-Understanding-General-and-Machining-Skills-323"
"https://virtualarkansas.coursearc.com/content/6-2-general-and-machining-skills/watch-it-milling-machine-fundamentals zip=Watch-It-Milling-Machine-Fundamentals-317"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/closing-lesson zip=Closing-the-Lesson-331"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/learn-it-1-electrical-and-electronic-systems zip=Learn-It-1-Electrical-and-Electronic-Systems-297"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/learn-it-2-hydraulic-and-pneumatic-systems zip=Learn-It-2-Hydraulic-and-Pneumatic-Systems-303"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/learn-it-3-mechanical-and-electromechanical-systems zip=Learn-It-3-Mechanical-and-Electromechanical-Systems-308"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/lesson-overview zip=Lesson-Overview-290"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/more-resources zip=More-Resources-324"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/solve-it-understanding-mir-process-specific-skills-part-1 zip=Solve-It-Understanding-MIR-Process-Specific-Skills---Part-1-318"
"https://virtualarkansas.coursearc.com/content/6-3-mir-process-specific-skills-part-1/watch-it-electrical-systems-maintenance zip=Watch-It-Electrical-Systems-Maintenance-313"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/closing-lesson zip=Closing-the-Lesson-325"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/learn-it-1-piping-operations-and-high-vacuum-systems zip=Learn-It-1-Piping-Operations--High-Vacuum-Systems-291"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/learn-it-2-network-and-communication-systems-and-programmable-systems zip=Learn-It-2-Network-and-Communication-Systems-and-Programmable-Systems-298"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/learn-it-3-mechanical-power-transmission-systems-and-belt-drives-and-chain-drives zip=Learn-It-3-Mechanical-Power-Transmission-Systems-and-Belt-Drives-and-Chain-Drives-304"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/lesson-overview zip=Lesson-Overview-285"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/more-resources zip=More-Resources-319"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/solve-it-understanding-mir-process-specific-skills-part-2 zip=Solve-It-Understanding-MIR-Process-Specific-Skills---Part-2-314"
"https://virtualarkansas.coursearc.com/content/6-4-mir-process-specific-skills-part-2/watch-it-piping-and-instrumentation-diagrams zip=Watch-It-Piping-and-Instrumentation-Diagrams-309"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/closing-lesson zip=Closing-the-Lesson-99"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/learn-it-1-laser-systems zip=Learn-It-1-Laser-Systems-91"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/learn-it-2-machine-automation-systems zip=Learn-It-2-Machine-Automation-Systems-93"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/learn-it-3-bearings-and-couplings-and-lubrication-processes zip=Learn-It-3-Bearings-and-Couplings-and-Lubrication-Processes-95"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/lesson-overview zip=Lesson-Overview-89"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/more-resources zip=More-Resources-98"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/solve-it-understanding-mir-process-specific-skills-part-3 zip=Solve-It-Understanding-MIR-Process-Specific-Skills---Part-3-97"
"https://virtualarkansas.coursearc.com/content/6-5-mir-process-specific-skills-part-3/watch-it-industrial-controls zip=Watch-It-Industrial-Controls-96"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/closing-lesson zip=Closing-the-Lesson-310"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/learn-it-1-computer-systems-and-process-controls zip=Learn-It-1-Computer-Systems-and-Process-Controls-274"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/learn-it-2-thermal-hvac-and-refrigeration-systems zip=Learn-It-2-Thermal-(HVAC)-and-Refrigeration-Systems-280"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/learn-it-3-pump-systems zip=Learn-It-3-Pump-Systems-286"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/lesson-overview zip=Lesson-Overview-268"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/more-resources zip=More-Resources-305"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/solve-it-understanding-mir-process-specific-skills-part-4 zip=Solve-It-Understanding-MIR-Process-Specific-Skills---Part-4-299"
"https://virtualarkansas.coursearc.com/content/6-6-mir-process-specific-skills-part-4/watch-it-service-level-engineer zip=Watch-It-Service-Level-Engineer-292"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/closing-lesson zip=Closing-the-Lesson-306"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/learn-it-1-water-treatment-systems-and-heat-exchange zip=Learn-It-1-Water-Treatment-Systems-and-Heat-Exchange-269"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/learn-it-2-optics-and-high-voltage-and-utility-systems zip=Learn-It-2-Optics-and-High-Voltage--Utility-Systems-275"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/learn-it-3-programmable-logic-controlled-industrial-equipment-maintenance-installation-and-repair zip=Learn-It-3-Programmable-Logic-Controlled-Industrial-Equipment-Maintenance-Installation-and-Repair-281"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/lesson-overview zip=Lesson-Overview-261"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/more-resources zip=More-Resources-300"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/solve-it-understanding-mir-process-specific-skills-part-5 zip=Solve-It-Understanding-MIR-Process-Specific-Skills---Part-5-293"
"https://virtualarkansas.coursearc.com/content/6-7-mir-process-specific-skills-part-5/watch-it-optics zip=Watch-It-Optics-287"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/closing-lesson zip=Closing-the-Lesson-301"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/learn-it-1-chemical-processing-and-x-ray-systems zip=Learn-It-1-Chemical-Processing-and-X-ray-Systems-262"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/learn-it-2-reliability-and-maintainability zip=Learn-It-2-Reliability-and-Maintainability-270"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/learn-it-3-clean-room zip=Learn-It-3-Clean-Room-276"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/lesson-overview zip=Lesson-Overview-255"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/more-resources zip=More-Resources-294"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/solve-it-understanding-reliability-and-maintainability zip=Solve-It-Understanding-Reliability-and-Maintainability-288"
"https://virtualarkansas.coursearc.com/content/6-8-reliability-and-maintainability/watch-it-reliability-engineers zip=Watch-It-Reliability-Engineers-282")

for i in urls6 "${urls6[@]}"
do

   php scrape.php $i
   
done;
cd zips
zip -r 6.zip .
mv 6.zip ../VirtualArkansasFinished
cd ..
rm -rf zips


urls7=("https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/closing-lesson zip=Closing-the-Lesson-295"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/learn-it-1-target-market zip=Learn-It-1-Target-Market-256"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/learn-it-2-production zip=Learn-It-2-Production-263"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/learn-it-3-sales zip=Learn-It-3-Sales-271"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/lesson-overview zip=Lesson-Overview-249"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/more-resources zip=More-Resources-289"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/solve-it-work-functions zip=Solve-It-Work-Functions-283"
"https://virtualarkansas.coursearc.com/content/7-1-critical-work-functions/watch-it-sales zip=Watch-It-Sales-277"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/closing-lesson zip=Closing-the-Lesson-284"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/learn-it-1-history-manufacturing zip=Learn-It-1-History-of-Manufacturing-250"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/learn-it-2-manufacturing-global-society zip=Learn-It-2-Manufacturing-in-a-Global-Society-257"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/lesson-overview zip=Lesson-Overview-241"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/more-resources zip=More-Resources-278"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/solve-it-understanding-manufacturing zip=Solve-It-Understanding-Manufacturing-272"
"https://virtualarkansas.coursearc.com/content/7-2-automated-material-handling-and-global-impact/watch-it-industrial-production-managers zip=Watch-It-Industrial-Production-Managers-264"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/closing-lesson zip=Closing-the-Lesson-279"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/learn-it-1-production-scheduling zip=Learn-It-1-Production-Scheduling-242"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/learn-it-2-managing-inventory zip=Learn-It-2-Managing-Inventory-251"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/lesson-overview zip=Lesson-Overview-234"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/more-resources zip=More-Resources-273"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/solve-it-knowledge-operations zip=Solve-It-Knowledge-of-Operations-265"
"https://virtualarkansas.coursearc.com/content/7-3-scheduling-planning-and-executing-operations/watch-it-operations zip=Watch-It-Operations-258"
"https://virtualarkansas.coursearc.com/content/7-4-managing-inventory/closing-lesson zip=Closing-the-Lesson-266"
"https://virtualarkansas.coursearc.com/content/7-4-managing-inventory/learn-it-1-maintaining-and-expediting-inventory zip=Learn-It-1-Maintaining-and-Expediting-Inventory-235"
"https://virtualarkansas.coursearc.com/content/7-4-managing-inventory/lesson-overview zip=Lesson-Overview-227"
"https://virtualarkansas.coursearc.com/content/7-4-managing-inventory/more-resources zip=More-Resources-259"
"https://virtualarkansas.coursearc.com/content/7-4-managing-inventory/solve-it-inventory-management zip=Solve-It-Inventory-Management-252"
"https://virtualarkansas.coursearc.com/content/7-4-managing-inventory/watch-it-managing-inventory zip=Watch-It-Managing-Inventory-243"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/closing-lesson zip=Closing-the-Lesson-267"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/learn-it-1-packaging-and-labeling zip=Learn-It-1-Packaging-and-Labeling-228"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/learn-it-2-warehouse-management zip=Learn-It-2-Warehouse-Management-236"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/lesson-overview zip=Lesson-Overview-221"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/more-resources zip=More-Resources-260"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/solve-it-understanding-packaging-and-labeling zip=Solve-It-Understanding-Packaging-and-Labeling-253"
"https://virtualarkansas.coursearc.com/content/7-5-packaging-and-distributing-product/watch-it-packaging-and-labeling zip=Watch-It-Packaging-and-Labeling-244"
"https://virtualarkansas.coursearc.com/content/7-6-production-systems-and-resources-planning/closing-lesson zip=Closing-the-Lesson-254"
"https://virtualarkansas.coursearc.com/content/7-6-production-systems-and-resources-planning/learn-it-1-production-management zip=Learn-It-1-Production-Management-222"
"https://virtualarkansas.coursearc.com/content/7-6-production-systems-and-resources-planning/lesson-overview zip=Lesson-Overview-214"
"https://virtualarkansas.coursearc.com/content/7-6-production-systems-and-resources-planning/more-resources zip=More-Resources-245"
"https://virtualarkansas.coursearc.com/content/7-6-production-systems-and-resources-planning/solve-it-understanding-production zip=Solve-It-Understanding-Production-237"
"https://virtualarkansas.coursearc.com/content/7-6-production-systems-and-resources-planning/watch-it-production zip=Watch-It-Production-229"
"https://virtualarkansas.coursearc.com/content/7-7-supply-chain-management/closing-lesson zip=Closing-the-Lesson-246"
"https://virtualarkansas.coursearc.com/content/7-7-supply-chain-management/learn-it-1-supply-chain-management zip=Learn-It-1-Supply-Chain-Management-215"
"https://virtualarkansas.coursearc.com/content/7-7-supply-chain-management/lesson-overview zip=Lesson-Overview-208"
"https://virtualarkansas.coursearc.com/content/7-7-supply-chain-management/more-resources zip=More-Resources-238"
"https://virtualarkansas.coursearc.com/content/7-7-supply-chain-management/solve-it-understanding-supply-chain-management zip=Solve-It-Understanding-Supply-Chain-Management-230"
"https://virtualarkansas.coursearc.com/content/7-7-supply-chain-management/watch-it-supply-chain-management zip=Watch-It-Supply-Chain-Management-223"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/closing-lesson zip=Closing-the-Lesson-247"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/learn-it-1-supply-chain-workflow zip=Learn-It-1-Supply-Chain-Workflow-209"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/learn-it-2-scheduling zip=Learn-It-2-Scheduling-216"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/lesson-overview zip=Lesson-Overview-202"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/more-resources zip=More-Resources-239"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/solve-it-supply-chain-workflow zip=Solve-It-Supply-Chain-Workflow-231"
"https://virtualarkansas.coursearc.com/content/7-8-supply-chain-workflow/watch-it-supply-chain-manager zip=Watch-It-Supply-Chain-Manager-224")

for i in urls7 "${urls7[@]}"
do

   php scrape.php $i
   
done;
cd zips
zip -r 7.zip .
mv 7.zip ../VirtualArkansasFinished
cd ..
rm -rf zips

urls8=("https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/closing-lesson zip=Closing-the-Lesson-248"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/learn-it-1-what-quality-assurance zip=Learn-It-1-What-is-Quality-Assurance-203"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/learn-it-2-tools-trade zip=Learn-It-2--Tools-of-the-Trade-210"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/learn-it-3-careers-and-skills zip=Learn-It-3-Careers--Skills-217"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/lesson-overview zip=Lesson-Overview-196"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/more-resources zip=More-Resources-240"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/solve-it-tools-trade zip=Solve-It-Tools-of-the-Trade-232"
"https://virtualarkansas.coursearc.com/content/8-1-critical-work-functions/watch-it-quality-control-lab-tech zip=Watch-It-Quality-Control-Lab-Tech-225"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/closing-lesson zip=Closing-the-Lesson-233"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/learn-it-1-quality-standards zip=Learn-It-1-Quality-Standards-190"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/learn-it-2-understanding-quality-management-systems zip=Learn-It-2-Understanding-Quality-Management-Systems-197"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/learn-it-3-quality-inspections zip=Learn-It-3-Quality-Inspections-204"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/lesson-overview zip=Lesson-Overview-184"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/more-resources zip=More-Resources-226"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/solve-it-understanding-quality-assurance-improvement-and-inspection zip=Solve-It-Understanding-Quality-Assurance-Improvement-and-Inspection-218"
"https://virtualarkansas.coursearc.com/content/8-2-quality-assurance-improvement-and-inspection/watch-it-different-jobs-quality-assurance zip=Watch-It-Different-Jobs-in-Quality-Assurance-211"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/closing-lesson zip=Closing-the-Lesson-219"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/learn-it-1-finding-root-cause zip=Learn-It-1-Finding-the-Root-Cause-185"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/learn-it-2-more-tools zip=Learn-It-2-More-Tools-191"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/lesson-overview zip=Lesson-Overview-177"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/more-resources zip=More-Resources-212"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/solve-it-using-problem-solving-tools zip=Solve-It-Using-Problem-Solving-Tools-205"
"https://virtualarkansas.coursearc.com/content/8-3-problem-solving-tools/watch-it-7-basic-quality-control-tools-efficient-project-management zip=Watch-It-7-Basic-Quality-Control-Tools-for-Efficient-Project-Management-198"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/closing-lesson zip=Closing-the-Lesson-220"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/learn-it-1-continuous-improvement zip=Learn-It-1-Continuous-Improvement-178"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/learn-it-2-quality-audits zip=Learn-It-2-Quality-Audits-186"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/learn-it-3-data-analysis zip=Learn-It-3-Data-Analysis-192"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/lesson-overview zip=Lesson-Overview-170"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/more-resources zip=More-Resources-213"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/solve-it-continuous-improvement-quality-audits-and-data-analysis zip=Solve-It-Continuous-Improvement-Quality-Audits-and-Data-Analysis-206"
"https://virtualarkansas.coursearc.com/content/8-4-quality-assurance-audits-and-continuous-improvement/watch-it-senior-product-manager zip=Watch-It-Senior-Product-Manager-199"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/closing-lesson zip=Closing-the-Lesson-207"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/learn-it-1-corrective-and-preventive-action zip=Learn-It-1-Corrective-and-Preventive-Action-171"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/learn-it-2-check-and-do zip=Learn-It-2-Check-and-Do-179"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/lesson-overview zip=Lesson-Overview-163"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/more-resources zip=More-Resources-200"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/solve-it-corrective-and-preventive-action zip=Solve-It-Corrective-and-Preventive-Action-193"
"https://virtualarkansas.coursearc.com/content/8-5-corrective-and-preventive-actions/watch-it-product-support-engineer zip=Watch-It-Product-Support-Engineer-187"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/closing-lesson zip=Closing-the-Lesson-201"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/learn-it-1-lean-manufacturing zip=Learn-It-1-Lean-Manufacturing-157"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/learn-it-2-value-stream-mapping zip=Learn-It-2-Value-Stream-Mapping-164"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/learn-it-3-benchmarking-and-best-practices zip=Learn-It-3-Benchmarking--Best-Practices-172"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/lesson-overview zip=Lesson-Overview-149"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/more-resources zip=More-Resources-194"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/solve-it-best-practice zip=Solve-It-Best-Practice-188"
"https://virtualarkansas.coursearc.com/content/8-6-principles-lean-manufacturing-benchmarking-and-best-practice/watch-it-5-functions-lean-manufacturing zip=Watch-It-5-Functions-of-Lean-Manufacturing-180"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/closing-lesson zip=Closing-the-Lesson-195"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/learn-it-1-statistical-process-control zip=Learn-It-1-Statistical-Process-Control-150"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/learn-it-2-statistical-tools zip=Learn-It-2-Statistical-Tools-158"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/learn-it-3-implementing-statistical-process-control zip=Learn-It-3-Implementing-Statistical-Process-Control-165"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/lesson-overview zip=Lesson-Overview-141"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/more-resources zip=More-Resources-189"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/solve-it-statistical-tools zip=Solve-It-Statistical-Tools-181"
"https://virtualarkansas.coursearc.com/content/8-7-probability-and-statistics-statistical-process-control/watch-it-product-support-engineer-mts-systems zip=Watch-It-Product-Support-Engineer---MTS-Systems-173"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/closing-lesson zip=Closing-the-Lesson-182"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/learn-it-1-data-analysis zip=Learn-It-1-Data-Analysis-142"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/learn-it-2-data-visualization zip=Learn-It-2-Data-Visualization-151"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/lesson-overview zip=Lesson-Overview-134"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/more-resources zip=More-Resources-174"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/solve-it-data-tools zip=Solve-It-Data-Tools-166"
"https://virtualarkansas.coursearc.com/content/8-8-data-analysis-and-presentation/watch-it-picking-right-data zip=Watch-It-Picking-the-Right-Data-159")

for i in urls8 "${urls8[@]}"
do

   php scrape.php $i
   
done;
cd zips
zip -r 8.zip .
mv 8.zip ../VirtualArkansasFinished
cd ..
rm -rf zips

urls9=("https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/closing-lesson zip=Closing-the-Lesson-183"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/learn-it-1-process-safety-management zip=Learn-It-1-Process-Safety-Management-135"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/learn-it-2-occupational-safety-and-health zip=Learn-It-2-Occupational-Safety-and-Health-143"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/learn-it-3-process-safety-who-responsible zip=Learn-It-3-Process-Safety--Who-is-Responsible-152"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/lesson-overview zip=Lesson-Overview-128"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/more-resources zip=More-Resources-175"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/solve-it-process-safety zip=Solve-It-Process-Safety-167"
"https://virtualarkansas.coursearc.com/content/9-1-introduction-process-safety/watch-it-manufacturing-reflection zip=Watch-It-Manufacturing-Reflection-160"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/closing-lesson zip=Closing-the-Lesson-176"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/learn-it-1-hazards-overview zip=Learn-It-1-Hazards-Overview-129"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/learn-it-2-chronic-vs-acute zip=Learn-It-2-Chronic-vs-Acute-136"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/learn-it-3-accident-prevention zip=Learn-It-3-Accident-Prevention-144"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/lesson-overview zip=Lesson-Overview-121"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/more-resources zip=More-Resources-168"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/solve-it-risk-assessment zip=Solve-It-Risk-Assessment-161"
"https://virtualarkansas.coursearc.com/content/9-2-continuous-improvement/watch-it-understanding-risk-assessment zip=Watch-It-Understanding-Risk-Assessment-153"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/closing-lesson zip=Closing-the-Lesson-169"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/learn-it-1-routes-entry-and-environmental-effects zip=Learn-It-1-Routes-of-Entry-and-Environmental-Effects-122"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/learn-it-2-air-pollution-and-control zip=Learn-It-2-Air-Pollution-and-Control-130"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/learn-it-3-water-pollution-and-solid-waste-control zip=Learn-It-3-Water-Pollution-and-Solid-Waste-Control-137"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/lesson-overview zip=Lesson-Overview-115"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/more-resources zip=More-Resources-162"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/solve-it-understanding-environmental-protection-and-waste-management zip=Solve-It-Understanding-Environmental-Protection-and-Waste-Management-154"
"https://virtualarkansas.coursearc.com/content/9-3-environmental-protection-and-waste-management/watch-it-hazards-nitrogen zip=Watch-It-Hazards-of-Nitrogen-145"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/closing-lesson zip=Closing-the-Lesson-155"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/learn-it-1-physical-health-hazards zip=Learn-It-1-Physical/Health-Hazards-116"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/learn-it-2-toxic-metals zip=Learn-It-2-Toxic-Metals-123"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/lesson-overview zip=Lesson-Overview-110"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/more-resources zip=More-Resources-146"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/solve-it-toxic-gases zip=Solve-It-Toxic-Gases-138"
"https://virtualarkansas.coursearc.com/content/9-4-investigation-incidences-hazards/watch-it-hazardous-substances-safety zip=Watch-It-Hazardous-Substances-Safety-131"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/closing-lesson zip=Closing-the-Lesson-156"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/learn-it-1-fire-basics zip=Learn-It-1-Fire-Basics-111"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/learn-it-2-firefighting-equipment zip=Learn-It-2-Firefighting-Equipment-117"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/learn-it-3-responding-fire-emergency zip=Learn-It-3-Responding-to-a-Fire-Emergency-124"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/lesson-overview zip=Lesson-Overview-106"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/more-resources zip=More-Resources-147"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/solve-it-computer-console zip=Solve-It-Computer-Console-139"
"https://virtualarkansas.coursearc.com/content/9-5-fire-basics-and-emergency-response/watch-it-safety-and-emergency-preparedness zip=Watch-It-Safety-and-Emergency-Preparedness-132"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/closing-lesson zip=Closing-the-Lesson-148"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/learn-it-1-key-elements-administrative-control zip=Learn-It-1-Key-Elements-of-Administrative-Control-107"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/learn-it-2-job-safety-and-responsible-care zip=Learn-It-2-Job-Safety-and-Responsible-Care-112"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/learn-it-3-community-awareness-and-emergency-response zip=Learn-It-3-Community-Awareness-and-Emergency-Response-118"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/lesson-overview zip=Lesson-Overview-103"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/more-resources zip=More-Resources-140"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/solve-it-mechanical-lifting zip=Solve-It-Mechanical-Lifting-133"
"https://virtualarkansas.coursearc.com/content/9-6-administrative-controls/watch-it-confined-space-entry zip=Watch-It-Confined-Space-Entry-125"
"https://virtualarkansas.coursearc.com/content/9-7-safety-permits-and-procedures/closing-lesson zip=Closing-the-Lesson-126"
"https://virtualarkansas.coursearc.com/content/9-7-safety-permits-and-procedures/learn-it-1-permits zip=Learn-It-1-Permits-104"
"https://virtualarkansas.coursearc.com/content/9-7-safety-permits-and-procedures/lesson-overview zip=Lesson-Overview-101"
"https://virtualarkansas.coursearc.com/content/9-7-safety-permits-and-procedures/more-resources zip=More-Resources-119"
"https://virtualarkansas.coursearc.com/content/9-7-safety-permits-and-procedures/solve-it-understanding-permits-and-procedures zip=Solve-It-Understanding-Permits-and-Procedures-113"
"https://virtualarkansas.coursearc.com/content/9-7-safety-permits-and-procedures/watch-it-rigging-safety-procedures zip=Watch-It-Rigging-Safety-Procedures-108"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/closing-lesson zip=Closing-the-Lesson-127"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/learn-it-1-occupational-safety-and-health-act zip=Learn-It-1-Occupational-Safety-and-Health-Act-102"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/learn-it-2-process-safety-management zip=Learn-It-2-Process-Safety-Management-105"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/lesson-overview zip=Lesson-Overview-100"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/more-resources zip=More-Resources-120"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/solve-it-psm zip=Solve-It-PSM-114"
"https://virtualarkansas.coursearc.com/content/9-8-standards-and-additional-knowledge/watch-it zip=Watch-It-109")

for i in urls9 "${urls9[@]}"
do

   php scrape.php $i
   
done;
cd zips
zip -r 9.zip .
mv 9.zip ../VirtualArkansasFinished
cd ..
rm -rf zips
exit
