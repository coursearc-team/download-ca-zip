First of all, thanks for purchasing Rustici Driver.

To get started, you'll want to pick the learning standard (i.e. which version
of SCORM, AICC, cmi5 or xAPI) via which you want to deliver your content. In
the complete version of Rustici Driver, we deliver templates for the
manifests that correspond to each standard. These can be found in
`scormdriver/ScormManifestTemplates` in your Rustici Driver folder. You'll need
to copy the appropriate manifest and related files to the top level of your
Rustici Driver folder.

For instance, if you wanted to create a course conformant with SCORM 1.2,
you'd copy the contents of the `scormdriver/ScormManifestTemplates/1.2`
folder (but not the 1.2 folder itself) to the top level of your Rustici
Driver folder.

After you've done that, you're ready to start implementing the Rustici Driver
API. We recommend that you check out the full documentation here:

http://rustici-docs.s3.amazonaws.com/driver/index.html
