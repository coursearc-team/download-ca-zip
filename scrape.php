<?php

/* TODO
    umbc shows a directory listing when clicking on a link

/******************************************************************************************************/
/*

Filename: 	scrape.php
Version: 	0.3.3
Name: 		CourseArc module content reader/writer
Purpose: 	This script accepts multiple (space separated) URLs as source(s) for an initial scrape, which is 
			transformed into a local file structure and zipped for archival purposes. Local content and
			assets are pulled and written into this directory structure:
			/{uri}
				/{inst}/module directory structure
					/student-glossary
						/js
						/css
				/css
					/custom
				/js
				/files
                /images
            /starter (files are copied from this directory to the archive(s))
			Where inst = first directory of structure, defaults to "/content"
            This entire directory structure is then zipped into a file: {uri}.zip
            
Input: 		Array of URLs. Each must begin with either "http://" or "https://".

Output: 	ZIP file of content, specifically HTML tags in "img", "a", "script", and "link", according
			to spec found here: https://docs.google.com/document/d/1Fq1NCpj6OAh9Y8sSNYvdML8FRFjMz-4mOrCORsuYeaU/edit
            
Debugging:
            pass 'debug' (without the quotes) as an argument to turn on debugging
            pass 'noZip' (without the quotes) as an argument to leave the files uncompressed

Additional Options:
            pass 'includeMath' (without the quotes) as an argument if there is math content and we should include mathjax files

Zip file:
            pass 'zip=ZIPNAME' (without quotes) where ZIP-NAME is the name of the file you wish to produce (e.g.; ZIP-NAME.zip)
            note, you must use hyphens (-), or underscores (_) between words or they will be omitted
            default zip name: domain.zip (e.g.; virtualarkansas.coursearc.com.zip)

*/
/*******************************************************************************************************/


// debug
$debug = false;
$noZip = false;

// globals
$caScormFileList = [];
$includeMath = false;
$glossary = false;
$scorm = false;
$courseName = '';
$links = [];
$masteryScore = 0;
$pointsPossible = 0;
$saveLocation = 'zips';
$firstUrl = '';
$allPages = []; //gets links from nav menu to help determine first and last pages of module for scorm packages
// loop through the passed arguments array, omitting the debugging flags
foreach ($argv as $value)
{
    if (strtolower($value) === 'debug') {
        $debug = true;
    }
    if (strtolower($value) === 'debugdeep') {
        $debugDeep = true;
    }
    if (strtolower($value) === 'nozip') {
        $noZip = true;
    }

    if (strtolower($value) === 'includemath') {
        $includeMath = true;
    }

    if (strtolower($value) === 'scorm') {
        $scorm = true;
    }
    if (substr($value, 0, 13) === 'masteryscore=') {
        $masteryScore = str_replace('masteryscore=', '', $value);
    }
    if (substr($value, 0, 15) === 'pointspossible=') {
        $pointsPossible = str_replace('pointspossible=', '', $value);
    }
    if (substr($value, 0, 13) === 'savelocation=') {
        $saveLocation = str_replace('savelocation=', '', $value);
    }
    if (substr($value, 0, 4) === 'zip=') {
        $zipName = str_replace('zip=', '', $value);
    }
    if (substr($value, 0, 4) === 'dir=') {
        $dirName = str_replace('dir=', '', $value);
    }
    if(strtolower($value) === 'failedscrapes') {
        $dirName = 'failedScrapes';
    }

//    if(!$scorm) {
//        if (strpos($value, '.csv', -4) !== false) {
//            $csvName = $value;
//        }
//    }
    if (substr($value, 0, 7) === 'delete=') {
        $deleteName = str_replace('delete=', '', $value);
    }
    
    if (($value !== 'scrape.php')
        && ($value !== 'debug')
        && (substr($value, 0, 13) !== 'masteryscore=')
        && (substr($value, 0, 15) !== 'pointspossible=')
        && (strtolower($value) !== 'debugdeep')
        && (strtolower($value) !== 'nozip')
        && (strtolower($value) !== 'scorm')
        && (strtolower($value) !== 'includemath')
        && (substr($value, 0, 4) !== 'zip=')
        && !isset($csvName) && !isset($dirName)
        && (substr($value, 0, 7) !== 'delete=')
        && (substr($value, 0, 13) !== 'savelocation=')) {
            $urlArray[] = rtrim($value, '/');
    }

}

makeCoursesDirectoriesAndCopySharedFiles();

if(isset($csvName))
{
    // we're parsing a csv file
    getUrlsFromCsvAndWriteToTxtFile($csvName);
    verifyUrls();
    scrapeUrls();
}
elseif(isset($dirName))
{
    // we're parsing a directory
    getUrlsFromDirectoryAndScrape($dirName);
}
else
{
    // we're parsing a string of urls
    // loop through each url and save files locally
    $i=0;
    foreach ($urlArray as $url)
    {
        checkHeaders($url);
        $urlParts = parse_url($url);
        $protocol = $urlParts['scheme'].'://';
        $domain = $urlParts['host'];
        
        $firstUrl = $urlParts['path'];

        if(!isset($zipName)) {
            $zipName = $domain;
        }

        checkIfZipExists($zipName);
                
        // grab the contents of the homepage
        $homepage = @file_get_contents($url);
        
        if ($homepage === false) {
            dieMessage("Could not get homepage contents");
        }

        // create an array to hold all of our scraped urls
        $scrapedUrls = array();
        
        // put the first page (e.g.; index.php) into the scrapedUrls array
        $scrapedUrls[] = $firstUrl;        

        // put course info into an array so we can list it on the main start page
        $startPageData[$i]['name'] = getCourseName($homepage);
        $startPageData[$i]['url'] = $url;
        
        // notify which site we're scraping, and the name of the first page
        infoMessage("\nSite URI: {$domain}");
        alert("Scraping: {$firstUrl}\n");
        getIndexFilenameAndPrintScrapingMessage($url);

        // delete dir if it already exists
        if (directoryExists($domain)) {
            shell_exec("rm -rf $domain");
        }

        makeDirectories($domain);

        crawlServerAndDownloadFiles($homepage);

        //Move  the content scraped to domain folder to courses folder
        shell_exec("mv " . $domain . " courses/" . $zipName);

        if(isset($deleteName)) {
            deleteCsvTxtFile($deleteName);
        }
        
        InfoMessage("💎 Finished scraping", 'standout');

        $i++;
    }
    if(!$scorm) {
        makeMainStartPage($startPageData, $zipName);
    }

    if(!$noZip && !$scorm)
    {
        infoMessage("📦 Zipping the ". (count($urlArray) > 1 ? 'courses' : 'course')."...", 'standout');

        // zip the courses directory
        shell_exec("zip -r $zipName.zip courses");

        // move the zip into the zips folder
        shell_exec("mv $zipName.zip zips");

        // delete the courses directory
        shell_exec("rm -rf courses");
    }
    /*
     * It is important copying and moving directories happen in this order.
     * This will set up rustici driver and add scorm dependecies to each of needed files in the configureScorm() functino
     */
    if($scorm && $noZip) {
        updateLocalJs("courses/shared/");

        $courseName = getCourseName($homepage);
        // move required scorm files into directory we will zip then move courses directory into scormcontent folder.       
        shell_exec("cp -r ScormSample " .  $zipName);
        makeDirectory($zipName . "/scormcontent/"); //Make Directory to move inside the newly created scormcontent/ directory 
        shell_exec("cp -r courses/. " .  $zipName . "/scormcontent/courses"); //cp command works differntly in linux and macOS. This should work for both so we can run locally also.
        configureScorm($zipName, $urlArray, $courseName);
        shell_exec("rm -rf " . $zipName . "/ScormManifestTemplates" ); //remove ScormManifestTemplates
        shell_exec("cd ". $zipName . " && zip -r ". $zipName . ".zip *"); //create zip file from all files in folder. Will not work in scorm if you just zip the directory
        shell_exec("cd ". $zipName . " && mv " .  $zipName . ".zip " . $saveLocation); //move zip file to the save directory
        shell_exec("rm -rf " . $zipName . " courses " . $domain . " zips" ); //remove directories that are created during the scrape process
    }
    
    cleanUpAfterCsvScrapes();

    $totalTime = formatExecutionTime(microtime(true), $_SERVER['REQUEST_TIME_FLOAT']);

    if(!isset($deleteName)) {
        beep();
    }

    successMessage("🎉 Finished in {$totalTime}", 'standout');
}

exit(0);


// FUNCTIONS
/*
 * This function modifies the scrom driver files to package scorm content
 * Updates files in new scorm package before zipping
 * If scorm content is not working in LMS it is likely that the changes to imsmanifest.xml /scormdriver/indexAPI.html did not happen and are causing problems
 * 
 */

function configureScorm($zipName, $urlArray, $courseName) {
    global $links, $pointsPossible, $masteryScore, $allPages, $caScormFileList;

    $firstLink = basename($allPages[0]);

    if(count($allPages) == 1) {
        $url = $urlArray[0] . '/' . $firstLink;//Building URL to page. Single page modules don't scrape contnent when passing the first page to the module. We pass a link to the module in this case from kickoff-scorm-pacakge.php
        $homepage = @file_get_contents($url);
        $courseName = getCourseName($homepage); //$courseName is used for title. If this is not set the scorm contnet will not work. It is not getting passed in to this function for signle page modules
        $finalLink = basename($allPages[0]);
    } else {
        $pos = count($allPages) -1;
        if ($pos == -1) { 
	        $pos = 0 ; 
        }
        $finalLink = basename($allPages[$pos]);
    }

    $courseNameEncoded = htmlspecialchars($courseName);

    //update the imsManifest.xml with Mastery Score, Title, href to first page in content
    $str = file_get_contents("$zipName/imsmanifest.xml");
    $str = str_replace('<title>Scorm Sample Course</title>', "<title>$courseNameEncoded</title>", $str);
    $str = str_replace('<adlcp:masteryscore>50</adlcp:masteryscore>', "<adlcp:masteryscore>$masteryScore</adlcp:masteryscore>", $str);
    file_put_contents("$zipName/imsmanifest.xml", $str);
    
    //update indexAPI in scromdriver with the start link for the course 
    $str = file_get_contents("$zipName/scormdriver/indexAPI.html");
    $str = str_replace('strContentLocation = "../scormcontent/courses/sample-course-sample-module/content/introduction.htm"; ', 'strContentLocation = "../scormcontent/courses/'.$zipName.'/content/'.$firstLink.'.htm"; ' , $str);
    $str = file_put_contents("$zipName/scormdriver/indexAPI.html", $str);

    //open last file of the course and insert AutoCompleteSCO.js after auto bookmark to show completion of the course
    $old = '<script type="text/javascript" src="../../../../scormdriver/auto-scripts/AutoBookmark.js"></script>';
    $new = '<script type="text/javascript" src="../../../../scormdriver/auto-scripts/AutoBookmark.js"></script>'."\n".'<script type="text/javascript" src="../../../../scormdriver/auto-scripts/AutoCompleteSCO.js"></script>';
    $str = file_get_contents("$zipName/scormcontent/courses/$zipName/content/$finalLink.htm");
    $str = str_replace($old, $new, $str);
    // $old = '</body>';
    // $new = "\n <script>\n$(document).ready(function() { \n scormSetPassFail(); \n });\n</script>\n</body>";//This is important! It will add ScormSetPassFail to the end of the last page of the module
    //$str = str_replace($old, $new, $str);
    $str = file_put_contents("$zipName/scormcontent/courses/$zipName/content/$finalLink.htm", $str);
    
    //add points param to ca_postGrade() call in ca_app_vue.js
    $str = file_get_contents("$zipName/scormcontent/courses/$zipName/js/ca_app_vue.js");
    $old = 'ca_postGrade(incorrectRatio, this.bId, comment);';
    $new = "ca_postGrade(incorrectRatio, this.bId, 'ca_app_vue', incorrectRatio);";
    $str = str_replace($old, $new, $str);
    $str = str_replace("/assets/fa-", "../css/fonts/fa-", $str);
    $str = file_put_contents("$zipName/scormcontent/courses/$zipName/js/ca_app_vue.js", $str);

    //update ca.js with masteryScore and pointsPossible
    $str = file_get_contents("$zipName/scormcontent/courses/shared/js/ca.js");
    $oldPoints = 'pointsPossible = 10';
    $newPoints = 'pointsPossible = '. $pointsPossible;
    $oldMastery = 'masteryScore = 60';
    $newMastery = 'masteryScore = ' . $masteryScore;
    $str = str_replace($oldPoints, $newPoints, $str);
    $str = str_replace($oldMastery, $newMastery, $str);
    $str = file_put_contents("$zipName/scormcontent/courses/shared/js/ca.js", $str);

    infoMessage("📦Creating Scorm 1.2 package $zipName", 'standout');
}
// this function crawls the given uri and uses a callback to download the remaining files
function crawlServerAndDownloadFiles($inputString)
{
   
    global $scrapedUrls, $domain, $glossary, $protocol, $scorm, $zipName, $includeMath;

    $positions = lookForTags($inputString);
    if (!$scorm) {
        copyMathjaxFromStarterDirectory($inputString);
    } else {
        if ($includeMath) {
            copyMathjaxFromStarterDirectory($inputString);
        }
    }

    if ($positions)
    {
        getLinks($positions);
        getImages($positions);
        getMedia($positions);
        getScripts($positions);
        getZipEmbed($positions); //this does not work locally because it is trying to copy from /srv/app/accounts which doesn't exist when copying from local env
        $urls = getAnchors($positions);

        foreach ($urls as $url)
        {
            debug($url);
            
            if ((substr($url, 0, 1) !== '#') && (substr($url, 0, 11) !== '/course-arc'))
            {
                if (! in_array($url, $scrapedUrls))
                {
                    if (strpos($url, 'http', 0) === false)
                    {
                        $scrapedUrls[] = $url;
                                                
                        alert("Scraping: $url", 'standout');
                        
                        if (isStudentGlossary($url)) {
                            $glossary = true;
                            $dir = '/content/student-glossary/';
                        } else {
                            $dir = '/content';
                        }
                        
                        $myString = @file_get_contents($protocol.$domain.$url.'?clientSideScoring=clientSideScoringEnabled123');
                        
                        debug("$protocol$domain.$url.?clientSideScoring=clientSideScoringEnabled123");

                        
                        if ($myString === false) {
                            message("**");
                            debug("WARNING: Unhandled URL exception\n$url");
                        }

                        if (strlen($url) > 2) {
                            $stpb = strrpos($url, '/', -1);
                            $stpe = strrpos($url, '/', -2);
                        } else {
                            warnMessage("Error: $url", 'beep');
                        }
                        
                        if ($stpb === false || $stpe === false) {
                            message("stpb = $stpb stpe = $stpe");
                            warnMessage("Could not find filename in $url", 'beep');
                        } else { 
                        	$filename = getIndexFilenameAndPrintScrapingMessage($url);
                        
							if (strlen($myString) > 0)
							{
								$fh = fopen($domain . $dir . "/" . $filename, "w");
							
								if ($fh == false) {
									warnMessage("Could not open file {$domain}{$dir}/{$url}.htm", 'beep');
								} else { 
									$hold = $myString;
									$myString = searchAndReplaceStrings($myString, $scorm, $url);
									fwrite($fh, $myString);
									fclose($fh);
							
									// crawl remaining files for each url
									crawlServerAndDownloadFiles($hold);
                                    if(!$scorm) {
                                        makeCourseStartPage($domain, $dir, $filename, $zipName);
                                    }
                                    
								}
							
								
							} else {
								debug("^^^ Zero byte length! Not writing, not crawling.");
							}
							$glossary = false;
                        }
                    }
                }
            }
        }
    }
}

function lookForTags($thisString)
{
    debug("*** looking for tags ***");
    $positions = array();
    $tagArray = ['img', 'a', 'link', 'script', 'source', 'iframe'];

    foreach ($tagArray as $tag)
    {
        $lastPos = 0;
        
        while (($lastPos = strpos($thisString, "<" . $tag . " ", $lastPos)) !== false)
        {
            $endPosition = strpos($thisString, ">", $lastPos);
            
            if ($endPosition !== 0) {
                $startPosition = strrpos($thisString, "<", $endPosition - strlen($thisString));
                $outstring = "\n" . substr($thisString, $startPosition, $endPosition - $startPosition + 1) . "\n";
                $positions[$startPosition]['pos'] = $startPosition;
                $positions[$startPosition]['string'] = $outstring;
                $positions[$startPosition]['tag'] = $tag;
                debug($outstring);
            } else {
                dieMessage("FATAL: Cannot find closing tag", 'beep');
            }
            $lastPos = $endPosition;
        }
    }
    
    debugDeep($positions);

    return $positions;
}

function getImages($positions)
{
    debug("*** getting images ***");

    $imgs = array();

    foreach ($positions as $position)
    {
        if ($position['tag'] == 'img')
        {
            debug("position = ".$position['string']);
            
            
            // replace &quot; with a doublequote (")
            $position['string'] = str_replace('&quot;', '"', $position['string']);

            $srcPosition = strpos($position['string'], 'src', 0);

            if ($srcPosition !== false)
            {
                debugDeep("srcPosition = {$srcPosition}");
                $startQuote = strpos($position['string'], '"', $srcPosition);

                if ($startQuote !== false)
                {
                    debugDeep("startQuote = {$startQuote}");
                    $endQuote = strpos($position['string'], '"', $startQuote + 1);
                    
                    if ($endQuote !== false)
                    {
                        debugDeep("endQuote = {$endQuote}");
                        debug(substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1));
                        
                        $img = substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1);
                        
                        if (substr($img, 0, 6) == '/files') {
                            $dir = '/files/';
                        } else {
                            $dir = '/images/';
                        }
                        
                        saveRemoteFilesLocally($img, $dir);
                        
                        $imgs[] = $img;
                    } else {
                        // dieMessage("FATAL: img endQuote not found", 'beep');
                        warningMessage("img endQuote not found. Skipping.", 'beep');
                    }
                } else {
                    // dieMessage("FATAL: img startQuote not found", 'beep');
                    warningMessage("img startQuote not found. Skipping.", 'beep');
                }
            } else {
                // dieMessage("FATAL: img src not found", 'beep');
                warningMessage("img src not found. Skipping.", 'beep');
            }
        }
    }
    
    debugDeep($imgs);

    return $imgs;
}

function getMedia($positions)
{
    debug("*** getting media files ***");

    $files = array();

    foreach ($positions as $position)
    {
        if ($position['tag'] == 'source')
        {
            debug("position = ".$position['string']);

            // replace &quot; with a doublequote (")
            $position['string'] = str_replace('&quot;', '"', $position['string']);

            $srcPosition = strpos($position['string'], 'src', 0);

            if ($srcPosition !== false)
            {
                debugDeep("srcPosition = {$srcPosition}");
                $startQuote = strpos($position['string'], '"', $srcPosition);

                if ($startQuote !== false)
                {
                    debugDeep("startQuote = {$startQuote}");
                    $endQuote = strpos($position['string'], '"', $startQuote + 1);
                    
                    if ($endQuote !== false)
                    {
                        debugDeep("endQuote = {$endQuote}");
                        debug(substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1));
                        
                        $file = substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1);
                        
                        saveMediaFilesLocally($file, '/files/');
                        
                        $files[] = $file;
                    } else {
                        dieMessage("FATAL: media endQuote not found", 'beep');
                    }
                } else {
                    dieMessage("FATAL: media startQuote not found", 'beep');
                }
            } else {
                dieMessage("FATAL: media src not found", 'beep');
            }
        }
    }
    
    debugDeep($files);
        
    return $files;
}

function getAnchors($positions)
{
    global $protocol, $domain;
    
    $urls = array();
    debug("*** Getting Anchors ***");
    
    foreach ($positions as $position)
    {
        //string check for <a data-title prevents extra /content files from being created. Reported in freshdesk ticket 3301 These come from slideshow block
        if ($position['tag'] == 'a' && !strpos($position['string'], '<a data-title'))
        {
            // determine where 'href' starts in the <a> element string
            debug("position = ".$position['string']);

            // replace &quot; with a doublequote (")
            $position['string'] = str_replace('&quot;', '"', $position['string']);

            $hrefPosition = strpos($position['string'], 'href', 0);
            
            if ($hrefPosition !== false)
            {
                debugDeep("hrefPosition = {$hrefPosition}");
                
                // get the first quote in the href (href="<-)
                $startQuote = strpos($position['string'], '"', $hrefPosition);
                
                if ($startQuote !== false)
                {
                    debugDeep("startQuote = {$startQuote}");
                    
                    // get the last quote in the href (href=""<-)
                    $endQuote = strpos($position['string'], '"', $startQuote + 1);
                    
                    if ($endQuote !== false)
                    {
                        debugDeep("endQuote = {$endQuote}");
                        $url = substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1);

                        // check if the url is to a downloadable file
                        if (substr($url, 0, 24) === '/index.php/download_file') {
                            saveFilesFromHrefsLocally($url, $position);
                        } else {
                            $temp = $url;
                            $extpos = preg_match('/(jpg)/', $temp);
                        
                            if ($extpos) {
                                saveRemoteFilesLocally($url, '/images/');
                            } else {
                            	$validLink = true; 
                            	
                                // we don't want to create a file for urls that start with "/?", so let's omit them
                                // TODO: we need to find out where those urls are being created and stop it there
                                // ALSO skip URLs to /print?cID=
                                if (strpos($url, "/print?cID=") !== false) { 
                                	$validLink = false; 
                                }
                                if (substr($url, 0, 2) === '/?') {
                                	$validLink = false; 
                                }
                                
                                if ($validLink) {
                                    $urls[] = $url;
                                }
                            }
                        }

                        debug(substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1));
                    } else {
                        warningMessage("endQuote not found. Skipping...");
                    }
                } else {
                    // dieMessage("FATAL: a startQuote not found");
                    warningMessage("startQuote not found. Skipping...");
                }
            } else {
                debug("⚠️  WARNING: an href was not found\nThat's ok. It's probably an internal link with js controls");
            }
        }
    }
    
    debugDeep($urls);
    
    return $urls;
}

function getZipEmbed($positions)
{
        $links = array();
        global $domain;
    foreach ($positions as $position)
    {
        if ($position['tag'] == 'iframe')
        {
            debug("position = ".$position['string']);

            // replace &quot; with a doublequote (")
            $position['string'] = str_replace('&quot;', '"', $position['string']);

            $relPosition = strpos($position['string'], 'src', 0);
            
            
//            echo $position['string'] . ' relPosition ';
            if ($relPosition !== false && strpos($position['string'], 'zipembed'))
            {  
                debugDeep("relPosition = {$relPosition}");
                $startQuote = strpos($position['string'], '"', $relPosition);
                
                if ($startQuote !== false)
                {
                    debugDeep("startQuote = {$startQuote}");
                    $endQuote = strpos($position['string'], '"', $startQuote + 1);
                    
                    if ($endQuote !== false)
                    {
                        debugDeep("endQuote = {$endQuote}");
                        $link = substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1);
                        
                        if (strpos($link, 'zipembed')){
                            $dirString = $link;
                            $str = substr(strrchr($dirString,'/'), 1);
                             $dir = substr($dirString, 0, - strlen($str));

                             $remove = basename($dir);
                             $destination = substr($dir, 0, strpos($dir, $remove));
                             $dirName = explode(".", $domain);
                             makeDirectory($domain."/".$dir, true);
                             //copies file from the server to local directory for zipembed content
                             shell_exec("cp -r /srv/worker/accounts/".$dirName[0].$dir." " .  $domain . $destination);
                         }

                        $links[] = $link;
                    } else {
                        dieMessage("FATAL: link endQuote not found", 'beep');
                    }
                } else {
                    dieMessage("FATAL: link startQuote not found", 'beep');
                }
            }
        }
    }
}


function getLinks($positions)
{
    $links = array();

    foreach ($positions as $position)
    {
        if ($position['tag'] == 'link')
        {
            debug("position = ".$position['string']);

            // replace &quot; with a doublequote (")
            $position['string'] = str_replace('&quot;', '"', $position['string']);

            $relPosition = strpos($position['string'], 'href', 0);
            
            if ($relPosition !== false)
            {
                debugDeep("relPosition = {$relPosition}");
                $startQuote = strpos($position['string'], '"', $relPosition);
                
                if ($startQuote !== false)
                {
                    debugDeep("startQuote = {$startQuote}");
                    $endQuote = strpos($position['string'], '"', $startQuote + 1);
                    
                    if ($endQuote !== false)
                    {
                        debugDeep("endQuote = {$endQuote}");
                        $link = substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1);
                       
                        if (isStudentGlossary()) {
                            $dir = '/content/student-glossary/css/';
                        } elseif (substr($link, 0, 6) == '/files' && !substr($link, 0, 18) == '/files//css-custom' && !substr($link, 0, 17) == '/files/css-custom') {
                            $dir = '/files/';
                        } elseif (substr($link, 0, 18) == '/files//css-custom' || substr($link, 0, 17) == '/files/css-custom') {                           
                            $dir = '/files/css-custom/';
                        } else {
                            $dir = '/css/';
                        }
//                        echo $link . ' ' . $dir . "\n";
                        saveRemoteFilesLocally($link, $dir);

                        $links[] = $link;
                    } else {
                        dieMessage("FATAL: link endQuote not found", 'beep');
                    }
                } else {
                    dieMessage("FATAL: link startQuote not found", 'beep');
                }
            } else {
                dieMessage("FATAL: link rel not found", 'beep');
            }
        }
    }

    return $links;
}

function getScripts($positions)
{
    debug("*** getting scripts ***");

    $scripts = array();

    foreach ($positions as $position)
    {
        if ($position['tag'] == 'script')
        {
            debug("position = ".$position['string']);

            // replace &quot; with a doublequote (")
            $position['string'] = str_replace('&quot;', '"', $position['string']);

            $relPosition = strpos($position['string'], 'src', 0);
            
            if ($relPosition !== false)
            {
                debugDeep("relPosition = {$relPosition}");
                $startQuote = strpos($position['string'], '"', $relPosition);
                
                if ($startQuote !== false)
                {
                    debugDeep("startQuote = {$startQuote}");
                    $endQuote = strpos($position['string'], '"', $startQuote + 1);
                    
                    if ($endQuote !== false)
                    {
                        debugDeep("endQuote = {$endQuote}");
                        $script = substr($position['string'], $startQuote + 1, $endQuote - $startQuote - 1);
                       
                        if (isStudentGlossary()) {
                            $dir = '/content/student-glossary/js/';
                        } elseif (substr($script, 0, 6) == '/files') {
                            $dir = '/files/';
                        } else {
                            $dir = '/js/';
                            $scripts[] = $script;
                        }

                        saveRemoteFilesLocally($script, $dir);
                        
                        $scripts[] = $script;
                    } else {
                        dieMessage("FATAL: script endQuote not found", 'beep');
                    }
                } else {
                    dieMessage("FATAL: script startQuote not found", 'beep');
                }
            }
            // GET INLINE SCRIPT TEXT HERE
            else {
                debug("⚠️  WARNING: src not found. That's ok. It's probably just an inline script.");
            }
        }
    }

    debugDeep($scripts);

    return $scripts;
}

function saveRemoteFilesLocally($entity, $dir)
{

    global $protocol, $domain;

    if(!$entity || ctype_space($entity)) { //somehow there are entity that come thorugh with whitespace. Treat whitespace as an empty $entity so the script doesn't die
        warningMessage("File not found. Continuing...");
        return;
    }

    if ((substr($entity, 0, 7) !== 'http://') && (substr($entity, 0, 8) !== 'https://') && (substr($entity, 0, 2) !== '//') && (substr($entity, 0, 10) !== 'data:image'))
    {
        $fileContents = @file_get_contents($protocol.$domain.$entity);

        if ($fileContents === false) {
            debug("⚠️  WARNING: Unhandled URL exception: {$entity}");
        }

        $stpb = strrpos($entity, '/', -1);
        $stpe = strrpos($entity, '/', -2);

        if ($stpb === false || $stpe === false) {
            message("stpb = {$stpb}\nstpe = {$stpe}", 'standout');
            dieMessage("Could not find filename: {$entity}", 'beep');
        }

        if (substr($entity, strlen($entity) - 1, 1) === '/') {
            $filename = substr($entity, $stpe + 1, $stpb - $stpe - 1);
        } else {
            $filename = substr($entity, $stpe + 1);
        }

        if ($filename == 'favicon.ico') {
            $dir = '/images/';
        }

        copyFilesFromStarterDirectory($domain);

        $filename = removeVersioningFromFilename($filename);
        
        if (!file_exists($domain . $dir . $filename))
        {
            // write files to disk...
//            successMessage("✍️  $entity -> $filename");
            
            $fh = fopen($domain . $dir . $filename, "w");
            
            if ($fh == false) {
                dieMessage("Could not open file: {$domain}{$dir}{$filename}");
            }
            elseif (substr($filename, -4) === '.css') {
                fwrite($fh, replaceCssLinks($filename, $fileContents));
            } else {
                fwrite($fh, $fileContents);
            }
            fclose($fh);
        } else {
            // file exists locally; skip it
            mutedMessage("✖️  {$entity} -> {$filename} exists.");
        }
    } else {
        // file is remotely hosted; skip it
        mutedMessage("✖️  {$entity} is remotely hosted.");
    }
}

function saveMediaFilesLocally($entity, $dir)
{
    global $protocol, $domain;

    $entity = trim($entity);

    if(!$entity) {
        warningMessage("Media not found. Continuing...");
        return;
    }

    $fileExtension = pathinfo($entity, PATHINFO_EXTENSION);
    $filename = basename($entity);

    $acceptedTypes = array('mp3', 'mp4');

    if (in_array($fileExtension, $acceptedTypes))
    {
        $fileContents = @file_get_contents($entity);

        if ($fileContents === false) {
            debug("⚠️  WARNING: Unhandled URL exception: {$entity}");
        }

        if (!file_exists($domain.$dir.$filename))
        {
            // write files to disk...
            successMessage("✍️  $entity -> $filename");
            
            $fh = fopen($domain.$dir.$filename, 'w');
            
            if ($fh == false) {
                dieMessage("Could not open file: {$domain}{$dir}{$filename}");
            } else {
                fwrite($fh, $fileContents);
            }
            fclose($fh);
        } else {
            // file exists locally; skip it
            mutedMessage("✖️  {$filename} exists.");
        }
    } else {
        // file is not in accepted array; skip it
        mutedMessage("✖️  {$entity} is not a media file.");
    }
}

function saveFilesFromHrefsLocally($url, $position)
{

    // mp3 files are handled via <a> elements
    // example: <a class="file ext-mp3" title="Trayectos_Mod0_LetraA.mp3" href="/index.php/download_file/view/8754/38532/">
    // we get the filename from the title, and then build a full/custom link to the file

    global $protocol, $domain;
    $dir = '/files/';
    // build the url: add the protocol and domain to the href
    $url = $protocol.$domain.$url;

    $dom = new DOMDocument;
    
    // the string contains the entire <a> element
    @$dom->loadHTML($position['string']);
    
    $links = $dom->getElementsByTagName('a');
    
    foreach ($links as $link)
    {
        // the filename is in the title attribute
        $filename = $link->getAttribute('title');
        
        $fileContents = @file_get_contents($url);

        if ($fileContents === false) {
            debug("⚠️  WARNING: Unhandled URL exception: {$url}");
        }

        if (!$filename || $filename=="") {
            debug("⚠️  WARNING: empty filename. Attempting to download/curl directly now.");
            $filename = directDownload($url, $domain.$dir);
        }

        if (!file_exists($domain.$dir.$filename)) {
            // write files to disk...
            successMessage("✍️  $url -> $filename");
            
            $fh = fopen($domain.$dir.$filename, 'w');
            
            if ($fh == false) {
                dieMessage("Could not open file: {$domain}{$dir}{$filename}");
            } else {
                fwrite($fh, $fileContents);
            }
            fclose($fh);
        } else {
            // file exists locally; skip it
            mutedMessage("✖️  {$filename} exists.");
        }

    }
}


// rewrite concrete urls with relative paths for local viewing
function searchAndReplaceStrings($content, $scorm, $url)
{
    global $protocol, $domain, $studentGlossary, $includeMath;
    
    $pathImg = '../images/';
    $pathCss = '../css/';
    $pathJs = '../js/';
    $pathFiles = '../files/';
    $pathShared = '../../shared/';

    // we need to go up one more level for the student glossary page
    if (strpos($content, '<title>Glossary</title>') !== false) {
        $pathImg = '../' . $pathImg;
        $pathCss = '../' . $pathCss;
        $pathJs = '../' . $pathJs;
        $pathFiles = '../' . $pathFiles;
        $pathShared = '../' . $pathShared;
    }
    
    $content = removeLearnosityScripts($content);
    
    // replace tiny_mce paths, which exist in the starter directory
    $content = str_replace('/concrete/js/tiny_mce', $pathShared.'js/tiny_mce', $content);

    // if we search for 'the generic string: /concrete/js/' we will overwrite everything prior, so we must use a more targeted search string
    $content = str_replace('src="/concrete/js/', 'src="'.$pathJs, $content);

    // if we search for the generic string: '/js/' we will overwrite everything prior, so we must use a more targeted search string
    $content = str_replace('src="/js/', 'src="'.$pathJs, $content);
    
    // replace any script urls that start with // as we can't access them on a local filesystem
    $content = str_replace('src="//', 'src="https://', $content);

    // fix local path for student glossary css directory
//    $content = str_replace('/files//css-custom/', $pathFiles.'css-custom/', $content);
    
   
    // these files exist in the starter directory, which is at the same level as project directory
    $content = str_replace('/themes/ca-generic/css/', $pathCss.'themes/ca-generic/', $content);
    $content = str_replace('/themes/colored-background/css/', $pathCss.'themes/colored-background/', $content);

    // fix paths for css directories
    $content = str_replace('/concrete/css/', $pathCss, $content);

    // student glossary css file
    $content = str_replace('/themes/glossary/css/glossary.css', 'css/glossary.css', $content);

    // if we search for the generic string: '/css/' we will overwrite everything prior, so we must use a more targeted search string
    $content = str_replace('href="/css/', 'href="' . $pathCss, $content);

    // fix images path
    $content = str_replace('/images/', $pathImg, $content);
    $content = str_replace('themes/ca-generic../images/', $pathImg, $content);
    
    // fix slideshow path
    $content = str_replace('/blocks/slideshow', $pathCss, $content);

    // move the path to the ca.js file to the shared directory
    $content = str_replace('../js/ca.js', $pathShared.'js/ca.js', $content);
    // fixes to paths that were causing javascript errors in the console
    $content = str_replace('<link href="/js/ca_app_vue.js', '<link href="' . $pathJs . 'ca_app_vue.js', $content);
    $content = str_replace('..../js/ca_app_vue.js', '../js/ca_app_vue.js', $content);
    
    // fix file paths
    $content = preg_replace('/\/files\/[0-9][0-9][0-9][0-9]\/[0-9][0-9][0-9][0-9]\/[0-9][0-9][0-9][0-9]\//', $pathFiles, $content);
    $content = str_replace('/files/cache', $pathFiles, $content);
    $content = str_replace('/files/css-custom/', $pathFiles, $content);
    //make sure only href="/content is replaced. this will ignore iframed links like src="https://michiganvirtual.h5p.com/content/1291764321586118238/embed"  in the url MVU freshdesk ticket 3290 
    $content = preg_replace('/href="\/content\/([^\/]+)*\/([^\/]+)*\/([^\/]+)*\//', 'href="../../../content/\1/\\2/\\3.htm', $content);
    $content = str_replace('/blocks/slideshow/view.css/', '/blocks/slideshow/view.css', $content);
    $content = str_replace('/blocks/imagehotspots/lib/font-awesome-4.6.3/css/font-awesome.min.css', $pathShared . 'css/font-awesome-4.6.3/css/font-awesome.min.css', $content);
    $content = str_replace('/assets/fa-', $pathShared . '/css/font/fa-', $content);
    $content = str_replace('../css/vue/vuetify.min.css', $pathShared . 'css/vue/vuetify.min.css', $content);

    $content = str_replace('/packages/attribution/images//', $pathImg . 'attribution/', $content);

    // convert mathjax cdn to local
    $content = str_replace('https://cdnjs.cloudflare.com/ajax/libs/mathjax/2.7.7/MathJax.js?config=TeX-AMS-MML_HTMLorMML', $pathShared.'js/mathjax/MathJax.js', $content);

    // convert wiris to mathjax
    $content = str_replace('./../../js/tiny_mce/plugins/tiny_mce_wiris/integration/WIRISplugins.js?viewer=image', $pathShared.'js/mathjax/MathJax.js', $content);

    // remove these entirely
    $content = str_replace('<link rel="stylesheet" href="/themes/glossary/js/simple-scrollbar/simple-scrollbar.css">', '', $content);
    $content = str_replace('<script src="/themes/glossary/js/simple-scrollbar/simple-scrollbar.min.js"></script>', '', $content);
    
    // remove full url from video files (including removing a space before the url)
//    $content = str_replace('<source src="'.$protocol.$domain, '<source src="', $content);
    $content = str_replace('<source src=" '.$protocol.$domain, '<source src="'.$protocol.$domain, $content);

    // fix favicon path
    $content = str_replace('/themes/glossary../../../../images/favicon.ico', $pathImg . 'favicon.ico', $content);
    
    // remove <.htmhead> from document
    $content = str_replace('<.htmhead>', '', $content);
    $content = str_replace('<.htma>', '</a>', $content);
    $content = str_replace('/files/css-custom', '../files/css-custom', $content);
    $content = str_replace('/files//css-custom', '../files/css-custom', $content);
    $content = str_replace('../files/universal.css', '../files/css-custom/universal.css', $content);
    $content = str_replace('/files/zipembed', '../files/zipembed', $content); //fix zipembed links in content
    if (strpos($content, '<title>Glossary</title>') !== false) {
        // we're working in student glossary pages
        // remove search form in student-glossary.htm
        $content = str_replace('<p class="instructions">Type a word to search.</p>', '', $content);
        $content = removeStudentGlossaryForm($content);
        // update links to student glossary.js
        $content = str_replace('/themes/glossary/js/glossary.js', 'js/glossary.js', $content);
        $content = str_replace('../files/css-custom/', 'css/', $content);
        $content = str_replace('../files//css-custom/', 'css/', $content);
        $content = str_replace('/themes/glossary/js/simple-scrollbar/simple-scrollbar.css', 'css/simple-scrollbar.css', $content);
    } else {
        $content = fixStudentGlossaryLink($content);
    }
    
    $content = removeVersionStringFromFilenames($content);
    $content = str_replace("js/mathjax/MathJax.js", "js/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML", $content);
    $content = fixRelativeLinks($content);
    $content = fixSideMenuLinks($content);
    $content = fixOverviewTableLinks($content);
    
    // make the glossary-term-link classes local so we can use local.js on them
    $content = str_replace('glossary-term-link', 'local-glossary-term-link', $content);

    // add our local.js file directly after ca.js
    $content = str_replace('<script type="text/javascript" src="../../shared/js/ca.js"></script>', '<script type="text/javascript" src="../../shared/js/ca.js"></script><script type="text/javascript" src="../../shared/js/local.js"></script>', $content);

    //fix mathjax link
    // $content = str_replace('<script type="text/javascript" src="http:../../shared/js/mathjax/MathJax.js"></script>', '<script type="text/javascript" src="../../shared/js/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>', $content);
    
    //fix attribution image
    $content = str_replace('/packages/attribution../images/attribution', '../images/attribution', $content);
    //dirty fix to make ajax request not call when ca_graphic_organizer and ca_poll  submit button is clicked is
    $content = str_replace("$.ajax({", "return false; $.ajax({", $content);
    //remove newrelic script tags
    $content = preg_replace('#<script(.*?)>\(window.NREUM(.*?)</script>#is', '', $content);
    $content = preg_replace('#<script(.*?)>(window.NREUM)(.*?)</script>#', '', $content);

    // Fix references to files//
    $content = str_replace("files//", "files/", $content);

    // Fix audio block references
    $content = preg_replace('#https://[^/]+\.coursearc\.review\.\./files/([^\s]+\.mp3)#', '../files/\1', $content);
    $content = preg_replace('#https://[^/]+\.coursearc\.com\.\./files/([^\s]+\.mp3)#', '../files/\1', $content);

    // Fix file path for image labeling blocks
    $content = preg_replace('#"imagePath":"\\\/files\\\/\d{4}\\\/\d{4}\\\/\d{4}\\\/([^\/"]+\.\w+)"#', '"imagePath":"../files/$1"', $content);

   if ($scorm) {
        //remove mathjax and add scormdriver.js and AutoBookmark.js to each .htm file
        if (!$includeMath) {
            $content = str_replace('<script type="text/javascript" src="../../shared/js/mathjax/MathJax.js?config=TeX-AMS-MML_HTMLorMML"></script>', '', $content);
        }
        $content = str_replace('<script type="text/javascript" src="../../shared/js/ca.js"></script>', "\n" . '<script type="text/javascript" src="../../shared/js/ca.js"></script>' ."\n". '<script type="text/javascript" src="../../../../scormdriver/scormdriver.js"></script>' ."\n" . '<script type="text/javascript" src="../../../../scormdriver/auto-scripts/AutoBookmark.js"></script><script>var isSCORM = true</script>'. "\n", $content);
        if (strpos($content, '<title>Glossary</title>') === false) {
            $old = '</body>';
            $new = "\n <script>\n$(document).ready(function() { \n showScore(); \n });\n</script>\n</body>";//This is important! this will show the score if the page has been completed       
            $content = str_replace($old, $new, $content);         
        }
        //Working in the student Gloasary fix for links to glossary terms that open in new popup
        if (strpos($content, '<title>Glossary</title>') !== false) {
            $old = '</body>';
            $new = "\n <script>\n"
                    . "    function showTerm() {\n
                    var url = window.location.href;\n
                    var termID = url.split('#').pop()\n
                     $('.terms-intro').hide();\n
                     $('.term-container').hide()\n
                     $('#'+termID).show().attr('aria-hidden','false');\n
                    }\n
    $(document).ready(function() { \n
        window.addEventListener('popstate', function (event) {\n
            // The URL changed...\n
             showTerm(); \n
        })
        showTerm(); \n
     });\n
     \n</script>\n</body>";//This is a fix to load gloassary terms in popup when they are clicked in content CA-599
            $content = str_replace($old, $new, $content);
        }
    } 
    return $content;
}

// update paths inside css files
function replaceCssLinks($filename, $content)
{
    $content = str_replace('url("fonts', 'url("../css/fonts', $content);

    if ($filename == 'ca.css') {
        $content = str_replace('/images/glossary-icon.png', '../images/glossary-icon.png', $content);
        
        // remove source mapping so we don't see errors in the console
        $content = str_replace('/*# sourceMappingURL=ca.css.map */', '', $content);
    } else {
        // student glossary
        $content = str_replace('../../colored-background/css/fonts/', '../../../../../css/fonts/', $content);
    }

    return $content;
}

// remove '?version=xxxx' from .css and .js file names which reside in directories
function removeVersioningFromFilename($filename)
{
    if ((strpos($filename, '.css?') !== false) || (strpos($filename, '.js?') !== false)) {
        $filename = strstr($filename, '?', true);
    }

    return $filename;
}

function makeCoursesDirectoriesAndCopySharedFiles()
{
    global $caScormFileList;
    global $csvName;
    global $saveLocation;
    message("\n⚙️  Setting Up...");
    
    makeDirectory('courses');
    makeDirectory('courses/shared');
    makeDirectory('courses/shared/css');
    makeDirectory('courses/shared/images');
    makeDirectory('courses/shared/js');
    makeDirectory('zips');
    
    $shared = 'courses/shared';

    // images
    copy('starter/images/logo.png', $shared.'/images/logo.png');
    copy('starter/images/glossary-icon.png', $shared.'/images/glossary-icon.png');
    
    // js
    copy('starter/js/ca.js', $shared.'/js/ca.js');
    copy('starter/js/local.js', $shared.'/js/local.js');

    recursiveCopy('starter/js/tiny_mce', $shared.'/js/tiny_mce');
    
    // css
    recursiveCopy('starter/css/themes', $shared.'/css/themes');
    recursiveCopy('starter/css/fonts', $shared.'/css/fonts');
    recursiveCopy('starter/css/font', $shared.'/css/fonts');
    recursiveCopy('starter/css/font-awesome-4.6.3', $shared.'/css/font-awesome-4.6.3');
    recursiveCopy('starter/css/slideshow', $shared.'/css/slideshow');
    recursiveCopy('starter/css/vue', $shared.'/css/vue');

    if($csvName) {
        makeDirectory('tmp');
        makeDirectory('tmp/failed');
        makeDirectory('tmp/valid');
    }
}

function copyFilesFromStarterDirectory($domain)
{
    copy('starter/images/logo.png', $domain.'/images/logo.png');
    copy('starter/images/glossary-icon.png', $domain.'/images/glossary-icon.png');
    copy('starter/images/check-new.png', $domain.'/images/check-new.png');
    copy('starter/images/x-new.png', $domain.'/images/x-new.png');
    
    recursiveCopy('starter/js/tiny_mce', $domain.'/js/tiny_mce');
    recursiveCopy('starter/css/fonts', $domain.'/css/fonts');
    recursiveCopy('starter/css/images', $domain.'/css/images');
    recursiveCopy('starter/css/themes', $domain.'/css/themes');
    // recursiveCopy('starter/css/font-awesome-4.6.3', $domain.'/css/font-awesome-4.6.3');
}

function copyMathjaxFromStarterDirectory($inputString) {
    // first, determine if this course uses mathjax
    if (strpos($inputString, 'mathjax') !== false)
    {
        // if mathjax has not already been copied to the shared directory, copy it
        if(! is_dir('courses/shared/js/mathjax')) {
            message("💾 Copying Mathjax...");
            recursiveCopy('starter/js/mathjax', 'courses/shared/js/mathjax');
        }
    }
}

function makeDirectory($directory, $recursive = false)
{
    if(directoryExists($directory)) {
        return false;
    }
    if($recursive) {
        if (!mkdir($directory, 0777, TRUE)) {
            die('Could not make directory: '.$directory . "\n");
        }
    } else {
        if (!mkdir($directory)) {
            die('Could not make directory: '.$directory);
        }
    }
}

function makeDirectories($domain)
{
    makeDirectory($domain);
    makeDirectory($domain.'/content', 'recursive');
    makeDirectory($domain.'/images');
    makeDirectory($domain.'/css');
    makeDirectory($domain.'/css/custom');
    makeDirectory($domain.'/files');
    makeDirectory($domain.'/files/css-custom');
    makeDirectory($domain.'/files/zipembed');
    makeDirectory($domain.'/js');
    makeDirectory($domain.'/content/student-glossary');
    makeDirectory($domain.'/content/student-glossary/js');
    makeDirectory($domain.'/content/student-glossary/css');
}

// https://gist.github.com/gserrano/4c9648ec9eb293b9377b
function recursiveCopy($src, $dst)
{
    $dir = opendir($src);
    @mkdir($dst);
    while (($file = readdir($dir))) {
        if (($file != '.') && ($file != '..')) {
            if (is_dir($src . '/' . $file)) {
                recursiveCopy($src . '/' . $file, $dst . '/' . $file);
            } else {
                copy($src . '/' . $file, $dst . '/' . $file);
            }
        }
    }
    closedir($dir);
}

function directoryExists($directory)
{
    if(is_dir($directory)) {
        return true;
    }
}

function getIndexFilenameAndPrintScrapingMessage($url)
{
    $stpb = strrpos($url, '/', -1);
    $stpe = strrpos($url, '/', -2);
                        
    if (substr($url, strlen($url) - 1, 1) == '/') {
        $filename = substr($url, $stpe + 1, $stpb - $stpe - 1) . '.htm';
    } else {
        $filename = substr($url, $stpe + 1) . '.htm';
    }
    
    infoMessage("👀 $filename");

    return $filename;
}

function makeFinalDirectoryName($url)
{
    global $courseName;
    $parts = parse_url($url);
    $path = ltrim($parts['path'], '/');
    $array = explode('/', $path);
    $return = null;
    
    foreach($array as $key => $value) {
        if($key == 0) {
            $return .= '/content/';
        } else {
            $return .= $value.'/';
        }
    }

    $return = str_replace('/', '-', $return);
    $return = str_replace('-content-', '', $return);
    $return = str_replace('-introduction', '', $return);
    $return = rtrim($return, '-');
    $courseName = $return;
    return rtrim($return, '/');
}

function isStudentGlossary($url = null)
{
    global $glossary;
    
    if($glossary) {
        return true;
    }
    elseif($url && (substr($url, 0, 17) === '/student-glossary')) {
        return true;
    }

    return false;
}

// create a start.htm page for easier access to each course's content
function makeCourseStartPage($domain, $dir, $filename, $zipName)
{
    // instead of clicking a link, let's just redirect right to the content
    $html = '<meta http-equiv="refresh" content="0; url='.ltrim($dir, '/').'/'.$filename.'">';

    $fh = fopen($domain.'/start.htm', 'w');
    fwrite($fh, $html);
}

// create a main start.htm page that will list all courses, with links to each course
function makeMainStartPage($startPageData, $zipName)
{
    
    $html  = '<html>';
    $html .= '  <head>';
    $html .= '      <title>Courses Start Page</title>';
    $html .= '      <style>';
    $html .= '          h3 {text-align:center;margin-top:50px;font-family:Arial,Helvetica,sans-serif;}';
    $html .= '          p {text-align:center;margin-top:40px;}';
    $html .= '          p a:link, a:visited {color:#0a0e7d;}';
    $html .= '          p a:active, a:hover {color:#ffa600;}';
    $html .= '      </style>';
    $html .= '  </head>';
    $html .= '  <body>';
    $html .= '      <p><img src="shared/images/logo.png"></p>';
    foreach($startPageData as $course) {
        $html .= '<h3><a href="'.$zipName.'/start.htm">'.$zipName.'</a></h3>';
    }
    $html .= '  </body>';
    $html .= '</html>';

    $fh = fopen('courses/start.htm', 'w');
    fwrite($fh, $html);
}

function fixRelativeLinks($html)
{
    global $url;
    
    $urlParts = parse_url($url);

    $parts = substr($urlParts['path'], 0, strrpos($urlParts['path'], '/'));

    return str_replace('../../..'.$parts.'/', '', $html);
}

// remove '?version=xxx' from css and js file names
function removeVersionStringFromFilenames($html)
{
    // javascript
    $result = preg_replace('/(<script type="text\/javascript".*)\?(.*?)(".*)/m', '$1$3', $html);
    
    // css
    $result = preg_replace('/(<link rel="stylesheet".*)\?(.*?)(".*)/m', '$1$3', $result);

    return $result;
}

// update the student-glossary link to a local url
function fixStudentGlossaryLink($html)
{
    $result = str_replace('href="/student-glossary', 'href="student-glossary', $html);
    $result = str_replace('">View Glossary</a>', '.htm">View Glossary</a>', $result);

    return $result;
}

// DOMDOCUMENT
function getCourseName($html)
{
    $dom = new DOMDocument;
    @$dom->loadHTML($html);

    foreach ($dom->getElementsByTagName('h1') as $node) {
        return $node->textContent;
    }
}

function removeStudentGlossaryForm($html)
{
    $dom = new DOMDocument;
    @$dom->loadHTML($html);

    $node = $dom->getElementById('searchForm');
    $node->parentNode->removeChild($node);

    return $dom->saveHTML();
}

function getSideMenuLinks($html)
{
    global $links;
    $links = array();
    $dom = new DOMDocument;
    @$dom->loadHTML($html);

    foreach ($dom->getElementsByTagName('a') as $node) {
        if (strpos($node->getAttribute('class'), 'nav-item') !== false) {
            $links[] = $node->getAttribute('href');
        }
    }

    return $links;
}

//Add .htm to side menu navigation links href so they will link when you run the files locally after extracting the zip
function fixSideMenuLinks($html)
{
    global $links, $allPages;
    $dom = new DOMDocument;
    @$dom->loadHTML($html);
    $links = getSideMenuLinks($html);
    $i=0;
    while($i < count($links)) {
        $html = str_replace('href="'.$links[$i].'"', 'href="'.basename($links[$i]).'.htm"', $html);
        $i++;
    }

    if(empty($allPages)) {
        $allPages = $links;
    }
        
    return $html;
}
// fixes the links in a 'table of contents' table
function fixOverviewTableLinks($content)
{
    // make sure the page we're scraping has a table of contents
    if (strpos($content, 'table class="overview') !== false)
    {
        // get the links from the page's side menu
        $links = getSideMenuLinks($content);

        $dom = new DOMDocument;
        @$dom->loadHTML($content);
        
        // loop through the table of contents and replace each link with the one which has the same array index from the side menu
        $i=0;
        foreach ($dom->getElementsByTagName('a') as $node) {
            if (strpos($node->getAttribute('href'), '/?cID=') !== false) {
                $node->setAttribute('href', $links[$i]);
                $i++;
            }
        }

        return $dom->saveHTML();
    }

    return $content;
}

// MESSAGING
function message($string, $standout = null)
{
    return makeMessage($string, $standout, 'default');
}

function successMessage($string, $standout = false)
{
        if($standout) {
            echo "\n\e[32m$string\e[39m\n\n";
        } else {
            echo "\e[32m$string\e[39m\n";
        }
        return;
}

function errorMessage($string, $standout = null)
{
    beep();
    
    return makeMessage("🧨  $string", $standout, 'error');
}

function warningMessage($string, $standout = false)
{
    return makeMessage("⚠️  $string", $standout, 'warning');
}

function infoMessage($string, $standout = false)
{
    return makeMessage($string, $standout, 'info');
}

function mutedMessage($string, $standout = false)
{
    return makeMessage($string, $standout, 'light');
}

function alert($string, $standout = false)
{
    return makeMessage($string, $standout, 'notice');
}

function makeMessage($string, $standout, $type)
{
    global $debug;

    if (!$debug) {
        return; //Added this to stop all of the un needed messaging writing to the databse. Even with blob type for the output it is too large. DieMessage() will still write to db on scorm fail.
    } else {
        if($type == 'success') {
            if($standout) {
                echo "\n\e[32m$string\e[39m\n\n";
            } else {
                echo "\e[32m$string\e[39m\n";
            }
        }
        elseif($type == 'error') {
            if($standout) {
                echo "\n\e[31m$string\e[39m\n\n";
            } else {
                echo "\e[31m$string\e[39m\n";
            }
        }
        elseif($type == 'warning') {
            if($standout) {
                echo "\n\e[33m$string\e[39m\n\n";
            } else {
                echo "\e[33m$string\e[39m\n";
            }
        }
        elseif($type == 'info') {
            if($standout) {
                echo "\n\e[36m$string\e[39m\n\n";
            } else {
                echo "\e[36m$string\e[39m\n";
            }
        }
        elseif($type == 'light') {
            if($standout) {
                echo "\n\e[90m$string\e[39m\n\n";
            } else {
                echo "\e[90m$string\e[39m\n";
            }
        }
        elseif($type == 'notice') {
            if($standout) {
                echo "\n\e[95m$string\e[39m\n\n";
            } else {
                echo "\e[95m$string\e[39m\n";
            }
        } else {
            if($standout) {
                echo "\n\e[39m$string\n\n";
            } else {
                echo "\e[39m$string\n";
            }
        }
    }
}

function dieMessage($string, $beep = false)
{
    echo "\n\e[31m🧨 $string\e[39m\n\n";

    if($beep) {
        beep();
    }

    die;
}


function warnMessage($string, $beep = false)
{
    echo "\n\e[31m🧨 $string\e[39m\n\n";

    if($beep) {
        beep();
    }
}


function debug($data)
{
    global $debug;
    
    if($debug) {
        if(is_array($data)) {
            print_r($data);
        } else {
            echo "\e[35m$data\e[39m\n";
        }
    }
}

function debugDeep($data)
{
    global $debugDeep;

    if($debugDeep) {
        return debug($data);
    }
}

// exit the script if the url does not return a 200
function checkHeaders($url)
{
    $headers = get_headers($url, 1);
    
    if($headers[0] == 'HTTP/1.1 404 Not Found') {
        return dieMessage("404! The URL {$url} was not found. Stopping.");
    }

    if($headers[0] != 'HTTP/1.1 200 OK') {
        return dieMessage("The URL {$url} returned a status of: ".$headers[0].". Stopping.");
    }
}

// exit the script if the zip file already exists
function checkIfZipExists($zipName)
{
    if (file_exists("{$zipName}.zip")) {
        return dieMessage("The file '{$zipName}.zip' already exists! Please remove it to create a new archive.", 'standout');
    }
}

// FUNCTIONS WHICH MAY NEED UPDATING...

// this will need to be updated if learnosity changes the script URLs
function removeLearnosityScripts($html)
{
    return str_replace('<script type="text/javascript" src="//questioneditor.learnosity.com/?v2020.1.LTS"></script><script src="//questions.learnosity.com/?v2020.1.LTS"></script>', '', $html);
}

function beep()
{
    global $scorm;
    // play alert sound
    if(!$scorm) {
        shell_exec('afplay /System/Library/Sounds/Ping.aiff');
    }
}
function formatExecutionTime($endtime, $starttime)
{
    $duration = $endtime - $starttime;

    $hours = (int) ($duration / 60 / 60);

    $minutes = (int) ($duration / 60) - $hours * 60;

    $seconds = (int) $duration - $hours * 60 * 60 - $minutes * 60;

    return ($hours == 0 ? "00":$hours) . ":" . ($minutes == 0 ? "00":($minutes < 10? "0".$minutes:$minutes)) . ":" . ($seconds == 0 ? "00":($seconds < 10? "0".$seconds:$seconds));
}



// Functions to read URLs from a CSV

// get the urls from a csv file and write them to individual txt files
function getUrlsFromCsvAndWriteToTxtFile($filename)
{
    message("📄 Extracting URLs from CSV...");
    
    $file = array_map('str_getcsv', file($filename, FILE_IGNORE_NEW_LINES | FILE_SKIP_EMPTY_LINES));

    $data = [];

    // parse the csv and put the urls into an array
    foreach ($file as $array)
    {
        if (!array_key_exists($array[0], $data)) {
            $data[$array[0]] = [];
        }

        array_push($data[$array[0]], $array[1]);
    }

    // write the urls to an external file
    foreach ($data as $key => $value)
    {
        $handle = fopen('tmp/valid/' . $key . '.txt', 'w');
        fwrite($handle, implode(',', $value));
        fclose($handle);
    }

    return $data;
}

// scrape the urls from a directory full of .txt files
function getUrlsFromDirectoryAndScrape($dirName)
{
    if(is_dir($dirName))
    {
        $directory = getcwd() . '/' . $dirName;
        $scanned_directory = array_diff(scandir($directory), ['..', '.']);

        if(count($scanned_directory) == 0)
        {
            dieMessage("The folder \"{$dirName}\" is empty!");
        }

        $directory = new DirectoryIterator(getcwd() . '/' . $dirName);
        
        foreach ($directory as $file)
        {
            $filename = $file->getFilename();

            if (!$file->isDot() && $filename[0] != '.')
            {
                // open the document and scrape the URL(s)
                $file = file($dirName . '/' . $filename);
                $urls = explode(',', $file[0]);

                foreach ($urls as $url)
                {
                    if (urlIsValid($url))
                    {
                        successMessage("✍️  Scraping {$filename}...");
                        
                        echo shell_exec("php scrape.php " . $urls[0] ." zip=" . rtrim($filename, '.txt'));

                        shell_exec('rm ' . getcwd() . '/' . $dirName . '/' .  $filename);
                    } else {
                        warningMessage("{$filename} contains invalid URls. Skipping.");
                    }
                }
            }
        }
    } else {
        dieMessage("The folder \"{$dirName}\" does not exist!");
    }
}

// loop through 'tmp/valid' and test each url in each .txt file
function verifyUrls()
{
    message("🔍 Verifying URLs...");
    
    $dir = new DirectoryIterator('tmp/valid');

    $filesToMove = [];

    foreach ($dir as $file)
    {
        $filename = $file->getFilename();

        if (!$file->isDot() && $filename[0] != '.')
        {
            // open the document and check that each url is valid
            $file = file('tmp/valid/' . $filename);
            $urls = explode(',', $file[0]);

            foreach ($urls as $url)
            {
                if (!in_array($filename, $filesToMove)) {
                    if (!urlIsValid($url)) {
                        array_push($filesToMove, $filename);
                    }
                }
            }
        }
    }

    // move file with a bad url to the bad directory
    if (!empty($filesToMove))
    {
        foreach ($filesToMove as $value) {
            if (!is_dir(getcwd() . '/tmp/failed')) {
                mkdir(getcwd() . '/tmp/failed');
            }

            rename(getcwd() . '/tmp/valid/' . $value, getcwd() . '/tmp/failed/' . $value);
        }
    }
}

// ping a url and make sure it exists
function urlIsValid($url)
{
    $headers = @get_headers($url, 1);

    if (!$headers || strpos($headers[0], '400') !== false)
    {
        return false;
    }

    if($headers[0] == 'HTTP/1.1 200 OK') {
        return true;
    }

    return false;
}

// 'tmp/failed' files have been removed from 'tmp/valid'; loop it again and scrape the remaining urls
function scrapeUrls()
{
    $dir = new DirectoryIterator('tmp/valid');

    foreach ($dir as $file)
    {
        $filename = $file->getFilename();

        if (!$file->isDot() && $filename[0] != '.')
        {
            successMessage("✍️  Scraping {$filename}...");
            
            // we're passing a 'delete' flag, so when the scraper is finished it will delete valid/$filename
            $output = shell_exec('php scrape.php ' . file_get_contents('tmp/valid/' . $filename) . ' delete=' . $filename . ' zip=' . rtrim($filename, '.txt'));
            
            echo $output;
        }

    }
}

function directDownload($url, $path) {
    global $caScormFileList;
    // Initialize a cURL session
    $ch = curl_init($url);

    // Set cURL options to follow redirects and return headers
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, false); // We need to get the Location header manually
    curl_setopt($ch, CURLOPT_HEADER, true);

    // Execute cURL to get the headers
    $response = curl_exec($ch);

    // Check if there's a redirect (HTTP 302)
    if (curl_errno($ch)) {
        errorMessage('cURL error in directDownload: ' . curl_error($ch));
        curl_close($ch);
        return false;
    }

    // Extract the "Location" header
    $location = '';
    if (preg_match('/^location:\s*(.+)$/im', $response, $matches)) {
        $location = trim($matches[1]);
    }

    // If a Location header is found, extract the filename
    $filename = '';
    if ($location) {
        $filename = basename(parse_url($location, PHP_URL_PATH));
    }

    // Check if filename was extracted
    if (!$filename) {
        errorMessage("Error in directDownload: failed to determine filename from the redirect URL.");
        curl_close($ch);
        return false;
    }

    // Now, follow the redirect to download the file
    curl_setopt($ch, CURLOPT_URL, $location);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    curl_setopt($ch, CURLOPT_HEADER, false); // Don't include headers in the output

    // Get the file content
    $fileContent = curl_exec($ch);

    // Check for errors during download
    if (curl_errno($ch)) {
        errorMessage('cURL error in directDownload: ' . curl_error($ch));
        return false;
    } else {
        // Save the file content to the destination path
        file_put_contents($path . $filename, $fileContent);
        debug("File downloaded successfully as $filename");

        // Add this filename into an array with the ID associated
        if (preg_match('~\/view\/(\d+)(?:\/|$)~', $url, $matches)) {
            $thisFileId = $matches[1]; // The captured ID
            $caScormFileList[$thisFileId] = $filename;
        }

        return $filename;
    }

    // Close the cURL session
    curl_close($ch);
}

function updateLocalJs($path) {
    global $caScormFileList;
    $filePath = $path . 'js/local.js';
    $prependContent = "var caScormFileList = " . json_encode($caScormFileList);
    $currentContent = '';
    if (file_exists($filePath)) {
        $currentContent = file_get_contents($filePath);
    } else {
        die("not found: $filePath");
    }
    $newContent = $prependContent . $currentContent;
    file_put_contents($filePath, $newContent);
}

// delete a .txt file after it has been scraped
function deleteCsvTxtFile($filename)
{
    if(file_exists(getcwd() . '/tmp/valid/' . $filename))
    {
        shell_exec('rm ' . getcwd() . '/tmp/valid/' . $filename);
    }
}

// delete 'tmp' and move the failed url files to 'failedScrapes'
function cleanUpAfterCsvScrapes()
{
    if(!is_dir(getcwd() . '/tmp/valid') || !is_dir(getcwd() . '/tmp/failed')) {
        return;
    }
    
    // check if there are still files in tmp/valid
    $directory = getcwd() . '/tmp/valid';
    $scanned_directory = array_diff(scandir($directory), ['..', '.']);

    // no files, clean up...
    if(count($scanned_directory) == 0)
    {
        $directory = getcwd() . '/tmp/failed';
        $scanned_directory = array_diff(scandir($directory), ['..', '.']);

        // move the failed urls to a higher level for easier reporting
        if(count($scanned_directory) > 0) {
            recursiveCopy(getcwd() . '/tmp/failed', getcwd() . '/failedScrapes');
        } else {
            shell_exec("rm -rf " . getcwd() . "/failedScrapes");
        }

        shell_exec("rm -rf " . getcwd() . "/tmp");
    }
}


// 👀⚙️📄🔍
