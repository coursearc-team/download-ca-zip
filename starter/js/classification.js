$.fn.classification = function(questions, bID, graded)
{
    var questionCache = {};
    var questions = questions;
    var $container = $(this);

    var answeredQuestions = {}
    var currentSlide = 1

    registerEventKeydown();

    function answer(questionId, categoryId) {
        var isCorrect = questions.findIndex(function(item) {
            return item.id == questionId && item.category_id == categoryId
        }) !== -1;
        var isLastQuestion = questions.findIndex(function(item) {
            return item.id == questionId
        }) === (questions.length - 1);
        answeredQuestions[questionId] = isCorrect;
        toggleResetBtn(true);
        displayResult(questionId);

        // Move to next slide if answer correctly
        if (isCorrect && !isLastQuestion) {
            setTimeout(function() {
                activeSlide(currentSlide + 1);
                globalTabIndex = currentSlide;
            }, 1000);
        }

        // Answered all questions
        /* if (Object.keys(answeredQuestions).length === questions.length) {
            submitResult()
        } */

        // Always submit results
        submitResult();
    }

    function displayResult(questionId) {
        var elem = $container.find('#question-'+ questionId);
        var resultClass = '.ca-classification__question-result'
        var text = answeredQuestions[questionId] ? 'Correct' : 'Try Again'
        $(elem).children(resultClass).text(text)
        $(elem).children(resultClass).addClass(answeredQuestions[questionId] ? 'is-success' : 'is-error')
        $(elem).children(resultClass).addClass('scale-in-center')
        
        var correctAnswers = Object.keys(answeredQuestions).filter(id => answeredQuestions[id] === true).length;
        var total = questions.length;
        var showMessage = '<p>' + correctAnswers + ' of ' + total + ' answered correctly</p>';
        $container.find('.displayResult').html(showMessage);
    }

    $(document).on('click', '.ca-classification__question-result.is-error', function() {
        var questionId = $(this).closest('.ca-classification__question').attr('id')
        resetQuestion(questionId);
    })

    function toggleResetBtn(show) {
        if (show) {
            $container.find('.reset-btn').css('display', 'block');
        } else {
            $container.find('.reset-btn').css('display', 'none');
        }
    }

    function initQuestionCache() {
        if ($.isEmptyObject(questionCache)) {
            $container.find('.ca-classification__question').each(function() {
                var id = $(this).attr('id')
                questionCache[id] = $(this).html()
            })
        }
    }

    function resetQuestion(id) {
        if (questionCache[id]) {
            $container.find('#'+id).html(questionCache[id]).sortable('refresh')
        }
    }

    function submitResult() {
        var correctAnswers = Object.keys(answeredQuestions).filter(id => answeredQuestions[id] === true);
        var wrongAnswers = Object.keys(answeredQuestions).filter(id => answeredQuestions[id] === false);
        var unansweredQuestions = questions.filter(question => answeredQuestions[question.id] === undefined);
        var correct = correctAnswers.length;
        var total = questions.length;
        var score = (correct / total).toFixed(2);
        var message = '';

        console.log("Correct answers: ", correctAnswers);
        console.log("Wrong answers: ", wrongAnswers);
        console.log("Correct answers: ", unansweredQuestions);
        // console.log("Not Answered answers: ", notAnswered);

        if (correctAnswers.length > 0) {
            message += '<p>The student got the following correct:</p><ul>';
            correctAnswers.forEach(function (questionId) {
                var question = questions.find(function (item) {
                    return item.id === questionId;
                })
                if (question) {
                    message += ('<li>' + question.text + '</li>');
                }
            })
            message += '</ul>';
        }

        if (wrongAnswers.length > 0) {
            message += '<p>The student got the following incorrect:</p><ul>';
            wrongAnswers.forEach(function (questionId) {
                var question = questions.find(function (item) {
                    return item.id === questionId;
                })
                if (question) {
                    message += ('<li>' + question.text + '</li>');
                }
            })
            message += '</ul>';
        }

        if (unansweredQuestions.length > 0) {
            message += '<p>The student didn\'t answer the following:</p><ul>';
            unansweredQuestions.forEach(function (question) {
                message += ('<li>' + question.text + '</li>')
            })
            message += '</ul>'
        }

        // Post grade/participation points
        ca_postGrade(score, bID, message);
    }

    $container.find('.reset-btn').click(function() {
        
        // Reset all questions
        $container.find('.ca-classification__question').each(function() {
            var id = $(this).attr('id')
            resetQuestion(id);
        })
        // Hide Reset Button
        toggleResetBtn(false);
        // Clear answered questions
        answeredQuestions = {};
        // Move back to first slide
        activeSlide(1);
        globalTabIndex = 1;

        $container.find('.displayResult').html('');
    })
    
    // Sortable

    $container.find('.ca-classification__category').sortable({
        scroll: true, 
        scrollSensitivity: 80,
        scrollSpeed: 100,
        receive: function (evt, ui) {
            var destCategory = $(ui.item).parent().data('cate-id');
            var selectedQuestion = $(ui.item).data('question-id');
            $(ui.item).remove();
            answer(selectedQuestion, destCategory);
        },
        over: function(event, ui) {
            if ($(this).children().length > 1) {
                $(ui.placeholder).css('display', 'none');
            } else {
                $(ui.placeholder).css('display', '');
            }
        }
    }).disableSelection();

    var currentlyScrolling = false;

    var SCROLL_AREA_HEIGHT = 40; // Distance from window's top and bottom edge.
    
    $container.find('.ca-classification__question:visible').sortable({
        connectWith: '.ca-classification__category',
        items: '> .ca-classification__question-icon',
        scroll: true,
        sort: function(event, ui) {

            if (currentlyScrolling) {
                return;
            }

            var windowHeight   = $(window).height();
            var mouseYPosition = event.clientY;

            if (mouseYPosition < SCROLL_AREA_HEIGHT) {
                currentlyScrolling = true;

                $('html, body').animate({
                    scrollTop: "-=" + windowHeight / 2 + "px" // Scroll up half of window height.
                }, 
                400, // 400ms animation.
                function() {
                    currentlyScrolling = false;
                });

            } else if (mouseYPosition > (windowHeight - SCROLL_AREA_HEIGHT)) {

                currentlyScrolling = true;

                $('html, body').animate({
                    scrollTop: "+=" + windowHeight / 2 + "px" // Scroll down half of window height.
                }, 
                400, // 400ms animation.
                function() {
                    currentlyScrolling = false;
                });

            }
        }
    }).disableSelection();

    // Cache question in the beginning
    initQuestionCache();

    var maxItems = $container.find('.glide__slide').length;
    // Glide navigation
    $container.find('.glide__container').on('click', '.glide__arrow', function(item) {
        var nextSlide;

        var slide = Number($(this).closest('.glide__slide').attr('data-slide'));
        if($(this).hasClass('glide__arrow--next')){

            nextSlide = slide + 1;
            if(nextSlide > maxItems){
                nextSlide = 1;
            }
        } else if($(this).hasClass('glide__arrow--prev')){

            nextSlide = slide - 1;
            if(nextSlide < 1){
                nextSlide = maxItems;
            }
        }
        activeSlide(nextSlide);
    })

    function activeSlide(slide) {
        var i = 10 - slide
        currentSlide = slide

        $container.find('.glide__slide')
            .removeClass('active')
            .removeAttr('style')
        $container.find('#slide-' + slide).addClass('active')
        $container.find('.glide__slide').each(function(index) {
            var indexRedux = index + 1
            if (indexRedux == slide) {
                $(this).css({
                    'z-index': '1000',
                    transform: 'scale(1)'
                })
                i = 8
            }

            var horizontalScale = (i / 10 + 0.08);
            horizontalScale = horizontalScale > 0 ? horizontalScale : 0;

            if (indexRedux < slide) {
                $(this).css({
                    'z-index': indexRedux + 1,
                    transform: 'scale(1, ' + horizontalScale + ')'
                })
                i++
            } else if (indexRedux > slide) {
                $(this).css({
                    'z-index': 10 - indexRedux,
                    transform: 'scale(1, ' + horizontalScale + ')'
                })
                i--
            }
        })
    }

    // Active first slide at the beginning
    activeSlide(currentSlide)

    var globalTabIndex = 0;
    var maxImages = $container.find('.img-classification').length;
    var maxCates = $container.find('.box-classification').length;
    var currentCateIndex = maxImages;
    function registerEventKeydown(){
        $('#body-main').on('keydown', function (event) {

            if ((event.key === ' ' || event.keyCode === 32) && event.target.tagName === 'IMG') {
                event.preventDefault();
                event.stopPropagation();
            }
            if((event.key === 'ArrowUp' || event.key === 'ArrowDown') && event.target.tagName !== 'BODY'){
                event.preventDefault();
                event.stopPropagation();
            }

            if (event.key === 'Escape') {
                $(this).find(':focus').blur();
                globalTabIndex = 0;
                currentCateIndex = maxImages;
                $container.find('.img-classification').each(function(i, item) {
                    $(item).show();
                    $(item).next('select').hide();
                });
            }
        });
    }

    function resetAriaGrabbed(){
        $container.find('.img-classification').each(function(i, item) {
            $(item).attr('aria-grabbed', false);
            $(item).next('select').attr('aria-grabbed', false);
        });
    }

    function focusOnSelect(index){
        // find the sibling to jumb
        var nextIndexEl = $container.find('[data-tabindex="' + index + '"]');
        var nextIndexSelect = nextIndexEl.next('select');
        if(nextIndexSelect.next('.ca-classification__question-result').text() != ''){
            return;
        }

        nextIndexEl.hide();
        nextIndexSelect.show().trigger('focus');
        resetAriaGrabbed();
        $(nextIndexEl).attr('aria-grabbed', true);
        nextIndexSelect.change(function(){
            var thisCate = $(this);

            if(thisCate.val() != ''){
                var destCategory = $(thisCate).val();
                var selectedQuestion = $(thisCate).data('question-id');
                answer(selectedQuestion, destCategory);
                thisCate.hide();
            }
        });
    }

    // $container.keyup(function(event) {
    //     if (event.key === 'Tab') {
    //         // if Shift key is pressed then should go backward
    //         globalTabIndex = event.shiftKey ? globalTabIndex - 1 : globalTabIndex + 1;
    //         if(globalTabIndex <= 0 || globalTabIndex > maxImages)
    //             globalTabIndex = 1;

    //         activeSlide(globalTabIndex);
    //         focusOnSelect(globalTabIndex);

    //         event.preventDefault();
    //         event.stopPropagation();
    //     }
    // });
}
