

$(document).ready(function () {
    
    // fix mp3 links so they work locally
    $('a[href^="/index.php/download_file/view/"]').each(function () {
        var title = $(this).attr('title');
        if (!title) {
            thisFileId = extractFileId($(this).attr('href'));
            if (thisFileId) {
                title = caScormFileList[thisFileId];
            }
        }
        var newUrl = '../files/' + title;
        $(this).attr('href', newUrl);
    });

    
    // open student glossary popup
    $('a.local-glossary-term-link').each(function () {
        var url = $(this).attr('href');
        url = 'student-glossary/' + COURSE_ID + url.replace('#term', '.htm#term');
        $(this).prop('href', url);
        $(this).click(function () {
            popup($(this).prop('href'), 'glossary' + COURSE_ID, 900, 600);
            return false;
        });
    });

    ca_postInteraction = function () { };

});


function extractFileId(url) {
    const pattern = /\/view\/(\d+)\//;
    const match = url.match(pattern);
    return match ? match[1] : null;
}