/*
 * jquery.simulate - simulate browser mouse and keyboard events
 *
 * Copyright (c) 2009 Eduardo Lundgren (eduardolundgren@gmail.com)
 * and Richard D. Worth (rdworth@gmail.com)
 *
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php) 
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 */

;(function($) {

$.fn.extend({
    simulate: function(type, options) {
        return this.each(function() {
            var opt = $.extend({}, $.simulate.defaults, options || {});
            new $.simulate(this, type, opt);
        });
    }
});

$.simulate = function(el, type, options) {
    this.target = el;
    this.options = options;

    if (/^drag$/.test(type)) {
        this[type].apply(this, [this.target, options]);
    } else {
        this.simulateEvent(el, type, options);
    }
};

$.extend($.simulate.prototype, {
    simulateEvent: function(el, type, options) {
        var evt = this.createEvent(type, options);
        this.dispatchEvent(el, type, evt, options);
        return evt;
    },
    createEvent: function(type, options) {
        if (/^mouse(over|out|down|up|move)|(dbl)?click$/.test(type)) {
            return this.mouseEvent(type, options);
        } else if (/^key(up|down|press)$/.test(type)) {
            return this.keyboardEvent(type, options);
        }
    },
    mouseEvent: function(type, options) {
        var evt;
        var e = $.extend({
            bubbles: true, cancelable: (type != "mousemove"), view: window, detail: 0,
            screenX: 0, screenY: 0, clientX: 0, clientY: 0,
            ctrlKey: false, altKey: false, shiftKey: false, metaKey: false,
            button: 0, relatedTarget: undefined
        }, options);

        var relatedTarget = $(e.relatedTarget)[0];

        if ($.isFunction(document.createEvent)) {
            evt = document.createEvent("MouseEvents");
            evt.initMouseEvent(type, e.bubbles, e.cancelable, e.view, e.detail,
                e.screenX, e.screenY, e.clientX, e.clientY,
                e.ctrlKey, e.altKey, e.shiftKey, e.metaKey,
                e.button, e.relatedTarget || document.body.parentNode);
        } else if (document.createEventObject) {
            evt = document.createEventObject();
            $.extend(evt, e);
            evt.button = { 0:1, 1:4, 2:2 }[evt.button] || evt.button;
        }
        return evt;
    },
    keyboardEvent: function(type, options) {
        var evt;

        var e = $.extend({ bubbles: true, cancelable: true, view: window,
            ctrlKey: false, altKey: false, shiftKey: false, metaKey: false,
            keyCode: 0, charCode: 0
        }, options);

        if ($.isFunction(document.createEvent)) {
            try {
                evt = document.createEvent("KeyEvents");
                evt.initKeyEvent(type, e.bubbles, e.cancelable, e.view,
                    e.ctrlKey, e.altKey, e.shiftKey, e.metaKey,
                    e.keyCode, e.charCode);
            } catch(err) {
                evt = document.createEvent("Events");
                evt.initEvent(type, e.bubbles, e.cancelable);
                $.extend(evt, { view: e.view,
                    ctrlKey: e.ctrlKey, altKey: e.altKey, shiftKey: e.shiftKey, metaKey: e.metaKey,
                    keyCode: e.keyCode, charCode: e.charCode
                });
            }
        } else if (document.createEventObject) {
            evt = document.createEventObject();
            $.extend(evt, e);
        }
        if (($.browser !== undefined) && ($.browser.msie || $.browser.opera)) {
            evt.keyCode = (e.charCode > 0) ? e.charCode : e.keyCode;
            evt.charCode = undefined;
        }
        return evt;
    },

    dispatchEvent: function(el, type, evt) {
        if (el.dispatchEvent) {
            el.dispatchEvent(evt);
        } else if (el.fireEvent) {
            el.fireEvent('on' + type, evt);
        }
        return evt;
    },

    drag: function(el) {
        var self = this, center = this.findCenter(this.target), 
			options = this.options,	x = Math.floor(center.x), y = Math.floor(center.y), 
            dx = options.dx || 0, dy = options.dy || 0, target = this.target;
        var coord = { clientX: x, clientY: y };
        this.simulateEvent(target, "mousedown", coord);
        coord = { clientX: x + 1, clientY: y + 1 };
        this.simulateEvent(document, "mousemove", coord);
        coord = { clientX: x + dx, clientY: y + dy };
        this.simulateEvent(document, "mousemove", coord);
        this.simulateEvent(document, "mousemove", coord);
        this.simulateEvent(target, "mouseup", coord);
    },
    findCenter: function(el) {
        var el = $(this.target), o = el.offset();
        return {
            x: o.left + el.outerWidth() / 2,
            y: o.top + el.outerHeight() / 2
        };
    }
});

$.extend($.simulate, {
    defaults: {
        speed: 'sync'
    },
    VK_TAB: 9,
    VK_ENTER: 13,
    VK_ESC: 27,
    VK_PGUP: 33,
    VK_PGDN: 34,
    VK_END: 35,
    VK_HOME: 36,
    VK_LEFT: 37,
    VK_UP: 38,
    VK_RIGHT: 39,
    VK_DOWN: 40
});

})(jQuery);
;/*!
 * jQuery UI Touch Punch 0.2.3
 *
 * Copyright 2011–2014, Dave Furfero
 * Dual licensed under the MIT or GPL Version 2 licenses.
 *
 * Depends:
 *  jquery.ui.widget.js
 *  jquery.ui.mouse.js
 */
(function ($) {

  // Detect touch support
  $.support.touch = 'ontouchend' in document;

  // Ignore browsers without touch support
  if (!$.support.touch) {
    return;
  }

  var mouseProto = $.ui.mouse.prototype,
      _mouseInit = mouseProto._mouseInit,
      _mouseDestroy = mouseProto._mouseDestroy,
      touchHandled;

  /**
   * Simulate a mouse event based on a corresponding touch event
   * @param {Object} event A touch event
   * @param {String} simulatedType The corresponding mouse event
   */
  function simulateMouseEvent (event, simulatedType) {

    // Ignore multi-touch events
    if (event.originalEvent.touches.length > 1) {
      return;
    }

    event.preventDefault();

    var touch = event.originalEvent.changedTouches[0],
        simulatedEvent = document.createEvent('MouseEvents');
    
    // Initialize the simulated mouse event using the touch event's coordinates
    simulatedEvent.initMouseEvent(
      simulatedType,    // type
      true,             // bubbles                    
      true,             // cancelable                 
      window,           // view                       
      1,                // detail                     
      touch.screenX,    // screenX                    
      touch.screenY,    // screenY                    
      touch.clientX,    // clientX                    
      touch.clientY,    // clientY                    
      false,            // ctrlKey                    
      false,            // altKey                     
      false,            // shiftKey                   
      false,            // metaKey                    
      0,                // button                     
      null              // relatedTarget              
    );

    // Dispatch the simulated event to the target element
    event.target.dispatchEvent(simulatedEvent);
  }

  /**
   * Handle the jQuery UI widget's touchstart events
   * @param {Object} event The widget element's touchstart event
   */
  mouseProto._touchStart = function (event) {

    var self = this;

    // Ignore the event if another widget is already being handled
    if (touchHandled || !self._mouseCapture(event.originalEvent.changedTouches[0])) {
      return;
    }

    // Set the flag to prevent other widgets from inheriting the touch event
    touchHandled = true;

    // Track movement to determine if interaction was a click
    self._touchMoved = false;

    // Simulate the mouseover event
    simulateMouseEvent(event, 'mouseover');

    // Simulate the mousemove event
    simulateMouseEvent(event, 'mousemove');

    // Simulate the mousedown event
    simulateMouseEvent(event, 'mousedown');
  };

  /**
   * Handle the jQuery UI widget's touchmove events
   * @param {Object} event The document's touchmove event
   */
  mouseProto._touchMove = function (event) {

    // Ignore event if not handled
    if (!touchHandled) {
      return;
    }

    // Interaction was not a click
    this._touchMoved = true;

    // Simulate the mousemove event
    simulateMouseEvent(event, 'mousemove');
  };

  /**
   * Handle the jQuery UI widget's touchend events
   * @param {Object} event The document's touchend event
   */
  mouseProto._touchEnd = function (event) {

    // Ignore event if not handled
    if (!touchHandled) {
      return;
    }

    // Simulate the mouseup event
    simulateMouseEvent(event, 'mouseup');

    // Simulate the mouseout event
    simulateMouseEvent(event, 'mouseout');

    // If the touch interaction did not move, it should trigger a click
    if (!this._touchMoved) {

      // Simulate the click event
      simulateMouseEvent(event, 'click');
    }

    // Unset the flag to allow other widgets to inherit the touch event
    touchHandled = false;
  };

  /**
   * A duck punch of the $.ui.mouse _mouseInit method to support touch events.
   * This method extends the widget with bound touch event handlers that
   * translate touch events to mouse events and pass them to the widget's
   * original mouse event handling methods.
   */
  mouseProto._mouseInit = function () {
    
    var self = this;

    // Delegate the touch handlers to the widget's element
    self.element.bind({
      touchstart: $.proxy(self, '_touchStart'),
      touchmove: $.proxy(self, '_touchMove'),
      touchend: $.proxy(self, '_touchEnd')
    });

    // Call the original $.ui.mouse init method
    _mouseInit.call(self);
  };

  /**
   * Remove the touch event handlers
   */
  mouseProto._mouseDestroy = function () {
    
    var self = this;

    // Delegate the touch handlers to the widget's element
    self.element.unbind({
      touchstart: $.proxy(self, '_touchStart'),
      touchmove: $.proxy(self, '_touchMove'),
      touchend: $.proxy(self, '_touchEnd')
    });

    // Call the original $.ui.mouse destroy method
    _mouseDestroy.call(self);
  };

})(jQuery);;var $latestAnswer;
var accessibleAnswers = [];
var ca_posts = {};
var ajax_request_counter = 0;

const ENTER_KEY_CODE = 13;
const ESCAPE_KEY_CODE = 27;
const SPACE_KEY_CODE = 32;
const LEFT_KEY_CODE = 37;
const UP_KEY_CODE = 38;
const RIGHT_KEY_CODE = 39;
const DOWN_KEY_CODE = 40;

$.fn.matching = function(graded, titleOfColumn1, titleOfColumn2, bId = '') {
    var $container = $(this); 

    // Do not run unless there are definition lists present
    if ($container.children('dl').length > 0) { 
    
        // Markup for questions and answers (to be shuffled and inserted later) 
        var count = 0;
        var titles = new Array();
        var definitions = new Array();
        var choices = new Array();
        $container.children('dl').each(function() { 
            titles.push(`<th class="descriptions" data-id="answer-${bId}-${count}">${$(this).children('dd').html()}</th>`)
            definitions.push(`  <td id="choices-${bId}-${count}" class="choices" data-row="answer-${bId}-${count}">
                                    <div id="choice-${bId}-${count}" class="choice" data-answer="answer-${bId}-${count}">
                                        ${$(this).children('dt').html()}
                                    </div>
                                </td>`);
            choices.push({ 
                index: count, 
                html: $(this).children('dt').html()
            });
            count++;         
         });
         
         // Shuffle 
        definitions = shuffleArray(definitions);
        choices = shuffleArray(choices);
    
        // Add markup for quiz 
        var table = '<table class="table-matching"><thead>\
            <th>' + (titleOfColumn1 ? titleOfColumn1 : 'Descriptions') + '</th>\
            <th>' + (titleOfColumn2 ? titleOfColumn2 : 'Answers') + '</th>\
            <th>Choices</th></thead><tbody>';
        for(i=0; i<titles.length; i++) {
            // Wrap each question/answer in a row
            let selectHtml = `  <button 
                                    id="matching-options-dropdown-${bId}-${i}" 
                                    class="dropdown-toggle matching-options-link" 
                                    aria-haspopup="true" 
                                    data-toggle="dropdown" 
                                    aria-expanded="false"
                                    aria-controls="dropdown-menu-${bId}-${i}"
                                    role="button"
                                    type="button"
                                >
                                    <span class="matching-options-placeholder">Select a choice</span>
                                </button>
                                <ul 
                                    class="dropdown-menu matching-options-dropdown dropdown-menu-${bId}-${i}" 
                                    aria-hidden="true" 
                                    aria-labelledby="matching-options-dropdown-${bId}-${i}"
                                >`;

            choices.forEach(choice => {
                selectHtml += ` <li>
                                    <button 
                                        type="button"
                                        role="menuitem"
                                        aria-label="Select ${choice.html}" 
                                        data-row="${choice.index}" 
                                        aria-haspopup="false" 
                                        class="dropdown-item matching-dropdown-item"
                                    >
                                        ${choice.html}
                                    </button>
                                </li>`;
            });

            selectHtml += `</ul>`;
            table += `<tr class="tr-zone tr-matching">${titles[i]}<td class="dropzone"><div class="drop dropdown">${selectHtml}</div></td>${definitions[i]}</tr>`;
        }
        table += '</tbody></table>';
        $container.append(table);
        $container.append(`<div class="buttons">
            <button name="latestAnswer" disabled="disabled" class="latest-move ca-button ca-accent1">Check Latest Move</button>
            <button name="latestQuestion" disabled="disabled" class="whole-question ca-button ca-accent1">Check Whole Question</button>
            <button name="resetAnswer" disabled="disabled" class="reset-answer ca-button ca-accent1-inverse">Reset</button>
            </div><div class="feedback" tabindex="-1"></div>`);
        
        //  Add dropdown control for configuration and public pages
            var closeAllDropdowns = function () {
                const dropdowns = $container.find('.dropdown');
                dropdowns.each((index, item) => {
                $(item).removeClass('open');
        });
            };

            var closeMatchingDropdown = function (self) {
                const dropdown = $(self).closest('.dropdown');
                
            if (dropdown.hasClass('open')) {
                dropdown.removeClass('open');

                const dropdownMenuItems = dropdown.find('.dropdown-menu');
                dropdownMenuItems.attr('aria-hidden', 'true');

                    const matchingOptionsLink = dropdown.find('.matching-options-link').first();
                matchingOptionsLink.attr('aria-expanded', 'false');
                setTimeout(() => {
                    matchingOptionsLink.focus();
                });
                }
            };

            var openMatchingDropdown = function (self) {
                closeAllDropdowns();

                const dropdown = $(self).closest('.dropdown');

            if (!dropdown.hasClass('open')) {
                dropdown.addClass('open');

                const dropdownMenuItems = dropdown.find('.dropdown-menu');
                dropdownMenuItems.attr('aria-hidden', 'false');

                const firstMatchingItem = dropdown.find('.matching-dropdown-item').first();
                setTimeout(() => {
                    firstMatchingItem.focus();
                });

                const matchingOptionsLink = dropdown.find('.matching-options-link').first();
                matchingOptionsLink.attr('aria-expanded', 'true');
                }
            };

            var switchToSiblingItem = function (self, isNext) {
                if (isNext) {
                    const next = $(self).closest('li').next();
                    if (next && next.length) {
                        next.find(".matching-dropdown-item").focus();
                    }
                } else {
                    const prev = $(self).closest('li').prev();
                    if (prev && prev.length) {
                        prev.find(".matching-dropdown-item").focus();
                    }
                }
            };

        $container.find(".matching-options-link").on('dragstart', function ($event) {
            $event.preventDefault();
            });

            $container.find('.matching-options-link').keydown(function ($event) {
            if ($event.which === SPACE_KEY_CODE || $event.which === ENTER_KEY_CODE || $event.which === DOWN_KEY_CODE) {
                    $event.preventDefault();
                openMatchingDropdown(this);
                }
            });

            $container.find(".matching-dropdown-item").click(function() {
                closeMatchingDropdown(this);
            });

        $container.find(".choice").click(function() {
            const matchingLink = $(this).next();

            if (matchingLink && matchingLink.length) {
                matchingLink.click();
            }
        });

            $container.find('.matching-dropdown-item').keydown(function ($event) {
                if ($event.which === SPACE_KEY_CODE || $event.which === ENTER_KEY_CODE || $event.which === ESCAPE_KEY_CODE) {
                    closeMatchingDropdown(this);
                } else if ($event.which === UP_KEY_CODE || $event.which === DOWN_KEY_CODE) {
                    $event.preventDefault();
                    switchToSiblingItem(this, $event.which === DOWN_KEY_CODE);
                }
            });

        $container.find('.matching-options-link').each((index, node) => {
            const observer = new MutationObserver((mutationsList) => {
                const expectedMutations = mutationsList.filter(mutation => mutation.type === 'attributes' && mutation.attributeName === 'aria-expanded');

                if (expectedMutations && expectedMutations.length) {
                    const target = $(expectedMutations[0].target);
                    target.next().attr('aria-hidden', target.attr('aria-expanded') === 'false');
                }
        });
            observer.observe(node, { attributes: true });
        });

        var updateQuestionState = function (self, draggable) {
                // Set latest answer
            $latestAnswer = draggable;
                
                // Enable latest move button
                $container.find('.latest-move').removeAttr("disabled");

                // Enable reset answer button
                $container.find('.reset-answer').removeAttr("disabled");

                // Clear out any feedback
                $container.find('.feedback').html('');
                $container.find('.feedback').attr('tabindex', '-1');

                // See if the check whole question button can be enabled
                var allAnswered = true;
                $container.find('.dropzone').each(function() {
                if ($(this).find('.choice').length < 1) {
                        allAnswered = false;
                    }
                });
            if(draggable.closest('.choices').length) {
                    allAnswered = false;
                }

                if (allAnswered ) {
                    $container.find('.whole-question').removeAttr("disabled");
                } else {
                    $container.find('.whole-question').attr("disabled", "true");
                }

                // Disable this dropzone
            $dropzone = draggable.closest('.dropzone');
            if ($dropzone && $dropzone.length) {
                    try {
                        // this triggers setOptions method in jquery library, and it throws exception internally
                    $dropzone.droppable("option", "disabled", true);
                    } catch (e) {
                        // ignore setting option exception
                    }
                    $dropzone.css('opacity', '1');
                }

                // Enable all dropzones that are empty
                setTimeout(function () {
                    $container.find('.dropzone').each(function () {
                    if ($(this).find('.choice').length < 1) {
                            try {
                                // this triggers setOptions method in jquery library, and it throws exception internally
                            $(this).droppable("option", "disabled", false);
                            } catch (e) {
                                // ignore setting option exception
                        }
                        }
                    });
                }, 100);
        };

        // Set up drag/drop
        $container.find('.choice').draggable({
            helper: "clone",
            containment: $container,
            start: function() {
                $(this).attr('aria-grabbed', 'true');
            },
            stop: function() {
                $(this).attr('aria-grabbed', 'false');
            }
        });
        mathInit($container); // Initialize formulas if needed
        $container.find('.dropzone, .choices').droppable({
            drop: function(event, ui) {
                $(this).attr('aria-grabbed', 'false');

                $container.find('.choice').removeClass("incorrect");
                // Place the answer in the dropzone
                const isInChoicesContainer = $(this).hasClass('choices');
                if (isInChoicesContainer) {
                    ui.draggable.detach().appendTo($(this));
                } else {
                    ui.draggable.detach().insertBefore($(this).find('.matching-options-link'));
            }

                // Update question state
                updateQuestionState(this, ui.draggable);
                refreshDropzonePlaceholders($container);
            }
        });

        function refreshDropzonePlaceholders($container) {
            const placeholders = $container.find('.matching-options-placeholder');
            placeholders.each((index, elem) => {
                const content = $(elem).next();
    
                if (content.length) {
                    $(elem).hide();
                } else {
                    $(elem).show();
                }
            });
        }

        var onMatchingItemSelect = function ($event, self) {
            //  Prevent link click
            $event.preventDefault();

            //  Move items between columns
            const rowIndex = parseInt($(self).attr('data-row'));
            const parentRow = $(self).closest('tr.tr-matching');
            const dragElement = $container.find(`#choice-${bId}-${rowIndex}`);
            const dropzone = parentRow.find('.dropzone .drop');
            const dropzoneContent = dropzone.children('.choice');

            //  Handle already selected element
            if (dropzoneContent.length) {
                const child = dropzoneContent.first();
                const childIndex = $(child).attr('id').replace(`choice-${bId}-`, '');
                const childChoices = $(`#choices-${bId}-${childIndex}`);

                child.detach().appendTo(childChoices);
            }

            //  Move item
            dragElement.detach().insertBefore(dropzone.find(".matching-options-link"));

            //  Update placeholders
            refreshDropzonePlaceholders($container);
            
            //  Handle popup closing
            parentRow.find('.dropdown').removeClass('open');
            parentRow.find('.matching-options-link')[0].focus();

            //  Update question state
            updateQuestionState(parentRow.find('.dropzone')[0], dragElement);
        };

        $container.find('.matching-dropdown-item').click(function ($event) {
            onMatchingItemSelect($event, this);
        });

        $container.find('.matching-dropdown-item').keydown(function ($event) {
            if ($event.which === SPACE_KEY_CODE || $event.which === ENTER_KEY_CODE) {
                onMatchingItemSelect($event, this);
            }
        });

        // Check latest move
        $container.find('.latest-move').click(function() { 
            const answerHtml = $latestAnswer.html().trim();
            const descriptionHtml = $latestAnswer.closest('.tr-matching').find('.descriptions').html().trim();
            let feedbackHtml;
            
            // Make sure it's in the dropzone, then check to see if it's in the correct row
            if ($latestAnswer.closest('.dropzone').length 
                && ($latestAnswer.attr('data-answer') === $latestAnswer.closest('.dropzone').siblings(".descriptions").attr("data-id"))
            ) {
                feedbackHtml = `<p class="correct">Correct!</p> <span>Answer "${answerHtml}" is correct for description "${descriptionHtml}"</span>`;
            } else {
                feedbackHtml = `<p class="incorrect">Incorrect.</p> <span>Answer "${answerHtml}" is incorrect for description "${descriptionHtml}"</span>`;
            }

            $container.find('.feedback').html(feedbackHtml);
            $container.find('.feedback').attr('tabindex', '0');
            $container.find('.feedback').get(0).focus();
        });
        
        // Check entire question
        $container.find('.whole-question').click(function () {
            $container.find('.choice').removeClass('incorrect');

            const dropzones = $container.find('.dropzone');
            const numOptions = dropzones.length;
            let isQuestionCorrect = true;
            let incorrectAns = "";
            let numCorrect = 0;      
            //keep bID and pointsPossible for scorm
            var bID = $container.parent().find('input[name=bID]').val();
            var pointsPossible = parseInt($('#' + bID + '-pointsPossible').val());
            
            dropzones.each(function () {
                if ($(this).find('.choice').length !== 1 
                    || $(this).find('.choice').eq(0).attr('data-answer') !== $(this).siblings('.descriptions').attr('data-id')) {
                    const answerHtml = $(this).find('.choice').html();
                    const descriptionHtml = $(this).closest('.tr-matching').find('.descriptions').html();

                    if (answerHtml) {
                        incorrectAns += `<li>Answer "${answerHtml.trim()}" for description "${descriptionHtml && descriptionHtml.trim()}"</li>`;
                        $(this).find('.choice').addClass('incorrect');
                    }
                    isQuestionCorrect = false;
                } else {
                    ++numCorrect;
                }
            });

            if (isQuestionCorrect) {
                $container.find('.feedback').html('<p class="correct">Correct!</p>');
            } else {
                $container.find('.feedback').html(`
                    <p class="incorrect">Incorrect. The following answers are incorrect:</p>
                    <ul class="feedback">${incorrectAns}</ul>
                `);
            }

            // Post grade/participation points
            if (graded === 1) {
                if (incorrectAns && incorrectAns != "") {
                    comment = "<p>The student got the following incorrect:</p> <ul>" + incorrectAns + "</ul>";
                } else {
                    comment = "<p>The student answered this question correctly.</p>";
                }
                //next 3 lines are scorm specific scoring
                var grade = numCorrect/numOptions;
                var points = (grade * pointsPossible);
                ca_postGrade(grade, bID, comment, Math.round(points)); //SCORM accepts whole numbers for points so we round grade * pointsPossible
            }

            $container.find('.feedback').attr('tabindex', '0');
            $container.find('.feedback').get(0).focus();
        }); 
        
        // Reset Answer button
        $container.find('.reset-answer').click(function() {
            $container.find('.dropzone').each(function() {
                    $(this).find('.choice').removeClass('incorrect');
                    $(this).droppable("option", "disabled", false);
                    $(this).find('.matching-options-placeholder').show();

                    $container.find('.whole-question').attr("disabled", "true");
                    $container.find('.latest-move').attr("disabled", "true");
                    $container.find('.reset-answer').attr("disabled", "true");
                    $container.find('.feedback').html('');
                    $container.find('.feedback').attr('tabindex', '-1');
            });

            let definitions = [];
            // Get all choice items
            $container.find('.choice').each(function() {
                definitions.push($(this).detach());
            });

            // ShuffeArray
            definitions = shuffleArray(definitions);
            let i = 0;
            $container.find('.choices').each(function(){
                $(this).html(definitions[i]);
                i++;
            })
        });

        // Delete (hide?) the DLs
        $container.children('dl').remove(); 
        
        // Add inline width to choices so they stay the correct size while dragging
        $('.choice').css("width", $('.choice').outerWidth());
    }
};

$.fn.imgMatching = function(graded) {
    var $container = $(this);

    // Do not run unless there are definition lists present

    $container.find( ".choice-container" ).draggable({
        containment: $container,
        appendTo: 'body',
        helper: "clone",
        start: function() {
            $(this).attr('aria-grabbed', 'true');
        },
        stop: function() {
            $(this).attr('aria-grabbed', 'false');
        }
    });
    $container.find(".drop, .choices" ).droppable({
        drop: function( event, ui ) {
            $(this).attr('aria-grabbed', 'false');

            ui.draggable.detach().appendTo($(this));

            // Set latest answer
            $latestAnswer = ui.draggable;

            // Update assistive text 
            $latestAnswer.find('.assistive-text').html('Image moved to ' + $latestAnswer.parents('tr').children('th').text());

            showHideImgMatchingButtons($container);

            // Disable this dropzone
            $dropzone = ui.draggable.closest('.drop');
            if ($dropzone.find('.choice-container').length > 0) {
                try {
                $dropzone.droppable("option", "disabled", true);
                } catch (e) {
                    // ignore exception
                }
                $dropzone.css('opacity', '1');
            }

            // Enable all dropzones that are empty
            setTimeout(function () {
                $container.find('.drop').each(function () {
                    if ($(this).children('.choice-container').length < 1) {
                        try {
                            $(this).droppable("option", "disabled", false);
                        } catch (e) {

                        }
                    }
                });
            }, 100);
        }
    });

    function showHideImgMatchingButtons($container) {
        // Enable latest move button
        if ($container.find('.drop .choice-container').length > 0) {
            $container.find('.latest-move').removeAttr("disabled");
        } else {
            $container.find('.latest-move').attr("disabled", "disabled");
        }

        // Enable reset answer button
        $container.find('.reset-answer').removeAttr("disabled");

        // Clear out any feedback
        $container.find('.feedback').html('');
        $container.find('.feedback').attr('tabindex', '-1');
        $container.find('.drop').removeClass('incorrect');

        // See if the check whole question button can be enabled
        var allAnswered = true;
        $container.find('.drop').each(function() {
            if ( $(this).children('.choice-container').length < 1) {
                allAnswered = false;
            }
        });

        if (allAnswered ) {
            $container.find('.whole-question').removeAttr("disabled");
        } else {
            $container.find('.whole-question').attr("disabled", "true");
        }
    }

    // Check latest move
    $container.find('.latest-move').click(function() {
        const descriptionHtml = $latestAnswer.closest('tr').find('th .word').html().trim();
        let feedbackHtml;

        // Make sure it's in the dropzone, then check to see if it's in the correct row
        if( $latestAnswer.parent().hasClass('drop') &&
            ( $latestAnswer.attr('data-answer') == $latestAnswer.parents('tr').children('th').attr('data-id'))
        ) {
            feedbackHtml = `<p class="correct">Correct!</p> <span>Answer for description "${descriptionHtml}" is correct </span>`;
        } else {
            feedbackHtml = `<p class="incorrect">Incorrect.</p> <span>Answer for description "${descriptionHtml}" is incorrect</span>`;
        }

        $container.find('.feedback').html(feedbackHtml);
        $container.find('.feedback').attr('tabindex', '0');
        $container.find('.feedback').get(0).focus();
    });

    // Check entire question
    $container.find('.whole-question').click(function() {
        $container.find('.drop').removeClass('incorrect');
        var questionCorrect = true;
        var incorrectAns = "";
        var numOptions = 0;
        var numCorrect = 0;
        $container.find('.drop').each(function() {
            if ( $(this).children('.choice-container').length != 1 ||
                ( $(this).children('.choice-container').eq(0).attr('data-answer') != $(this).parents('tr').children('th').attr('data-id') )
            ) {
                if ($(this).children('.choice-container').html()) {
                    incorrectAns = incorrectAns +  "<li>" + $(this).parents('tr').children('th').html() + "</li>";
                    $(this).addClass('incorrect');
                }
                questionCorrect = false;
            } else {
                numCorrect++;
            }
            numOptions++;
        });

        if (questionCorrect) {
            $container.find('.feedback').html('<p class="correct">Correct!</p>');
        } else {
            $container.find('.feedback').html('<p class="incorrect">Incorrect. The following answers are incorrect:</p><ul class="feedback">' + incorrectAns + '</ul>');
        }

        // Post grade/participation points
        if (graded == 1) {
            if (incorrectAns && incorrectAns != "") {
                comment = "<p>The student got the following incorrect:</p> <ul>" + incorrectAns + "</ul>";
            } else {
                comment ="<p>The student answered this question correctly.</p>";
            }
            //next 5 lines are scorm specific scoring
            var bID = $container.parent().find('input[name=bID]').val();
            var pointsPossible = $('#'+bID+'-pointsPossible').val();
            var grade = numCorrect/numOptions;
            var points = Math.round((pointsPossible * grade));
            ca_postGrade(numCorrect/numOptions, $container.parent().find('input[name=bID]').val(), comment, points);
        }

        $container.find('.feedback').attr('tabindex', '0');
        $container.find('.feedback').get(0).focus();
    });

    // Reset Answer button
    $container.find('.reset-answer').click(function() {
        var resetAnwerContext = $(this);
        let data = [];
        $container.find('.drop').each(function() {
            if ($(this).children('.choice-container').length > 0 ) {
                data.push($(this).children('.choice-container').detach());
                $(this).removeClass('incorrect');

                $(this).droppable("option", "disabled", false);

                $container.find('.whole-question').attr("disabled", "true");
                $container.find('.latest-move').attr("disabled", "true");
                $container.find('.reset-answer').attr("disabled", "true");
                $container.find('.feedback').html('');
                $container.find('.feedback').attr('tabindex', '-1');
            }
        });
        $(resetAnwerContext).closest('.image-matching').find('.choices .choice-container').each(function() {
            data.push($(this).detach());
        });

        data = shuffleArray(data);
        for(var i =0; i < data.length; i++){
            $(data[i]).appendTo($(resetAnwerContext).closest('.image-matching').children('.choices'));
        }
    });

    $container.find('.choice-container').each(function(i, item){
        $(item).click(function(){
            $(this).focus();
        });
    });

    $selectedItem = $container.find('.choice-container:focus');
    $dropZoneSelected = $container.find('.drop:focus');
    $curentIndex = 0;
    $isKeyDown = false;

    $container.find('.dropzone').each(function(i, item){
        $(item).click(function(){
            $(this).find('.drop').focus();
            $curentIndex = $(this).find('.drop').attr('tabindex');
        });
    });

    // Add in keyboard nav
    $('body').on('keyup', function(e) {

        // if ($container.find('.choice-container:focus').length == 1) {
            $draggable = $container.find('.choice-container:focus');
        $focusedItem = $container.find('.choice-container:focus');

        let isChanged = false;

            // Reposition if one of the directional keys is pressed
            switch (e.keyCode) {

                case 37:  // Left
                if($($focusedItem).prev().length){
                    $focusedItem = $($focusedItem).prev();
                    $($focusedItem).focus();
                    isChanged = true;
                }
                break;
                case 38:  // Up
                break;
            case 39:  // Right
                if($($focusedItem).next().length){
                    $focusedItem = $($focusedItem).next();
                    $($focusedItem).focus();
                    isChanged = true;
                    }
                    break;
                case 40:  // Down
                    break; // Down
            case 32: // Space
                $selectedItem = $focusedItem;
                $container.find('.choice-container').each(function(i, item){
                    if($(item).data('answer') !== $($selectedItem).data('answer')){
                        $(item).removeClass('chosen');
            }
                });
                $selectedItem.toggleClass('chosen');
                if(!$selectedItem.hasClass('chosen')){
                    $selectedItem = null;
                }
                isChanged = true;
                break;
            case 9: // Tab
                if($selectedItem && $selectedItem.length) {
                    var firstDropzoneFocused = $container.find('.drop:focus');
                    $curentIndex = $container.find('.drop:focus').attr('tabindex');
                    if($curentIndex > 0){
                        if(firstDropzoneFocused.length == 0){
                            firstDropzoneFocused = $container.find('table.matching tr:first .drop');
                        }
                        $(firstDropzoneFocused).focus();
                        $dropZoneSelected = firstDropzoneFocused;
                    }
                    isChanged = true;
                }

                break;

            case 13: // Enter
                if($dropZoneSelected.length > 0 && $selectedItem && $selectedItem.length > 0){
                    $selectedItem.removeClass('chosen').removeAttr('tabindex').detach().appendTo($dropZoneSelected).trigger('focus');
                    $selectedItem = null;
                    isChanged = true;
            }

                break;
            default: return true; // Exit and bubble
        }

        if (isChanged) {
            showHideImgMatchingButtons($container);
        }

            // Don't scroll page
            e.preventDefault();
        //}
    });

};

//Removed refreshProgressFooter and ca_postGradeInfo() because it is not used in scorm
function refreshProgressFooter() {
}

//ca_postGrade() has been modified for a 4th param so keep this the same and any call to this function should stay the same in this scorm flie
function ca_postGrade(grade, bID, comment, points) {

    $form = $('#block-interaction-form-'+ bID);
    var sd = window.parent;
    var dataChunk = sd.GetDataChunk();
    var data = {};
    var pointsPossible = $('#'+bID+'-pointsPossible').val();

    if (typeof(points) == "undefined") {
        var points = pointsPossible;
    }

    if (comment.includes('The student participated in this activity')) {
        //get points possible for participation activity
        data = processDataChunk(dataChunk, pointsPossible, bID);
    } else {
        if(comment.includes('ca_app_vue')) {
            points = Math.round((pointsPossible * points));
            console.log('ca_app_vue');
        }
        data = processDataChunk(dataChunk, points, bID);
    }
    console.log('ca_postGrade', grade, bID, comment, points);
    newChunk = JSON.stringify(data);
    sd.SetDataChunk(newChunk);
}

//SCORM specific function that is not in the sites ca.js
function processDataChunk(dataChunk, points, bID) {
    var data = {};
        if (dataChunk) { //retreive data state to help with multiple scoring blocks
          data = JSON.parse(dataChunk);
          if(data[bID]) { //if points exist for scoring module overwire the points
              data[bID] = points;
          } else {// if they haven't taken scoring module append new score
              data[bID] = points;
          }
            
        } else { //first time setting data chunk
             data[bID] = points;
        }
    return data;
}
/*****START OF SCORM SPECIFC SCORING FUNCTIONS*********/
//SCORM specific function that is not in the sites ca.js
//called when set complete is called on the final page of the course to determine if pass/fail and set score
function scormSetPassFail()
{
    var pointsPossible = 10; //do not change these values. The scorm scrape script is looking for this string to replace     
    var masteryScore = 60; //do not change these values. The scorm scrape script is looking for this string to replace
    var sd = window.parent;
    var dataChunk = sd.GetDataChunk();
    var data;
    var totalPoints;
    var score;
   
    if (dataChunk) { //retreive data state to help with multiple scoring blocks
            data = JSON.parse(dataChunk);
            totalPoints = sumDataChunk(data);            
            
            score = (totalPoints/pointsPossible) * 100; //calculate percentage to set score
            sd.SetScore(score,100,0);
            
            if (score && score >= masteryScore) {
                sd.SetPassed();
            }
            if (score && score < masteryScore) {
                sd.SetFailed();
            }

            sd.Finish();
            
        }
}
//SCORM specific function sums all of the values of an object
function sumDataChunk(obj) {
  return Object.keys(obj).reduce((sum,key)=>sum+parseFloat(obj[key]||0),0);
}
//SCORM specific function that is not in the sites ca.js
function showScore(){
    var sd = window.parent;
    var dataChunk = sd.GetDataChunk();
    var elements = document.querySelectorAll("input[id*='pointsPossible']");
    var blocks = getAllBlocksOnPage();
    var score = 0;
    var scoreSet = false;
    var pointsPossible = 0;

    if(dataChunk) {
        dataChunk = JSON.parse(dataChunk);
        for(var i = 0; i < blocks.length; i++) {
            if (dataChunk[blocks[i]]) {
                scoreSet = true;
                score += parseInt(dataChunk[blocks[i]]);
            }
            pointsPossible += parseInt($('#'+blocks[i] + '-pointsPossible').val())
        }   
        var scoreBadge = '<h3>You have completed this page with a score of '+ score + '/' + pointsPossible + '</h3>';
        if(scoreSet) {
            $('h1').after(scoreBadge);  
        }
        
    }
}

//SCORM specific function that is not in the sites ca.js
function getAllBlocksOnPage() {
    var elements = document.querySelectorAll("input[id*='pointsPossible']");
    var blocks = [];
    
    for(var i = 0; i < elements.length; i++) {
        blocks.push(elements[i].id.split("-")[0]);
    }   
    
    return blocks;
}
/*****END OF SCORM SPECIFC SCORING FUNCTIONS*********/
$.fn.definitionList = function(options) {
    
    var settings = $.extend({
        cols: 3
    }, options);

    var $container = $(this); 
    
    $container.addClass('interactive-definition-list');
    $container.addClass('cols' + settings.cols); 

    // Do not run unless there are definition lists present
    if ($container.children('dl').length > 0) { 
    
        // Take all words and put into a DIV
        var counter = 1;
        $container.append('<div class="words"></div>'); 
        $container.append('<div class="definitions"><div class="container"></div></div>'); 
        $container.children('dl').each(function() {

            $container.children('.definitions').children('.container').append('<div tabindex="0" role="button" class="definition" id="definition-' + $container.attr('id') + '-' + counter + '"><p><strong>' + $(this).children('dt').html() + '</strong>: <br/> ' +  $(this).children('dd').html() + '</p></div>');

            if (counter % settings.cols == 1) {
                $container.children('.words').append('<div class="word-row"></div>');
            }

            $container.children('.words').append('<div tabindex="0" role="button" class="word ca-accent1" id="word-' + $container.attr('id') + '-' + counter + '"><p>' + $(this).children('dt').html() + '</p></div>');
            
            counter++;
        });

        mathInit($container); // Initialize formulas if needed
        
        $container.find('.word').click(function() { 
            var id = $(this).attr('id').replace("word",""); 
            $container.find('.word').hide(); 
            $container.find('#definition' + id).css("display", "table-cell").addClass('active').focus();
            $container.find('.definitions').css("z-index", "10");
            $container.find('.words').css("z-index", "5");

            // Track an interaction
            //next 3 lines are scorm specific scoring
            var bID = $container.parent().find('input[name=bID]').val();
            var pointsPossible = $('#'+bID+'-pointsPossible').val();            
            ca_postGrade(1, bID, 'The student participated in this activity.', pointsPossible);

        });
        
        $container.find('.definition').click(function() { 
            var id = $(this).attr('id').replace("definition",""); 
            $(this).hide().removeClass("active");
            $container.find('.word').css("display", "table-cell"); 
            $container.find('#word' + id).focus();
            $container.find('.definitions').css("z-index", "5");
            $container.find('.words').css("z-index", "10");
        });
        
        $container.find('.word, .definition').keydown(function(e) { 
            if ( 9== e.which && this == e.target ){
                window.setTimeout( function(){
                    $('.outline').removeClass('outline');
                    $(document.activeElement).focus(); 
                    $(document.activeElement).addClass('outline');
                }, 100 );
            } else if ( 13== e.which ){
                $(document.activeElement).trigger('click'); 
            } 
        }); 
        
        $container.find('.word, .definition').focus(function(e) { 
            window.setTimeout( function(){
                    $('.outline').removeClass('outline');
                    // $(document.activeElement).focus(); 
                    $(document.activeElement).addClass('outline');
                }, 100 );
        });
        
        // Delete (hide?) the DLs
        $container.children('dl').remove(); 
    }
};


$.fn.imageLabelingMatching = function(labelData, graded) {
    var $container = $(this);
    var activeElement;

    // storage for the choices that are applied to the questions
    var choicesApplied = [];

    // Do not run unless there are choices list present
    if (labelData) {
        // reorder tabindex

        // Markup for questions and answers (to be shuffled and inserted later)
        var count = 1;
        var choicesBank = new Array();
        for(var key in labelData) {
            var label = labelData[key];
            choicesBank.push('<li class="choice-item" data-title="'+ label.hotspotTitle +'"><div class="choices" holder-answer="answer' + (count) + '"><div class="choice choice-dropzone" data-answer="answer' + (count) + '">' + label.hotspotTitle + '</div></div></li>')
            count++;
        }

        // randomize the choices array
        choicesBank = shuffleArray(choicesBank);

        // Add markup for choices
        var choicesDOMHtml = '<div class="image-labeling-choice-wrapper" aria-hidden="true"><div><h4>Choices</h4></div><ul>' + choicesBank.join(' ') + '</ul></div>';

        $container.append(choicesDOMHtml);
        setTabIndexforChoices();

        $container.append('<div class="buttons">\
            <button name="latestAnswer" disabled="disabled" class="latest-move ca-button ca-accent1">Check Latest Move</button>\
            <button name="latestQuestion" disabled="disabled" class="whole-question ca-button ca-accent1">Check Whole Question</button>\
            <button name="resetButton" disabled="disabled" class="reset-answer ca-button ca-accent1-inverse">Reset</button>\
            </div><div class="feedback"></div>');

        // Set up drag/drop
        $container.find('.choice').draggable({
            helper:"clone",
            containment: $container
        });

        $container.find('.dropzone, .choices').droppable({
            drop: function(event, ui) {
                if($(this).find('.choice').length > 0){
                    return;
                }
                $container.find('.choice').removeClass("incorrect");
                ui.draggable.detach().appendTo($(this));
                $(this).find('.answer-select-dropdown').addClass('hide');

                // Set latest answer
                if ($(this).is('.dropzone')) {
                    setLatestAnswer(ui.draggable);
                } else {
                    choicesApplied.pop(); // remove the latest answer if we move the answer out
                }

                updateLatestMoveButtonStatus();

                // See if the check whole question button can be enabled
                checkWholeQuestions(ui.draggable);
                
                // Disable this dropzone
                disableDropzone(ui.draggable.parent());

                // Enable all dropzones that are empty
                enableDropzone();

                // Enable reset answer button
                $container.find('.reset-answer').removeAttr("disabled");
            }
        });

        // Check latest move
        $container.find('.latest-move').click(function() {
            if (choicesApplied.length === 0){
                return;
            }

            var latestAnswer = choicesApplied[choicesApplied.length - 1];
            if (!latestAnswer) {
                return;
            }

            if(latestAnswer.parent().hasClass('ca-image-label-dropzone') && isAnswerCorrect(latestAnswer)) {
                $container.find('.feedback').html('<p class="correct">Correct!</p>');
            } else {
                $container.find('.feedback').html('<p class="incorrect">Incorrect.</p>');
                latestAnswer.addClass('incorrect').removeClass('hide');
                latestAnswer.siblings('.answer-select-dropdown').addClass('hide');
            }
        });

        // Check entire question
        $container.find('.whole-question').click(function() {
            resetImageLabelDropzoneState();
            $container.find('.choice').removeClass('incorrect');

            var totalCorrect = 0;
            var totalAnswered = 0;
            var incorrectAns = "";
            var questionCorrect = true;

            $container.find('.ca-image-label-dropzone .choice').each(function(el) {
                if (!isAnswerCorrect($(this))) {
                    questionCorrect = false;

                    if ($(this).html()) {
                        incorrectAns = incorrectAns +  "<li>" + $(this).html() + "</li>";
                        $(this).addClass('incorrect');
                    }
                } else {
                    totalCorrect++;
                    $(this).removeClass('incorrect');
                }
                totalAnswered++;
            });

            if (questionCorrect) {
                $container.find('.feedback').html('<p class="correct">Correct!</p>');
            } else {
                $container.find('.feedback').html('<p class="incorrect">Incorrect. The following answers are incorrect:</p><ul class="feedback">' + incorrectAns + '</ul>');
            }

            // Post grade/participation points
            if (graded == 1) {
                if (incorrectAns && incorrectAns != "") {
                    comment = "<p>The student got the following incorrect:</p> <ul>" + incorrectAns + "</ul>";
                } else {
                    comment ="<p>The student answered this question correctly.</p>";
                }
                //next 5 lines are scorm specific scoring
                var bID = $container.parent().find('input[name=bID]').val();
                var pointsPossible = $('#'+bID+'-pointsPossible').val();
                var grade = totalCorrect/totalAnswered;
                var points = Math.round((pointsPossible * grade));
                ca_postGrade(totalCorrect/totalAnswered, bID, comment, points);
            }
        });

        // Tab key focus
        $container.find(".ca-image-label-dropzone").focus(function(event) {
            event.stopPropagation();

            var choiceEl = $(this).children('.choice');
            var parentEl = $(this);

            onDropzoneFocused(choiceEl, parentEl);
        });

        // Reset dropzone state
        $container.find('.image-inner').on('click', 'img', function(event) {
            event.stopPropagation();
            resetDropzoneState();
            enableDropzone();
        });

        // Click on choice label
        $container.find('.ca-image-label-dropzone').on('click', '.choice', function(event) {
            event.stopPropagation();
            onDropzoneFocused($(this), $(this).parent());
        });

        $container.find('.reset-answer').click(function() {
            var resetAnwerContext = $(this);
            let data = [];
            $container.find('.whole-question').attr("disabled", "true");
            $container.find('.latest-move').attr("disabled", "true");
            $container.find('.reset-answer').attr("disabled", "true");
            $container.find('.feedback').html('');

            $container.find('.image-inner .ca-image-label-dropzone').each(function() {
                if ($(this).children('.choice-dropzone').length > 0 ) {
                    data.push($(this).children('.choice-dropzone').removeClass('hide').detach());
                    $(this).children('.answer-select-dropdown').val('');
                    var option = $(this).children('.answer-select-dropdown').children('option')[0];
                    if (option) {
                        option.text = '';
                    }
                    $(this).removeClass('incorrect');
                }
            });

            enableDropzone();

            $container.find('.choice-item').each(function() {
                if($(this).find('.choice').length > 0)
                    data.push($(this).find('.choice').detach());
            });

            data = shuffleArray(data);
            for(var i =0; i < data.length; i++){
                $(data[i]).appendTo($(resetAnwerContext).closest('.image-matching').children('.choices'));
            }
            $container.find('.choice-item').each(function(i,v) {
                $(this).find('.choices').append(data[i]);
            });
        });

        // Dropdown change handler event
        $container.find('.ca-image-label-dropzone').on('change', 'select.answer-select-dropdown', function() {
            var value = $(this).val()
            var altText = $(this).closest('.ca-image-label-dropzone').attr('data-original-title');
            var dataValue = $(this).closest('.ca-image-label-dropzone').attr('data-value');
            var parentEl = $(this).parent();
            if (!value) {
                moveCurrentChoiceToChoices($(this));
            } else {
                var labelEl = $container.find('.image-labeling-choice-wrapper ul li .choice[data-answer=' + value + ']');
                if (labelEl && labelEl.length > 0) {
                    setLatestAnswer(labelEl);
                    moveCurrentChoiceToChoices($(this));
                    parentEl.append(labelEl.parent().html());
                    var dropdown = onChoiceDropzoneFocused($(labelEl).closest('li'),$(labelEl).closest('.image-labeling-choice-wrapper'));
                    setCurrentChoiceSelected(dropdown, altText, dataValue);
                    dropdown.trigger('change');
                    setTimeout(function() {
                        checkWholeQuestions(labelEl, true);
                    },0);
                }
            }

            if (!value) {
                var option = $(this).children('option')[0];
                if (option) {
                    option.text = '';
                }
            } else {
                var option = $(this).children('option')[0];
                if (option) {
                    option.text = 'Clear answer';
                }
            }

            parentEl.focus();

            setTimeout(() => {
                parentEl.children('select').focus();
                parentEl.attr('tabindex', -1);
            });
        });

        // handle when dropdown get focused by mouse click or keyboard navigation
        function onDropzoneFocused(choiceEl, parentEl) {
            resetDropzoneState();

            var answer;
            var choices = $container.find('.image-labeling-choice-wrapper ul li .choices .choice');
            var tabIndex = 0;
            
            if (choiceEl) {
                answer = choiceEl.attr("data-answer");
                choiceEl.addClass('hide');
            }

            if (parentEl.children('select')) {
                parentEl.children('select').remove();
            }

            if (parentEl.children('select').length == 0) {
                var title = parentEl.attr('title') ? parentEl.attr('title') : parentEl.data('original-title');
                parentEl.append(generateDropdownControl(answer, tabIndex, title));
            } else {
                parentEl.children('select').removeClass('hide');
            }

            if (answer) {
                parentEl.children('select').val(answer);
            }

            var dropdownControl = parentEl.children('select');
            // make the dropdown inside focused, so that we can press enter to show the dropdown, or arrow navigation to select the choices
            dropdownControl.focus();
            dropdownControl.blur(event => {
                parentEl.attr('tabindex', 0);
            });
            //manually trigger the tooltip
            parentEl.tooltip('show');
            parentEl.attr('tabindex', -1);

            $container.find('.answer-select-dropdown').focus(() => {
                parentEl.attr('tabindex', -1);
            });
        }

        function isAnswerCorrect(answer) {
            if (!answer) {
                return false;
            }
            return answer.parent().hasClass('dropzone') && (answer.attr('data-answer') == answer.parent().attr("data-id"))
        }

        function resetDropzoneState() {
            $container.find('.ca-image-label-dropzone').each(function() {
                // hide all dropdown controls
                $(this).children('select').addClass('hide');
                // if the choice has been selected, show it
                $(this).children('.choice').removeClass('hide');
            });
        }

        // generate the dropdown inside the dropzone
        function generateDropdownControl(currentAnswer, tabIndex, title) {
            var count = 1;
            var unAnswers = getUnAnswers();
            var selectEl = '<select class="answer-select-dropdown form-control" aria-label="' + title + '">';

            if (currentAnswer) {
                selectEl += '<option value="">Clear answer</option>';
            } else {
                selectEl += '<option value=""></option>';
            }

            var options = [];
            for(var key in labelData) {
                var answer =  ('answer' + count);
                if (unAnswers.indexOf(answer) != -1 || answer === currentAnswer) {
                    var label = labelData[key];
                    options.push('<option value="answer' + count + '">' + label.hotspotTitle + '</option>');
                }
                count++;
            }
            options = shuffleArray(options);
            for(var i = 0; i < options.length; i ++){
                selectEl += options[i];
            }

            selectEl += '</select>';

            var dropdownControl = $(selectEl);

            // show the tooltips
            dropdownControl.parent().tooltip({
                boundary: 'window',
                title: dropdownControl.parent().attr('title'),
                container: 'body',
                trigger: 'manual',
                placement: 'right'
            });

            // remove the tooltip when leave the control (eg pressing tab to switch, or mouse click to the other)
            dropdownControl.on('focusout', function() {
                dropdownControl.parent().tooltip('hide');
            });

            return dropdownControl;
        }

        function getUnAnswers() {
            var unAnswers = [];
            $container.find('.image-labeling-choice-wrapper ul li .choice').each(function() {
                if(!$(this).closest('.choices').hasClass('hide') || ($(this).closest('li').find('select') && !$(this).closest('li').find('select').val())){
                    unAnswers.push($(this).attr("data-answer"));
                }
            });
            return unAnswers;
        }

        function setLatestAnswer(el) {
            // Set latest answer
            $latestAnswer = el;

            // store the current answer to the end of the answers
            choicesApplied.push(el);

            updateLatestMoveButtonStatus();
            // Enable reset answer button
            $container.find('.reset-answer').removeAttr("disabled");

            // Clear out any feedback
            $container.find('.feedback').html('');
        }

        function updateLatestMoveButtonStatus() {
            // Enable latest move button if answers available
            if (choicesApplied.length > 0) {
                $container.find('.latest-move').removeAttr("disabled");
            } else {
                $container.find('.latest-move').attr("disabled", true);
            }
        }

        function checkWholeQuestions(el,movedToLabeling) {
            var allAnswered = true;
            $container.find('.dropzone').each(function() {
                if ( $(this).children('.choice').length < 1) {
                    allAnswered = false;
                }
            });

            if(!movedToLabeling && el.parent().hasClass('choices') ) {
                allAnswered = false;
            }

            if (allAnswered ) {
                $container.find('.whole-question').removeAttr("disabled");
            } else {
                $container.find('.whole-question').attr("disabled", "true");
            }
        }

        function disableDropzone(dropzone) {
            if (dropzone.hasClass('dropzone')) {
                try {
                    dropzone.droppable("option", "disabled", true);
                } catch (e) {
                    // ignore set option exception
                }
                dropzone.css('opacity', '1');
            }
        }

        function enableDropzone() {
            setTimeout(function () {
                $container.find('.dropzone').each(function () {
                    if ($(this).children('.choice').length < 1) {
                        try {
                            $(this).droppable("option", "disabled", false);
                        } catch (e) {
                            // ignore exception
                        }
                    }
                });
            }, 100);
        }

        function moveCurrentChoiceToChoices(el) {
            var parentEl = el.parent();
            var currentChoiceEl = parentEl.children('.choice');
            if (currentChoiceEl && currentChoiceEl.length > 0) {
                var currentChoiceAnswerId = currentChoiceEl.attr('data-answer');
                var holderEl = $container.find('.image-labeling-choice-wrapper ul li .choices[holder-answer=' + currentChoiceAnswerId + ']');
                if (holderEl && holderEl.length > 0) {
                    //currentChoiceEl.appendTo(holderEl);
                    currentChoiceEl.removeClass('hide');
                    holderEl.removeClass('hide').html(currentChoiceEl);
                    holderEl.siblings('.label-select-dropdown').remove();
                }
            }else{
                var currentDropzoneAnwser = el.val();
                var holderEl = $container.find('.image-labeling-choice-wrapper ul li .choices[holder-answer=' + currentDropzoneAnwser + ']');
            }
        }

        function saveAnswer() {
            $container.parent().find('input[name=bID]').val();
        }

        // Add inline width to choices so they stay the correct size while dragging
        $container.find('.choice').css("width", ($container.find('.choice').parent().outerWidth() - 4));
        $container.find('.choices').css("width", ($container.find('.choice').parent().outerWidth() - 4));

        function setTabIndexforChoices(){
            $container.find('.choice-item').each(function(){
                $(this).attr('tabindex',-1);
            });
        }

        function registerFocusoutForDropdown(){
            $container.find('.label-select-dropdown').on('focusout',function(){
                var that = $(this);
                setTimeout(function(){
                    if($(that).siblings('.choices.hide').length > 0){
                        $(that).siblings('.choices.hide').removeClass('hide');
                        $(that).remove();
                    }
                },0);
            });
        }

        $container.find('.image-labeling-choice-wrapper').on('click', '.choice-dropzone', function(event) {
            event.stopPropagation();
            $(this).closest('li').focus();
        });

        $container.find('.choice-item').on('keydown', function (event) {
            if (event.key === ' ' || event.key === 'Enter') {
                onChoiceDropzoneFocused($(this), $(this).closest('.image-labeling-choice-wrapper'))
            }
        });

        // handle when dropdown get focused by mouse click or keyboard navigation
        function onChoiceDropzoneFocused(choiceEl, choiceWraper) {
            var currentLabel;

            var tabIndex = choiceEl.attr('tabindex');
            var choices = choiceEl.find('.choices');

            if (choiceEl) {
                currentLabel = choiceEl.attr("data-answer");
                if(choices.find('.choice-dropzone').length > 0){
                    choices.addClass('hide');
                }
            }

            if (choiceEl.children('select')) {
                choiceEl.children('select').remove();
            }

            if (choiceEl.children('select').length == 0 && choiceEl.find('.choice-dropzone').length > 0) {
                choiceEl.append(generateLabelsDropdownControl(currentLabel, tabIndex, choices.attr('holder-answer')));
            } else {
                choiceEl.children('select').removeClass('hide');
            }
            var dropdownControl = choiceEl.children('select');
            // make the dropdown inside focused, so that we can press enter to show the dropdown, or arrow navigation to select the choices
            dropdownControl.focus();
            registerFocusoutForDropdown();

            return dropdownControl;
        }

        // Dropdown change handler event
        var previousLabeling = null;
        $container.find('.image-labeling-choice-wrapper').on('focus','select.label-select-dropdown', function () {
            // Store the current value on focus and on change
            previousLabeling = this.value;
        }).on('change', 'select.label-select-dropdown', function() {
            var value = $(this).val();
            var dropChoice = $(this).siblings('.choices').children('.choice-dropzone').detach();

            // 1. Remove old Label
            if(previousLabeling){
                var oldLabeling = getLabelDropzone(previousLabeling);
                if(oldLabeling && oldLabeling.length > 0){
                    choicesApplied.pop();
                    $(oldLabeling).html('');
                }
            }

            // 2. Find label
            var labeling = getLabelDropzone(value);
            if(labeling.length){
                $(labeling).html(dropChoice);
                labeling.find('.choice').removeClass('incorrect');
                setLatestAnswer(labeling.find('.choice'));

                // See if the check whole question button can be enabled
                checkWholeQuestions(labeling.find('.choice'));
            }

            // 3. Update latest move button status
            updateLatestMoveButtonStatus();

            // Enable reset answer button
            $container.find('.reset-answer').removeAttr("disabled");

            // 4. update other label select
            updateOtherLabelingSelect(previousLabeling, value, $(this));

            resetDropzoneState();
            // Find the answer has class hide and remove class and then focus the parent element
            $(this).siblings('.choices').removeClass('hide');
            $(this).closest('li').focus();
            $(this).remove();
        });

        function getLabelDropzone(value){
            return $container.find(".ca-image-label-dropzone[data-value='" + value + "']");
        }

        function updateOtherLabelingSelect(previousValue, newValue, currentSelect){
            $container.find('.label-select-dropdown').each(function() {
                var oldSelectedVal = $(this).val();

                if(currentSelect.attr('data-answer') !== $(this).attr('data-answer')){
                    var currentIndex = $(this).closest('li').attr('tabindex');
                    var newSelect = generateLabelsDropdownControl(null,currentIndex, $(this).attr('data-answer'));
                    if(oldSelectedVal){
                        newSelect.append('<option value="'+oldSelectedVal+'">' + oldSelectedVal + '</option>');
                    }
                    newSelect.val(oldSelectedVal);
                    $(this).replaceWith(newSelect);
                }
            });
        }

        function setCurrentChoiceSelected(selector, label, value){
            if(selector.find('option[value="'+value+'"]').length == 0){
                selector.append('<option value="'+value+'">' + label + '</option>');
            }
            selector.val(value);
        }

        function generateLabelsDropdownControl(currentLabel, tabIndex, dataAnswer) {
            var count = 1;
            var labelsUnChoice = getLabelsUnChoice();
            var selectEl = '<select class="label-select-dropdown form-control">';

            selectEl += '<option value=""></option>';

            for(var i in labelsUnChoice) {
                selectEl += '<option value="'+labelsUnChoice[i].value+'">' + labelsUnChoice[i].label + '</option>';
            }

            selectEl += '</select>';

            var dropdownControl = $(selectEl);
            dropdownControl.attr('data-answer',dataAnswer);

            dropdownControl.attr('tabindex', tabIndex);

            // mimic the tab index behaviour

            return dropdownControl;
        }

        function getLabelsUnChoice() {
            var labels = [];
            $container.find('.image-labeling-choice-wrapper').siblings('.image-inner').find('.ca-image-label-dropzone').each(function() {
                if(!$(this).children('.choice-dropzone').length)
                    labels.push({label: $(this).attr("title") || $(this).attr('data-original-title'), value: $(this).attr("data-value")});
            });
            return labels;
        }

        function resetImageLabelDropzoneState() {
            $container.find('.ca-image-label-dropzone').each(function() {
                // hide all dropdown controls
                $(this).find('.answer-select-dropdown').addClass('hide');
                // if the choice has been selected, show it
                $(this).find('.choice-dropzone').removeClass('hide');
            });
        }
    }
};

/**
 * Randomize array element order in-place.
 * Using Fisher-Yates shuffle algorithm.
 */
function shuffleArray(array) {
    for (var i = array.length - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var temp = array[i];
        array[i] = array[j];
        array[j] = temp;
    }
    return array;
}
        
$(  document ).ready(function() {
    if(!CCM_EDIT_MODE){
        $('.wysiwyg-placeholder').each(function(i, item){
            if($(this).text() != ''){
                var bId = $(this).attr('data-bid');

                $blockToMove = $('#block' + bId);
                if ($blockToMove.length == 0) {
                    $blockToMove = $('.block-wrapper[data-orig-id=' + bId + ']');
                }
                if ($blockToMove.length == 0) {
                    $blockToMove = $('.block-wrapper[data-orig-id=' + $(this).attr('data-origbid') + ']');
                }

                const selfNestedBlock = $blockToMove.has($(this));
                if (selfNestedBlock && selfNestedBlock.length) {
                    return;
                }

                $(this).html($blockToMove.detach());
            }
        });
    }
});;$.fn.classification = function(questions, bID, graded)
{
    var questionCache = {};
    var questions = questions;
    var $container = $(this);

    var answeredQuestions = {}
    var currentSlide = 1

    registerEventKeydown();

    function answer(questionId, categoryId) {
        var isCorrect = questions.findIndex(function(item) {
            return item.id == questionId && item.category_id == categoryId
        }) !== -1;
        var isLastQuestion = questions.findIndex(function(item) {
            return item.id == questionId
        }) === (questions.length - 1);
        answeredQuestions[questionId] = isCorrect;
        toggleResetBtn(true);
        displayResult(questionId);

        // Move to next slide if answer correctly
        if (isCorrect && !isLastQuestion) {
            setTimeout(function() {
                activeSlide(currentSlide + 1);
                globalTabIndex = currentSlide;
            }, 1000);
        }

        // Answered all questions
        /* if (Object.keys(answeredQuestions).length === questions.length) {
            submitResult()
        } */

        // Always submit results
        submitResult();
    }

    function displayResult(questionId) {
        var elem = $container.find('#question-'+ questionId);
        var resultClass = '.ca-classification__question-result'
        var text = answeredQuestions[questionId] ? 'Correct' : 'Try Again'
        $(elem).children(resultClass).text(text)
        $(elem).children(resultClass).addClass(answeredQuestions[questionId] ? 'is-success' : 'is-error')
        $(elem).children(resultClass).addClass('scale-in-center')
        
        var correctAnswers = Object.keys(answeredQuestions).filter(id => answeredQuestions[id] === true).length;
        var total = questions.length;
        var showMessage = '<p>' + correctAnswers + ' of ' + total + ' answered correctly</p>';
        $container.find('.displayResult').html(showMessage);
    }

    $(document).on('click', '.ca-classification__question-result.is-error', function() {
        var questionId = $(this).closest('.ca-classification__question').attr('id')
        resetQuestion(questionId);
    })

    function toggleResetBtn(show) {
        if (show) {
            $container.find('.reset-btn').css('display', 'block');
        } else {
            $container.find('.reset-btn').css('display', 'none');
        }
    }

    function initQuestionCache() {
        if ($.isEmptyObject(questionCache)) {
            $container.find('.ca-classification__question').each(function() {
                var id = $(this).attr('id')
                questionCache[id] = $(this).html()
            })
        }
    }

    function resetQuestion(id) {
        if (questionCache[id]) {
            $container.find('#'+id).html(questionCache[id]).sortable('refresh')
        }
    }

    function submitResult() {
        var correctAnswers = Object.keys(answeredQuestions).filter(id => answeredQuestions[id] === true);
        var wrongAnswers = Object.keys(answeredQuestions).filter(id => answeredQuestions[id] === false);
        var unansweredQuestions = questions.filter(question => answeredQuestions[question.id] === undefined);
        var correct = correctAnswers.length;
        var total = questions.length;
        var score = (correct / total).toFixed(2);
        var message = '';

//        console.log("Correct answers: ", correctAnswers);
//        console.log("Wrong answers: ", wrongAnswers);
//        console.log("Correct answers: ", unansweredQuestions);
        // console.log("Not Answered answers: ", notAnswered);

        if (correctAnswers.length > 0) {
            message += '<p>The student got the following correct:</p><ul>';
            correctAnswers.forEach(function (questionId) {
                var question = questions.find(function (item) {
                    return item.id === questionId;
                })
                if (question) {
                    message += ('<li>' + question.text + '</li>');
                }
            })
            message += '</ul>';
        }

        if (wrongAnswers.length > 0) {
            message += '<p>The student got the following incorrect:</p><ul>';
            wrongAnswers.forEach(function (questionId) {
                var question = questions.find(function (item) {
                    return item.id === questionId;
                })
                if (question) {
                    message += ('<li>' + question.text + '</li>');
                }
            })
            message += '</ul>';
        }

        if (unansweredQuestions.length > 0) {
            message += '<p>The student didn\'t answer the following:</p><ul>';
            unansweredQuestions.forEach(function (question) {
                message += ('<li>' + question.text + '</li>')
            })
            message += '</ul>'
        }

        // Post grade/participation points
        //next 3 lines are scorm specific scoring
        var pointsPossible = parseInt($('#' + bID + '-pointsPossible').val());
        var points = Math.round((pointsPossible * score));
        ca_postGrade(score, bID, message, points);
    }

    $container.find('.reset-btn').click(function() {
        
        // Reset all questions
        $container.find('.ca-classification__question').each(function() {
            var id = $(this).attr('id')
            resetQuestion(id);
        })
        // Hide Reset Button
        toggleResetBtn(false);
        // Clear answered questions
        answeredQuestions = {};
        // Move back to first slide
        activeSlide(1);
        globalTabIndex = 1;

        $container.find('.displayResult').html('');
    })
    
    // Sortable

    $container.find('.ca-classification__category').sortable({
        scroll: true, 
        scrollSensitivity: 80,
        scrollSpeed: 100,
        receive: function (evt, ui) {
            var destCategory = $(ui.item).parent().data('cate-id');
            var selectedQuestion = $(ui.item).data('question-id');
            $(ui.item).remove();
            answer(selectedQuestion, destCategory);
        },
        over: function(event, ui) {
            if ($(this).children().length > 1) {
                $(ui.placeholder).css('display', 'none');
            } else {
                $(ui.placeholder).css('display', '');
            }
        }
    }).disableSelection();

    var currentlyScrolling = false;

    var SCROLL_AREA_HEIGHT = 40; // Distance from window's top and bottom edge.
    
    $container.find('.ca-classification__question:visible').sortable({
        connectWith: '.ca-classification__category',
        items: '> .ca-classification__question-icon',
        scroll: true,
        sort: function(event, ui) {

            if (currentlyScrolling) {
                return;
            }

            var windowHeight   = $(window).height();
            var mouseYPosition = event.clientY;

            if (mouseYPosition < SCROLL_AREA_HEIGHT) {
                currentlyScrolling = true;

                $('html, body').animate({
                    scrollTop: "-=" + windowHeight / 2 + "px" // Scroll up half of window height.
                }, 
                400, // 400ms animation.
                function() {
                    currentlyScrolling = false;
                });

            } else if (mouseYPosition > (windowHeight - SCROLL_AREA_HEIGHT)) {

                currentlyScrolling = true;

                $('html, body').animate({
                    scrollTop: "+=" + windowHeight / 2 + "px" // Scroll down half of window height.
                }, 
                400, // 400ms animation.
                function() {
                    currentlyScrolling = false;
                });

            }
        }
    }).disableSelection();

    // Cache question in the beginning
    initQuestionCache();

    var maxItems = $container.find('.glide__slide').length;
    // Glide navigation
    $container.find('.glide__container').on('click', '.glide__arrow', function(item) {
        var nextSlide;

        var slide = Number($(this).closest('.glide__slide').attr('data-slide'));
        if($(this).hasClass('glide__arrow--next')){

            nextSlide = slide + 1;
            if(nextSlide > maxItems){
                nextSlide = 1;
            }
        } else if($(this).hasClass('glide__arrow--prev')){

            nextSlide = slide - 1;
            if(nextSlide < 1){
                nextSlide = maxItems;
            }
        }
        activeSlide(nextSlide);
    })

    function activeSlide(slide) {
        var i = 10 - slide
        currentSlide = slide

        $container.find('.glide__slide')
            .removeClass('active')
            .removeAttr('style')
        $container.find('#slide-' + slide).addClass('active')
        $container.find('.glide__slide').each(function(index) {
            var indexRedux = index + 1
            if (indexRedux == slide) {
                $(this).css({
                    'z-index': '1000',
                    transform: 'scale(1)'
                })
                i = 8
            }

            var horizontalScale = (i / 10 + 0.08);
            horizontalScale = horizontalScale > 0 ? horizontalScale : 0;

            if (indexRedux < slide) {
                $(this).css({
                    'z-index': indexRedux + 1,
                    transform: 'scale(1, ' + horizontalScale + ')'
                })
                i++
            } else if (indexRedux > slide) {
                $(this).css({
                    'z-index': 10 - indexRedux,
                    transform: 'scale(1, ' + horizontalScale + ')'
                })
                i--
            }
        })
    }

    // Active first slide at the beginning
    activeSlide(currentSlide)

    var globalTabIndex = 0;
    var maxImages = $container.find('.img-classification').length;
    var maxCates = $container.find('.box-classification').length;
    var currentCateIndex = maxImages;
    function registerEventKeydown(){
        $('#body-main').on('keydown', function (event) {

            if ((event.key === ' ' || event.keyCode === 32) && event.target.tagName === 'IMG') {
                event.preventDefault();
                event.stopPropagation();
            }
            if((event.key === 'ArrowUp' || event.key === 'ArrowDown') && event.target.tagName !== 'BODY'){
                event.preventDefault();
                event.stopPropagation();
            }

            if (event.key === 'Escape') {
                $(this).find(':focus').blur();
                globalTabIndex = 0;
                currentCateIndex = maxImages;
                $container.find('.img-classification').each(function(i, item) {
                    $(item).show();
                    $(item).next('select').hide();
                });
            }
        });
    }

    function resetAriaGrabbed(){
        $container.find('.img-classification').each(function(i, item) {
            $(item).attr('aria-grabbed', false);
            $(item).next('select').attr('aria-grabbed', false);
        });
    }

    function focusOnSelect(index){
        // find the sibling to jumb
        var nextIndexEl = $container.find('[data-tabindex="' + index + '"]');
        var nextIndexSelect = nextIndexEl.next('select');
        if(nextIndexSelect.next('.ca-classification__question-result').text() != ''){
            return;
        }

        nextIndexEl.hide();
        nextIndexSelect.show().trigger('focus');
        resetAriaGrabbed();
        $(nextIndexEl).attr('aria-grabbed', true);
        nextIndexSelect.change(function(){
            var thisCate = $(this);

            if(thisCate.val() != ''){
                var destCategory = $(thisCate).val();
                var selectedQuestion = $(thisCate).data('question-id');
                answer(selectedQuestion, destCategory);
                thisCate.hide();
            }
        });
    }

    // $container.keyup(function(event) {
    //     if (event.key === 'Tab') {
    //         // if Shift key is pressed then should go backward
    //         globalTabIndex = event.shiftKey ? globalTabIndex - 1 : globalTabIndex + 1;
    //         if(globalTabIndex <= 0 || globalTabIndex > maxImages)
    //             globalTabIndex = 1;

    //         activeSlide(globalTabIndex);
    //         focusOnSelect(globalTabIndex);

    //         event.preventDefault();
    //         event.stopPropagation();
    //     }
    // });
}
;function popup(url, id, width, height) {
    var l = (window.screen.width / 2) - ((width / 2) + 10);
    var t = (window.screen.height / 2) - ((height/ 2) + 50);
    window.open(url, id, "height=" + height + ",width=" + width + ",location=0,menubar=no,status=no,toolbar=no, left=" + l + ",top=" + t);
}

function touchHandler(event) {
    var touch = event.changedTouches[0];

    var simulatedEvent = document.createEvent("MouseEvent");
        simulatedEvent.initMouseEvent({
        touchstart: "mousedown",
        touchmove: "mousemove",
        touchend: "mouseup"
    }[event.type], true, true, window, 1,
        touch.screenX, touch.screenY,
        touch.clientX, touch.clientY, false,
        false, false, false, 0, null);

    touch.target.dispatchEvent(simulatedEvent);
    event.preventDefault();
}

function touchInit() {
    document.addEventListener("touchstart", touchHandler, true);
    document.addEventListener("touchmove", touchHandler, true);
    document.addEventListener("touchend", touchHandler, true);
    document.addEventListener("touchcancel", touchHandler, true);
}

function inIframe () {
    try {
        return window.self !== window.top;
    } catch (e) {
        return true;
    }
}

function iFrameResize() {
    var message = JSON.stringify({
        subject: "lti.frameResize",
        height: $('body').height()+10
    });

    parent.postMessage(message, "*");

    if (typeof CA_FEATURE_MODULE_PRINT !== "undefined") {
        var message = JSON.stringify({
            subject: "lti.printResize",
            height: $(document).height()+15,
            cid: CCM_CID
        });
        parent.postMessage(message, "*");
    }
}

function mathInit($container) {
    $container.find('math').each(function () {
        if (this.closest(".skip-render")){
            return;
        }
        $mathElement = $(this).parent();
        if (!$mathElement.is('span')) {
            $(this).wrap('<span></span>');
            $mathElement = $(this).parent();
        }
        mathElement = $mathElement.get(0);
        if (typeof com != "undefined") {
            com.wiris.js.JsPluginViewer.parseElement(mathElement, true, function () { });
        }

        if (typeof MathJax != "undefined") {
            MathJax.Hub.Queue(["Typeset", MathJax.Hub]);
        }
    });
}

function isMathJaxUndefined(){
    return typeof MathJax == "undefined";
}

function isWirisUndefined(){
    return typeof com == "undefined";
}

function getScrollPos() {
    return window.pageYOffset !== undefined ? window.pageYOffset : window.scrollTop;
}

/* Adaptive learning / conditional release */
function disableLinks(cid) {
    $( ".nav-item-"+cid ).nextAll().css({
        "pointer-events": "none",
        "cursor": "not-allowed",
        "opacity": 0.6
    });
    if (cid == CCM_CID) {
        $(".ccm-next-previous-nextlink a").css({
            "pointer-events": "none",
            "cursor": "not-allowed",
            "opacity": 0.6
        });
    }
    $("table.overview .overview-row-"+cid ).nextAll().find('a').css({
            "pointer-events": "none",
            "cursor": "not-allowed",
            "opacity": 0.6
        }
    );

}
function enableLinks(cid) {
    $( ".nav-item-"+cid ).nextAll().removeAttr("style");
    $(".ccm-next-previous-nextlink a").removeAttr("style");
}

$(document).ready(function() {

    /* Inline JS Quizzes */
    $('.inline-quiz-form').submit(function() {
        return false;
    });

    /* Dialog for Popup link */
    /*
    $( ".dialog" ).dialog({
        autoOpen: false,
        width: '90%',
       height: 'auto',
        modal: true,
        minHeight:'auto',
        dialogClass: 'popup-link-dialog',
        closeText: 'x',
        resizable: false
    });
    */
    /*
    $( ".popup-link" ).click(function() {
        var $popupContainer = $($(this).attr("href"));
        $popupContainer.dialog( "open" );
    }); */

    /* Open up All Download/PDF/DOC/external links in a new window */
    $('a[href$=".pdf"]').attr('target', '_blank');
    $('a[href$=".doc"]').attr('target', '_blank');
    $('a').each(function() {
        var isInternalButton = this.href === 'javascript:void(0)';
        var a = new RegExp('/' + window.location.host + '/');
        if (!isInternalButton && (!a.test(this.href) || $(this).prop('href').indexOf('download_file') != -1)) {
            if ($(this).prop('href').indexOf('readspeaker.com') == -1) {
            $(this).attr('target', '_blank');
            $(this).append('<span class="sr-only"> (opens in a new tab)</span>');
        }
        }
    });

    /* Hide coursearc sidebars if within an iFrame (LMS) */
    if (inIframe()) {
        $('.cms_container, body').addClass('in-iframe').removeClass('no-iframe');
        $('#page-properties, #cms-comments, .hide-in-iframe').hide();
        $('#page-edit-from-lms').show();
        setTimeout(function() {
            $('#edit-in-coursearc-button').prop('href', $('#ccm-nav-check-out').prop('href'));
        }, 1000);
    }

    /* Handle math tags that were missed and not rendered on page load */
    setTimeout(function() {
        mathInit($('body'));
    }, 1000);

    /* resize parent frame in LMS */
    parent.postMessage(JSON.stringify({subject: 'lti.scrollToTop'}), '*');
    setTimeout(function () {
        iFrameResize();
    }, 1000);

    let searchParams = new URLSearchParams(window.location.search);
    if (!searchParams.has('method') || searchParams.get('method') !== 'submit') {
            parent.postMessage(JSON.stringify({subject: 'lti.scrollToTop'}), '*');
    }

    /* Skip nav link */
    // bind a click event to the 'skip' link
    $(".skip-nav-btn").click(function(event){

        // strip the leading hash and declare
        // the content we're skipping to
        var skipTo="#"+this.href.split('#')[1];

        // Setting 'tabindex' to -1 takes an element out of normal
        // tab flow but allows it to be focused via javascript
        $(skipTo).attr('tabindex', -1).on('blur focusout', function () {

            // when focus leaves this element,
            // remove the tabindex attribute
            $(this).removeAttr('tabindex');

        }).focus(); // focus on the content container
    });
//Removed test_cookie and student progress modal function from SCORM content as it is not needed for SCORM

    /* Quiz hints */
    $('.quizzing-block .hint-button').click(function(event) {
        $(this).next().fadeIn().attr('aria-expanded','true');
        event.preventDefault();
        return false;
    });

    /* Accordions */
    $(".interactive-accordion .section-title").on("click", function() {
        var $currentSection = $(this).parents(".accordion");

        $currentSection.find('.arrow').toggleClass('arrow-down').toggleClass('arrow-right');
        $currentSection.find('.section-body').slideToggle();
        $currentSection.find('.section-body, .section-title').toggleClass('active');

        // Toggle aria-expanded
        var expanded = "true";
        if ($currentSection.find('.section-title').attr('aria-expanded') == "true") {
            expanded = "false";
        }
        $currentSection.find('.section-title').attr('aria-expanded', expanded);

        return false;
    });

    $('.glossary-link a').click(function() {
        popup($(this).prop('href'), 'glossary'+COURSE_ID, 900, 600);
        return false;
    });
    $('a.glossary-term-link').each(function() {
        var url = $(this).attr('href');
        url = '/student-glossary/' + COURSE_ID + '/' + url.replace('#term', '');
        $(this).prop('href', url);
        $(this).click(function() {
            popup($(this).prop('href'), 'glossary'+COURSE_ID, 900, 600);
            return false;
        });
    });

    /* Viewing block interactions */
    initBlockInteractions();

    /* Deal with copied modules */
    if (typeof thisModulePath != "undefined" && typeof copyOfPath != "undefined") {
        $('#body-main a').each(function () {
            $thisLink = $(this);
            href = $thisLink.attr('href');
            if (href) {
                copyOfPath.forEach(function (oldPath) {
                    href = href.replace(oldPath + "/", thisModulePath + "/");
                });

                $thisLink.prop('href', href);
            }
        });
    }

    /* Module Printing */
    if (typeof CA_FEATURE_MODULE_PRINT !== "undefined") {
        if (window.matchMedia) {
            var mediaQueryList = window.matchMedia('print');
            mediaQueryList.addListener(iFrameResize);
        }
        window.onbeforeprint = iFrameResize;

        // Listen for a message to show modals
        var hasCss = false;
        window.addEventListener('message', function(e) {

            if (typeof e.data === "string") {
                var message = JSON.parse(e.data);
            } else if (typeof e.data === "object") {
                var message = e.data;
            }
                if (typeof message === "object") {
                if (message.subject == 'ca.print') {
                    // Show modal and send message back to parent to size frame accordingly
                    if (!hasCss) {
                        $('head').append('<link rel="stylesheet" href="/css/print.css" type="text/css" />');
                        setTimeout(iFrameResize, 3000);
                        hasCss = true;
                    }
                }
            }
        });
    }

});

function initBlockInteractions() {
    $('.panel-student-progress select#student_id').change(function() {
        if ($(this).val() != "0") {
            var url = '/?cID=' + CCM_CID + '&action=studentInteractions&student_id=' + $(this).val();
            document.location.href = url;
        }
    });
}

function showLoader() {
    $('body').append('<div id="ccm-dialog-loader-wrapper" class="ccm-ui"><img id="ccm-dialog-loader" src="/themes/cms/css/ca-loader.gif"></div>');
}

function hideLoader() {
    $('#ccm-dialog-loader-wrapper').remove();
}

function showAnswerInputs(button, answerArray, bId) {
    var $button = $(button);
    var $inputs = $.makeArray( $button.parent('form').children('fieldset').children('input[type=text]') );
    var $form = $button.parent('form');
    var points = 0;//scorm specific
    $button.parent('form').children('.markup').remove();
    var info = [];
    var numCorrect = 0;

    if (answerArray.length == ($inputs.length + 1) ||  answerArray.length == ($inputs.length) ) {

        $button.parent('form').children('fieldset').children('input[type=text]').each(function(i) {

            // Handle floats if the correct answer is a float
            if (!isNaN(parseFloat(answerArray[i].replace(/,/g, '')))) {
                var correctAnswer = parseFloat(answerArray[i].replace(/,/g, '')) ;
                var userAnswer = parseFloat($(this).val().replace(/,/g, '')) ;
            } else {
                // Otherwise, just see if the strings match
                var correctAnswer = answerArray[i];
                var userAnswer = $(this).val();
            }

            if ( userAnswer == correctAnswer ) {
                isCorrect = true;
                numCorrect++;
                $(this).addClass('correct');
                $(this).removeClass('incorrect');
                $form.append(`<div class="markup student-correct" id="markup-${bId}"><p>Correct.</p></div>`);
                points = getBlockScorePossible(bId);//scorm specific
            } else {
                isCorrect = false;
                $(this).addClass('incorrect');
                $(this).removeClass('correct');
                $form.append(`<div class="markup student-incorrect" id="markup-${bId}"><p>Incorrect. The correct answer is: <em>` + numberWithCommas(answerArray[i]) + '</em></p></div>');
            }

            info.push({
                studentAnswer: userAnswer,
                isCorrect: isCorrect
            });

            // $(this).val(numberWithCommas(answerArray[i]));
        });

        // Add feedback if provided
        if (answerArray.length == ($inputs.length + 1)) {
            $button.parent('form').find('.markup').append('<p>' + answerArray[ $inputs.length ] + '</p>');
        }

        mathInit($button.parent('form').find('.markup'));
        // Post interaction scorm specific
        ca_postGrade(1, $form.find('input[name=bID]').val(), 'The student participated in this self-check activity.', points);

    }
}

function checkRadioAccessibility(button, answer, type) {
    $parentDiv = $(button).parents('.accessibile-quiz-container');
    bID = parseInt($parentDiv.attr('data-bid'));
    var $form = $(button).parent('form');
    var points = 0;//scorm specific

    // Get the index of this question
    $thisQuestionForm = $(button).parent('form');
    index = parseInt($thisQuestionForm.attr('data-questionid'));

    // Check this question
    accessibleAnswers[bID][index+1] = checkRadio(button, answer, false);
    
    $form.find(`.student-${correctIncorrect}`).attr('tabindex', '0')
	$form.find(`.student-${correctIncorrect}`).get(0).focus();

    // Check entire question so we can pass the grade back to the LMS
    var i = 1;
    var totalCorrect = 0;
    var totalQuestionsCount = $parentDiv.find('fieldset').length;
    incorrectText = "";
    $parentDiv.find('fieldset').each(function() {

        if (accessibleAnswers[bID][i]) {
            totalCorrect ++;
            points = getBlockScorePossible(bID);//scorm specific
        } else {
            if (type == "categorization") {
                incorrectText = incorrectText + '<li>' + $(this).find('legend').text() + '</li>';
            } else {
                incorrectText = incorrectText + '<li>' + $(this).find('label.ans' + $thisQuestionForm.attr('data-answer')).text() + '</li>';
            }
        }

        i++;
    });

    var grade = 0;
    if (totalQuestionsCount === 0) {
        grade = 0;
    } else {
        grade = totalCorrect / totalQuestionsCount;
    }

    if (incorrectText != "") {
        comment = '<p>The student got the following incorrect:</p><ul>' + incorrectText + '</ul>';
    } else {
        comment = '<p>The student answered this question correctly.</p>';
    }
    //scorm specific
    ca_postGrade(grade, bID, comment, points);
}

function checkRadio(button, answer, postInteraction, bid = null) {
    var $form = $(button).parent('form');
    var $answered = $form.find('input[type="radio"]:checked');
    var didAnswer = true;
    var bID = $form.find('input[name=bID]').val();//scorm specific
    var points = 0;//scorm specific
    
    if ($answered.val() === answer[0]) {
        // Correct answer
        $form.children('label').removeClass('incorrect');
        $answered.parent('label').addClass('correct');
        $form.children('.question').removeClass('incorrect');
        $form.children('.question').addClass('correct');
        $form.children('.markup').remove();
        correctIncorrect = 'correct';
        isCorrect = true;
        points = getBlockScorePossible(bID);//scorm specific
    } else {
        // Incorrect answer
        $form.children('label').removeClass('correct');
        $answered.parent('label').addClass('incorrect');
        $form.children('.question').removeClass('correct');
        $form.children('.question').addClass('incorrect');
        $form.children('.markup').remove();
        correctIncorrect = 'incorrect';
        isCorrect = false;
        if ($answered.length == 0) {
			didAnswer = false;
		}
    }

    $correct = $form.find('input[value="' + answer[0]  + '"]');
    if (didAnswer) {
		$correct.parent('label').css("font-weight", "bold");
	}

    $form.children('.markup').remove();
    var feedback = '';
    if(bid) {
        feedback = '<div class="markup student-' + correctIncorrect + `" id="markup-${bid}"><p>Your answer is ` + correctIncorrect + '. ';
    } else {
        feedback = '<div class="markup student-' + correctIncorrect + '"><p>Your answer is ' + correctIncorrect + '. ';
    }
    
    if (!isCorrect) {
        $answer = $form.find('label.ans' + answer[0]).clone();
        $answer.find('input').remove();
        answerText = $answer.html().trim();
        if (didAnswer) {
			feedback = feedback + 'The correct answer is: <em>' + answerText + '</em>';
		}
    }
    if (answer.length > 1) {
        feedback = feedback + '</p><p>' + answer[1] + '</p></div>';
    } else {
        feedback = feedback + '</p>';
    }
    $form.append($(feedback).hide().fadeIn());
    mathInit($form)

    if (postInteraction) {
        //scorm specific
        ca_postGrade(1, $form.find('input[name=bID]').val(), 'The student participated in this self-check activity.', points);
    } else {
        return isCorrect;
    }

}

function showAnswer(button, answer, bid) {
    var $button = $(button);
    var $form = $button.parent('form');
    var bID = $form.find('input[name=bID]').val();//scorm specific
    var points = getBlockScorePossible(bID);//scorm specific
    $form.children('.markup').remove();
    $form.append($(`<div class="correct markup" id="markup-${bid}">` + answer + '</div>').hide().fadeIn());
    mathInit($form)
    //scorm specific
    ca_postGrade(1, $form.find('input[name=bID]').val(), 'The student participated in this self-check activity.',points);
}
//scorm specific
function getBlockScorePossible(bID) {
    var points = parseInt($('#'+bID+'-pointsPossible').val());
    return points;
}

function validateAnswer(input, answer) {
    var isValid = true;
    var $input = $(input);

    // Handle floats if the correct answer is a float
    if (!isNaN(parseFloat(answer.replace(/,/g, '')))) {
        var correctAnswer = parseFloat(answer.replace(/,/g, '')) ;
        userAnswer = $input.val();
        if (!isNaN(parseFloat(userAnswer.replace(/,/g, '')))) {
            var userAnswerFloat = parseFloat(userAnswer.replace(/,/g, ''));
            if (userAnswerFloat != correctAnswer) {
                isValid = false;
            }
        } else {
            isValid = false;
        }
    } else {
        // Otherwise, just see if the strings match
        var correctAnswer = answer;
        var userAnswer = $(this).val();
        if (correctAnswer != userAnswer) {
            isValid = false;
        }
    }

    if (isValid) {
        $input.addClass('correct');
        $input.removeClass('incorrect');
    } else {
        $input.addClass('incorrect');
        $input.removeClass('correct');
    }

    $input.val(numberWithCommas(correctAnswer));
}

function numberWithCommas(x) {
    x = x.toString();
    if ( !isNaN(parseFloat(x.replace(/,/g, ''))) && x > 999) {
        return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    } else {
        return x;
    }
}

function resetFocusForBlock($form) {
    const firstQuestion = $form.find('.quiz-question-with-answer').first();
    let firstElement;

    if (firstQuestion.hasClass('quiz-question-with-multiple-answers')) {
        const answers = firstQuestion.find('.quiz-answers label').filter(".student-correct, .student-incorrect");

        if (answers.length) {
            firstElement = answers.find('input').get(0);
        } else {
            firstElement = firstQuestion.find('input').get(0);
        }
    } else {
        firstElement = firstQuestion.find('input, select').get(0);
    }

    firstElement && firstElement.focus();
}

/*SCORM does not reset scores or deal with cookies so these lines are removed. */
// Cookies
function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    }
    else var expires = "";

    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

function link_is_external(link_element) {
    return (link_element.host !== window.location.host);
}

function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1].replace(/\+/g, ' '));
}

function updateUrlParams(key, value, url) {
    if (!url) url = window.location.href;
    var re = new RegExp("([?&])" + key + "=.*?(&|#|$)(.*)", "gi"),
        hash;

    if (re.test(url)) {
        if (typeof value !== 'undefined' && value !== null) {
            return url.replace(re, '$1' + key + "=" + value + '$2$3');
        }
        else {
            hash = url.split('#');
            url = hash[0].replace(re, '$1$3').replace(/(&|\?)$/, '');
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                url += '#' + hash[1];
            }
            return url;
        }
    }
    else {
        if (typeof value !== 'undefined' && value !== null) {
            var separator = url.indexOf('?') !== -1 ? '&' : '?';
            hash = url.split('#');
            url = hash[0] + separator + key + '=' + value;
            if (typeof hash[1] !== 'undefined' && hash[1] !== null) {
                url += '#' + hash[1];
            }
            return url;
        }
        else {
            return url;
        }
    }
}

// region window

window.CommonConstants = {};

window.CommonConstants.postRequestType = 'POST';

window.CommonConstants.zero = 0;

window.CommonConstants.stringEmpty = '';

function printClick() {
    $('textarea[id*=question]').each(function () {
        var printText = $(this).val();
        $(this).next('div').html(printText);
    });
    window.print();
};//region constans

const ANCHOR_TAG_NAME = 'a';
const BUTTON_TAG_NAME = 'button';
const SPAN_TAG_NAME = 'span';
const LI_TAG_NAME = 'li';
const UL_TAG_NAME = 'ul';

const DATA_FUNCTION_ATTR = 'data-function';
const HREF_ATTR = 'href';
const ARIA_EXPANDED_ATTR = 'aria-expanded';
const RIGHT_KEY_EVENT_ADDED_ATTR = 'right-key-event-added';
const DOWN_KEY_EVENT_ADDED_ATTR = 'down-key-event-added';
const UP_KEY_EVENT_ADDED_ATTR = 'up-key-event-added';
const LEFT_KEY_EVENT_ADDED_ATTR = 'left-key-event-added';

const CLICK_EVT_NAME = 'click';

const EVENT_SET_TIMEOUT = 1000;
const TAB_CODE = 9;
const SPACE_CODE = 32;
const ENTER_CODE = 13;
const LEFT_CODE = 37;
const UP_CODE = 38;
const RIGHT_CODE = 39;
const DOWN_CODE = 40;
const ESC_CODE = 27;

const DROPDOWN_SUBMENU_CLASS = 'dropdown-submenu';
const DROPDOWN_SUBMENU_SELECTOR = `.${DROPDOWN_SUBMENU_CLASS}`;
const DROPDOWN_MENU_CLASS = 'dropdown-menu';
const DROPDOWN_MENU_SELECTOR = `.${DROPDOWN_MENU_CLASS}`;
const SUBMENU_DROPDOWN_ANCHOR_CLASS = 'submenu-dropdown-anchor';
const SUBMENU_DROPDOWN_ANCHOR_SELECTOR = `.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`;
const BREADCRUMB_SINGLE_URL_CLASS = 'breadcrumb-single-url';
const BREADCRUMB_SINGLE_URL_SELECTOR = `.${BREADCRUMB_SINGLE_URL_CLASS}`;
const ARROW_RIGHT_CLASS = 'arrow-right-button';
const ARROW_RIGHT_SELECTOR = `.${ARROW_RIGHT_CLASS}`;
const ARROW_DOWN_CLASS = 'arrow-down-button';
const ARROW_DOWN_SELECTOR = `.${ARROW_DOWN_CLASS}`;
const EDIT_ICON_BREADCRUMBS_CLASS = 'edit-icon-breadcrumbs';
const EDIT_ICON_BREADCRUMBS_SELECTOR = `.${EDIT_ICON_BREADCRUMBS_CLASS}`;
const BREADCRUMB_URL_LINK = '.breadcrumb-url-link';
const DROPDOWN_FLEX_CONTAINER_CLASS = 'dropdown-flex-container';
const DROPDOWN_FLEX_CONTAINER_SELECTOR = `.${DROPDOWN_FLEX_CONTAINER_CLASS}`;
const MENU_ITEM_LINK_CLASS = 'menu-item-link';
const MENU_ITEM_LINK_SELECTOR = `.${MENU_ITEM_LINK_CLASS}`;
const DROPDOWN_CLASS = 'dropdown';
const DROPDOWN_SELECTOR = `.${DROPDOWN_CLASS}`;

const DROPDOWN_SUBMENU_ITEM_SELECTOR = `${DROPDOWN_SUBMENU_SELECTOR} .${DROPDOWN_MENU_CLASS} ${ANCHOR_TAG_NAME}`;

const VIEW_ALL_DROPDOWN_BUTTON_HTML = '<li><a href="/course-manager/all-courses">View All Courses</a></li>';
const ADD_COURSE_DROPDOWN_BUTTON_HTML = '<li><a href="/course-manager/contents/add/0"><i class="glyphicon-plus icon-white"></i> Add New Course</a></li>';
const DROPDOWN_MENU_QUERY_SELECTOR = `#courses-dropdown .${DROPDOWN_MENU_CLASS}`
const COURSE_HISTORY_LOCAL_STORAGE_ID = 'course_history'
const DASHBOARD_COURSE_LIST_SELECTOR = '#dashboard-course-list .nav-list'
const VIEW_ALL_DASHBOARD_BUTTON_HTML = '<li><a href="/course-manager/all-courses">View All Courses</a></li>';
const LAST_OPENED_COURSE_NUMBER = 20;
const COURSE_LEVEL_NAME = 'course';
const MODULE_LEVEL_NAME = 'module';

//endregion

//region CourseArc History

var courseHistoryStore;

function loadActiveCoursesFromHistory() {
    let courseHistory = JSON.parse(localStorage.getItem(COURSE_HISTORY_LOCAL_STORAGE_ID));
    const cids = courseHistory && courseHistory.filter(x => x.cID).map(x => x.cID);
    const breadcrumbsControllerUrl = `/index.php/tools/breadcrumbs/?cIDs[]=${cids}`;

    if (cids && cids.length) {
        $.ajax({
            type: window.CommonConstants.postRequestType,
            url: breadcrumbsControllerUrl,
            async: false,
            success: result => {
                if (result && result.success) {
                    courseHistoryStore = result.activeCourses;
                    courseHistoryStore = courseHistoryStore.sort((a, b) =>
                        courseHistory.findIndex(course => course.cID === a.cID) - courseHistory.findIndex(course => course.cID === b.cID));

                    localStorage.setItem(COURSE_HISTORY_LOCAL_STORAGE_ID, JSON.stringify(result.activeCourses));
                }
            }
        });
    }
}

function getCourseHistory() {
    let courseHistory;

    if (!courseHistoryStore || !courseHistoryStore.length) {
        courseHistory = null;
    } else {
        courseHistory = JSON.parse(JSON.stringify(courseHistoryStore));
    }

    return courseHistory;
}

function saveCourseHistory(pageName, cID) {
    let history = JSON.parse(localStorage.getItem(COURSE_HISTORY_LOCAL_STORAGE_ID));

    if (!history || !history.length) {
        history = [];
    }

	//	Insert new page into the history list
    history = history.filter(x => x.cID !== cID);
    history.unshift({
        name: pageName,
        link: window.location.href,
        cID: cID
    });

	//	Remove old history records if needed
    if (history.length > LAST_OPENED_COURSE_NUMBER) {
        history = history.slice(window.CommonConstants.zero, LAST_OPENED_COURSE_NUMBER);
    }

    localStorage.setItem(COURSE_HISTORY_LOCAL_STORAGE_ID, JSON.stringify(history));
}

//endregion

//region functions

// The following 3 functions need to go together:
function getDashboardItems() {
    let courseHistory = getCourseHistory();

    if (courseHistory) {
        const courseList = document.querySelector(DASHBOARD_COURSE_LIST_SELECTOR);
        let items = window.CommonConstants.stringEmpty;

        courseHistory.forEach(x => {
            items += `<li><a href="${x.link}">${x.name}</a></li>`;
        });

        courseList.innerHTML = VIEW_ALL_DASHBOARD_BUTTON_HTML + items;
    }
}

function getNavDropdownItems() {
    let courseHistory = getCourseHistory();

    if (courseHistory) {
        const dropdownMenu = document.querySelector(DROPDOWN_MENU_QUERY_SELECTOR);
        let items = window.CommonConstants.stringEmpty;

        courseHistory.forEach(x => {
            items += `<li><a aria-haspopup="false" href="${x.link}" aria-label="Open ${x.title} page">${x.name}</a></li>`;
        });

        dropdownMenu.innerHTML = VIEW_ALL_DROPDOWN_BUTTON_HTML + ADD_COURSE_DROPDOWN_BUTTON_HTML + items;
    }
}

function getDropdownItems(key, cId) {
    let items = window.CommonConstants.stringEmpty;

    if (key === COURSE_LEVEL_NAME) {
        const courseHistory = getCourseHistory();
        const dropdownToogleAnchor = document.getElementById('dropdown-' + key);
        const itemList = document.getElementById('dd-' + key);

        if (courseHistory) {
            courseHistory.forEach(x => {
                // const title = x.name.length >= 55 ? `title="${x.name}"` : '';
                items += `<li role="none"><a class="menu-item-link" aria-label="Open ${x.name} page" role="menuitem" aria-haspopup="false" href="${x.link}">${x.name}</a></li>`;
            });

            itemList.innerHTML = items;

            assignOpenDropdownToAnchor(dropdownToogleAnchor, false);
        }
    } else if (key === MODULE_LEVEL_NAME) {
        $.ajax(`/index.php/tools/sitemap?cIDs[]=${cId}`, {
            success: result => {
                if(!result.error) {
                    result[0].children.forEach(x => { items += generateNestedList(x) });

                    const dropdownToogleAnchor = document.getElementById('dropdown-' + key);
                    assignOpenDropdownToAnchor(dropdownToogleAnchor, false);

                    const listContainer = document.getElementById('dd-' + key);

                    listContainer.innerHTML = items;

                    const anchorTagItems = document.getElementsByClassName(SUBMENU_DROPDOWN_ANCHOR_CLASS);

                    anchorTagItems.forEach(anchor => {
                        assignOpenDropdownToAnchorWithDisplay(anchor, true);
                    });

                    dropdownToogleAnchor.addEventListener(CLICK_EVT_NAME, (event) => {
                        const targetTag = event.target.tagName.toLowerCase();
                        if (targetTag === ANCHOR_TAG_NAME || targetTag === SPAN_TAG_NAME) {
                            window.location.href = dropdownToogleAnchor.getAttribute(DATA_FUNCTION_ATTR) || dropdownToogleAnchor.getAttribute(HREF_ATTR);
                        } else {
                            anchorTagItems.forEach(anchor => {
                                hideAnchorDropdown(anchor);
                            });

                            setTimeout(() => event.target.focus());
                        }
                    });
                }
            }
        });
    }
}

function generateNestedList(item) {
    // const title = item.title.length >= 55 ? `title="${item.title}"` : '';
	let result = `	<li role="none">
                        <a class="menu-item-link" role="menuitem" aria-haspopup="false" href="${item.cPath}" aria-label="Open ${item.title} page">
                            ${item.title}
                        </a>
                    </li>`;

    if (item.children) {
        let childrenDropdowns = '';
        item.children.forEach(x => {childrenDropdowns += generateNestedList(x)});
        // const title = item.title.length >= 55 ? `title="${item.title}"` : '';

        result = `
        <li class="${DROPDOWN_CLASS} ${DROPDOWN_SUBMENU_CLASS}" role="menuitem">
            <a role="link" href="${item.cPath}" class="${SUBMENU_DROPDOWN_ANCHOR_CLASS}" aria-haspopup="false" aria-label="Open ${item.title} page">
                ${item.title}
            </a>
            <button role="button" class="${ARROW_RIGHT_CLASS}" aria-label="Show submenu of ${item.title}" ${ARIA_EXPANDED_ATTR}="false" aria-haspopup="true"
                aria-controls="pagesUL" data-toggle="dropdown">
                <div></div>
            </button>
            <ul class="${DROPDOWN_MENU_CLASS}" id="pagesUL" role="menu" aria-hidden="true">
                ${childrenDropdowns}
            </ul>
        </li>`;
    }

    return result;
}

function assignKeydownEventOnItem(selector, attribute, keydownFunction) {
    if (!$(selector).first().attr(attribute)) {
        $(selector).each(keydownFunction);

        $(selector).each(function(i, elem) {
            $(elem).attr(attribute, (true).toString());
        });
    }
}

function preventDefaultEvent(event) {
    if (event) {
        event.preventDefault();
        event.stopPropagation();
    }
}

function toggleSubDropdownMenu(anchor) {
    toggleAnchorDropdown(anchor);

    anchor.setAttribute(ARIA_EXPANDED_ATTR, `${$(anchor).parent().hasClass('open')}`);
}

function assignOpenDropdownToAnchorWithDisplay(element, isPreventDefault = false) {
    const anchor = element;

    anchor.addEventListener(CLICK_EVT_NAME, (event) => {
        if (isPreventDefault) {
            event.preventDefault();
        }

        const targetTag = event.target.tagName.toLowerCase();
        if (targetTag === ANCHOR_TAG_NAME || targetTag === SPAN_TAG_NAME) {
            window.location.href = anchor.getAttribute(DATA_FUNCTION_ATTR) || anchor.getAttribute(HREF_ATTR);
        } else {
            toggleSubDropdownMenu(anchor);
        }

        setTimeout(() => event.target.focus());
    });
}

function assignOpenDropdownToAnchor(element, isPreventDefault = false) {
    const anchor = element;

    anchor.addEventListener(CLICK_EVT_NAME, (event) => {
        if (isPreventDefault) {
            event.preventDefault();
        }

        const targetTag = event.target.tagName.toLowerCase();
        if (targetTag === ANCHOR_TAG_NAME || targetTag === SPAN_TAG_NAME) {
            window.location.href = anchor.getAttribute(DATA_FUNCTION_ATTR) || anchor.getAttribute(HREF_ATTR);
        } else {
            showAnchorDropdown(anchor);
        }

        setTimeout(() => event.target.focus());
    });
}

//endregion

//region Navigation Menu

function assignArrowNavigationEventsOnNavigationMenu() {
    const items = getMenusAvailableForNavigation();

    items.keydown(function(event) {
        if (event.target === this) {
            let container = $(this).closest(DROPDOWN_FLEX_CONTAINER_SELECTOR);
            let newElement;

            switch(event.which) {
                case RIGHT_CODE:
                    newElement = getNextElementInNavigationMenu(this, container);

                    if (!newElement) {
                        container = container.next() && container.next().next();
                        newElement = getNextElementInNavigationMenu(null, container);
                    }

                    if (newElement) {
                        moveFocusOnRightLeftKeyPressed($(newElement), false);
                    }
                    
                    break;
                case LEFT_CODE:
                    newElement = getPrevElementInNavigationMenu(this, container);

                    if (!newElement) {
                        container = container.prev() && container.prev().prev();
                        newElement = getPrevElementInNavigationMenu(null, container);
                    }

                    if (newElement) {
                        moveFocusOnRightLeftKeyPressed($(newElement), false);
                    }

                    break;
                default:
                    break;
            }
        }
    });
}

function getNextElementInNavigationMenu(newElement, container) {
    const items = getMenusAvailableForNavigation();
    const availableItemsInContainer = items.filter((index, element) => {
        const parent = $(element).closest(DROPDOWN_FLEX_CONTAINER_SELECTOR);
        const isChildElement = container.get(window.CommonConstants.zero) === parent.get(window.CommonConstants.zero);

        return isChildElement;
    });
    const indexOfLastAvailableElement = availableItemsInContainer.length - 1;
    let result = null;
    let currentElementIndex;

    if (newElement) {
        currentElementIndex = availableItemsInContainer.index($(newElement));
    } else {
        currentElementIndex = -1;
    }

    if (availableItemsInContainer.length && currentElementIndex !== indexOfLastAvailableElement) {
        const indexOfNewElement = currentElementIndex + 1;
        result = availableItemsInContainer.get(indexOfNewElement);
    }

    return result;
}

function getPrevElementInNavigationMenu(newElement, container) {
    const items = getMenusAvailableForNavigation();
    const availableItemsInContainer = items.filter((index, element) => {
        const parent = $(element).closest(DROPDOWN_FLEX_CONTAINER_SELECTOR);
        const isChildElement = container.get(window.CommonConstants.zero) === parent.get(window.CommonConstants.zero);

        return isChildElement;
    });
    const indexOfFirstAvailableElement = window.CommonConstants.zero;
    let result = null;
    let currentElementIndex;

    if (newElement) {
        currentElementIndex = availableItemsInContainer.index($(newElement));
    } else {
        currentElementIndex = availableItemsInContainer.length;
    }

    if (availableItemsInContainer.length && currentElementIndex !== indexOfFirstAvailableElement) {
        const indexOfNewElement = currentElementIndex - 1;
        result = availableItemsInContainer.get(indexOfNewElement);
    }

    return result;
}

function moveFocusOnRightLeftKeyPressed(newElement, isLeft) {
    let elementToFocus;

    if (newElement.prop("tagName").toLowerCase() === UL_TAG_NAME) {
        elementToFocus = isLeft ? newElement.children().last() : newElement.children().first();
    } else {
        elementToFocus = newElement;
    }

        elementToFocus.focus();
}

function assignFocusNavigationEventsOnNavigationMenu() {
    const items = getMenusAvailableForNavigation();

    items.focus(function() {
        hideAllAnchorDropdowns($(ARROW_DOWN_SELECTOR));
        // showAnchorDropdown(this);
    });

    $(BREADCRUMB_URL_LINK).focus(function() {
        hideAllAnchorDropdowns($(ARROW_DOWN_SELECTOR));
    });
}

function assignKeyboardNavigationEventsOnNavigationMenu() {
    const downArrows = $(ARROW_DOWN_SELECTOR);
    const dropdownLinks = $(BREADCRUMB_SINGLE_URL_SELECTOR);
    const editButtons = $(EDIT_ICON_BREADCRUMBS_SELECTOR);

    downArrows.keydown(function($event) {
        if ($event.target === this) {
            const button = $(this);

            switch($event.which) {
                case DOWN_CODE:
                case SPACE_CODE:
                    showAnchorDropdown(button);
                    break;
                case ENTER_CODE:
                    $event.preventDefault();
                    showAnchorDropdown(button);
                    setTimeout(() => $(
                            `${DROPDOWN_MENU_SELECTOR} > ${LI_TAG_NAME} > ${ANCHOR_TAG_NAME}`,
                            button.parent()
                        ).first().focus());
                    break;
                case UP_CODE:
                    showAnchorDropdown(button);
                    setTimeout(() => {
                        const parentElement = $(`${DROPDOWN_MENU_SELECTOR}`, button.parent()).first().children().last();
                        let elementToFocus = $(BUTTON_TAG_NAME, parentElement);

                        if (!elementToFocus || !elementToFocus.length) {
                            elementToFocus = $(ANCHOR_TAG_NAME, parentElement);
                        }
                        
                        elementToFocus.first().focus();
                    });
                    break;
                default:
                    break;
            }
        }
    });

    const items = $.merge(
        dropdownLinks,
        editButtons
    );

    items.keydown(function($event) {
        if ($event.target === this) {
            switch($event.which) {
                case ENTER_CODE:
                case SPACE_CODE:
                    $event.preventDefault();

                    const url = $(this).attr(HREF_ATTR) || $(this).attr(DATA_FUNCTION_ATTR);
                    window.location.href = url;
                    break;
                default:
                    break;
            }
        }
    });

    items.click(function($event) {
        $event.preventDefault();
        const url = $(this).attr(HREF_ATTR) || $(this).attr(DATA_FUNCTION_ATTR);
        window.location.href = url;
    });
}

function toggleAnchorDropdown(button) {
    const dropdownContainer = $(button).next();
    dropdownContainer.parent().toggleClass('open');
    $(button).attr(ARIA_EXPANDED_ATTR, $(button).parent().hasClass('open').toString());
}

function showAnchorDropdown(button) {
    const isOpened = $(button).parent().hasClass('open');

    if (!isOpened) {
        const dropdownContainer = $(button).next();
        dropdownContainer.parent().toggleClass('open');
        $(button).attr(ARIA_EXPANDED_ATTR, (true).toString());
    }
}

function hideAnchorDropdown(button) {
    const isOpened = $(button).parent().hasClass('open');

    if (isOpened) {
        const dropdownContainer = $(button).next();
        dropdownContainer.parent().toggleClass('open');
        $(button).attr(ARIA_EXPANDED_ATTR, (false).toString());
    }
}

function hideAllAnchorDropdowns(buttons) {
    buttons.each(function (index, button) {
        hideAnchorDropdown(button);
    });
}

function getMenusAvailableForNavigation() {
    const urlLinks = $(BREADCRUMB_URL_LINK);
    const editIcons = $(EDIT_ICON_BREADCRUMBS_SELECTOR);
    const breadcrumbDropdownLinks = $(BREADCRUMB_SINGLE_URL_SELECTOR);
    const openDropdownButtons = $(ARROW_DOWN_SELECTOR);

    let items = $.merge(urlLinks, breadcrumbDropdownLinks);
    items = $.merge(items, openDropdownButtons);
    items = $.merge(items, editIcons);

    return items;
}

//endregion

//region Menu Items

function assignKeyboardNavigationEventsOnMenuItems() {
    const menulinks = $(MENU_ITEM_LINK_SELECTOR);
    const menulinksWithDropdown = $(SUBMENU_DROPDOWN_ANCHOR_SELECTOR);
    const arrowRightButtons = $(ARROW_RIGHT_SELECTOR);

    const items = $.merge(menulinks, menulinksWithDropdown);

    items.keydown(function($event) {
        if ($event.target === this) {
            switch($event.which) {
                case SPACE_CODE:
                    const url = $(this).attr(HREF_ATTR);
                    window.location.href = url;
                    break;
                case ESC_CODE:
                    closeCurrentDropdownMenu(this);
                    break;
                default:
                    break;
            }
        }
    });

    menulinksWithDropdown.keydown(function ($event) {
        if ($event.target === this) {
            switch($event.which) {
                case DOWN_CODE:
                    $event.preventDefault();
                    $event.stopPropagation();
                    selectNextSubmenuElement(this);
                    break;
                case UP_CODE:
                    $event.preventDefault();
                    $event.stopPropagation();
                    selectPrevSubmenuAnchor(this);
                    break;
                case RIGHT_CODE:
                    selectSubMenuDropdownButton(this);
                    break;
                default:
                    break;
            }
        }
    });

    arrowRightButtons.keydown(function ($event) {
        if ($event.target === this) {
            switch($event.which) {
                case LEFT_CODE:
                    selectSubMenuAnchorLink(this);
                    break;
                case RIGHT_CODE:
                case SPACE_CODE:
                case ENTER_CODE:
                    openSubmenuDropdown(this);
                    break;
                case DOWN_CODE:
                    $event.preventDefault();
                    $event.stopPropagation();
                    selectNextSubmenuElement(this);
                    break;
                case UP_CODE:
                    $event.preventDefault();
                    $event.stopPropagation();
                    selectPrevSubmenuAnchor(this);
                    break;
                case ESC_CODE:
                    closeCurrentDropdownMenu(this);
                    break;
                default:
                    break;
            }
        }
    });
}

function closeCurrentDropdownMenu(element) {
    let parentMenu = $(element).parent().parents(DROPDOWN_SUBMENU_SELECTOR);
    
    if (parentMenu && parentMenu.length) {
        hideDropdownSubmenu(parentMenu);
    } else {
        parentMenu = $(element).closest(`${DROPDOWN_SELECTOR}:not(${DROPDOWN_SUBMENU_SELECTOR})`);
        hideDropdownMenu(parentMenu)
    }
}

function hideDropdownMenu(menuElement) {
    const isOpened = $(menuElement).hasClass('open');

    if (isOpened) {
        menuElement.toggleClass('open');

        const anchor = $(BREADCRUMB_SINGLE_URL_SELECTOR, menuElement).first();
        $(anchor).attr(ARIA_EXPANDED_ATTR, (false).toString());

        const button = $(ARROW_DOWN_SELECTOR, menuElement).first();
        button.focus();
    }
}

function hideDropdownSubmenu(menuElement) {
    const isOpened = $(menuElement).hasClass('open');

    if (isOpened) {
        menuElement.toggleClass('open');
        const button = $(ARROW_RIGHT_SELECTOR, menuElement).first();
        $(button).attr(ARIA_EXPANDED_ATTR, (false).toString());
        $(button).focus();
    }
}

function selectSubMenuDropdownButton(anchor) {
    const dropdownButton = $(anchor).next();
    dropdownButton.focus();
}

function selectSubMenuAnchorLink(button) {
    const anchorLink = $(button).prev();
    anchorLink.focus();
}

function selectNextSubmenuElement(element) {
    const parentListItem = $(element).parent();
    const nextListItem = parentListItem.next();

    if ($(element).prop("tagName").toLowerCase() === ANCHOR_TAG_NAME) {
        $(ARROW_RIGHT_SELECTOR, parentListItem).first().focus();
    } else if (nextListItem && nextListItem.length) {
        $(SUBMENU_DROPDOWN_ANCHOR_SELECTOR, nextListItem).first().focus();
    }
}

function selectPrevSubmenuAnchor(element) {
    const parentListItem = $(element).parent();
    const prevListItem = parentListItem.prev();

    if ($(element).prop("tagName").toLowerCase() === BUTTON_TAG_NAME) {
        $(SUBMENU_DROPDOWN_ANCHOR_SELECTOR, parentListItem).first().focus();
    } else if (prevListItem && prevListItem.length) {
        $(ARROW_RIGHT_SELECTOR, prevListItem).focus();
    }
}

function openSubmenuDropdown(button) {
    showAnchorDropdown(button);

    $(ANCHOR_TAG_NAME, $(button).next()).first().focus();
}

//region Submenus

function assignDownKeyForSubmenus() {
    if (!$(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`).first().attr(DOWN_KEY_EVENT_ADDED_ATTR)) {
        $(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`).keydown(function(e) {
            if (e.which == DOWN_CODE && e.target == this) {
                let nextSibling = $(this).closest(LI_TAG_NAME).next();
                let nextAnchor = $(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`, nextSibling);
                nextAnchor.focus();
            }
        });

        $(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`).each(function(i, elem) {
            $(elem).attr(DOWN_KEY_EVENT_ADDED_ATTR, (true).toString());
        });
    }
}

function assignUpKeyForSubmenus() {
    if (!$(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`).first().attr(UP_KEY_EVENT_ADDED_ATTR)) {
        $(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`).keydown(function(e) {
            if (e.which == UP_CODE && e.target == this) {
                let prevSibling = $(this).closest(LI_TAG_NAME).prev();
                let prevAnchor = $(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`, prevSibling);
                prevAnchor.focus();
            }
        });

        $(`.${SUBMENU_DROPDOWN_ANCHOR_CLASS}`).each(function(i, elem) {
            $(elem).attr(UP_KEY_EVENT_ADDED_ATTR, (true).toString());
        });
    }
}

function assignLeftKeyForSubmenus() {
    if (!$(`${DROPDOWN_SUBMENU_SELECTOR} .${DROPDOWN_MENU_CLASS} ${ANCHOR_TAG_NAME}`).first().attr(LEFT_KEY_EVENT_ADDED_ATTR)) {
        $(`${DROPDOWN_SUBMENU_SELECTOR} .${DROPDOWN_MENU_CLASS} ${ANCHOR_TAG_NAME}`).keydown(function(e) {
            if (e.which == LEFT_CODE && e.target == this) {
                let container = $(this).closest(`${DROPDOWN_SUBMENU_SELECTOR}`);
                toggleSubDropdownMenu($(ARROW_RIGHT_SELECTOR, container).parent().get(0));

                setTimeout(() => {
                    let foundParentItem = $(ARROW_RIGHT_SELECTOR, container).first();
                    foundParentItem.focus();
                });
            }
        });

        $(`${DROPDOWN_SUBMENU_SELECTOR} .${DROPDOWN_MENU_CLASS} ${ANCHOR_TAG_NAME}`).each(function(i, elem) {
            $(elem).attr(LEFT_KEY_EVENT_ADDED_ATTR, (true).toString());
        });

        $(ARROW_RIGHT_SELECTOR).focus(function (e) {
            if (e.target == this) {
                hideAllAnchorDropdowns($(ARROW_RIGHT_SELECTOR));
            }
        });
    }
}

//endregion

//region init

loadActiveCoursesFromHistory();

$(() => {
    setTimeout(() => {
        assignDownKeyForSubmenus();
        assignUpKeyForSubmenus();
        assignLeftKeyForSubmenus();

        assignKeydownEventOnItem(`${DROPDOWN_SUBMENU_SELECTOR}`, 'last-stop-event-added', function(i, elem) {
            const lastItem = $(`.${DROPDOWN_MENU_CLASS} ${ANCHOR_TAG_NAME}`, elem).last();
            lastItem.keydown(function(event) {
                if (event.target == this && event.which == DOWN_CODE) {
                    preventDefaultEvent(event);
                }
            });
        });

        assignKeydownEventOnItem(`${DROPDOWN_SUBMENU_SELECTOR}`, 'first-stop-event-added', function(i, elem) {
            const firstItem = $(`.${DROPDOWN_MENU_CLASS} ${ANCHOR_TAG_NAME}`, elem).first();
            firstItem.keydown(function(event) {
                if (event.target == this && event.which == UP_CODE) {
                    preventDefaultEvent(event);
                }
            });
        });

        assignKeyboardNavigationEventsOnNavigationMenu();
        assignFocusNavigationEventsOnNavigationMenu();
        assignArrowNavigationEventsOnNavigationMenu();


        assignKeyboardNavigationEventsOnMenuItems();
        $('.breadcrumb-single-url, .breadcrumb-url-link, .menu-item-link, .submenu-dropdown-anchor').bind('mouseenter', function(){
            var $this = $(this);
            if(this.offsetWidth < this.scrollWidth && !$this.attr('title')){
                $this.attr('title', $this.text());
            }
        });
        
    }, EVENT_SET_TIMEOUT);
});

//endregion
;/*! Video.js v4.9.0 Copyright 2014 Brightcove, Inc. https://github.com/videojs/video.js/blob/master/LICENSE */ 
(function() {var b=void 0,f=!0,j=null,l=!1;function m(){return function(){}}function p(a){return function(){return this[a]}}function q(a){return function(){return a}}var s;document.createElement("video");document.createElement("audio");document.createElement("track");function t(a,c,d){if("string"===typeof a){0===a.indexOf("#")&&(a=a.slice(1));if(t.Ba[a])return t.Ba[a];a=t.v(a)}if(!a||!a.nodeName)throw new TypeError("The element or ID supplied is not valid. (videojs)");return a.player||new t.Player(a,c,d)}
var videojs=window.videojs=t;t.Ub="4.9";t.Uc="https:"==document.location.protocol?"https://":"http://";
t.options={techOrder:["html5","flash"],html5:{},flash:{},width:300,height:150,defaultVolume:0,playbackRates:[],inactivityTimeout:2E3,children:{mediaLoader:{},posterImage:{},textTrackDisplay:{},loadingSpinner:{},bigPlayButton:{},controlBar:{},errorDisplay:{}},language:document.getElementsByTagName("html")[0].getAttribute("lang")||navigator.languages&&navigator.languages[0]||navigator.we||navigator.language||"en",languages:{},notSupportedMessage:"No compatible source was found for this video."};
"GENERATED_CDN_VSN"!==t.Ub&&(videojs.options.flash.swf=t.Uc+"vjs.zencdn.net/"+t.Ub+"/video-js.swf");t.fd=function(a,c){t.options.languages[a]=t.options.languages[a]!==b?t.ga.Va(t.options.languages[a],c):c;return t.options.languages};t.Ba={};"function"===typeof define&&define.amd?define([],function(){return videojs}):"object"===typeof exports&&"object"===typeof module&&(module.exports=videojs);t.qa=t.CoreObject=m();
t.qa.extend=function(a){var c,d;a=a||{};c=a.init||a.i||this.prototype.init||this.prototype.i||m();d=function(){c.apply(this,arguments)};d.prototype=t.g.create(this.prototype);d.prototype.constructor=d;d.extend=t.qa.extend;d.create=t.qa.create;for(var e in a)a.hasOwnProperty(e)&&(d.prototype[e]=a[e]);return d};t.qa.create=function(){var a=t.g.create(this.prototype);this.apply(a,arguments);return a};
t.d=function(a,c,d){if(t.g.isArray(c))return u(t.d,a,c,d);var e=t.getData(a);e.C||(e.C={});e.C[c]||(e.C[c]=[]);d.w||(d.w=t.w++);e.C[c].push(d);e.X||(e.disabled=l,e.X=function(c){if(!e.disabled){c=t.qc(c);var d=e.C[c.type];if(d)for(var d=d.slice(0),k=0,n=d.length;k<n&&!c.yc();k++)d[k].call(a,c)}});1==e.C[c].length&&(a.addEventListener?a.addEventListener(c,e.X,l):a.attachEvent&&a.attachEvent("on"+c,e.X))};
t.o=function(a,c,d){if(t.tc(a)){var e=t.getData(a);if(e.C){if(t.g.isArray(c))return u(t.o,a,c,d);if(c){var g=e.C[c];if(g){if(d){if(d.w)for(e=0;e<g.length;e++)g[e].w===d.w&&g.splice(e--,1)}else e.C[c]=[];t.jc(a,c)}}else for(g in e.C)c=g,e.C[c]=[],t.jc(a,c)}}};t.jc=function(a,c){var d=t.getData(a);0===d.C[c].length&&(delete d.C[c],a.removeEventListener?a.removeEventListener(c,d.X,l):a.detachEvent&&a.detachEvent("on"+c,d.X));t.Hb(d.C)&&(delete d.C,delete d.X,delete d.disabled);t.Hb(d)&&t.Hc(a)};
t.qc=function(a){function c(){return f}function d(){return l}if(!a||!a.Ib){var e=a||window.event;a={};for(var g in e)"layerX"!==g&&("layerY"!==g&&"keyboardEvent.keyLocation"!==g)&&("returnValue"==g&&e.preventDefault||(a[g]=e[g]));a.target||(a.target=a.srcElement||document);a.relatedTarget=a.fromElement===a.target?a.toElement:a.fromElement;a.preventDefault=function(){e.preventDefault&&e.preventDefault();a.returnValue=l;a.zd=c;a.defaultPrevented=f};a.zd=d;a.defaultPrevented=l;a.stopPropagation=function(){e.stopPropagation&&
e.stopPropagation();a.cancelBubble=f;a.Ib=c};a.Ib=d;a.stopImmediatePropagation=function(){e.stopImmediatePropagation&&e.stopImmediatePropagation();a.yc=c;a.stopPropagation()};a.yc=d;if(a.clientX!=j){g=document.documentElement;var h=document.body;a.pageX=a.clientX+(g&&g.scrollLeft||h&&h.scrollLeft||0)-(g&&g.clientLeft||h&&h.clientLeft||0);a.pageY=a.clientY+(g&&g.scrollTop||h&&h.scrollTop||0)-(g&&g.clientTop||h&&h.clientTop||0)}a.which=a.charCode||a.keyCode;a.button!=j&&(a.button=a.button&1?0:a.button&
4?1:a.button&2?2:0)}return a};t.k=function(a,c){var d=t.tc(a)?t.getData(a):{},e=a.parentNode||a.ownerDocument;"string"===typeof c&&(c={type:c,target:a});c=t.qc(c);d.X&&d.X.call(a,c);if(e&&!c.Ib()&&c.bubbles!==l)t.k(e,c);else if(!e&&!c.defaultPrevented&&(d=t.getData(c.target),c.target[c.type])){d.disabled=f;if("function"===typeof c.target[c.type])c.target[c.type]();d.disabled=l}return!c.defaultPrevented};
t.Q=function(a,c,d){function e(){t.o(a,c,e);d.apply(this,arguments)}if(t.g.isArray(c))return u(t.Q,a,c,d);e.w=d.w=d.w||t.w++;t.d(a,c,e)};function u(a,c,d,e){t.hc.forEach(d,function(d){a(c,d,e)})}var v=Object.prototype.hasOwnProperty;t.e=function(a,c){var d;c=c||{};d=document.createElement(a||"div");t.g.Y(c,function(a,c){-1!==a.indexOf("aria-")||"role"==a?d.setAttribute(a,c):d[a]=c});return d};t.ba=function(a){return a.charAt(0).toUpperCase()+a.slice(1)};t.g={};
t.g.create=Object.create||function(a){function c(){}c.prototype=a;return new c};t.g.Y=function(a,c,d){for(var e in a)v.call(a,e)&&c.call(d||this,e,a[e])};t.g.z=function(a,c){if(!c)return a;for(var d in c)v.call(c,d)&&(a[d]=c[d]);return a};t.g.od=function(a,c){var d,e,g;a=t.g.copy(a);for(d in c)v.call(c,d)&&(e=a[d],g=c[d],a[d]=t.g.Ta(e)&&t.g.Ta(g)?t.g.od(e,g):c[d]);return a};t.g.copy=function(a){return t.g.z({},a)};
t.g.Ta=function(a){return!!a&&"object"===typeof a&&"[object Object]"===a.toString()&&a.constructor===Object};t.g.isArray=Array.isArray||function(a){return"[object Array]"===Object.prototype.toString.call(a)};t.Bd=function(a){return a!==a};t.bind=function(a,c,d){function e(){return c.apply(a,arguments)}c.w||(c.w=t.w++);e.w=d?d+"_"+c.w:c.w;return e};t.ua={};t.w=1;t.expando="vdata"+(new Date).getTime();t.getData=function(a){var c=a[t.expando];c||(c=a[t.expando]=t.w++,t.ua[c]={});return t.ua[c]};
t.tc=function(a){a=a[t.expando];return!(!a||t.Hb(t.ua[a]))};t.Hc=function(a){var c=a[t.expando];if(c){delete t.ua[c];try{delete a[t.expando]}catch(d){a.removeAttribute?a.removeAttribute(t.expando):a[t.expando]=j}}};t.Hb=function(a){for(var c in a)if(a[c]!==j)return l;return f};t.Sa=function(a,c){return-1!==(" "+a.className+" ").indexOf(" "+c+" ")};t.m=function(a,c){t.Sa(a,c)||(a.className=""===a.className?c:a.className+" "+c)};
t.p=function(a,c){var d,e;if(t.Sa(a,c)){d=a.className.split(" ");for(e=d.length-1;0<=e;e--)d[e]===c&&d.splice(e,1);a.className=d.join(" ")}};t.A=t.e("video");t.N=navigator.userAgent;t.$c=/iPhone/i.test(t.N);t.Zc=/iPad/i.test(t.N);t.ad=/iPod/i.test(t.N);t.Yc=t.$c||t.Zc||t.ad;var aa=t,x;var y=t.N.match(/OS (\d+)_/i);x=y&&y[1]?y[1]:b;aa.le=x;t.Wc=/Android/i.test(t.N);var ba=t,z;var A=t.N.match(/Android (\d+)(?:\.(\d+))?(?:\.(\d+))*/i),B,C;
A?(B=A[1]&&parseFloat(A[1]),C=A[2]&&parseFloat(A[2]),z=B&&C?parseFloat(A[1]+"."+A[2]):B?B:j):z=j;ba.Tb=z;t.bd=t.Wc&&/webkit/i.test(t.N)&&2.3>t.Tb;t.Xc=/Firefox/i.test(t.N);t.me=/Chrome/i.test(t.N);t.dc=!!("ontouchstart"in window||window.Vc&&document instanceof window.Vc);t.Jc=function(a,c){t.g.Y(c,function(c,e){e===j||"undefined"===typeof e||e===l?a.removeAttribute(c):a.setAttribute(c,e===f?"":e)})};
t.za=function(a){var c,d,e,g;c={};if(a&&a.attributes&&0<a.attributes.length){d=a.attributes;for(var h=d.length-1;0<=h;h--){e=d[h].name;g=d[h].value;if("boolean"===typeof a[e]||-1!==",autoplay,controls,loop,muted,default,".indexOf(","+e+","))g=g!==j?f:l;c[e]=g}}return c};
t.se=function(a,c){var d="";document.defaultView&&document.defaultView.getComputedStyle?d=document.defaultView.getComputedStyle(a,"").getPropertyValue(c):a.currentStyle&&(d=a["client"+c.substr(0,1).toUpperCase()+c.substr(1)]+"px");return d};t.Gb=function(a,c){c.firstChild?c.insertBefore(a,c.firstChild):c.appendChild(a)};t.Oa={};t.v=function(a){0===a.indexOf("#")&&(a=a.slice(1));return document.getElementById(a)};
t.ya=function(a,c){c=c||a;var d=Math.floor(a%60),e=Math.floor(a/60%60),g=Math.floor(a/3600),h=Math.floor(c/60%60),k=Math.floor(c/3600);if(isNaN(a)||Infinity===a)g=e=d="-";g=0<g||0<k?g+":":"";return g+(((g||10<=h)&&10>e?"0"+e:e)+":")+(10>d?"0"+d:d)};t.hd=function(){document.body.focus();document.onselectstart=q(l)};t.he=function(){document.onselectstart=q(f)};t.trim=function(a){return(a+"").replace(/^\s+|\s+$/g,"")};t.round=function(a,c){c||(c=0);return Math.round(a*Math.pow(10,c))/Math.pow(10,c)};
t.zb=function(a,c){return{length:1,start:function(){return a},end:function(){return c}}};
t.get=function(a,c,d,e){var g,h,k,n;d=d||m();"undefined"===typeof XMLHttpRequest&&(window.XMLHttpRequest=function(){try{return new window.ActiveXObject("Msxml2.XMLHTTP.6.0")}catch(a){}try{return new window.ActiveXObject("Msxml2.XMLHTTP.3.0")}catch(c){}try{return new window.ActiveXObject("Msxml2.XMLHTTP")}catch(d){}throw Error("This browser does not support XMLHttpRequest.");});h=new XMLHttpRequest;k=t.Ud(a);n=window.location;k.protocol+k.host!==n.protocol+n.host&&window.XDomainRequest&&!("withCredentials"in
h)?(h=new window.XDomainRequest,h.onload=function(){c(h.responseText)},h.onerror=d,h.onprogress=m(),h.ontimeout=d):(g="file:"==k.protocol||"file:"==n.protocol,h.onreadystatechange=function(){4===h.readyState&&(200===h.status||g&&0===h.status?c(h.responseText):d(h.responseText))});try{h.open("GET",a,f),e&&(h.withCredentials=f)}catch(r){d(r);return}try{h.send()}catch(w){d(w)}};
t.Yd=function(a){try{var c=window.localStorage||l;c&&(c.volume=a)}catch(d){22==d.code||1014==d.code?t.log("LocalStorage Full (VideoJS)",d):18==d.code?t.log("LocalStorage not allowed (VideoJS)",d):t.log("LocalStorage Error (VideoJS)",d)}};t.sc=function(a){a.match(/^https?:\/\//)||(a=t.e("div",{innerHTML:'<a href="'+a+'">x</a>'}).firstChild.href);return a};
t.Ud=function(a){var c,d,e,g;g="protocol hostname port pathname search hash host".split(" ");d=t.e("a",{href:a});if(e=""===d.host&&"file:"!==d.protocol)c=t.e("div"),c.innerHTML='<a href="'+a+'"></a>',d=c.firstChild,c.setAttribute("style","display:none; position:absolute;"),document.body.appendChild(c);a={};for(var h=0;h<g.length;h++)a[g[h]]=d[g[h]];e&&document.body.removeChild(c);return a};
function D(a,c){var d,e;d=Array.prototype.slice.call(c);e=m();e=window.console||{log:e,warn:e,error:e};a?d.unshift(a.toUpperCase()+":"):a="log";t.log.history.push(d);d.unshift("VIDEOJS:");if(e[a].apply)e[a].apply(e,d);else e[a](d.join(" "))}t.log=function(){D(j,arguments)};t.log.history=[];t.log.error=function(){D("error",arguments)};t.log.warn=function(){D("warn",arguments)};
t.vd=function(a){var c,d;a.getBoundingClientRect&&a.parentNode&&(c=a.getBoundingClientRect());if(!c)return{left:0,top:0};a=document.documentElement;d=document.body;return{left:t.round(c.left+(window.pageXOffset||d.scrollLeft)-(a.clientLeft||d.clientLeft||0)),top:t.round(c.top+(window.pageYOffset||d.scrollTop)-(a.clientTop||d.clientTop||0))}};t.hc={};t.hc.forEach=function(a,c,d){if(t.g.isArray(a)&&c instanceof Function)for(var e=0,g=a.length;e<g;++e)c.call(d||t,a[e],e,a);return a};t.ga={};
t.ga.Va=function(a,c){var d,e,g;a=t.g.copy(a);for(d in c)c.hasOwnProperty(d)&&(e=a[d],g=c[d],a[d]=t.g.Ta(e)&&t.g.Ta(g)?t.ga.Va(e,g):c[d]);return a};
t.a=t.qa.extend({i:function(a,c,d){this.c=a;this.l=t.g.copy(this.l);c=this.options(c);this.U=c.id||(c.el&&c.el.id?c.el.id:a.id()+"_component_"+t.w++);this.Hd=c.name||j;this.b=c.el||this.e();this.O=[];this.Pa={};this.Qa={};this.vc();this.J(d);if(c.Ic!==l){var e,g;e=t.bind(this.j(),this.j().reportUserActivity);this.d("touchstart",function(){e();clearInterval(g);g=setInterval(e,250)});a=function(){e();clearInterval(g)};this.d("touchmove",e);this.d("touchend",a);this.d("touchcancel",a)}}});s=t.a.prototype;
s.dispose=function(){this.k({type:"dispose",bubbles:l});if(this.O)for(var a=this.O.length-1;0<=a;a--)this.O[a].dispose&&this.O[a].dispose();this.Qa=this.Pa=this.O=j;this.o();this.b.parentNode&&this.b.parentNode.removeChild(this.b);t.Hc(this.b);this.b=j};s.c=f;s.j=p("c");s.options=function(a){return a===b?this.l:this.l=t.ga.Va(this.l,a)};s.e=function(a,c){return t.e(a,c)};s.s=function(a){var c=this.c.language(),d=this.c.languages();return d&&d[c]&&d[c][a]?d[c][a]:a};s.v=p("b");
s.ja=function(){return this.u||this.b};s.id=p("U");s.name=p("Hd");s.children=p("O");s.xd=function(a){return this.Pa[a]};s.ka=function(a){return this.Qa[a]};s.S=function(a,c){var d,e;"string"===typeof a?(e=a,c=c||{},d=c.componentClass||t.ba(e),c.name=e,d=new window.videojs[d](this.c||this,c)):d=a;this.O.push(d);"function"===typeof d.id&&(this.Pa[d.id()]=d);(e=e||d.name&&d.name())&&(this.Qa[e]=d);"function"===typeof d.el&&d.el()&&this.ja().appendChild(d.el());return d};
s.removeChild=function(a){"string"===typeof a&&(a=this.ka(a));if(a&&this.O){for(var c=l,d=this.O.length-1;0<=d;d--)if(this.O[d]===a){c=f;this.O.splice(d,1);break}c&&(this.Pa[a.id]=j,this.Qa[a.name]=j,(c=a.v())&&c.parentNode===this.ja()&&this.ja().removeChild(a.v()))}};s.vc=function(){var a,c,d,e;a=this;if(c=this.options().children)if(t.g.isArray(c))for(var g=0;g<c.length;g++)d=c[g],"string"==typeof d?(e=d,d={}):e=d.name,a[e]=a.S(e,d);else t.g.Y(c,function(c,d){d!==l&&(a[c]=a.S(c,d))})};s.T=q("");
s.d=function(a,c){t.d(this.b,a,t.bind(this,c));return this};s.o=function(a,c){t.o(this.b,a,c);return this};s.Q=function(a,c){t.Q(this.b,a,t.bind(this,c));return this};s.k=function(a){t.k(this.b,a);return this};s.J=function(a){a&&(this.la?a.call(this):(this.ab===b&&(this.ab=[]),this.ab.push(a)));return this};s.Fa=function(){this.la=f;var a=this.ab;if(a&&0<a.length){for(var c=0,d=a.length;c<d;c++)a[c].call(this);this.ab=[];this.k("ready")}};s.Sa=function(a){return t.Sa(this.b,a)};
s.m=function(a){t.m(this.b,a);return this};s.p=function(a){t.p(this.b,a);return this};s.show=function(){this.b.style.display="block";return this};s.W=function(){this.b.style.display="none";return this};function E(a){a.p("vjs-lock-showing")}s.disable=function(){this.W();this.show=m()};s.width=function(a,c){return F(this,"width",a,c)};s.height=function(a,c){return F(this,"height",a,c)};s.rd=function(a,c){return this.width(a,f).height(c)};
function F(a,c,d,e){if(d!==b){if(d===j||t.Bd(d))d=0;a.b.style[c]=-1!==(""+d).indexOf("%")||-1!==(""+d).indexOf("px")?d:"auto"===d?"":d+"px";e||a.k("resize");return a}if(!a.b)return 0;d=a.b.style[c];e=d.indexOf("px");return-1!==e?parseInt(d.slice(0,e),10):parseInt(a.b["offset"+t.ba(c)],10)}
function G(a){var c,d,e,g,h,k,n,r;c=0;d=j;a.d("touchstart",function(a){1===a.touches.length&&(d=a.touches[0],c=(new Date).getTime(),g=f)});a.d("touchmove",function(a){1<a.touches.length?g=l:d&&(k=a.touches[0].pageX-d.pageX,n=a.touches[0].pageY-d.pageY,r=Math.sqrt(k*k+n*n),22<r&&(g=l))});h=function(){g=l};a.d("touchleave",h);a.d("touchcancel",h);a.d("touchend",function(a){d=j;g===f&&(e=(new Date).getTime()-c,250>e&&(a.preventDefault(),this.k("tap")))})}
t.t=t.a.extend({i:function(a,c){t.a.call(this,a,c);G(this);this.d("tap",this.r);this.d("click",this.r);this.d("focus",this.Ya);this.d("blur",this.Xa)}});s=t.t.prototype;
s.e=function(a,c){var d;c=t.g.z({className:this.T(),role:"button","aria-live":"polite",tabIndex:0},c);d=t.a.prototype.e.call(this,a,c);c.innerHTML||(this.u=t.e("div",{className:"vjs-control-content"}),this.xb=t.e("span",{className:"vjs-control-text",innerHTML:this.s(this.ta)||"Need Text"}),this.u.appendChild(this.xb),d.appendChild(this.u));return d};s.T=function(){return"vjs-control "+t.a.prototype.T.call(this)};s.r=m();s.Ya=function(){t.d(document,"keydown",t.bind(this,this.Z))};
s.Z=function(a){if(32==a.which||13==a.which)a.preventDefault(),this.r()};s.Xa=function(){t.o(document,"keydown",t.bind(this,this.Z))};
t.R=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.gd=this.ka(this.l.barName);this.handle=this.ka(this.l.handleName);this.d("mousedown",this.Za);this.d("touchstart",this.Za);this.d("focus",this.Ya);this.d("blur",this.Xa);this.d("click",this.r);this.c.d("controlsvisible",t.bind(this,this.update));a.d(this.Dc,t.bind(this,this.update));this.F={};this.F.move=t.bind(this,this.$a);this.F.end=t.bind(this,this.Lb)}});s=t.R.prototype;
s.dispose=function(){t.o(document,"mousemove",this.F.move,l);t.o(document,"mouseup",this.F.end,l);t.o(document,"touchmove",this.F.move,l);t.o(document,"touchend",this.F.end,l);t.o(document,"keyup",t.bind(this,this.Z));t.a.prototype.dispose.call(this)};s.e=function(a,c){c=c||{};c.className+=" vjs-slider";c=t.g.z({role:"slider","aria-valuenow":0,"aria-valuemin":0,"aria-valuemax":100,tabIndex:0},c);return t.a.prototype.e.call(this,a,c)};
s.Za=function(a){a.preventDefault();t.hd();this.m("vjs-sliding");t.d(document,"mousemove",this.F.move);t.d(document,"mouseup",this.F.end);t.d(document,"touchmove",this.F.move);t.d(document,"touchend",this.F.end);this.$a(a)};s.$a=m();s.Lb=function(){t.he();this.p("vjs-sliding");t.o(document,"mousemove",this.F.move,l);t.o(document,"mouseup",this.F.end,l);t.o(document,"touchmove",this.F.move,l);t.o(document,"touchend",this.F.end,l);this.update()};
s.update=function(){if(this.b){var a,c=this.Fb(),d=this.handle,e=this.gd;isNaN(c)&&(c=0);a=c;if(d){a=this.b.offsetWidth;var g=d.v().offsetWidth;a=g?g/a:0;c*=1-a;a=c+a/2;d.v().style.left=t.round(100*c,2)+"%"}e&&(e.v().style.width=t.round(100*a,2)+"%")}};
function H(a,c){var d,e,g,h;d=a.b;e=t.vd(d);h=g=d.offsetWidth;d=a.handle;if(a.options().vertical)return h=e.top,e=c.changedTouches?c.changedTouches[0].pageY:c.pageY,d&&(d=d.v().offsetHeight,h+=d/2,g-=d),Math.max(0,Math.min(1,(h-e+g)/g));g=e.left;e=c.changedTouches?c.changedTouches[0].pageX:c.pageX;d&&(d=d.v().offsetWidth,g+=d/2,h-=d);return Math.max(0,Math.min(1,(e-g)/h))}s.Ya=function(){t.d(document,"keyup",t.bind(this,this.Z))};
s.Z=function(a){if(37==a.which||40==a.which)a.preventDefault(),this.Mc();else if(38==a.which||39==a.which)a.preventDefault(),this.Nc()};s.Xa=function(){t.o(document,"keyup",t.bind(this,this.Z))};s.r=function(a){a.stopImmediatePropagation();a.preventDefault()};t.$=t.a.extend();t.$.prototype.defaultValue=0;
t.$.prototype.e=function(a,c){c=c||{};c.className+=" vjs-slider-handle";c=t.g.z({innerHTML:'<span class="vjs-control-text">'+this.defaultValue+"</span>"},c);return t.a.prototype.e.call(this,"div",c)};t.ha=t.a.extend();function ca(a,c){a.S(c);c.d("click",t.bind(a,function(){E(this)}))}
t.ha.prototype.e=function(){var a=this.options().kc||"ul";this.u=t.e(a,{className:"vjs-menu-content"});a=t.a.prototype.e.call(this,"div",{append:this.u,className:"vjs-menu"});a.appendChild(this.u);t.d(a,"click",function(a){a.preventDefault();a.stopImmediatePropagation()});return a};t.I=t.t.extend({i:function(a,c){t.t.call(this,a,c);this.selected(c.selected)}});t.I.prototype.e=function(a,c){return t.t.prototype.e.call(this,"li",t.g.z({className:"vjs-menu-item",innerHTML:this.l.label},c))};
t.I.prototype.r=function(){this.selected(f)};t.I.prototype.selected=function(a){a?(this.m("vjs-selected"),this.b.setAttribute("aria-selected",f)):(this.p("vjs-selected"),this.b.setAttribute("aria-selected",l))};t.M=t.t.extend({i:function(a,c){t.t.call(this,a,c);this.Aa=this.wa();this.S(this.Aa);this.P&&0===this.P.length&&this.W();this.d("keyup",this.Z);this.b.setAttribute("aria-haspopup",f);this.b.setAttribute("role","button")}});s=t.M.prototype;s.sa=l;
s.wa=function(){var a=new t.ha(this.c);this.options().title&&a.ja().appendChild(t.e("li",{className:"vjs-menu-title",innerHTML:t.ba(this.options().title),ee:-1}));if(this.P=this.createItems())for(var c=0;c<this.P.length;c++)ca(a,this.P[c]);return a};s.va=m();s.T=function(){return this.className+" vjs-menu-button "+t.t.prototype.T.call(this)};s.Ya=m();s.Xa=m();s.r=function(){this.Q("mouseout",t.bind(this,function(){E(this.Aa);this.b.blur()}));this.sa?I(this):J(this)};
s.Z=function(a){a.preventDefault();32==a.which||13==a.which?this.sa?I(this):J(this):27==a.which&&this.sa&&I(this)};function J(a){a.sa=f;a.Aa.m("vjs-lock-showing");a.b.setAttribute("aria-pressed",f);a.P&&0<a.P.length&&a.P[0].v().focus()}function I(a){a.sa=l;E(a.Aa);a.b.setAttribute("aria-pressed",l)}t.D=function(a){"number"===typeof a?this.code=a:"string"===typeof a?this.message=a:"object"===typeof a&&t.g.z(this,a);this.message||(this.message=t.D.pd[this.code]||"")};t.D.prototype.code=0;
t.D.prototype.message="";t.D.prototype.status=j;t.D.Ra="MEDIA_ERR_CUSTOM MEDIA_ERR_ABORTED MEDIA_ERR_NETWORK MEDIA_ERR_DECODE MEDIA_ERR_SRC_NOT_SUPPORTED MEDIA_ERR_ENCRYPTED".split(" ");
t.D.pd={1:"You aborted the video playback",2:"A network error caused the video download to fail part-way.",3:"The video playback was aborted due to a corruption problem or because the video used features your browser did not support.",4:"The video could not be loaded, either because the server or network failed or because the format is not supported.",5:"The video is encrypted and we do not have the keys to decrypt it."};for(var K=0;K<t.D.Ra.length;K++)t.D[t.D.Ra[K]]=K,t.D.prototype[t.D.Ra[K]]=K;
var L,M,N,O;
L=["requestFullscreen exitFullscreen fullscreenElement fullscreenEnabled fullscreenchange fullscreenerror".split(" "),"webkitRequestFullscreen webkitExitFullscreen webkitFullscreenElement webkitFullscreenEnabled webkitfullscreenchange webkitfullscreenerror".split(" "),"webkitRequestFullScreen webkitCancelFullScreen webkitCurrentFullScreenElement webkitCancelFullScreen webkitfullscreenchange webkitfullscreenerror".split(" "),"mozRequestFullScreen mozCancelFullScreen mozFullScreenElement mozFullScreenEnabled mozfullscreenchange mozfullscreenerror".split(" "),"msRequestFullscreen msExitFullscreen msFullscreenElement msFullscreenEnabled MSFullscreenChange MSFullscreenError".split(" ")];
M=L[0];for(O=0;O<L.length;O++)if(L[O][1]in document){N=L[O];break}if(N){t.Oa.Eb={};for(O=0;O<N.length;O++)t.Oa.Eb[M[O]]=N[O]}
t.Player=t.a.extend({i:function(a,c,d){this.L=a;a.id=a.id||"vjs_video_"+t.w++;this.fe=a&&t.za(a);c=t.g.z(da(a),c);this.Ua=c.language||t.options.language;this.Fd=c.languages||t.options.languages;this.G={};this.Ec=c.poster;this.yb=c.controls;a.controls=l;c.Ic=l;P(this,"audio"===this.L.nodeName.toLowerCase());t.a.call(this,this,c,d);this.controls()?this.m("vjs-controls-enabled"):this.m("vjs-controls-disabled");P(this)&&this.m("vjs-audio");t.Ba[this.U]=this;c.plugins&&t.g.Y(c.plugins,function(a,c){this[a](c)},
this);var e,g,h,k,n,r;e=t.bind(this,this.reportUserActivity);this.d("mousedown",function(){e();clearInterval(g);g=setInterval(e,250)});this.d("mousemove",function(a){if(a.screenX!=n||a.screenY!=r)n=a.screenX,r=a.screenY,e()});this.d("mouseup",function(){e();clearInterval(g)});this.d("keydown",e);this.d("keyup",e);h=setInterval(t.bind(this,function(){if(this.pa){this.pa=l;this.userActive(f);clearTimeout(k);var a=this.options().inactivityTimeout;0<a&&(k=setTimeout(t.bind(this,function(){this.pa||this.userActive(l)}),
a))}}),250);this.d("dispose",function(){clearInterval(h);clearTimeout(k)})}});s=t.Player.prototype;s.language=function(a){if(a===b)return this.Ua;this.Ua=a;return this};s.languages=p("Fd");s.l=t.options;s.dispose=function(){this.k("dispose");this.o("dispose");t.Ba[this.U]=j;this.L&&this.L.player&&(this.L.player=j);this.b&&this.b.player&&(this.b.player=j);this.n&&this.n.dispose();t.a.prototype.dispose.call(this)};
function da(a){var c,d,e={sources:[],tracks:[]};c=t.za(a);d=c["data-setup"];d!==j&&t.g.z(c,t.JSON.parse(d||"{}"));t.g.z(e,c);if(a.hasChildNodes()){var g,h;a=a.childNodes;g=0;for(h=a.length;g<h;g++)c=a[g],d=c.nodeName.toLowerCase(),"source"===d?e.sources.push(t.za(c)):"track"===d&&e.tracks.push(t.za(c))}return e}
s.e=function(){var a=this.b=t.a.prototype.e.call(this,"div"),c=this.L,d;c.removeAttribute("width");c.removeAttribute("height");if(c.hasChildNodes()){var e,g,h,k,n;e=c.childNodes;g=e.length;for(n=[];g--;)h=e[g],k=h.nodeName.toLowerCase(),"track"===k&&n.push(h);for(e=0;e<n.length;e++)c.removeChild(n[e])}d=t.za(c);t.g.Y(d,function(c){"class"==c?a.className=d[c]:a.setAttribute(c,d[c])});c.id+="_html5_api";c.className="vjs-tech";c.player=a.player=this;this.m("vjs-paused");this.width(this.l.width,f);this.height(this.l.height,
f);c.parentNode&&c.parentNode.insertBefore(a,c);t.Gb(c,a);this.b=a;this.d("loadstart",this.Md);this.d("waiting",this.Sd);this.d(["canplay","canplaythrough","playing","ended"],this.Rd);this.d("seeking",this.Pd);this.d("seeked",this.Od);this.d("ended",this.Id);this.d("play",this.Nb);this.d("firstplay",this.Kd);this.d("pause",this.Mb);this.d("progress",this.Nd);this.d("durationchange",this.Bc);this.d("fullscreenchange",this.Ld);return a};
function Q(a,c,d){a.n&&(a.la=l,a.n.dispose(),a.n=l);"Html5"!==c&&a.L&&(t.h.Bb(a.L),a.L=j);a.eb=c;a.la=l;var e=t.g.z({source:d,parentEl:a.b},a.l[c.toLowerCase()]);d&&(a.mc=d.type,d.src==a.G.src&&0<a.G.currentTime&&(e.startTime=a.G.currentTime),a.G.src=d.src);a.n=new window.videojs[c](a,e);a.n.J(function(){this.c.Fa()})}s.Md=function(){this.error(j);this.paused()?(R(this,l),this.Q("play",function(){R(this,f)})):this.k("firstplay")};s.uc=l;
function R(a,c){c!==b&&a.uc!==c&&((a.uc=c)?(a.m("vjs-has-started"),a.k("firstplay")):a.p("vjs-has-started"))}s.Nb=function(){this.p("vjs-paused");this.m("vjs-playing")};s.Sd=function(){this.m("vjs-waiting")};s.Rd=function(){this.p("vjs-waiting")};s.Pd=function(){this.m("vjs-seeking")};s.Od=function(){this.p("vjs-seeking")};s.Kd=function(){this.l.starttime&&this.currentTime(this.l.starttime);this.m("vjs-has-started")};s.Mb=function(){this.p("vjs-playing");this.m("vjs-paused")};
s.Nd=function(){1==this.bufferedPercent()&&this.k("loadedalldata")};s.Id=function(){this.l.loop?(this.currentTime(0),this.play()):this.paused()||this.pause()};s.Bc=function(){var a=S(this,"duration");a&&(0>a&&(a=Infinity),this.duration(a),Infinity===a?this.m("vjs-live"):this.p("vjs-live"))};s.Ld=function(){this.isFullscreen()?this.m("vjs-fullscreen"):this.p("vjs-fullscreen")};function T(a,c,d){if(a.n&&!a.n.la)a.n.J(function(){this[c](d)});else try{a.n[c](d)}catch(e){throw t.log(e),e;}}
function S(a,c){if(a.n&&a.n.la)try{return a.n[c]()}catch(d){throw a.n[c]===b?t.log("Video.js: "+c+" method not defined for "+a.eb+" playback technology.",d):"TypeError"==d.name?(t.log("Video.js: "+c+" unavailable on "+a.eb+" playback technology element.",d),a.n.la=l):t.log(d),d;}}s.play=function(){T(this,"play");return this};s.pause=function(){T(this,"pause");return this};s.paused=function(){return S(this,"paused")===l?l:f};
s.currentTime=function(a){return a!==b?(T(this,"setCurrentTime",a),this):this.G.currentTime=S(this,"currentTime")||0};s.duration=function(a){if(a!==b)return this.G.duration=parseFloat(a),this;this.G.duration===b&&this.Bc();return this.G.duration||0};s.remainingTime=function(){return this.duration()-this.currentTime()};s.buffered=function(){var a=S(this,"buffered");if(!a||!a.length)a=t.zb(0,0);return a};
s.bufferedPercent=function(){var a=this.duration(),c=this.buffered(),d=0,e,g;if(!a)return 0;for(var h=0;h<c.length;h++)e=c.start(h),g=c.end(h),g>a&&(g=a),d+=g-e;return d/a};s.volume=function(a){if(a!==b)return a=Math.max(0,Math.min(1,parseFloat(a))),this.G.volume=a,T(this,"setVolume",a),t.Yd(a),this;a=parseFloat(S(this,"volume"));return isNaN(a)?1:a};s.muted=function(a){return a!==b?(T(this,"setMuted",a),this):S(this,"muted")||l};s.Da=function(){return S(this,"supportsFullScreen")||l};s.xc=l;
s.isFullscreen=function(a){return a!==b?(this.xc=!!a,this):this.xc};s.isFullScreen=function(a){t.log.warn('player.isFullScreen() has been deprecated, use player.isFullscreen() with a lowercase "s")');return this.isFullscreen(a)};
s.requestFullscreen=function(){var a=t.Oa.Eb;this.isFullscreen(f);a?(t.d(document,a.fullscreenchange,t.bind(this,function(c){this.isFullscreen(document[a.fullscreenElement]);this.isFullscreen()===l&&t.o(document,a.fullscreenchange,arguments.callee);this.k("fullscreenchange")})),this.b[a.requestFullscreen]()):this.n.Da()?T(this,"enterFullScreen"):(this.pc(),this.k("fullscreenchange"));return this};
s.requestFullScreen=function(){t.log.warn('player.requestFullScreen() has been deprecated, use player.requestFullscreen() with a lowercase "s")');return this.requestFullscreen()};s.exitFullscreen=function(){var a=t.Oa.Eb;this.isFullscreen(l);if(a)document[a.exitFullscreen]();else this.n.Da()?T(this,"exitFullScreen"):(this.Cb(),this.k("fullscreenchange"));return this};s.cancelFullScreen=function(){t.log.warn("player.cancelFullScreen() has been deprecated, use player.exitFullscreen()");return this.exitFullscreen()};
s.pc=function(){this.Ad=f;this.sd=document.documentElement.style.overflow;t.d(document,"keydown",t.bind(this,this.rc));document.documentElement.style.overflow="hidden";t.m(document.body,"vjs-full-window");this.k("enterFullWindow")};s.rc=function(a){27===a.keyCode&&(this.isFullscreen()===f?this.exitFullscreen():this.Cb())};s.Cb=function(){this.Ad=l;t.o(document,"keydown",this.rc);document.documentElement.style.overflow=this.sd;t.p(document.body,"vjs-full-window");this.k("exitFullWindow")};
s.selectSource=function(a){for(var c=0,d=this.l.techOrder;c<d.length;c++){var e=t.ba(d[c]),g=window.videojs[e];if(g){if(g.isSupported())for(var h=0,k=a;h<k.length;h++){var n=k[h];if(g.canPlaySource(n))return{source:n,n:e}}}else t.log.error('The "'+e+'" tech is undefined. Skipped browser support check for that tech.')}return l};
s.src=function(a){if(a===b)return S(this,"src");t.g.isArray(a)?U(this,a):"string"===typeof a?this.src({src:a}):a instanceof Object&&(a.type&&!window.videojs[this.eb].canPlaySource(a)?U(this,[a]):(this.G.src=a.src,this.mc=a.type||"",this.J(function(){T(this,"src",a.src);"auto"==this.l.preload&&this.load();this.l.autoplay&&this.play()})));return this};
function U(a,c){var d=a.selectSource(c),e;d?d.n===a.eb?a.src(d.source):Q(a,d.n,d.source):(e=setTimeout(t.bind(a,function(){this.error({code:4,message:this.s(this.options().notSupportedMessage)})}),0),a.Fa(),a.d("dispose",function(){clearTimeout(e)}))}s.load=function(){T(this,"load");return this};s.currentSrc=function(){return S(this,"currentSrc")||this.G.src||""};s.nd=function(){return this.mc||""};s.Ca=function(a){return a!==b?(T(this,"setPreload",a),this.l.preload=a,this):S(this,"preload")};
s.autoplay=function(a){return a!==b?(T(this,"setAutoplay",a),this.l.autoplay=a,this):S(this,"autoplay")};s.loop=function(a){return a!==b?(T(this,"setLoop",a),this.l.loop=a,this):S(this,"loop")};s.poster=function(a){if(a===b)return this.Ec;this.Ec=a;T(this,"setPoster",a);this.k("posterchange");return this};
s.controls=function(a){return a!==b?(a=!!a,this.yb!==a&&((this.yb=a)?(this.p("vjs-controls-disabled"),this.m("vjs-controls-enabled"),this.k("controlsenabled")):(this.p("vjs-controls-enabled"),this.m("vjs-controls-disabled"),this.k("controlsdisabled"))),this):this.yb};t.Player.prototype.Sb;s=t.Player.prototype;
s.usingNativeControls=function(a){return a!==b?(a=!!a,this.Sb!==a&&((this.Sb=a)?(this.m("vjs-using-native-controls"),this.k("usingnativecontrols")):(this.p("vjs-using-native-controls"),this.k("usingcustomcontrols"))),this):this.Sb};s.da=j;s.error=function(a){if(a===b)return this.da;if(a===j)return this.da=a,this.p("vjs-error"),this;this.da=a instanceof t.D?a:new t.D(a);this.k("error");this.m("vjs-error");t.log.error("(CODE:"+this.da.code+" "+t.D.Ra[this.da.code]+")",this.da.message,this.da);return this};
s.ended=function(){return S(this,"ended")};s.seeking=function(){return S(this,"seeking")};s.pa=f;s.reportUserActivity=function(){this.pa=f};s.Rb=f;s.userActive=function(a){return a!==b?(a=!!a,a!==this.Rb&&((this.Rb=a)?(this.pa=f,this.p("vjs-user-inactive"),this.m("vjs-user-active"),this.k("useractive")):(this.pa=l,this.n&&this.n.Q("mousemove",function(a){a.stopPropagation();a.preventDefault()}),this.p("vjs-user-active"),this.m("vjs-user-inactive"),this.k("userinactive"))),this):this.Rb};
s.playbackRate=function(a){return a!==b?(T(this,"setPlaybackRate",a),this):this.n&&this.n.featuresPlaybackRate?S(this,"playbackRate"):1};s.wc=l;function P(a,c){return c!==b?(a.wc=!!c,a):a.wc}t.Ia=t.a.extend();t.Ia.prototype.l={te:"play",children:{playToggle:{},currentTimeDisplay:{},timeDivider:{},durationDisplay:{},remainingTimeDisplay:{},liveDisplay:{},progressControl:{},fullscreenToggle:{},volumeControl:{},muteToggle:{},playbackRateMenuButton:{}}};t.Ia.prototype.e=function(){return t.e("div",{className:"vjs-control-bar"})};
t.Xb=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.Xb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-live-controls vjs-control"});this.u=t.e("div",{className:"vjs-live-display",innerHTML:'<span class="vjs-control-text">'+this.s("Stream Type")+"</span>"+this.s("LIVE"),"aria-live":"off"});a.appendChild(this.u);return a};t.$b=t.t.extend({i:function(a,c){t.t.call(this,a,c);a.d("play",t.bind(this,this.Nb));a.d("pause",t.bind(this,this.Mb))}});s=t.$b.prototype;s.ta="Play";
s.T=function(){return"vjs-play-control "+t.t.prototype.T.call(this)};s.r=function(){this.c.paused()?this.c.play():this.c.pause()};s.Nb=function(){t.p(this.b,"vjs-paused");t.m(this.b,"vjs-playing");this.b.children[0].children[0].innerHTML=this.s("Pause")};s.Mb=function(){t.p(this.b,"vjs-playing");t.m(this.b,"vjs-paused");this.b.children[0].children[0].innerHTML=this.s("Play")};t.hb=t.a.extend({i:function(a,c){t.a.call(this,a,c);a.d("timeupdate",t.bind(this,this.fa))}});
t.hb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-current-time vjs-time-controls vjs-control"});this.u=t.e("div",{className:"vjs-current-time-display",innerHTML:'<span class="vjs-control-text">Current Time </span>0:00',"aria-live":"off"});a.appendChild(this.u);return a};t.hb.prototype.fa=function(){var a=this.c.bb?this.c.G.currentTime:this.c.currentTime();this.u.innerHTML='<span class="vjs-control-text">'+this.s("Current Time")+"</span> "+t.ya(a,this.c.duration())};
t.ib=t.a.extend({i:function(a,c){t.a.call(this,a,c);a.d("timeupdate",t.bind(this,this.fa))}});t.ib.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-duration vjs-time-controls vjs-control"});this.u=t.e("div",{className:"vjs-duration-display",innerHTML:'<span class="vjs-control-text">'+this.s("Duration Time")+"</span> 0:00","aria-live":"off"});a.appendChild(this.u);return a};
t.ib.prototype.fa=function(){var a=this.c.duration();a&&(this.u.innerHTML='<span class="vjs-control-text">'+this.s("Duration Time")+"</span> "+t.ya(a))};t.fc=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.fc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-time-divider",innerHTML:"<div><span>/</span></div>"})};t.pb=t.a.extend({i:function(a,c){t.a.call(this,a,c);a.d("timeupdate",t.bind(this,this.fa))}});
t.pb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-remaining-time vjs-time-controls vjs-control"});this.u=t.e("div",{className:"vjs-remaining-time-display",innerHTML:'<span class="vjs-control-text">'+this.s("Remaining Time")+"</span> -0:00","aria-live":"off"});a.appendChild(this.u);return a};t.pb.prototype.fa=function(){this.c.duration()&&(this.u.innerHTML='<span class="vjs-control-text">'+this.s("Remaining Time")+"</span> -"+t.ya(this.c.remainingTime()))};
t.Ja=t.t.extend({i:function(a,c){t.t.call(this,a,c)}});t.Ja.prototype.ta="Fullscreen";t.Ja.prototype.T=function(){return"vjs-fullscreen-control "+t.t.prototype.T.call(this)};t.Ja.prototype.r=function(){this.c.isFullscreen()?(this.c.exitFullscreen(),this.xb.innerHTML=this.s("Fullscreen")):(this.c.requestFullscreen(),this.xb.innerHTML=this.s("Non-Fullscreen"))};t.ob=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.ob.prototype.l={children:{seekBar:{}}};
t.ob.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-progress-control vjs-control"})};t.bc=t.R.extend({i:function(a,c){t.R.call(this,a,c);a.d("timeupdate",t.bind(this,this.oa));a.J(t.bind(this,this.oa))}});s=t.bc.prototype;s.l={children:{loadProgressBar:{},playProgressBar:{},seekHandle:{}},barName:"playProgressBar",handleName:"seekHandle"};s.Dc="timeupdate";s.e=function(){return t.R.prototype.e.call(this,"div",{className:"vjs-progress-holder","aria-label":"video progress bar"})};
s.oa=function(){var a=this.c.bb?this.c.G.currentTime:this.c.currentTime();this.b.setAttribute("aria-valuenow",t.round(100*this.Fb(),2));this.b.setAttribute("aria-valuetext",t.ya(a,this.c.duration()))};s.Fb=function(){return this.c.currentTime()/this.c.duration()};s.Za=function(a){t.R.prototype.Za.call(this,a);this.c.bb=f;this.je=!this.c.paused();this.c.pause()};s.$a=function(a){a=H(this,a)*this.c.duration();a==this.c.duration()&&(a-=0.1);this.c.currentTime(a)};
s.Lb=function(a){t.R.prototype.Lb.call(this,a);this.c.bb=l;this.je&&this.c.play()};s.Nc=function(){this.c.currentTime(this.c.currentTime()+5)};s.Mc=function(){this.c.currentTime(this.c.currentTime()-5)};t.lb=t.a.extend({i:function(a,c){t.a.call(this,a,c);a.d("progress",t.bind(this,this.update))}});t.lb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-load-progress",innerHTML:'<span class="vjs-control-text"><span>'+this.s("Loaded")+"</span>: 0%</span>"})};
t.lb.prototype.update=function(){var a,c,d,e,g=this.c.buffered();a=this.c.duration();var h,k=this.c;h=k.buffered();k=k.duration();h=h.end(h.length-1);h>k&&(h=k);k=this.b.children;this.b.style.width=100*(h/a||0)+"%";for(a=0;a<g.length;a++)c=g.start(a),d=g.end(a),(e=k[a])||(e=this.b.appendChild(t.e())),e.style.left=100*(c/h||0)+"%",e.style.width=100*((d-c)/h||0)+"%";for(a=k.length;a>g.length;a--)this.b.removeChild(k[a-1])};t.Zb=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});
t.Zb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-play-progress",innerHTML:'<span class="vjs-control-text"><span>'+this.s("Progress")+"</span>: 0%</span>"})};t.La=t.$.extend({i:function(a,c){t.$.call(this,a,c);a.d("timeupdate",t.bind(this,this.fa))}});t.La.prototype.defaultValue="00:00";t.La.prototype.e=function(){return t.$.prototype.e.call(this,"div",{className:"vjs-seek-handle","aria-live":"off"})};
t.La.prototype.fa=function(){var a=this.c.bb?this.c.G.currentTime:this.c.currentTime();this.b.innerHTML='<span class="vjs-control-text">'+t.ya(a,this.c.duration())+"</span>"};t.rb=t.a.extend({i:function(a,c){t.a.call(this,a,c);a.n&&a.n.featuresVolumeControl===l&&this.m("vjs-hidden");a.d("loadstart",t.bind(this,function(){a.n.featuresVolumeControl===l?this.m("vjs-hidden"):this.p("vjs-hidden")}))}});t.rb.prototype.l={children:{volumeBar:{}}};
t.rb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-volume-control vjs-control"})};t.qb=t.R.extend({i:function(a,c){t.R.call(this,a,c);a.d("volumechange",t.bind(this,this.oa));a.J(t.bind(this,this.oa))}});s=t.qb.prototype;s.oa=function(){this.b.setAttribute("aria-valuenow",t.round(100*this.c.volume(),2));this.b.setAttribute("aria-valuetext",t.round(100*this.c.volume(),2)+"%")};s.l={children:{volumeLevel:{},volumeHandle:{}},barName:"volumeLevel",handleName:"volumeHandle"};
s.Dc="volumechange";s.e=function(){return t.R.prototype.e.call(this,"div",{className:"vjs-volume-bar","aria-label":"volume level"})};s.$a=function(a){this.c.muted()&&this.c.muted(l);this.c.volume(H(this,a))};s.Fb=function(){return this.c.muted()?0:this.c.volume()};s.Nc=function(){this.c.volume(this.c.volume()+0.1)};s.Mc=function(){this.c.volume(this.c.volume()-0.1)};t.gc=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});
t.gc.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-volume-level",innerHTML:'<span class="vjs-control-text"></span>'})};t.sb=t.$.extend();t.sb.prototype.defaultValue="00:00";t.sb.prototype.e=function(){return t.$.prototype.e.call(this,"div",{className:"vjs-volume-handle"})};
t.ia=t.t.extend({i:function(a,c){t.t.call(this,a,c);a.d("volumechange",t.bind(this,this.update));a.n&&a.n.featuresVolumeControl===l&&this.m("vjs-hidden");a.d("loadstart",t.bind(this,function(){a.n.featuresVolumeControl===l?this.m("vjs-hidden"):this.p("vjs-hidden")}))}});t.ia.prototype.e=function(){return t.t.prototype.e.call(this,"div",{className:"vjs-mute-control vjs-control",innerHTML:'<div><span class="vjs-control-text">'+this.s("Mute")+"</span></div>"})};
t.ia.prototype.r=function(){this.c.muted(this.c.muted()?l:f)};t.ia.prototype.update=function(){var a=this.c.volume(),c=3;0===a||this.c.muted()?c=0:0.33>a?c=1:0.67>a&&(c=2);this.c.muted()?this.b.children[0].children[0].innerHTML!=this.s("Unmute")&&(this.b.children[0].children[0].innerHTML=this.s("Unmute")):this.b.children[0].children[0].innerHTML!=this.s("Mute")&&(this.b.children[0].children[0].innerHTML=this.s("Mute"));for(a=0;4>a;a++)t.p(this.b,"vjs-vol-"+a);t.m(this.b,"vjs-vol-"+c)};
t.ra=t.M.extend({i:function(a,c){t.M.call(this,a,c);a.d("volumechange",t.bind(this,this.update));a.n&&a.n.featuresVolumeControl===l&&this.m("vjs-hidden");a.d("loadstart",t.bind(this,function(){a.n.featuresVolumeControl===l?this.m("vjs-hidden"):this.p("vjs-hidden")}));this.m("vjs-menu-button")}});t.ra.prototype.wa=function(){var a=new t.ha(this.c,{kc:"div"}),c=new t.qb(this.c,t.g.z({vertical:f},this.l.xe));c.d("focus",function(){a.m("vjs-lock-showing")});c.d("blur",function(){E(a)});a.S(c);return a};
t.ra.prototype.r=function(){t.ia.prototype.r.call(this);t.M.prototype.r.call(this)};t.ra.prototype.e=function(){return t.t.prototype.e.call(this,"div",{className:"vjs-volume-menu-button vjs-menu-button vjs-control",innerHTML:'<div><span class="vjs-control-text">'+this.s("Mute")+"</span></div>"})};t.ra.prototype.update=t.ia.prototype.update;t.ac=t.M.extend({i:function(a,c){t.M.call(this,a,c);this.Sc();this.Rc();a.d("loadstart",t.bind(this,this.Sc));a.d("ratechange",t.bind(this,this.Rc))}});s=t.ac.prototype;
s.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-playback-rate vjs-menu-button vjs-control",innerHTML:'<div class="vjs-control-content"><span class="vjs-control-text">'+this.s("Playback Rate")+"</span></div>"});this.zc=t.e("div",{className:"vjs-playback-rate-value",innerHTML:1});a.appendChild(this.zc);return a};s.wa=function(){var a=new t.ha(this.j()),c=this.j().options().playbackRates;if(c)for(var d=c.length-1;0<=d;d--)a.S(new t.nb(this.j(),{rate:c[d]+"x"}));return a};
s.oa=function(){this.v().setAttribute("aria-valuenow",this.j().playbackRate())};s.r=function(){for(var a=this.j().playbackRate(),c=this.j().options().playbackRates,d=c[0],e=0;e<c.length;e++)if(c[e]>a){d=c[e];break}this.j().playbackRate(d)};function ea(a){return a.j().n&&a.j().n.featuresPlaybackRate&&a.j().options().playbackRates&&0<a.j().options().playbackRates.length}s.Sc=function(){ea(this)?this.p("vjs-hidden"):this.m("vjs-hidden")};
s.Rc=function(){ea(this)&&(this.zc.innerHTML=this.j().playbackRate()+"x")};t.nb=t.I.extend({kc:"button",i:function(a,c){var d=this.label=c.rate,e=this.Gc=parseFloat(d,10);c.label=d;c.selected=1===e;t.I.call(this,a,c);this.j().d("ratechange",t.bind(this,this.update))}});t.nb.prototype.r=function(){t.I.prototype.r.call(this);this.j().playbackRate(this.Gc)};t.nb.prototype.update=function(){this.selected(this.j().playbackRate()==this.Gc)};
t.Ka=t.t.extend({i:function(a,c){t.t.call(this,a,c);a.poster()&&this.src(a.poster());(!a.poster()||!a.controls())&&this.W();a.d("posterchange",t.bind(this,function(){this.src(a.poster())}));P(a)||a.d("play",t.bind(this,this.W))}});var fa="backgroundSize"in t.A.style;t.Ka.prototype.e=function(){var a=t.e("div",{className:"vjs-poster",tabIndex:-1});fa||a.appendChild(t.e("img"));return a};
t.Ka.prototype.src=function(a){var c=this.v();a!==b&&(fa?c.style.backgroundImage='url("'+a+'")':c.firstChild.src=a)};t.Ka.prototype.r=function(){this.j().controls()&&this.c.play()};t.Yb=t.a.extend({i:function(a,c){t.a.call(this,a,c)}});t.Yb.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-loading-spinner"})};t.fb=t.t.extend();
t.fb.prototype.e=function(){return t.t.prototype.e.call(this,"div",{className:"vjs-big-play-button",innerHTML:'<span aria-hidden="true"></span>',"aria-label":"play video"})};t.fb.prototype.r=function(){this.c.play()};t.jb=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.update();a.d("error",t.bind(this,this.update))}});t.jb.prototype.e=function(){var a=t.a.prototype.e.call(this,"div",{className:"vjs-error-display"});this.u=t.e("div");a.appendChild(this.u);return a};
t.jb.prototype.update=function(){this.j().error()&&(this.u.innerHTML=this.s(this.j().error().message))};
t.q=t.a.extend({i:function(a,c,d){c=c||{};c.Ic=l;t.a.call(this,a,c,d);this.featuresProgressEvents||(this.Ac=f,this.Fc=setInterval(t.bind(this,function(){var a=this.j().bufferedPercent();this.jd!=a&&this.j().k("progress");this.jd=a;1===a&&clearInterval(this.Fc)}),500));this.featuresTimeupdateEvents||(this.Kb=f,this.j().d("play",t.bind(this,this.Qc)),this.j().d("pause",t.bind(this,this.cb)),this.Q("timeupdate",function(){this.featuresTimeupdateEvents=f;ga(this)}));var e,g;g=this;e=this.j();a=function(){if(e.controls()&&
!e.usingNativeControls()){var a;g.d("mousedown",g.r);g.d("touchstart",function(){a=this.c.userActive()});g.d("touchmove",function(){a&&this.j().reportUserActivity()});g.d("touchend",function(a){a.preventDefault()});G(g);g.d("tap",g.Qd)}};c=t.bind(g,g.Wd);this.J(a);e.d("controlsenabled",a);e.d("controlsdisabled",c);this.J(function(){this.networkState&&0<this.networkState()&&this.j().k("loadstart")})}});s=t.q.prototype;
s.Wd=function(){this.o("tap");this.o("touchstart");this.o("touchmove");this.o("touchleave");this.o("touchcancel");this.o("touchend");this.o("click");this.o("mousedown")};s.r=function(a){0===a.button&&this.j().controls()&&(this.j().paused()?this.j().play():this.j().pause())};s.Qd=function(){this.j().userActive(!this.j().userActive())};function ga(a){a.Kb=l;a.cb();a.o("play",a.Qc);a.o("pause",a.cb)}
s.Qc=function(){this.lc&&this.cb();this.lc=setInterval(t.bind(this,function(){this.j().k("timeupdate")}),250)};s.cb=function(){clearInterval(this.lc);this.j().k("timeupdate")};s.dispose=function(){this.Ac&&(this.Ac=l,clearInterval(this.Fc));this.Kb&&ga(this);t.a.prototype.dispose.call(this)};s.Pb=function(){this.Kb&&this.j().k("timeupdate")};s.Kc=m();t.q.prototype.featuresVolumeControl=f;t.q.prototype.featuresFullscreenResize=l;t.q.prototype.featuresPlaybackRate=l;
t.q.prototype.featuresProgressEvents=l;t.q.prototype.featuresTimeupdateEvents=l;t.media={};
t.h=t.q.extend({i:function(a,c,d){this.featuresVolumeControl=t.h.ld();this.featuresPlaybackRate=t.h.kd();this.movingMediaElementInDOM=!t.Yc;this.featuresProgressEvents=this.featuresFullscreenResize=f;t.q.call(this,a,c,d);for(d=t.h.kb.length-1;0<=d;d--)t.d(this.b,t.h.kb[d],t.bind(this,this.td));if((c=c.source)&&this.b.currentSrc!==c.src)this.b.src=c.src;if(t.dc&&a.options().nativeControlsForTouch!==l){var e,g,h,k;e=this;g=this.j();c=g.controls();e.b.controls=!!c;h=function(){e.b.controls=f};k=function(){e.b.controls=
l};g.d("controlsenabled",h);g.d("controlsdisabled",k);c=function(){g.o("controlsenabled",h);g.o("controlsdisabled",k)};e.d("dispose",c);g.d("usingcustomcontrols",c);g.usingNativeControls(f)}a.J(function(){this.L&&(this.l.autoplay&&this.paused())&&(delete this.L.poster,this.play())});this.Fa()}});s=t.h.prototype;s.dispose=function(){t.h.Bb(this.b);t.q.prototype.dispose.call(this)};
s.e=function(){var a=this.c,c=a.L,d;if(!c||this.movingMediaElementInDOM===l)c?(d=c.cloneNode(l),t.h.Bb(c),c=d,a.L=j):(c=t.e("video"),t.Jc(c,t.g.z(a.fe||{},{id:a.id()+"_html5_api","class":"vjs-tech"}))),c.player=a,t.Gb(c,a.v());d=["autoplay","preload","loop","muted"];for(var e=d.length-1;0<=e;e--){var g=d[e],h={};"undefined"!==typeof a.l[g]&&(h[g]=a.l[g]);t.Jc(c,h)}return c};s.td=function(a){"error"==a.type&&this.error()?this.j().error(this.error().code):(a.bubbles=l,this.j().k(a))};s.play=function(){this.b.play()};
s.pause=function(){this.b.pause()};s.paused=function(){return this.b.paused};s.currentTime=function(){return this.b.currentTime};s.Pb=function(a){try{this.b.currentTime=a}catch(c){t.log(c,"Video is not ready. (Video.js)")}};s.duration=function(){return this.b.duration||0};s.buffered=function(){return this.b.buffered};s.volume=function(){return this.b.volume};s.ce=function(a){this.b.volume=a};s.muted=function(){return this.b.muted};s.$d=function(a){this.b.muted=a};s.width=function(){return this.b.offsetWidth};
s.height=function(){return this.b.offsetHeight};s.Da=function(){return"function"==typeof this.b.webkitEnterFullScreen&&(/Android/.test(t.N)||!/Chrome|Mac OS X 10.5/.test(t.N))?f:l};
s.oc=function(){var a=this.b;"webkitDisplayingFullscreen"in a&&this.Q("webkitbeginfullscreen",t.bind(this,function(){this.c.isFullscreen(f);this.Q("webkitendfullscreen",t.bind(this,function(){this.c.isFullscreen(l);this.c.k("fullscreenchange")}));this.c.k("fullscreenchange")}));a.paused&&a.networkState<=a.ke?(this.b.play(),setTimeout(function(){a.pause();a.webkitEnterFullScreen()},0)):a.webkitEnterFullScreen()};s.ud=function(){this.b.webkitExitFullScreen()};
s.src=function(a){if(a===b)return this.b.src;this.b.src=a};s.load=function(){this.b.load()};s.currentSrc=function(){return this.b.currentSrc};s.poster=function(){return this.b.poster};s.Kc=function(a){this.b.poster=a};s.Ca=function(){return this.b.Ca};s.be=function(a){this.b.Ca=a};s.autoplay=function(){return this.b.autoplay};s.Xd=function(a){this.b.autoplay=a};s.controls=function(){return this.b.controls};s.loop=function(){return this.b.loop};s.Zd=function(a){this.b.loop=a};s.error=function(){return this.b.error};
s.seeking=function(){return this.b.seeking};s.ended=function(){return this.b.ended};s.playbackRate=function(){return this.b.playbackRate};s.ae=function(a){this.b.playbackRate=a};s.networkState=function(){return this.b.networkState};t.h.isSupported=function(){try{t.A.volume=0.5}catch(a){return l}return!!t.A.canPlayType};t.h.vb=function(a){try{return!!t.A.canPlayType(a.type)}catch(c){return""}};t.h.ld=function(){var a=t.A.volume;t.A.volume=a/2+0.1;return a!==t.A.volume};
t.h.kd=function(){var a=t.A.playbackRate;t.A.playbackRate=a/2+0.1;return a!==t.A.playbackRate};var V,ha=/^application\/(?:x-|vnd\.apple\.)mpegurl/i,ia=/^video\/mp4/i;t.h.Cc=function(){4<=t.Tb&&(V||(V=t.A.constructor.prototype.canPlayType),t.A.constructor.prototype.canPlayType=function(a){return a&&ha.test(a)?"maybe":V.call(this,a)});t.bd&&(V||(V=t.A.constructor.prototype.canPlayType),t.A.constructor.prototype.canPlayType=function(a){return a&&ia.test(a)?"maybe":V.call(this,a)})};
t.h.ie=function(){var a=t.A.constructor.prototype.canPlayType;t.A.constructor.prototype.canPlayType=V;V=j;return a};t.h.Cc();t.h.kb="loadstart suspend abort error emptied stalled loadedmetadata loadeddata canplay canplaythrough playing waiting seeking seeked ended durationchange timeupdate progress play pause ratechange volumechange".split(" ");
t.h.Bb=function(a){if(a){a.player=j;for(a.parentNode&&a.parentNode.removeChild(a);a.hasChildNodes();)a.removeChild(a.firstChild);a.removeAttribute("src");if("function"===typeof a.load)try{a.load()}catch(c){}}};
t.f=t.q.extend({i:function(a,c,d){t.q.call(this,a,c,d);var e=c.source;d=c.parentEl;var g=this.b=t.e("div",{id:a.id()+"_temp_flash"}),h=a.id()+"_flash_api",k=a.l,k=t.g.z({readyFunction:"videojs.Flash.onReady",eventProxyFunction:"videojs.Flash.onEvent",errorEventProxyFunction:"videojs.Flash.onError",autoplay:k.autoplay,preload:k.Ca,loop:k.loop,muted:k.muted},c.flashVars),n=t.g.z({wmode:"opaque",bgcolor:"#000000"},c.params),h=t.g.z({id:h,name:h,"class":"vjs-tech"},c.attributes);e&&(e.type&&t.f.Dd(e.type)?
(e=t.f.Oc(e.src),k.rtmpConnection=encodeURIComponent(e.wb),k.rtmpStream=encodeURIComponent(e.Qb)):k.src=encodeURIComponent(t.sc(e.src)));t.Gb(g,d);c.startTime&&this.J(function(){this.load();this.play();this.currentTime(c.startTime)});t.Xc&&this.J(function(){t.d(this.v(),"mousemove",t.bind(this,function(){this.j().k({type:"mousemove",bubbles:l})}))});a.d("stageclick",a.reportUserActivity);this.b=t.f.nc(c.swf,g,k,n,h)}});t.f.prototype.dispose=function(){t.q.prototype.dispose.call(this)};
t.f.prototype.play=function(){this.b.vjs_play()};t.f.prototype.pause=function(){this.b.vjs_pause()};t.f.prototype.src=function(a){if(a===b)return this.currentSrc();t.f.Cd(a)?(a=t.f.Oc(a),this.ue(a.wb),this.ve(a.Qb)):(a=t.sc(a),this.b.vjs_src(a));if(this.c.autoplay()){var c=this;setTimeout(function(){c.play()},0)}};t.f.prototype.setCurrentTime=function(a){this.Gd=a;this.b.vjs_setProperty("currentTime",a);t.q.prototype.Pb.call(this)};
t.f.prototype.currentTime=function(){return this.seeking()?this.Gd||0:this.b.vjs_getProperty("currentTime")};t.f.prototype.currentSrc=function(){var a=this.b.vjs_getProperty("currentSrc");if(a==j){var c=this.rtmpConnection(),d=this.rtmpStream();c&&d&&(a=t.f.de(c,d))}return a};t.f.prototype.load=function(){this.b.vjs_load()};t.f.prototype.poster=function(){this.b.vjs_getProperty("poster")};t.f.prototype.setPoster=m();t.f.prototype.buffered=function(){return t.zb(0,this.b.vjs_getProperty("buffered"))};
t.f.prototype.Da=q(l);t.f.prototype.oc=q(l);function ja(){var a=W[X],c=a.charAt(0).toUpperCase()+a.slice(1);ka["set"+c]=function(c){return this.b.vjs_setProperty(a,c)}}function la(a){ka[a]=function(){return this.b.vjs_getProperty(a)}}
var ka=t.f.prototype,W="rtmpConnection rtmpStream preload defaultPlaybackRate playbackRate autoplay loop mediaGroup controller controls volume muted defaultMuted".split(" "),ma="error networkState readyState seeking initialTime duration startOffsetTime paused played seekable ended videoTracks audioTracks videoWidth videoHeight textTracks".split(" "),X;for(X=0;X<W.length;X++)la(W[X]),ja();for(X=0;X<ma.length;X++)la(ma[X]);t.f.isSupported=function(){return 10<=t.f.version()[0]};
t.f.vb=function(a){if(!a.type)return"";a=a.type.replace(/;.*/,"").toLowerCase();if(a in t.f.wd||a in t.f.Pc)return"maybe"};t.f.wd={"video/flv":"FLV","video/x-flv":"FLV","video/mp4":"MP4","video/m4v":"MP4"};t.f.Pc={"rtmp/mp4":"MP4","rtmp/flv":"FLV"};t.f.onReady=function(a){var c;if(c=(a=t.v(a))&&a.parentNode&&a.parentNode.player)a.player=c,t.f.checkReady(c.n)};t.f.checkReady=function(a){a.v()&&(a.v().vjs_getProperty?a.Fa():setTimeout(function(){t.f.checkReady(a)},50))};t.f.onEvent=function(a,c){t.v(a).player.k(c)};
t.f.onError=function(a,c){var d=t.v(a).player,e="FLASH: "+c;"srcnotfound"==c?d.error({code:4,message:e}):d.error(e)};t.f.version=function(){var a="0,0,0";try{a=(new window.ActiveXObject("ShockwaveFlash.ShockwaveFlash")).GetVariable("$version").replace(/\D+/g,",").match(/^,?(.+),?$/)[1]}catch(c){try{navigator.mimeTypes["application/x-shockwave-flash"].enabledPlugin&&(a=(navigator.plugins["Shockwave Flash 2.0"]||navigator.plugins["Shockwave Flash"]).description.replace(/\D+/g,",").match(/^,?(.+),?$/)[1])}catch(d){}}return a.split(",")};
t.f.nc=function(a,c,d,e,g){a=t.f.yd(a,d,e,g);a=t.e("div",{innerHTML:a}).childNodes[0];d=c.parentNode;c.parentNode.replaceChild(a,c);var h=d.childNodes[0];setTimeout(function(){h.style.display="block"},1E3);return a};
t.f.yd=function(a,c,d,e){var g="",h="",k="";c&&t.g.Y(c,function(a,c){g+=a+"="+c+"&amp;"});d=t.g.z({movie:a,flashvars:g,allowScriptAccess:"always",allowNetworking:"all"},d);t.g.Y(d,function(a,c){h+='<param name="'+a+'" value="'+c+'" />'});e=t.g.z({data:a,width:"100%",height:"100%"},e);t.g.Y(e,function(a,c){k+=a+'="'+c+'" '});return'<object type="application/x-shockwave-flash"'+k+">"+h+"</object>"};t.f.de=function(a,c){return a+"&"+c};
t.f.Oc=function(a){var c={wb:"",Qb:""};if(!a)return c;var d=a.indexOf("&"),e;-1!==d?e=d+1:(d=e=a.lastIndexOf("/")+1,0===d&&(d=e=a.length));c.wb=a.substring(0,d);c.Qb=a.substring(e,a.length);return c};t.f.Dd=function(a){return a in t.f.Pc};t.f.dd=/^rtmp[set]?:\/\//i;t.f.Cd=function(a){return t.f.dd.test(a)};
t.cd=t.a.extend({i:function(a,c,d){t.a.call(this,a,c,d);if(!a.l.sources||0===a.l.sources.length){c=0;for(d=a.l.techOrder;c<d.length;c++){var e=t.ba(d[c]),g=window.videojs[e];if(g&&g.isSupported()){Q(a,e);break}}}else a.src(a.l.sources)}});t.Player.prototype.textTracks=function(){return this.Ea=this.Ea||[]};
function na(a,c,d,e,g){var h=a.Ea=a.Ea||[];g=g||{};g.kind=c;g.label=d;g.language=e;c=t.ba(c||"subtitles");var k=new window.videojs[c+"Track"](a,g);h.push(k);k.Ab()&&a.J(function(){setTimeout(function(){Y(k.j(),k.id())},0)})}function Y(a,c,d){for(var e=a.Ea,g=0,h=e.length,k,n;g<h;g++)k=e[g],k.id()===c?(k.show(),n=k):d&&(k.K()==d&&0<k.mode())&&k.disable();(c=n?n.K():d?d:l)&&a.k(c+"trackchange")}
t.B=t.a.extend({i:function(a,c){t.a.call(this,a,c);this.U=c.id||"vjs_"+c.kind+"_"+c.language+"_"+t.w++;this.Lc=c.src;this.qd=c["default"]||c.dflt;this.ge=c.title;this.Ua=c.srclang;this.Ed=c.label;this.ca=[];this.tb=[];this.ma=this.na=0}});s=t.B.prototype;s.K=p("H");s.src=p("Lc");s.Ab=p("qd");s.title=p("ge");s.language=p("Ua");s.label=p("Ed");s.md=p("ca");s.ed=p("tb");s.readyState=p("na");s.mode=p("ma");s.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-"+this.H+" vjs-text-track"})};
s.show=function(){oa(this);this.ma=2;t.a.prototype.show.call(this)};s.W=function(){oa(this);this.ma=1;t.a.prototype.W.call(this)};s.disable=function(){2==this.ma&&this.W();this.c.o("timeupdate",t.bind(this,this.update,this.U));this.c.o("ended",t.bind(this,this.reset,this.U));this.reset();this.c.ka("textTrackDisplay").removeChild(this);this.ma=0};
function oa(a){0===a.na&&a.load();0===a.ma&&(a.c.d("timeupdate",t.bind(a,a.update,a.U)),a.c.d("ended",t.bind(a,a.reset,a.U)),("captions"===a.H||"subtitles"===a.H)&&a.c.ka("textTrackDisplay").S(a))}s.load=function(){0===this.na&&(this.na=1,t.get(this.Lc,t.bind(this,this.Td),t.bind(this,this.Jd)))};s.Jd=function(a){this.error=a;this.na=3;this.k("error")};
s.Td=function(a){var c,d;a=a.split("\n");for(var e="",g=1,h=a.length;g<h;g++)if(e=t.trim(a[g])){-1==e.indexOf("--\x3e")?(c=e,e=t.trim(a[++g])):c=this.ca.length;c={id:c,index:this.ca.length};d=e.split(/[\t ]+/);c.startTime=pa(d[0]);c.xa=pa(d[2]);for(d=[];a[++g]&&(e=t.trim(a[g]));)d.push(e);c.text=d.join("<br/>");this.ca.push(c)}this.na=2;this.k("loaded")};
function pa(a){var c=a.split(":");a=0;var d,e,g;3==c.length?(d=c[0],e=c[1],c=c[2]):(d=0,e=c[0],c=c[1]);c=c.split(/\s+/);c=c.splice(0,1)[0];c=c.split(/\.|,/);g=parseFloat(c[1]);c=c[0];a+=3600*parseFloat(d);a+=60*parseFloat(e);a+=parseFloat(c);g&&(a+=g/1E3);return a}
s.update=function(){if(0<this.ca.length){var a=this.c.options().trackTimeOffset||0,a=this.c.currentTime()+a;if(this.Ob===b||a<this.Ob||this.Wa<=a){var c=this.ca,d=this.c.duration(),e=0,g=l,h=[],k,n,r,w;a>=this.Wa||this.Wa===b?w=this.Db!==b?this.Db:0:(g=f,w=this.Jb!==b?this.Jb:c.length-1);for(;;){r=c[w];if(r.xa<=a)e=Math.max(e,r.xa),r.Na&&(r.Na=l);else if(a<r.startTime){if(d=Math.min(d,r.startTime),r.Na&&(r.Na=l),!g)break}else g?(h.splice(0,0,r),n===b&&(n=w),k=w):(h.push(r),k===b&&(k=w),n=w),d=Math.min(d,
r.xa),e=Math.max(e,r.startTime),r.Na=f;if(g)if(0===w)break;else w--;else if(w===c.length-1)break;else w++}this.tb=h;this.Wa=d;this.Ob=e;this.Db=k;this.Jb=n;k=this.tb;n="";a=0;for(c=k.length;a<c;a++)n+='<span class="vjs-tt-cue">'+k[a].text+"</span>";this.b.innerHTML=n;this.k("cuechange")}}};s.reset=function(){this.Wa=0;this.Ob=this.c.duration();this.Jb=this.Db=0};t.Vb=t.B.extend();t.Vb.prototype.H="captions";t.cc=t.B.extend();t.cc.prototype.H="subtitles";t.Wb=t.B.extend();t.Wb.prototype.H="chapters";
t.ec=t.a.extend({i:function(a,c,d){t.a.call(this,a,c,d);if(a.l.tracks&&0<a.l.tracks.length){c=this.c;a=a.l.tracks;for(var e=0;e<a.length;e++)d=a[e],na(c,d.kind,d.label,d.language,d)}}});t.ec.prototype.e=function(){return t.a.prototype.e.call(this,"div",{className:"vjs-text-track-display"})};t.aa=t.I.extend({i:function(a,c){var d=this.ea=c.track;c.label=d.label();c.selected=d.Ab();t.I.call(this,a,c);this.c.d(d.K()+"trackchange",t.bind(this,this.update))}});
t.aa.prototype.r=function(){t.I.prototype.r.call(this);Y(this.c,this.ea.U,this.ea.K())};t.aa.prototype.update=function(){this.selected(2==this.ea.mode())};t.mb=t.aa.extend({i:function(a,c){c.track={K:function(){return c.kind},j:a,label:function(){return c.kind+" off"},Ab:q(l),mode:q(l)};t.aa.call(this,a,c);this.selected(f)}});t.mb.prototype.r=function(){t.aa.prototype.r.call(this);Y(this.c,this.ea.U,this.ea.K())};
t.mb.prototype.update=function(){for(var a=this.c.textTracks(),c=0,d=a.length,e,g=f;c<d;c++)e=a[c],e.K()==this.ea.K()&&2==e.mode()&&(g=l);this.selected(g)};t.V=t.M.extend({i:function(a,c){t.M.call(this,a,c);1>=this.P.length&&this.W()}});t.V.prototype.va=function(){var a=[],c;a.push(new t.mb(this.c,{kind:this.H}));for(var d=0;d<this.c.textTracks().length;d++)c=this.c.textTracks()[d],c.K()===this.H&&a.push(new t.aa(this.c,{track:c}));return a};
t.Ga=t.V.extend({i:function(a,c,d){t.V.call(this,a,c,d);this.b.setAttribute("aria-label","Captions Menu")}});t.Ga.prototype.H="captions";t.Ga.prototype.ta="Captions";t.Ga.prototype.className="vjs-captions-button";t.Ma=t.V.extend({i:function(a,c,d){t.V.call(this,a,c,d);this.b.setAttribute("aria-label","Subtitles Menu")}});t.Ma.prototype.H="subtitles";t.Ma.prototype.ta="Subtitles";t.Ma.prototype.className="vjs-subtitles-button";
t.Ha=t.V.extend({i:function(a,c,d){t.V.call(this,a,c,d);this.b.setAttribute("aria-label","Chapters Menu")}});s=t.Ha.prototype;s.H="chapters";s.ta="Chapters";s.className="vjs-chapters-button";s.va=function(){for(var a=[],c,d=0;d<this.c.textTracks().length;d++)c=this.c.textTracks()[d],c.K()===this.H&&a.push(new t.aa(this.c,{track:c}));return a};
s.wa=function(){for(var a=this.c.textTracks(),c=0,d=a.length,e,g,h=this.P=[];c<d;c++)if(e=a[c],e.K()==this.H)if(0===e.readyState())e.load(),e.d("loaded",t.bind(this,this.wa));else{g=e;break}a=this.Aa;a===b&&(a=new t.ha(this.c),a.ja().appendChild(t.e("li",{className:"vjs-menu-title",innerHTML:t.ba(this.H),ee:-1})));if(g){e=g.ca;for(var k,c=0,d=e.length;c<d;c++)k=e[c],k=new t.gb(this.c,{track:g,cue:k}),h.push(k),a.S(k);this.S(a)}0<this.P.length&&this.show();return a};
t.gb=t.I.extend({i:function(a,c){var d=this.ea=c.track,e=this.cue=c.cue,g=a.currentTime();c.label=e.text;c.selected=e.startTime<=g&&g<e.xa;t.I.call(this,a,c);d.d("cuechange",t.bind(this,this.update))}});t.gb.prototype.r=function(){t.I.prototype.r.call(this);this.c.currentTime(this.cue.startTime);this.update(this.cue.startTime)};t.gb.prototype.update=function(){var a=this.cue,c=this.c.currentTime();this.selected(a.startTime<=c&&c<a.xa)};
t.g.z(t.Ia.prototype.l.children,{subtitlesButton:{},captionsButton:{},chaptersButton:{}});
if("undefined"!==typeof window.JSON&&"function"===window.JSON.parse)t.JSON=window.JSON;else{t.JSON={};var Z=/[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g;t.JSON.parse=function(a,c){function d(a,e){var k,n,r=a[e];if(r&&"object"===typeof r)for(k in r)Object.prototype.hasOwnProperty.call(r,k)&&(n=d(r,k),n!==b?r[k]=n:delete r[k]);return c.call(a,e,r)}var e;a=String(a);Z.lastIndex=0;Z.test(a)&&(a=a.replace(Z,function(a){return"\\u"+("0000"+a.charCodeAt(0).toString(16)).slice(-4)}));
if(/^[\],:{}\s]*$/.test(a.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g,"@").replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g,"]").replace(/(?:^|:|,)(?:\s*\[)+/g,"")))return e=eval("("+a+")"),"function"===typeof c?d({"":e},""):e;throw new SyntaxError("JSON.parse(): invalid or malformed JSON data");}}
t.ic=function(){var a,c,d,e;a=document.getElementsByTagName("video");c=document.getElementsByTagName("audio");var g=[];if(a&&0<a.length){d=0;for(e=a.length;d<e;d++)g.push(a[d])}if(c&&0<c.length){d=0;for(e=c.length;d<e;d++)g.push(c[d])}if(g&&0<g.length){d=0;for(e=g.length;d<e;d++)if((c=g[d])&&c.getAttribute)c.player===b&&(a=c.getAttribute("data-setup"),a!==j&&videojs(c));else{t.ub();break}}else t.Tc||t.ub()};t.ub=function(){setTimeout(t.ic,1)};
"complete"===document.readyState?t.Tc=f:t.Q(window,"load",function(){t.Tc=f});t.ub();t.Vd=function(a,c){t.Player.prototype[a]=c};var qa=this;function $(a,c){var d=a.split("."),e=qa;!(d[0]in e)&&e.execScript&&e.execScript("var "+d[0]);for(var g;d.length&&(g=d.shift());)!d.length&&c!==b?e[g]=c:e=e[g]?e[g]:e[g]={}};$("videojs",t);$("_V_",t);$("videojs.options",t.options);$("videojs.players",t.Ba);$("videojs.TOUCH_ENABLED",t.dc);$("videojs.cache",t.ua);$("videojs.Component",t.a);t.a.prototype.player=t.a.prototype.j;t.a.prototype.options=t.a.prototype.options;t.a.prototype.init=t.a.prototype.i;t.a.prototype.dispose=t.a.prototype.dispose;t.a.prototype.createEl=t.a.prototype.e;t.a.prototype.contentEl=t.a.prototype.ja;t.a.prototype.el=t.a.prototype.v;t.a.prototype.addChild=t.a.prototype.S;
t.a.prototype.getChild=t.a.prototype.ka;t.a.prototype.getChildById=t.a.prototype.xd;t.a.prototype.children=t.a.prototype.children;t.a.prototype.initChildren=t.a.prototype.vc;t.a.prototype.removeChild=t.a.prototype.removeChild;t.a.prototype.on=t.a.prototype.d;t.a.prototype.off=t.a.prototype.o;t.a.prototype.one=t.a.prototype.Q;t.a.prototype.trigger=t.a.prototype.k;t.a.prototype.triggerReady=t.a.prototype.Fa;t.a.prototype.show=t.a.prototype.show;t.a.prototype.hide=t.a.prototype.W;
t.a.prototype.width=t.a.prototype.width;t.a.prototype.height=t.a.prototype.height;t.a.prototype.dimensions=t.a.prototype.rd;t.a.prototype.ready=t.a.prototype.J;t.a.prototype.addClass=t.a.prototype.m;t.a.prototype.removeClass=t.a.prototype.p;t.a.prototype.buildCSSClass=t.a.prototype.T;t.a.prototype.localize=t.a.prototype.s;t.Player.prototype.ended=t.Player.prototype.ended;t.Player.prototype.enterFullWindow=t.Player.prototype.pc;t.Player.prototype.exitFullWindow=t.Player.prototype.Cb;
t.Player.prototype.preload=t.Player.prototype.Ca;t.Player.prototype.remainingTime=t.Player.prototype.remainingTime;t.Player.prototype.supportsFullScreen=t.Player.prototype.Da;t.Player.prototype.currentType=t.Player.prototype.nd;t.Player.prototype.requestFullScreen=t.Player.prototype.requestFullScreen;t.Player.prototype.requestFullscreen=t.Player.prototype.requestFullscreen;t.Player.prototype.cancelFullScreen=t.Player.prototype.cancelFullScreen;t.Player.prototype.exitFullscreen=t.Player.prototype.exitFullscreen;
t.Player.prototype.isFullScreen=t.Player.prototype.isFullScreen;t.Player.prototype.isFullscreen=t.Player.prototype.isFullscreen;$("videojs.MediaLoader",t.cd);$("videojs.TextTrackDisplay",t.ec);$("videojs.ControlBar",t.Ia);$("videojs.Button",t.t);$("videojs.PlayToggle",t.$b);$("videojs.FullscreenToggle",t.Ja);$("videojs.BigPlayButton",t.fb);$("videojs.LoadingSpinner",t.Yb);$("videojs.CurrentTimeDisplay",t.hb);$("videojs.DurationDisplay",t.ib);$("videojs.TimeDivider",t.fc);
$("videojs.RemainingTimeDisplay",t.pb);$("videojs.LiveDisplay",t.Xb);$("videojs.ErrorDisplay",t.jb);$("videojs.Slider",t.R);$("videojs.ProgressControl",t.ob);$("videojs.SeekBar",t.bc);$("videojs.LoadProgressBar",t.lb);$("videojs.PlayProgressBar",t.Zb);$("videojs.SeekHandle",t.La);$("videojs.VolumeControl",t.rb);$("videojs.VolumeBar",t.qb);$("videojs.VolumeLevel",t.gc);$("videojs.VolumeMenuButton",t.ra);$("videojs.VolumeHandle",t.sb);$("videojs.MuteToggle",t.ia);$("videojs.PosterImage",t.Ka);
$("videojs.Menu",t.ha);$("videojs.MenuItem",t.I);$("videojs.MenuButton",t.M);$("videojs.PlaybackRateMenuButton",t.ac);t.M.prototype.createItems=t.M.prototype.va;t.V.prototype.createItems=t.V.prototype.va;t.Ha.prototype.createItems=t.Ha.prototype.va;$("videojs.SubtitlesButton",t.Ma);$("videojs.CaptionsButton",t.Ga);$("videojs.ChaptersButton",t.Ha);$("videojs.MediaTechController",t.q);t.q.prototype.featuresVolumeControl=t.q.prototype.re;t.q.prototype.featuresFullscreenResize=t.q.prototype.ne;
t.q.prototype.featuresPlaybackRate=t.q.prototype.oe;t.q.prototype.featuresProgressEvents=t.q.prototype.pe;t.q.prototype.featuresTimeupdateEvents=t.q.prototype.qe;t.q.prototype.setPoster=t.q.prototype.Kc;$("videojs.Html5",t.h);t.h.Events=t.h.kb;t.h.isSupported=t.h.isSupported;t.h.canPlaySource=t.h.vb;t.h.patchCanPlayType=t.h.Cc;t.h.unpatchCanPlayType=t.h.ie;t.h.prototype.setCurrentTime=t.h.prototype.Pb;t.h.prototype.setVolume=t.h.prototype.ce;t.h.prototype.setMuted=t.h.prototype.$d;
t.h.prototype.setPreload=t.h.prototype.be;t.h.prototype.setAutoplay=t.h.prototype.Xd;t.h.prototype.setLoop=t.h.prototype.Zd;t.h.prototype.enterFullScreen=t.h.prototype.oc;t.h.prototype.exitFullScreen=t.h.prototype.ud;t.h.prototype.playbackRate=t.h.prototype.playbackRate;t.h.prototype.setPlaybackRate=t.h.prototype.ae;$("videojs.Flash",t.f);t.f.isSupported=t.f.isSupported;t.f.canPlaySource=t.f.vb;t.f.onReady=t.f.onReady;t.f.embed=t.f.nc;t.f.version=t.f.version;$("videojs.TextTrack",t.B);
t.B.prototype.label=t.B.prototype.label;t.B.prototype.kind=t.B.prototype.K;t.B.prototype.mode=t.B.prototype.mode;t.B.prototype.cues=t.B.prototype.md;t.B.prototype.activeCues=t.B.prototype.ed;$("videojs.CaptionsTrack",t.Vb);$("videojs.SubtitlesTrack",t.cc);$("videojs.ChaptersTrack",t.Wb);$("videojs.autoSetup",t.ic);$("videojs.plugin",t.Vd);$("videojs.createTimeRange",t.zb);$("videojs.util",t.ga);t.ga.mergeOptions=t.ga.Va;t.addLanguage=t.fd;})();
(new Image,window,navigator,encodeURIComponent);
;var curSlide = 1; 

$.fn.slideshow = function() {
    var $container = $(this); 
    
    // Add arrows for nav 
    $container.append('\
        <span class="arrow-left disabled">&laquo;</span>\
        <span class="arrow-right">&raquo;</span>\
    '); 
    
    $container.children(".arrow-left").click(function() { 
        if (!$(this).hasClass("disabled")) {
            switchSlide("back");
        }
        return false;
    });
    
    $container.children(".arrow-right").click(function() { 
        if (!$(this).hasClass("disabled")) {
            switchSlide();
        }
        return false;
    });
    
    // Move all slides except #1 over to the right 
    $container.children('.slide').each(function() { 
        var curId = $(this).prop('id').replace("slide",""); 
        $(this).css("left", (curId-1) * $container.width()).css("display","block");
    });
    
    function switchSlide(direction) {
        var animateSpeed = 1000; 
        var prevSlide = curSlide; 
        
        // Temporarily disable back/next buttons 
        $container.children(".arrow-right, .arrow-left").addClass("disabled"); 
        
        if (direction === "back") {

            curSlide--;
    
            // If we're at slide 1
            if ((curSlide) === 0) {
                // The new slide will be the end slide.  
                curSlide = $('.slide').length;
            }
    
            // Since the slide will come in from the left, 
            // it needs to start to the left of the viewport (rather than right)
            $('#slide' + curSlide).css("left",-1*$container.width());
    
            // Move the previous slide right of viewport
            $("#slide" + prevSlide).animate({
                left: 1 * $container.width()
            }, animateSpeed);
    
            // Move new slide into the viewport
            $('#slide' + curSlide).animate({
                left: 0
            }, animateSpeed, function() { 
                // Enable back/next buttons 
                if (curSlide == 0) {
                    $container.children(".arrow-right").removeClass("disabled"); 
                } else {
                    $container.children(".arrow-right, .arrow-left").removeClass("disabled"); 
                }
            });
    
        } else {
    
            curSlide++;
    
            // If we've reached last slide, start over
            if ((curSlide) > $('.slide').length) {
                curSlide = 1;
            }
    
            // Move previous slide left of viewport
            $("#slide" + prevSlide).animate({
                left: -1 * $('body').width()
            }, animateSpeed);
    
            // Move new slide into the viewport
            $('#slide' + curSlide).animate({
                left: 0
            }, animateSpeed,function() { 
                // Enable back/next buttons 
                if (curSlide == $container.children('.slide').length) { 
                    $container.children(".arrow-left").removeClass("disabled"); 
                } else {
                    $container.children(".arrow-right, .arrow-left").removeClass("disabled"); 
                }
    
                // Position next slide just to the right of the viewport
                if ((curSlide) === $('.slide').length) {
                    $('#slide1').css("left",$('body').width());
                } else {
                    $('#slide' + (curSlide+1)).css("left",$('body').width());
                }
            });
    
        }

    }
};/*
 * jQuery FlexSlider v2.6.2
 * Copyright 2012 WooThemes
 * Contributing Author: Tyler Smith
 */!function(a){var b=!0;a.flexslider=function(c,d){var e=a(c);e.vars=a.extend({},a.flexslider.defaults,d);var k,f=e.vars.namespace,g=window.navigator&&window.navigator.msPointerEnabled&&window.MSGesture,h=("ontouchstart"in window||g||window.DocumentTouch&&document instanceof DocumentTouch)&&e.vars.touch,i="click touchend MSPointerUp",j="",l="vertical"===e.vars.direction,m=e.vars.reverse,n=e.vars.itemWidth>0,o="fade"===e.vars.animation,p=""!==e.vars.asNavFor,q={};a.data(c,"flexslider",e),q={init:function(){e.animating=!1,e.currentSlide=parseInt(e.vars.startAt?e.vars.startAt:0,10),isNaN(e.currentSlide)&&(e.currentSlide=0),e.animatingTo=e.currentSlide,e.atEnd=0===e.currentSlide||e.currentSlide===e.last,e.containerSelector=e.vars.selector.substr(0,e.vars.selector.search(" ")),e.slides=a(e.vars.selector,e),e.container=a(e.containerSelector,e),e.count=e.slides.length,e.syncExists=a(e.vars.sync).length>0,"slide"===e.vars.animation&&(e.vars.animation="swing"),e.prop=l?"top":"marginLeft",e.args={},e.manualPause=!1,e.stopped=!1,e.started=!1,e.startTimeout=null,e.transitions=!e.vars.video&&!o&&e.vars.useCSS&&function(){var a=document.createElement("div"),b=["perspectiveProperty","WebkitPerspective","MozPerspective","OPerspective","msPerspective"];for(var c in b)if(void 0!==a.style[b[c]])return e.pfx=b[c].replace("Perspective","").toLowerCase(),e.prop="-"+e.pfx+"-transform",!0;return!1}(),e.ensureAnimationEnd="",""!==e.vars.controlsContainer&&(e.controlsContainer=a(e.vars.controlsContainer).length>0&&a(e.vars.controlsContainer)),""!==e.vars.manualControls&&(e.manualControls=a(e.vars.manualControls).length>0&&a(e.vars.manualControls)),""!==e.vars.customDirectionNav&&(e.customDirectionNav=2===a(e.vars.customDirectionNav).length&&a(e.vars.customDirectionNav)),e.vars.randomize&&(e.slides.sort(function(){return Math.round(Math.random())-.5}),e.container.empty().append(e.slides)),e.doMath(),e.setup("init"),e.vars.controlNav&&q.controlNav.setup(),e.vars.directionNav&&q.directionNav.setup(),e.vars.keyboard&&(1===a(e.containerSelector).length||e.vars.multipleKeyboard)&&a(document).bind("keyup",function(a){var b=a.keyCode;if(!e.animating&&(39===b||37===b)){var c=39===b?e.getTarget("next"):37===b&&e.getTarget("prev");e.flexAnimate(c,e.vars.pauseOnAction)}}),e.vars.mousewheel&&e.bind("mousewheel",function(a,b,c,d){a.preventDefault();var f=b<0?e.getTarget("next"):e.getTarget("prev");e.flexAnimate(f,e.vars.pauseOnAction)}),e.vars.pausePlay&&q.pausePlay.setup(),e.vars.slideshow&&e.vars.pauseInvisible&&q.pauseInvisible.init(),e.vars.slideshow&&(e.vars.pauseOnHover&&e.hover(function(){e.manualPlay||e.manualPause||e.pause()},function(){e.manualPause||e.manualPlay||e.stopped||e.play()}),e.vars.pauseInvisible&&q.pauseInvisible.isHidden()||(e.vars.initDelay>0?e.startTimeout=setTimeout(e.play,e.vars.initDelay):e.play())),p&&q.asNav.setup(),h&&e.vars.touch&&q.touch(),(!o||o&&e.vars.smoothHeight)&&a(window).bind("resize orientationchange focus",q.resize),e.find("img").attr("draggable","false"),setTimeout(function(){e.vars.start(e)},200)},asNav:{setup:function(){e.asNav=!0,e.animatingTo=Math.floor(e.currentSlide/e.move),e.currentItem=e.currentSlide,e.slides.removeClass(f+"active-slide").eq(e.currentItem).addClass(f+"active-slide"),g?(c._slider=e,e.slides.each(function(){var b=this;b._gesture=new MSGesture,b._gesture.target=b,b.addEventListener("MSPointerDown",function(a){a.preventDefault(),a.currentTarget._gesture&&a.currentTarget._gesture.addPointer(a.pointerId)},!1),b.addEventListener("MSGestureTap",function(b){b.preventDefault();var c=a(this),d=c.index();a(e.vars.asNavFor).data("flexslider").animating||c.hasClass("active")||(e.direction=e.currentItem<d?"next":"prev",e.flexAnimate(d,e.vars.pauseOnAction,!1,!0,!0))})})):e.slides.on(i,function(b){b.preventDefault();var c=a(this),d=c.index(),g=c.offset().left-a(e).scrollLeft();g<=0&&c.hasClass(f+"active-slide")?e.flexAnimate(e.getTarget("prev"),!0):a(e.vars.asNavFor).data("flexslider").animating||c.hasClass(f+"active-slide")||(e.direction=e.currentItem<d?"next":"prev",e.flexAnimate(d,e.vars.pauseOnAction,!1,!0,!0))})}},controlNav:{setup:function(){e.manualControls?q.controlNav.setupManual():q.controlNav.setupPaging()},setupPaging:function(){var d,g,b="thumbnails"===e.vars.controlNav?"control-thumbs":"control-paging",c=1;if(e.controlNavScaffold=a('<ol class="'+f+"control-nav "+f+b+'"></ol>'),controlRole=e.vars.useAriaAttributes===!0?' role="button" ':"",e.pagingCount>1)for(var h=0;h<e.pagingCount;h++){g=e.slides.eq(h),void 0===g.attr("data-thumb-alt")&&g.attr("data-thumb-alt","");var k=""!==g.attr("data-thumb-alt")?k=' alt="'+g.attr("data-thumb-alt")+'"':"";if(d="thumbnails"===e.vars.controlNav?'<img src="'+g.attr("data-thumb")+'"'+k+controlRole+"/>":'<a href="#"'+controlRole+">"+c+"</a>","thumbnails"===e.vars.controlNav&&!0===e.vars.thumbCaptions){var l=g.attr("data-thumbcaption");""!==l&&void 0!==l&&(d+='<span class="'+f+'caption">'+l+"</span>")}e.controlNavScaffold.append("<li>"+d+"</li>"),c++}e.controlsContainer?a(e.controlsContainer).append(e.controlNavScaffold):e.append(e.controlNavScaffold),q.controlNav.set(),q.controlNav.active(),e.controlNavScaffold.delegate("a, img",i,function(b){if(b.preventDefault(),""===j||j===b.type){var c=a(this),d=e.controlNav.index(c);c.hasClass(f+"active")||(e.direction=d>e.currentSlide?"next":"prev",e.flexAnimate(d,e.vars.pauseOnAction))}""===j&&(j=b.type),q.setToClearWatchedEvent()})},setupManual:function(){e.controlNav=e.manualControls,q.controlNav.active(),e.controlNav.bind(i,function(b){if(b.preventDefault(),""===j||j===b.type){var c=a(this),d=e.controlNav.index(c);c.hasClass(f+"active")||(d>e.currentSlide?e.direction="next":e.direction="prev",e.flexAnimate(d,e.vars.pauseOnAction))}""===j&&(j=b.type),q.setToClearWatchedEvent()})},set:function(){var b="thumbnails"===e.vars.controlNav?"img":"a";e.controlNav=a("."+f+"control-nav li "+b,e.controlsContainer?e.controlsContainer:e)},active:function(){e.controlNav.removeClass(f+"active").eq(e.animatingTo).addClass(f+"active")},update:function(b,c){e.pagingCount>1&&"add"===b?e.controlNavScaffold.append(a('<li><a href="#">'+e.count+"</a></li>")):1===e.pagingCount?e.controlNavScaffold.find("li").remove():e.controlNav.eq(c).closest("li").remove(),q.controlNav.set(),e.pagingCount>1&&e.pagingCount!==e.controlNav.length?e.update(c,b):q.controlNav.active()}},directionNav:{setup:function(){var b=a('<ul class="'+f+'direction-nav"><li class="'+f+'nav-prev"><a role="button" class="'+f+'prev" href="#">'+e.vars.prevText+'</a></li><li class="'+f+'nav-next"><a role="button" class="'+f+'next" href="#">'+e.vars.nextText+"</a></li></ul>");e.customDirectionNav?e.directionNav=e.customDirectionNav:e.controlsContainer?(a(e.controlsContainer).append(b),e.directionNav=a("."+f+"direction-nav li a",e.controlsContainer)):(e.append(b),e.directionNav=a("."+f+"direction-nav li a",e)),q.directionNav.update(),e.directionNav.bind(i,function(b){b.preventDefault();var c;""!==j&&j!==b.type||(c=a(this).hasClass(f+"next")?e.getTarget("next"):e.getTarget("prev"),e.flexAnimate(c,e.vars.pauseOnAction)),""===j&&(j=b.type),q.setToClearWatchedEvent()})},update:function(){var a=f+"disabled";1===e.pagingCount?e.directionNav.addClass(a).attr("tabindex","-1"):e.vars.animationLoop?e.directionNav.removeClass(a).removeAttr("tabindex"):0===e.animatingTo?e.directionNav.removeClass(a).filter("."+f+"prev").addClass(a).attr("tabindex","-1"):e.animatingTo===e.last?e.directionNav.removeClass(a).filter("."+f+"next").addClass(a).attr("tabindex","-1"):e.directionNav.removeClass(a).removeAttr("tabindex")}},pausePlay:{setup:function(){controlRole=e.vars.useAriaAttributes===!0?' role="button" ':"";var b=a('<div class="'+f+'pauseplay"><a href="#" + '+controlRole+"></a></div>");e.controlsContainer?(e.controlsContainer.append(b),e.pausePlay=a("."+f+"pauseplay a",e.controlsContainer)):(e.append(b),e.pausePlay=a("."+f+"pauseplay a",e)),q.pausePlay.update(e.vars.slideshow?f+"pause":f+"play"),e.pausePlay.bind(i,function(b){b.preventDefault(),""!==j&&j!==b.type||(a(this).hasClass(f+"pause")?(e.manualPause=!0,e.manualPlay=!1,e.pause()):(e.manualPause=!1,e.manualPlay=!0,e.play())),""===j&&(j=b.type),q.setToClearWatchedEvent()})},update:function(a){"play"===a?e.pausePlay.removeClass(f+"pause").addClass(f+"play").html(e.vars.playText):e.pausePlay.removeClass(f+"play").addClass(f+"pause").html(e.vars.pauseText)}},touch:function(){function u(a){a.stopPropagation(),e.animating?a.preventDefault():(e.pause(),c._gesture.addPointer(a.pointerId),t=0,f=l?e.h:e.w,i=Number(new Date),d=n&&m&&e.animatingTo===e.last?0:n&&m?e.limit-(e.itemW+e.vars.itemMargin)*e.move*e.animatingTo:n&&e.currentSlide===e.last?e.limit:n?(e.itemW+e.vars.itemMargin)*e.move*e.currentSlide:m?(e.last-e.currentSlide+e.cloneOffset)*f:(e.currentSlide+e.cloneOffset)*f)}function v(a){a.stopPropagation();var b=a.target._slider;if(b){var e=-a.translationX,g=-a.translationY;return t+=l?g:e,h=t,q=l?Math.abs(t)<Math.abs(-e):Math.abs(t)<Math.abs(-g),a.detail===a.MSGESTURE_FLAG_INERTIA?void setImmediate(function(){c._gesture.stop()}):void((!q||Number(new Date)-i>500)&&(a.preventDefault(),!o&&b.transitions&&(b.vars.animationLoop||(h=t/(0===b.currentSlide&&t<0||b.currentSlide===b.last&&t>0?Math.abs(t)/f+2:1)),b.setProps(d+h,"setTouch"))))}}function w(c){c.stopPropagation();var e=c.target._slider;if(e){if(e.animatingTo===e.currentSlide&&!q&&null!==h){var g=m?-h:h,j=g>0?e.getTarget("next"):e.getTarget("prev");e.canAdvance(j)&&(Number(new Date)-i<550&&Math.abs(g)>50||Math.abs(g)>f/2)?e.flexAnimate(j,e.vars.pauseOnAction):o||e.flexAnimate(e.currentSlide,e.vars.pauseOnAction,!0)}a=null,b=null,h=null,d=null,t=0}}var a,b,d,f,h,i,j,k,p,q=!1,r=0,s=0,t=0;g?(c.style.msTouchAction="none",c._gesture=new MSGesture,c._gesture.target=c,c.addEventListener("MSPointerDown",u,!1),c._slider=e,c.addEventListener("MSGestureChange",v,!1),c.addEventListener("MSGestureEnd",w,!1)):(j=function(g){e.animating?g.preventDefault():(window.navigator.msPointerEnabled||1===g.touches.length)&&(e.pause(),f=l?e.h:e.w,i=Number(new Date),r=g.touches[0].pageX,s=g.touches[0].pageY,d=n&&m&&e.animatingTo===e.last?0:n&&m?e.limit-(e.itemW+e.vars.itemMargin)*e.move*e.animatingTo:n&&e.currentSlide===e.last?e.limit:n?(e.itemW+e.vars.itemMargin)*e.move*e.currentSlide:m?(e.last-e.currentSlide+e.cloneOffset)*f:(e.currentSlide+e.cloneOffset)*f,a=l?s:r,b=l?r:s,c.addEventListener("touchmove",k,!1),c.addEventListener("touchend",p,!1))},k=function(c){r=c.touches[0].pageX,s=c.touches[0].pageY,h=l?a-s:a-r,q=l?Math.abs(h)<Math.abs(r-b):Math.abs(h)<Math.abs(s-b);var g=500;(!q||Number(new Date)-i>g)&&(c.preventDefault(),!o&&e.transitions&&(e.vars.animationLoop||(h/=0===e.currentSlide&&h<0||e.currentSlide===e.last&&h>0?Math.abs(h)/f+2:1),e.setProps(d+h,"setTouch")))},p=function(g){if(c.removeEventListener("touchmove",k,!1),e.animatingTo===e.currentSlide&&!q&&null!==h){var j=m?-h:h,l=j>0?e.getTarget("next"):e.getTarget("prev");e.canAdvance(l)&&(Number(new Date)-i<550&&Math.abs(j)>50||Math.abs(j)>f/2)?e.flexAnimate(l,e.vars.pauseOnAction):o||e.flexAnimate(e.currentSlide,e.vars.pauseOnAction,!0)}c.removeEventListener("touchend",p,!1),a=null,b=null,h=null,d=null},c.addEventListener("touchstart",j,!1))},resize:function(){!e.animating&&e.is(":visible")&&(n||e.doMath(),o?q.smoothHeight():n?(e.slides.width(e.computedW),e.update(e.pagingCount),e.setProps()):l?(e.viewport.height(e.h),e.setProps(e.h,"setTotal")):(e.vars.smoothHeight&&q.smoothHeight(),e.newSlides.width(e.computedW),e.setProps(e.computedW,"setTotal")))},smoothHeight:function(a){if(!l||o){var b=o?e:e.viewport;a?b.animate({height:e.slides.eq(e.animatingTo).innerHeight()},a).css("overflow","visible"):b.innerHeight(e.slides.eq(e.animatingTo).innerHeight())}},sync:function(b){var c=a(e.vars.sync).data("flexslider"),d=e.animatingTo;switch(b){case"animate":c.flexAnimate(d,e.vars.pauseOnAction,!1,!0);break;case"play":c.playing||c.asNav||c.play();break;case"pause":c.pause()}},uniqueID:function(b){return b.filter("[id]").add(b.find("[id]")).each(function(){var b=a(this);b.attr("id",b.attr("id")+"_clone")}),b},pauseInvisible:{visProp:null,init:function(){var a=q.pauseInvisible.getHiddenProp();if(a){var b=a.replace(/[H|h]idden/,"")+"visibilitychange";document.addEventListener(b,function(){q.pauseInvisible.isHidden()?e.startTimeout?clearTimeout(e.startTimeout):e.pause():e.started?e.play():e.vars.initDelay>0?setTimeout(e.play,e.vars.initDelay):e.play()})}},isHidden:function(){var a=q.pauseInvisible.getHiddenProp();return!!a&&document[a]},getHiddenProp:function(){var a=["webkit","moz","ms","o"];if("hidden"in document)return"hidden";for(var b=0;b<a.length;b++)if(a[b]+"Hidden"in document)return a[b]+"Hidden";return null}},setToClearWatchedEvent:function(){clearTimeout(k),k=setTimeout(function(){j=""},3e3)}},e.flexAnimate=function(b,c,d,f,g){if(e.vars.animationLoop||b===e.currentSlide||(e.direction=b>e.currentSlide?"next":"prev"),p&&1===e.pagingCount&&(e.direction=e.currentItem<b?"next":"prev"),!e.animating&&(e.canAdvance(b,g)||d)&&e.is(":visible")){if(p&&f){var i=a(e.vars.asNavFor).data("flexslider");if(e.atEnd=0===b||b===e.count-1,i.flexAnimate(b,!0,!1,!0,g),e.direction=e.currentItem<b?"next":"prev",i.direction=e.direction,Math.ceil((b+1)/e.visible)-1===e.currentSlide||0===b)return e.currentItem=b,e.slideAttributes(b),!1;e.currentItem=b,e.slideAttributes(b),b=Math.floor(b/e.visible)}if(e.animating=!0,e.animatingTo=b,c&&e.pause(),e.vars.before(e),e.syncExists&&!g&&q.sync("animate"),e.vars.controlNav&&q.controlNav.active(),n||e.slideAttributes(b),e.atEnd=0===b||b===e.last,e.vars.directionNav&&q.directionNav.update(),b===e.last&&(e.vars.end(e),e.vars.animationLoop||e.pause()),o)h?(e.slides.eq(e.currentSlide).css({opacity:0,zIndex:1,display:"none"}),e.slides.eq(b).css({opacity:1,zIndex:2,display:"block"}),e.wrapup(j)):(e.slides.eq(e.currentSlide).css({zIndex:1,display:"none"}).animate({opacity:0},e.vars.animationSpeed,e.vars.easing),e.slides.eq(b).css({zIndex:2,display:"block"}).animate({opacity:1},e.vars.animationSpeed,e.vars.easing,e.wrapup));else{var k,r,s,j=l?e.slides.filter(":first").height():e.computedW;n?(k=e.vars.itemMargin,s=(e.itemW+k)*e.move*e.animatingTo,r=s>e.limit&&1!==e.visible?e.limit:s):r=0===e.currentSlide&&b===e.count-1&&e.vars.animationLoop&&"next"!==e.direction?m?(e.count+e.cloneOffset)*j:0:e.currentSlide===e.last&&0===b&&e.vars.animationLoop&&"prev"!==e.direction?m?0:(e.count+1)*j:m?(e.count-1-b+e.cloneOffset)*j:(b+e.cloneOffset)*j,e.setProps(r,"",e.vars.animationSpeed),e.transitions?(e.vars.animationLoop&&e.atEnd||(e.animating=!1,e.currentSlide=e.animatingTo),e.container.unbind("webkitTransitionEnd transitionend"),e.container.bind("webkitTransitionEnd transitionend",function(){clearTimeout(e.ensureAnimationEnd),e.wrapup(j)}),clearTimeout(e.ensureAnimationEnd),e.ensureAnimationEnd=setTimeout(function(){e.wrapup(j)},e.vars.animationSpeed+100)):e.container.animate(e.args,e.vars.animationSpeed,e.vars.easing,function(){e.wrapup(j)})}e.vars.smoothHeight&&q.smoothHeight(e.vars.animationSpeed)}},e.wrapup=function(a){o||n||(0===e.currentSlide&&e.animatingTo===e.last&&e.vars.animationLoop?e.setProps(a,"jumpEnd"):e.currentSlide===e.last&&0===e.animatingTo&&e.vars.animationLoop&&e.setProps(a,"jumpStart")),e.animating=!1,e.currentSlide=e.animatingTo,e.vars.after(e)},e.animateSlides=function(){!e.animating&&b&&e.flexAnimate(e.getTarget("next"))},e.pause=function(){clearInterval(e.animatedSlides),e.animatedSlides=null,e.playing=!1,e.vars.pausePlay&&q.pausePlay.update("play"),e.syncExists&&q.sync("pause")},e.slideAttributes=function(a){f=e.vars.namespace,1==e.vars.useAriaAttributes?(e.slides.removeClass(f+"active-slide").attr("aria-hidden","true").eq(a).addClass(f+"active-slide").removeAttr("aria-hidden"),e.vars.ariaPolite&&(e.slides.find(f+"caption").removeAttr("aria-live"),e.playing===!1&&e.slides.eq(a).find("."+f+"caption").attr("aria-live","polite"))):e.slides.removeClass(f+"active-slide").eq(a).addClass(f+"active-slide")},e.play=function(){e.playing&&clearInterval(e.animatedSlides),e.animatedSlides=e.animatedSlides||setInterval(e.animateSlides,e.vars.slideshowSpeed),e.started=e.playing=!0,e.vars.pausePlay&&q.pausePlay.update("pause"),e.syncExists&&q.sync("play")},e.stop=function(){e.pause(),e.stopped=!0},e.canAdvance=function(a,b){var c=p?e.pagingCount-1:e.last;return!!b||(!(!p||e.currentItem!==e.count-1||0!==a||"prev"!==e.direction)||(!p||0!==e.currentItem||a!==e.pagingCount-1||"next"===e.direction)&&(!(a===e.currentSlide&&!p)&&(!!e.vars.animationLoop||(!e.atEnd||0!==e.currentSlide||a!==c||"next"===e.direction)&&(!e.atEnd||e.currentSlide!==c||0!==a||"next"!==e.direction))))},e.getTarget=function(a){return e.direction=a,"next"===a?e.currentSlide===e.last?0:e.currentSlide+1:0===e.currentSlide?e.last:e.currentSlide-1},e.setProps=function(a,b,c){var d=function(){var c=a?a:(e.itemW+e.vars.itemMargin)*e.move*e.animatingTo,d=function(){if(n)return"setTouch"===b?a:m&&e.animatingTo===e.last?0:m?e.limit-(e.itemW+e.vars.itemMargin)*e.move*e.animatingTo:e.animatingTo===e.last?e.limit:c;switch(b){case"setTotal":return m?(e.count-1-e.currentSlide+e.cloneOffset)*a:(e.currentSlide+e.cloneOffset)*a;case"setTouch":return m?a:a;case"jumpEnd":return m?a:e.count*a;case"jumpStart":return m?e.count*a:a;default:return a}}();return d*-1+"px"}();e.transitions&&(d=l?"translate3d(0,"+d+",0)":"translate3d("+d+",0,0)",c=void 0!==c?c/1e3+"s":"0s",e.container.css("-"+e.pfx+"-transition-duration",c),e.container.css("transition-duration",c)),e.args[e.prop]=d,(e.transitions||void 0===c)&&e.container.css(e.args),e.container.css("transform",d)},e.setup=function(b){if(o)e.slides.css({width:"100%",float:"left",marginRight:"-100%",position:"relative"}),"init"===b&&(h?e.slides.css({opacity:0,display:"none",webkitTransition:"opacity "+e.vars.animationSpeed/1e3+"s ease",zIndex:1}).eq(e.currentSlide).css({opacity:1,zIndex:2,display:"block"}):0==e.vars.fadeFirstSlide?e.slides.css({opacity:0,display:"none",zIndex:1}).eq(e.currentSlide).css({zIndex:2,display:"block"}).css({opacity:1}):e.slides.css({opacity:0,display:"none",zIndex:1}).eq(e.currentSlide).css({zIndex:2,display:"block"}).animate({opacity:1},e.vars.animationSpeed,e.vars.easing)),e.vars.smoothHeight&&q.smoothHeight();else{var c,d;"init"===b&&(e.viewport=a('<div class="'+f+'viewport"></div>').css({overflow:"hidden",position:"relative"}).appendTo(e).append(e.container),e.cloneCount=0,e.cloneOffset=0,m&&(d=a.makeArray(e.slides).reverse(),e.slides=a(d),e.container.empty().append(e.slides))),e.vars.animationLoop&&!n&&(e.cloneCount=2,e.cloneOffset=1,"init"!==b&&e.container.find(".clone").remove(),e.container.append(q.uniqueID(e.slides.first().clone().addClass("clone")).attr("aria-hidden","true")).prepend(q.uniqueID(e.slides.last().clone().addClass("clone")).attr("aria-hidden","true"))),e.newSlides=a(e.vars.selector,e),c=m?e.count-1-e.currentSlide+e.cloneOffset:e.currentSlide+e.cloneOffset,l&&!n?(e.container.height(200*(e.count+e.cloneCount)+"%").css("position","absolute").width("100%"),setTimeout(function(){e.newSlides.css({display:"block"}),e.doMath(),e.viewport.height(e.h),e.setProps(c*e.h,"init")},"init"===b?100:0)):(e.container.width(200*(e.count+e.cloneCount)+"%"),e.setProps(c*e.computedW,"init"),setTimeout(function(){e.doMath(),e.newSlides.css({width:e.computedW,marginRight:e.computedM,float:"left",display:"block"}),e.vars.smoothHeight&&q.smoothHeight()},"init"===b?100:0))}n||e.slideAttributes(e.currentSlide),e.vars.init(e)},e.doMath=function(){var a=e.slides.first(),b=e.vars.itemMargin,c=e.vars.minItems,d=e.vars.maxItems;e.w=void 0===e.viewport?e.width():e.viewport.width(),e.h=a.height(),e.boxPadding=a.outerWidth()-a.width(),n?(e.itemT=e.vars.itemWidth+b,e.itemM=b,e.minW=c?c*e.itemT:e.w,e.maxW=d?d*e.itemT-b:e.w,e.itemW=e.minW>e.w?(e.w-b*(c-1))/c:e.maxW<e.w?(e.w-b*(d-1))/d:e.vars.itemWidth>e.w?e.w:e.vars.itemWidth,e.visible=Math.floor(e.w/e.itemW),e.move=e.vars.move>0&&e.vars.move<e.visible?e.vars.move:e.visible,e.pagingCount=Math.ceil((e.count-e.visible)/e.move+1),e.last=e.pagingCount-1,e.limit=1===e.pagingCount?0:e.vars.itemWidth>e.w?e.itemW*(e.count-1)+b*(e.count-1):(e.itemW+b)*e.count-e.w-b):(e.itemW=e.w,e.itemM=b,e.pagingCount=e.count,e.last=e.count-1),e.computedW=e.itemW-e.boxPadding,e.computedM=e.itemM},e.update=function(a,b){e.doMath(),n||(a<e.currentSlide?e.currentSlide+=1:a<=e.currentSlide&&0!==a&&(e.currentSlide-=1),e.animatingTo=e.currentSlide),e.vars.controlNav&&!e.manualControls&&("add"===b&&!n||e.pagingCount>e.controlNav.length?q.controlNav.update("add"):("remove"===b&&!n||e.pagingCount<e.controlNav.length)&&(n&&e.currentSlide>e.last&&(e.currentSlide-=1,e.animatingTo-=1),q.controlNav.update("remove",e.last))),e.vars.directionNav&&q.directionNav.update()},e.addSlide=function(b,c){var d=a(b);e.count+=1,e.last=e.count-1,l&&m?void 0!==c?e.slides.eq(e.count-c).after(d):e.container.prepend(d):void 0!==c?e.slides.eq(c).before(d):e.container.append(d),e.update(c,"add"),e.slides=a(e.vars.selector+":not(.clone)",e),e.setup(),e.vars.added(e)},e.removeSlide=function(b){var c=isNaN(b)?e.slides.index(a(b)):b;e.count-=1,e.last=e.count-1,isNaN(b)?a(b,e.slides).remove():l&&m?e.slides.eq(e.last).remove():e.slides.eq(b).remove(),e.doMath(),e.update(c,"remove"),e.slides=a(e.vars.selector+":not(.clone)",e),e.setup(),e.vars.removed(e)},q.init()},a(window).blur(function(a){b=!1}).focus(function(a){b=!0}),a.flexslider.defaults={namespace:"flex-",selector:".slides > li",animation:"fade",easing:"swing",direction:"horizontal",reverse:!1,animationLoop:!0,smoothHeight:!1,startAt:0,slideshow:!0,slideshowSpeed:7e3,animationSpeed:600,initDelay:0,randomize:!1,fadeFirstSlide:!0,thumbCaptions:!1,pauseOnAction:!0,pauseOnHover:!1,pauseInvisible:!0,useCSS:!0,touch:!0,video:!1,controlNav:!0,directionNav:!0,prevText:"Previous",nextText:"Next",keyboard:!0,multipleKeyboard:!1,mousewheel:!1,pausePlay:!1,pauseText:"Pause",playText:"Play",controlsContainer:"",manualControls:"",customDirectionNav:"",sync:"",asNavFor:"",itemWidth:0,itemMargin:0,minItems:1,maxItems:0,move:0,allowOneSlide:!0,useAriaAttributes:!0,ariaPolite:!0,start:function(){},before:function(){},after:function(){},end:function(){},added:function(){},removed:function(){},init:function(){}},a.fn.flexslider=function(b){if(void 0===b&&(b={}),"object"==typeof b)return this.each(function(){var c=a(this),d=b.selector?b.selector:".slides > li",e=c.find(d);1===e.length&&b.allowOneSlide===!1||0===e.length?(e.fadeIn(400),b.start&&b.start(c)):void 0===c.data("flexslider")&&new a.flexslider(this,b)});var c=a(this).data("flexslider");switch(b){case"play":c.play();break;case"pause":c.pause();break;case"stop":c.stop();break;case"next":c.flexAnimate(c.getTarget("next"),!0);break;case"prev":case"previous":c.flexAnimate(c.getTarget("prev"),!0);break;default:"number"==typeof b&&c.flexAnimate(b,!0)}}}(jQuery);;/*
 
 ============================================
 License for Application
 ============================================
 
 This license is governed by United States copyright law, and with respect to matters
 of tort, contract, and other causes of action it is governed by North Carolina law,
 without regard to North Carolina choice of law provisions.  The forum for any dispute
 resolution shall be in Wake County, North Carolina.
 
 Redistribution and use in source and binary forms, with or without modification, are
 permitted provided that the following conditions are met:
 
 1. Redistributions of source code must retain the above copyright notice, this list
 of conditions and the following disclaimer.
 
 2. Redistributions in binary form must reproduce the above copyright notice, this
 list of conditions and the following disclaimer in the documentation and/or other
 materials provided with the distribution.
 
 3. The name of the author may not be used to endorse or promote products derived from
 this software without specific prior written permission.
 
 THIS SOFTWARE IS PROVIDED BY THE AUTHOR "AS IS" AND ANY EXPRESS OR IMPLIED
 WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY
 AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE AUTHOR BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
 LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
 THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
 ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 */

// jQuery formatted selector to search for focusable items
var focusableElementsString = "a[href], area[href], input:not([disabled]), select:not([disabled]), textarea:not([disabled]), button:not([disabled]), iframe, object, embed, *[tabindex], *[contenteditable]";

// jQuery formatted selector to search for elements outside modal that should NOT receive focus
var nonFocusableElementsString = "body > .container, body, #page-edit-from-lms, #ccm-page-controls-wrapper";

// store the item that has focus before opening the modal window
var focusedElementBeforeModal;

$(document).ready(function() {
    jQuery('.popup-link').click(function(e) {
        var modal = $(this).attr('href');
        showModal($(modal), $(this));

        jQuery(modal).keydown(function(event) {
            trapTabKey($(this), event);
            trapEscapeKey($(this), event);
        });
    });
    jQuery('.modalCloseButton').click(function(e) {
        hideModal();
        $('.modal').off('keydown');
    });

});

function trapEscapeKey(obj, evt) {

    // if escape pressed
    if (evt.which == 27) {

        // get list of all children elements in given object
        var o = obj.find('*');

        // get list of focusable items
        var cancelElement;
        cancelElement = o.filter(".modalCloseButton");

        // close the modal window
        cancelElement.click();
        evt.preventDefault();
    }

}

function trapTabKey(obj, evt) {

    // if tab or shift-tab pressed
    if (evt.which == 9) {

        // get list of all children elements in given object
        var o = obj.find('*');

        // get list of focusable items
        var focusableItems;
        focusableItems = o.filter(focusableElementsString).filter(':visible');

        // get currently focused item
        var focusedItem;
        focusedItem = jQuery(':focus');

        // get the number of focusable items
        var numberOfFocusableItems;
        numberOfFocusableItems = focusableItems.length;

        // get the index of the currently focused item
        var focusedItemIndex;
        focusedItemIndex = focusableItems.index(focusedItem);

        if (evt.shiftKey) {
            //back tab
            // if focused on first item and user presses back-tab, go to the last focusable item
            if (focusedItemIndex == 0) {
                focusableItems.get(numberOfFocusableItems - 1).focus();
                evt.preventDefault();
            }

        } else {
            //forward tab
            // if focused on the last item and user presses tab, go to the first focusable item
            if (focusedItemIndex == numberOfFocusableItems - 1) {
                focusableItems.get(0).focus();
                evt.preventDefault();
            }
        }

    }

}

function setInitialFocusModal(obj) {
    // get list of all children elements in given object
    var o = obj.find('*');

    // set focus to first focusable item
    var focusableItems;
    focusableItems = o.filter(focusableElementsString).filter(':visible').first().focus();

}

function enterButtonModal() {
    // BEGIN logic for executing the Enter button action for the modal window
    alert('form submitted');
    // END logic for executing the Enter button action for the modal window
    hideModal();
}

function setFocusToFirstItemInModal(obj){
    // get list of all children elements in given object
    var o = obj.find('*');

    // set the focus to the first keyboard focusable item
    window.setTimeout( function(){
        o.filter(focusableElementsString).filter(':visible').first().focus();
    }, 100 );

}

function showModal(obj, targetEle) {
    
    jQuery('body > .container').attr('aria-hidden', 'true'); // mark the main page as hidden

    // Move modal markup after .container
    obj.detach().insertAfter('body > .container, body > .cms_container');

    // Show modal
    jQuery('.modalOverlay').css('display', 'block'); // insert an overlay to prevent clicking and make a visual change to indicate the main apge is not available
    obj.css('display', 'block'); // make the modal window visible
    obj.attr('aria-hidden', 'false'); // mark the modal window as visible
    
    // If the modal is bigger than the height of the browser, make the position absolute 
    var maxHeight = $(window).height() * (1 - (parseInt(obj.css('top'))/100) ); 
    if (obj.height() > maxHeight ) { 
        obj.css('position', 'absolute').css('top', '25%');
    } 

    // attach a listener to redirect the tab to the modal window if the user somehow gets out of the modal window
    jQuery('body').on('focusin',nonFocusableElementsString,function() {
       setFocusToFirstItemInModal(obj);
    });

    // save current focus
    focusedElementBeforeModal = jQuery(':focus');

    setFocusToFirstItemInModal(obj);

    // re-position when modal dialog is in iframe and not in viewport
    if(window.parent){
        var targetTop = $(targetEle).offset().top;
        var modalHeight = $(obj).height();
        var iFrameHeight = $(window).height();

        // orginal top
        var customizeTop = $(obj).offset().top;

        // This case is about modal dialog is not in viewport.
        if(modalHeight < targetTop){
            var totalHeight = targetTop + modalHeight;
            // If total height (offset Top + modalheight) is greater than total height of Iframe
            if( totalHeight > iFrameHeight){
                customizeTop = targetTop - (totalHeight - iFrameHeight) - 50;
            } else {
                customizeTop = targetTop - 50;
}
        } else if($(obj).offset().top + modalHeight < targetTop - 50){
            customizeTop = $(targetEle).offset().top;
        }
        obj.css('top', customizeTop + 'px');
    }

    // Set focus to first div in modal
    obj.find('div[role=document]').attr('tabIndex', '-1').focus();
}

function showModalTop(obj) {

    // jQuery('#mainPage').attr('aria-hidden', 'true'); // mark the main page as hidden
    jQuery('.modalOverlay').css('display', 'block'); // insert an overlay to prevent clicking and make a visual change to indicate the main apge is not available
    obj.css('display', 'block'); // make the modal window visible
    obj.attr('aria-hidden', 'false'); // mark the modal window as visible
    obj.css('top', '25px');

    // If the modal is bigger than the height of the browser, make the position absolute
    var maxHeight = $(window).height() * (1 - (parseInt(obj.css('top'))/100) );
    if (obj.height() > maxHeight ) {
        obj.css('position', 'absolute').css('top', '25px');
    }

    // save current focus
    focusedElementBeforeModal = jQuery(':focus');

    setFocusToFirstItemInModal(obj);
}

function hideModal() {
    jQuery('.modalOverlay').css('display', 'none'); // remove the overlay in order to make the main screen available again
    jQuery('.modal').css('display', 'none'); // hide the modal window
    jQuery('.modal').attr('aria-hidden', 'true'); // mark the modal window as hidden
    jQuery('body > .container').attr('aria-hidden', 'false'); // mark the main page as hidden// mark the main page as visible

    // remove the listener which redirects tab keys in the main content area to the modal
    jQuery('body').off('focusin',nonFocusableElementsString);

    // set focus back to element that had it before the modal was opened
    if (focusedElementBeforeModal && focusedElementBeforeModal.focus) {
    focusedElementBeforeModal.focus();
    }
};$(document).ready(function () {

    function clearPopups(context) {
        $container = $(context).parent();

        $container.find(".ca-image-hotspots-popup").hide().remove();
        $container.children(".ca-image-hotspots-overlay").remove();
    }

    function openHotspotPopup(context) {
        const that = $(context);
        $container = $(context).parent();

        let ca_hotspots_content = '';
        ca_hotspots_content += '<div class="ca-image-hotspots-popup">';
        ca_hotspots_content += '<div class="ca-inner" tabindex="0">';
        ca_hotspots_content += $(context).find(".ca-image-hotspots-content").html();
        ca_hotspots_content += '</div>';
        ca_hotspots_content += '<button class="ca-image-hotspots-close ca-accent1" aria-label="Close" tabindex="0"><span aria-hidden="true">X</span><span class="sr-only">close</span></button>';
        ca_hotspots_content += '</div>';

        clearPopups(context);

        $(context).after(ca_hotspots_content);

        $container.find(".ca-image-hotspots-popup").delay(10).queue(function () {
            let caPosition = $(that).position();
            let boxHight = $(this).height();
            let boxWidth = $(this).width();
            let fixButton = 20;
            let imageInnerWidth = $(that).closest('.image-inner').width();

            if (boxHight > caPosition.top) {
                $(this).css({ top: caPosition.top + fixButton });
            } else {
                $(this).css({ top: caPosition.top - boxHight - fixButton });
            }

            $(this).addClass("active").dequeue();
            let left = caPosition.left - boxWidth / 2;
            if (left < 0) {
                left = 20;
            } else if (imageInnerWidth < caPosition.left + boxWidth / 2 + 20) {
                left = imageInnerWidth - boxWidth - 20;
            }

            $(this).css({ left: left });

            $(this).find('.ca-inner').get(0).focus();

            $(this).find('.startModal').click(function () {
                $(this).closest('.hotspots-inner-content').find('.modal').show();
            });
            
            $(this).find('.modalCloseButton').click(function () {
                $(this).closest('.hotspots-inner-content').find('.modal').hide();
            });

            $(this).on('keydown', function (e) {
                if (e.which == 27) {
                    onHidePopupClick(this);
                }
            });

            // Remove Popup
            $(this).find('.ca-image-hotspots-close').on("click", function () {
                $(this).closest(".ca-image-hotspots-popup").remove();
            });

            $(this).find('.ca-image-hotspots-close').on("keydown", function (e) {
                if (e.which === 13 || e.which === 32) {
                    e.preventDefault();
                    $(this).closest(".ca-image-hotspots-popup").remove();
                }
        });

            // Save hotspot opened
            if ($(that).attr('data-opened') !== "true") {
                $(that).attr('aria-label', $(that).attr('aria-label') + ' previously expanded');
                $(that).attr('data-opened', true);
            }
     });

        // Track interaction get blockID from hotspot transcript div
        //Next 3 lines are scorm specific
        var bID  = $container.next('.hotspot-transcript').attr('data-bid');
        var points = getBlockScorePossible(bID);        
        ca_postGrade(1, bID, 'The student participated in this activity.',points);
    }

    function onHidePopupClick(context) {
        const hotspotButton = $(context).prev();
        hotspotButton.focus();

        clearPopups(context);
    }

    $(".ca-image-hotspots").click(function () {
        openHotspotPopup(this);
    });
   
    $(".ca-image-hotspots").on('keydown', function (e) {
        if (e.which === 27) {
            clearPopups(this);
        } else if (e.which === 13 || e.which === 32) {
            e.preventDefault();
            openHotspotPopup(this);
        }
}); 

    //next 4 lines are scorm specific
    $('.hotspot-transcript a').click(function() {
        var points = getBlockScorePossible($(this).parents('.hotspot-transcript').attr('data-bid'));
        ca_postGrade(1, $(this).parents('.hotspot-transcript').attr('data-bid'), 'The student participated in this activity.',points);
    });
});



!function t(e,i,r){function a(l,s){if(!i[l]){if(!e[l]){var h="function"==typeof require&&require;if(!s&&h)return h(l,!0);if(n)return n(l,!0);var d=new Error("Cannot find module '"+l+"'");throw d.code="MODULE_NOT_FOUND",d}var o=i[l]={exports:{}};e[l][0].call(o.exports,function(t){var i=e[l][1][t];return a(i||t)},o,o.exports,t,e,i,r)}return i[l].exports}for(var n="function"==typeof require&&require,l=0;l<r.length;l++)a(r[l]);return a}({1:[function(t,e,i){function r(t,e){var i={}.hasOwnProperty;for(var r in e)i.call(e,r)&&(t[r]=e[r]);return t}var a,n,l=[].slice,s={}.toString;a=t("presets").presets,n=function(t){return"data:image/svg+xml;base64,"+btoa(t)},function(){var t,e,i;t={head:function(t){return'<?xml version="1.0" encoding="utf-8"?>\n        <svg xmlns="http://www.w3.org/2000/svg" viewBox="'+t+'">'},gradient:function(t,e){var i,r,a,s,h,d,o,u,f,c;for(null==t&&(t=45),null==e&&(e=1),i=l.call(arguments,2),r=[this.head("0 0 100 100")],a=4*i.length+1,t=t*Math.PI/180,s=Math.pow(Math.cos(t),2),h=Math.sqrt(s-Math.pow(s,2)),t>.25*Math.PI&&(h=Math.pow(Math.sin(t),2),s=Math.sqrt(h-Math.pow(h,2))),d=100*s,o=100*h,r.push('<defs><linearGradient id="gradient" x1="0" x2="'+s+'" y1="0" y2="'+h+'">'),u=0;u<a;++u)c=100*(f=u)/(a-1),r.push('<stop offset="'+c+'%" stop-color="'+i[f%i.length]+'"/>');return r.push('</linearGradient></defs>\n<rect x="0" y="0" width="400" height="400" fill="url(#gradient)">\n<animateTransform lbAttributeName="transform" type="translate" from="-'+d+",-"+o+'"\nto="0,0" dur="'+e+'s" repeatCount="indefinite"/></rect></svg>'),n(r.join(""))},stripe:function(t,e,i){var r,a;return null==t&&(t="#b4b4b4"),null==e&&(e="#e6e6e6"),null==i&&(i=1),r=[this.head("0 0 100 100")],r=r.concat(['<rect fill="'+e+'" width="100" height="100"/>',"<g><g>",function(){var e,i=[];for(e=0;e<13;++e)a=e,i.push('<polygon fill="'+t+'" points="'+(20*a-90)+",100 "+(20*a-100)+",100 "+(20*a-60)+",0 "+(20*a-50)+',0 "/>');return i}().join(""),'</g><animateTransform lbAttributeName="transform" type="translate" ','from="0,0" to="20,0" dur="'+i+'s" repeatCount="indefinite"/></g></svg>'].join("")),n(r)},bubble:function(t,e,i,r,a,l){var s,h,d,o,u,f;for(null==t&&(t="#39d"),null==e&&(e="#9cf"),null==i&&(i=15),null==r&&(r=1),null==a&&(a=6),null==l&&(l=1),s=[this.head("0 0 200 200"),'<rect x="0" y="0" width="200" height="200" fill="'+t+'"/>'],h=0;h<i;++h)d=-h/i*r,o=184*Math.random()+8,u=(.7*Math.random()+.3)*a,f=r*(1+.5*Math.random()),s.push(['<circle cx="'+o+'" cy="0" r="'+u+'" fill="none" stroke="'+e+'" stroke-width="'+l+'">','<animate lbAttributeName="cy" values="190;-10" times="0;1" ','dur="'+f+'s" begin="'+d+'s" repeatCount="indefinite"/>',"</circle>",'<circle cx="'+o+'" cy="0" r="'+u+'" fill="none" stroke="'+e+'" stroke-width="'+l+'">','<animate lbAttributeName="cy" values="390;190" times="0;1" ','dur="'+f+'s" begin="'+d+'s" repeatCount="indefinite"/>',"</circle>"].join(""));return n(s.join("")+"</svg>")}},e={queue:{},running:!1,main:function(t){var e,i,r,a,n,l,s=this;e=!1,i=[];for(r in a=this.queue)(l=(n=a[r])(t))||i.push(n),e=e||l;for(r in a=this.queue)n=a[r],i.indexOf(n)>=0&&delete this.queue[r];return e?requestAnimationFrame(function(t){return s.main(t)}):this.running=!1},add:function(t,e){var i=this;if(this.queue[t]||(this.queue[t]=e),!this.running)return this.running=!0,requestAnimationFrame(function(t){return i.main(t)})}},window.ldBar=i=function(i,n){var l,h,d,o,u,f,c,g,p,w,m,k,b,x,y,v,M,q,A,C,S,B,_,L,N=this;null==n&&(n={}),l={xlink:"http://www.w3.org/1999/xlink"},(h="String"===s.call(i).slice(8,-1)?document.querySelector(i):i).ldBar||(h.ldBar=this),~(d=h.getAttribute("class")||"").indexOf("ldBar")||h.setAttribute("class",d+" ldBar"),o="ldBar-"+Math.random().toString(16).substring(2),u={key:o,clip:o+"-clip",filter:o+"-filter",pattern:o+"-pattern",mask:o+"-mask",maskPath:o+"-mask-path"},f=function(t,e){var i,r;t=c(t);for(i in e)r=e[i],"lbAttr"!==i&&t.appendChild(f(i,r||{}));return t.lbAttrs(e.lbAttr||{}),t},c=function(t){return document.createElementNS("http://www.w3.org/2000/svg",t)},(g=document.body.__proto__.__proto__.__proto__).lbText=function(t){return this.appendChild(document.createTextNode(t))},g.lbAttrs=function(t){var e,i,r,a=[];for(e in t)i=t[e],(r=/([^:]+):([^:]+)/.exec(e))&&l[r[1]]?a.push(this.setAttributeNS(l[r[1]],e,i)):a.push(this.setAttribute(e,i));return a},g.lbStyles=function(t){var e,i,r=[];for(e in t)i=t[e],r.push(this.style[e]=i);return r},g.lbAppend=function(t){return this.appendChild(document.createElementNS("http://www.w3.og/2000/svg",t))},g.lbAttr=function(t,e){return null!=e?this.setAttribute(t,e):this.getAttribute(t)},(p={type:"stroke",img:"",path:"M10 10L90 10","fill-dir":"btt",fill:"#25b","fill-background":"#ddd","fill-background-extrude":3,"pattern-size":null,"stroke-dir":"normal",stroke:"#25b","stroke-width":"3","stroke-trail":"#ddd","stroke-trail-width":.5,duration:1,easing:"linear",value:0,"img-size":null,bbox:null,"set-dim":!0,"aspect-ratio":"xMidYMid"}).preset=h.lbAttr("data-preset")||n.preset,null!=p.preset&&r(p,a[p.preset]);for(w in p)(m=m=h.lbAttr("data-"+w))&&(p[w]=m);return r(p,n),p.img&&(p.path=null),k="stroke"===p.type,b=function(e){var i,r;return i=/data:ldbar\/res,([^()]+)\(([^)]+)\)/,(r=i.exec(e))?r=t[r[1]].apply(t,r[2].split(",")):e},p.fill=b(p.fill),p.stroke=b(p.stroke),"false"===p["set-dim"]&&(p["set-dim"]=!1),x={lbAttr:{"xmlns:xlink":"http://www.w3.org/1999/xlink",preserveAspectRatio:p["aspect-ratio"],width:"100%",height:"100%"},defs:{filter:{lbAttr:{id:u.filter,x:-1,y:-1,width:3,height:3},feMorphology:{lbAttr:{operator:+p["fill-background-extrude"]>=0?"dilate":"erode",radius:Math.abs(+p["fill-background-extrude"])}},feColorMatrix:{lbAttr:{values:"0 0 0 0 1    0 0 0 0 1    0 0 0 0 1    0 0 0 1 0",result:"cm"}}},mask:{lbAttr:{id:u.mask},image:{lbAttr:{"xlink:href":p.img,filter:"url(#"+u.filter+")",x:0,y:0,width:100,height:100,preserveAspectRatio:p["aspect-ratio"]}}},g:{mask:{lbAttr:{id:u.maskPath},path:{lbAttr:{d:p.path||"",fill:"#fff",stroke:"#fff",filter:"url(#"+u.filter+")"}}}},clipPath:{lbAttr:{id:u.clip},rect:{lbAttr:{class:"mask",fill:"#000"}}},pattern:{lbAttr:{id:u.pattern,patternUnits:"userSpaceOnUse",x:0,y:0,width:300,height:300},image:{lbAttr:{x:0,y:0,width:300,height:300}}}}},y=f("svg",x),(v=document.createElement("div")).setAttribute("class","ldBar-label"),h.appendChild(y),h.appendChild(v),M=[0,0],q=0,this.fit=function(){var t,e,i,r;if((t=p.bbox)?(e=t.split(" ").map(function(t){return+t.trim()}),e={x:e[0],y:e[1],width:e[2],height:e[3]}):e=M[1].getBBox(),e&&0!==e.width&&0!==e.height||(e={x:0,y:0,width:100,height:100}),i=1.5*Math.max.apply(null,["stroke-width","stroke-trail-width","fill-background-extrude"].map(function(t){return p[t]})),y.lbAttrs({viewBox:[e.x-i,e.y-i,e.width+2*i,e.height+2*i].join(" ")}),p["set-dim"]&&["width","height"].map(function(t){if(!h.style[t]||N.fit[t])return h.style[t]=e[t]+2*i+"px",N.fit[t]=!0}),r=M[0].querySelector("rect"))return r.lbAttrs({x:e.x-i,y:e.y-i,width:e.width+2*i,height:e.height+2*i})},p.path?(M[0]=k?f("g",{path:{lbAttr:{d:p.path,fill:"none",class:"baseline"}}}):f("g",{rect:{lbAttr:{x:0,y:0,width:100,height:100,mask:"url(#"+u.maskPath+")",fill:p["fill-background"],class:"frame"}}}),y.appendChild(M[0]),M[1]=f("g",{path:{lbAttr:{d:p.path,class:k?"mainline":"solid","clip-path":"fill"===p.type?"url(#"+u.clip+")":""}}}),y.appendChild(M[1]),A=M[0].querySelector(k?"path":"rect"),C=M[1].querySelector("path"),k&&C.lbAttrs({fill:"none"}),S=y.querySelector("pattern image"),(B=new Image).addEventListener("load",function(){var t,e;return t=(e=p["pattern-size"])?{width:+e,height:+e}:B.width&&B.height?{width:B.width,height:B.height}:{width:300,height:300},y.querySelector("pattern").lbAttrs({width:t.width,height:t.height}),S.lbAttrs({width:t.width,height:t.height})}),/.+\..+|^data:/.exec(k?p.stroke:p.fill)&&(B.src=k?p.stroke:p.fill,S.lbAttrs({"xlink:href":B.src})),k&&(A.lbAttrs({stroke:p["stroke-trail"],"stroke-width":p["stroke-trail-width"]}),C.lbAttrs({"stroke-width":p["stroke-width"],stroke:/.+\..+|^data:/.exec(p.stroke)?"url(#"+u.pattern+")":p.stroke})),p.fill&&!k&&C.lbAttrs({fill:/.+\..+|^data:/.exec(p.fill)?"url(#"+u.pattern+")":p.fill}),q=C.getTotalLength(),this.fit(),this.inited=!0):p.img&&(L=p["img-size"]?{width:+(_=p["img-size"].split(","))[0],height:+_[1]}:{width:100,height:100},M[0]=f("g",{rect:{lbAttr:{x:0,y:0,width:100,height:100,mask:"url(#"+u.mask+")",fill:p["fill-background"]}}}),y.querySelector("mask image").lbAttrs({width:L.width,height:L.height}),M[1]=f("g",{image:{lbAttr:{width:L.width,height:L.height,x:0,y:0,preserveAspectRatio:p["aspect-ratio"],"clip-path":"fill"===p.type?"url(#"+u.clip+")":"","xlink:href":p.img,class:"solid"}}}),(B=new Image).addEventListener("load",function(){var t,e;return e=p["img-size"]?{width:+(t=p["img-size"].split(","))[0],height:+t[1]}:B.width&&B.height?{width:B.width,height:B.height}:{width:100,height:100},y.querySelector("mask image").lbAttrs({width:e.width,height:e.height}),M[1].querySelector("image").lbAttrs({width:e.width,height:e.height}),N.fit(),N.set(void 0,!1),N.inited=!0}),B.src=p.img,y.appendChild(M[0]),y.appendChild(M[1])),y.lbAttrs({width:"100%",height:"100%"}),this.transition={value:{src:0,des:0},time:{},ease:function(t,e,i,r){return(t/=.5*r)<1?.5*i*t*t+e:(t-=1,.5*-i*(t*(t-2)-1)+e)},handler:function(t){var e,i,r,a,n,l,s,h,d;return null==this.time.src&&(this.time.src=t),e=[this.value.des-this.value.src,.001*(t-this.time.src),+p.duration||1],i=e[0],r=e[1],a=e[2],v.lbTextContent=n=Math.round(this.ease(r,this.value.src,i,a)),k?(l=C,s={"stroke-dasharray":"reverse"===p["stroke-dir"]?"0 "+q*(100-n)*.01+" "+q*n*.01+" 0":.01*n*q+" "+(.01*(100-n)*q+1)}):(h=M[1].getBBox(),s="btt"!==(d=p["fill-dir"])&&d?"ttb"===d?{y:h.y,height:h.height*n*.01,x:h.x,width:h.width}:"ltr"===d?{y:h.y,height:h.height,x:h.x,width:h.width*n*.01}:"rtl"===d?{y:h.y,height:h.height,x:h.x+h.width*(100-n)*.01,width:h.width*n*.01}:void 0:{y:h.y+h.height*(100-n)*.01,height:h.height*n*.01,x:h.x,width:h.width},l=y.querySelector("rect")),l.lbAttrs(s),!(r>=a)||(delete this.time.src,!1)},start:function(t,i,r){var a,n=this;return a=this.value,a.src=t,a.des=i,h.offsetWidth||h.offsetHeight||h.getClientRects().length,r&&(h.offsetWidth||h.offsetHeight||h.getClientRects().length)?e.add(u.key,function(t){return n.handler(t)}):(this.time.src=0,void this.handler(1e3))}},this.set=function(t,e){var i,r;return null==e&&(e=!0),i=this.value||0,null!=t?this.value=t:t=this.value,r=this.value,this.transition.start(i,r,e)},this.set(+p.value||0,!1),this},window.addEventListener("load",function(){var t,e,r,a,n=[];for(t=0,r=(e=document.querySelectorAll(".ldBar")).length;t<r;++t)(a=e[t]).ldBar||n.push(a.ldBar=new i(a));return n},!1)}()},{presets:2}],2:[function(t,e,i){(void 0!==i&&i||this).presets={rainbow:{type:"stroke",path:"M10 10L90 10",stroke:"data:ldbar/res,gradient(0,1,#a551df,#fd51ad,#ff7f82,#ffb874,#ffeb90)",bbox:"10 10 80 0"},energy:{type:"fill",path:"M15 5L85 5A5 5 0 0 1 85 15L15 15A5 5 0 0 1 15 5",stroke:"#f00",fill:"data:ldbar/res,gradient(45,2,#4e9,#8fb,#4e9)","fill-dir":"ltr","fill-background":"#444","fill-background-extrude":1,bbox:"10 5 80 10"},stripe:{type:"fill",path:"M15 5L85 5A5 5 0 0 1 85 15L15 15A5 5 0 0 1 15 5",stroke:"#f00",fill:"data:ldbar/res,stripe(#25b,#58e,1)","fill-dir":"ltr","fill-background":"#ddd","fill-background-extrude":1,bbox:"10 5 80 10"},lbText:{type:"fill",img:'data:image/svg+xml,<svg xmlns="http://www.w3.org/2000/svg" width="70" height="20" viewBox="0 0 70 20"><lbText x="35" y="10" lbText-anchor="middle" dominant-baseline="central" font-family="arial">LOADING</lbText></svg>',"fill-background-extrude":1.3,"pattern-size":100,"fill-dir":"ltr","img-size":"70,20",bbox:"0 0 70 20"},line:{type:"stroke",path:"M10 10L90 10",stroke:"#25b","stroke-width":3,"stroke-trail":"#ddd","stroke-trail-width":1,bbox:"10 10 80 0"},fan:{type:"stroke",path:"M10 90A40 40 0 0 1 90 90","fill-dir":"btt",fill:"#25b","fill-background":"#ddd","fill-background-extrude":3,"stroke-dir":"normal",stroke:"#25b","stroke-width":"3","stroke-trail":"#ddd","stroke-trail-width":.5,bbox:"10 50 80 40"},circle:{type:"stroke",path:"M50 10A40 40 0 0 1 50 90A40 40 0 0 1 50 10","fill-dir":"btt",fill:"#25b","fill-background":"#ddd","fill-background-extrude":3,"stroke-dir":"normal",stroke:"#25b","stroke-width":"3","stroke-trail":"#ddd","stroke-trail-width":.5,bbox:"10 10 80 80"},bubble:{type:"fill",path:"M50 10A40 40 0 0 1 50 90A40 40 0 0 1 50 10","fill-dir":"btt",fill:"data:ldbar/res,bubble(#39d,#cef)","pattern-size":"150","fill-background":"#ddd","fill-background-extrude":2,"stroke-dir":"normal",stroke:"#25b","stroke-width":"3","stroke-trail":"#ddd","stroke-trail-width":.5,bbox:"10 10 80 80"}}},{}]},{},[1]);;!function(e){if("object"==typeof exports&&"undefined"!=typeof module)module.exports=e();else if("function"==typeof define&&define.amd)define([],e);else{("undefined"!=typeof window?window:"undefined"!=typeof global?global:"undefined"!=typeof self?self:this).markdownit=e()}}(function(){return function o(i,a,c){function l(t,e){if(!a[t]){if(!i[t]){var r="function"==typeof require&&require;if(!e&&r)return r(t,!0);if(u)return u(t,!0);var n=new Error("Cannot find module '"+t+"'");throw n.code="MODULE_NOT_FOUND",n}var s=a[t]={exports:{}};i[t][0].call(s.exports,function(e){var r=i[t][1][e];return l(r||e)},s,s.exports,o,i,a,c)}return a[t].exports}for(var u="function"==typeof require&&require,e=0;e<c.length;e++)l(c[e]);return l}({1:[function(e,r,t){"use strict";r.exports=e("entities/maps/entities.json")},{"entities/maps/entities.json":52}],2:[function(e,r,t){"use strict";r.exports=["address","article","aside","base","basefont","blockquote","body","caption","center","col","colgroup","dd","details","dialog","dir","div","dl","dt","fieldset","figcaption","figure","footer","form","frame","frameset","h1","h2","h3","h4","h5","h6","head","header","hr","html","iframe","legend","li","link","main","menu","menuitem","meta","nav","noframes","ol","optgroup","option","p","param","section","source","summary","table","tbody","td","tfoot","th","thead","title","tr","track","ul"]},{}],3:[function(e,r,t){"use strict";var n="<[A-Za-z][A-Za-z0-9\\-]*(?:\\s+[a-zA-Z_:][a-zA-Z0-9:._-]*(?:\\s*=\\s*(?:[^\"'=<>`\\x00-\\x20]+|'[^']*'|\"[^\"]*\"))?)*\\s*\\/?>",s="<\\/[A-Za-z][A-Za-z0-9\\-]*\\s*>",o=new RegExp("^(?:"+n+"|"+s+"|\x3c!----\x3e|\x3c!--(?:-?[^>-])(?:-?[^-])*--\x3e|<[?].*?[?]>|<![A-Z]+\\s+[^>]*>|<!\\[CDATA\\[[\\s\\S]*?\\]\\]>)"),i=new RegExp("^(?:"+n+"|"+s+")");r.exports.HTML_TAG_RE=o,r.exports.HTML_OPEN_CLOSE_TAG_RE=i},{}],4:[function(e,r,t){"use strict";var n=Object.prototype.hasOwnProperty;function i(e,r){return n.call(e,r)}function a(e){return!(55296<=e&&e<=57343)&&(!(64976<=e&&e<=65007)&&(65535!=(65535&e)&&65534!=(65535&e)&&(!(0<=e&&e<=8)&&(11!==e&&(!(14<=e&&e<=31)&&(!(127<=e&&e<=159)&&!(1114111<e)))))))}function c(e){if(65535<e){var r=55296+((e-=65536)>>10),t=56320+(1023&e);return String.fromCharCode(r,t)}return String.fromCharCode(e)}var s=/\\([!"#$%&'()*+,\-.\/:;<=>?@[\\\]^_`{|}~])/g,o=new RegExp(s.source+"|"+/&([a-z#][a-z0-9]{1,31});/gi.source,"gi"),l=/^#((?:x[a-f0-9]{1,8}|[0-9]{1,8}))/i,u=e("./entities");var p=/[&<>"]/,h=/[&<>"]/g,f={"&":"&amp;","<":"&lt;",">":"&gt;",'"':"&quot;"};function d(e){return f[e]}var m=/[.?*+^$[\]\\(){}|-]/g;var _=e("uc.micro/categories/P/regex");t.lib={},t.lib.mdurl=e("mdurl"),t.lib.ucmicro=e("uc.micro"),t.assign=function(t){return Array.prototype.slice.call(arguments,1).forEach(function(r){if(r){if("object"!=typeof r)throw new TypeError(r+"must be object");Object.keys(r).forEach(function(e){t[e]=r[e]})}}),t},t.isString=function(e){return"[object String]"===(r=e,Object.prototype.toString.call(r));var r},t.has=i,t.unescapeMd=function(e){return e.indexOf("\\")<0?e:e.replace(s,"$1")},t.unescapeAll=function(e){return e.indexOf("\\")<0&&e.indexOf("&")<0?e:e.replace(o,function(e,r,t){return r||(n=e,o=0,i(u,s=t)?u[s]:35===s.charCodeAt(0)&&l.test(s)&&a(o="x"===s[1].toLowerCase()?parseInt(s.slice(2),16):parseInt(s.slice(1),10))?c(o):n);var n,s,o})},t.isValidEntityCode=a,t.fromCodePoint=c,t.escapeHtml=function(e){return p.test(e)?e.replace(h,d):e},t.arrayReplaceAt=function(e,r,t){return[].concat(e.slice(0,r),t,e.slice(r+1))},t.isSpace=function(e){switch(e){case 9:case 32:return!0}return!1},t.isWhiteSpace=function(e){if(8192<=e&&e<=8202)return!0;switch(e){case 9:case 10:case 11:case 12:case 13:case 32:case 160:case 5760:case 8239:case 8287:case 12288:return!0}return!1},t.isMdAsciiPunct=function(e){switch(e){case 33:case 34:case 35:case 36:case 37:case 38:case 39:case 40:case 41:case 42:case 43:case 44:case 45:case 46:case 47:case 58:case 59:case 60:case 61:case 62:case 63:case 64:case 91:case 92:case 93:case 94:case 95:case 96:case 123:case 124:case 125:case 126:return!0;default:return!1}},t.isPunctChar=function(e){return _.test(e)},t.escapeRE=function(e){return e.replace(m,"\\$&")},t.normalizeReference=function(e){return e.trim().replace(/\s+/g," ").toUpperCase()}},{"./entities":1,mdurl:58,"uc.micro":65,"uc.micro/categories/P/regex":63}],5:[function(e,r,t){"use strict";t.parseLinkLabel=e("./parse_link_label"),t.parseLinkDestination=e("./parse_link_destination"),t.parseLinkTitle=e("./parse_link_title")},{"./parse_link_destination":6,"./parse_link_label":7,"./parse_link_title":8}],6:[function(e,r,t){"use strict";var a=e("../common/utils").isSpace,c=e("../common/utils").unescapeAll;r.exports=function(e,r,t){var n,s,o=r,i={ok:!1,pos:0,lines:0,str:""};if(60===e.charCodeAt(r)){for(r++;r<t;){if(10===(n=e.charCodeAt(r))||a(n))return i;if(62===n)return i.pos=r+1,i.str=c(e.slice(o+1,r)),i.ok=!0,i;92===n&&r+1<t?r+=2:r++}return i}for(s=0;r<t&&32!==(n=e.charCodeAt(r))&&!(n<32||127===n);)if(92===n&&r+1<t)r+=2;else{if(40===n&&s++,41===n){if(0===s)break;s--}r++}return o===r||0!==s||(i.str=c(e.slice(o,r)),i.lines=0,i.pos=r,i.ok=!0),i}},{"../common/utils":4}],7:[function(e,r,t){"use strict";r.exports=function(e,r,t){var n,s,o,i,a=-1,c=e.posMax,l=e.pos;for(e.pos=r+1,n=1;e.pos<c;){if(93===(o=e.src.charCodeAt(e.pos))&&0===--n){s=!0;break}if(i=e.pos,e.md.inline.skipToken(e),91===o)if(i===e.pos-1)n++;else if(t)return e.pos=l,-1}return s&&(a=e.pos),e.pos=l,a}},{}],8:[function(e,r,t){"use strict";var c=e("../common/utils").unescapeAll;r.exports=function(e,r,t){var n,s,o=0,i=r,a={ok:!1,pos:0,lines:0,str:""};if(t<=r)return a;if(34!==(s=e.charCodeAt(r))&&39!==s&&40!==s)return a;for(r++,40===s&&(s=41);r<t;){if((n=e.charCodeAt(r))===s)return a.pos=r+1,a.lines=o,a.str=c(e.slice(i+1,r)),a.ok=!0,a;10===n?o++:92===n&&r+1<t&&(r++,10===e.charCodeAt(r)&&o++),r++}return a}},{"../common/utils":4}],9:[function(e,r,t){"use strict";var n=e("./common/utils"),s=e("./helpers"),o=e("./renderer"),i=e("./parser_core"),a=e("./parser_block"),c=e("./parser_inline"),l=e("linkify-it"),u=e("mdurl"),p=e("punycode"),h={default:e("./presets/default"),zero:e("./presets/zero"),commonmark:e("./presets/commonmark")},f=/^(vbscript|javascript|file|data):/,d=/^data:image\/(gif|png|jpeg|webp);/;function m(e){var r=e.trim().toLowerCase();return!f.test(r)||!!d.test(r)}var _=["http:","https:","mailto:"];function g(e){var r=u.parse(e,!0);if(r.hostname&&(!r.protocol||0<=_.indexOf(r.protocol)))try{r.hostname=p.toASCII(r.hostname)}catch(e){}return u.encode(u.format(r))}function b(e){var r=u.parse(e,!0);if(r.hostname&&(!r.protocol||0<=_.indexOf(r.protocol)))try{r.hostname=p.toUnicode(r.hostname)}catch(e){}return u.decode(u.format(r))}function k(e,r){if(!(this instanceof k))return new k(e,r);r||n.isString(e)||(r=e||{},e="default"),this.inline=new c,this.block=new a,this.core=new i,this.renderer=new o,this.linkify=new l,this.validateLink=m,this.normalizeLink=g,this.normalizeLinkText=b,this.utils=n,this.helpers=n.assign({},s),this.options={},this.configure(e),r&&this.set(r)}k.prototype.set=function(e){return n.assign(this.options,e),this},k.prototype.configure=function(r){var e,t=this;if(n.isString(r)&&!(r=h[e=r]))throw new Error('Wrong `markdown-it` preset "'+e+'", check name');if(!r)throw new Error("Wrong `markdown-it` preset, can't be empty");return r.options&&t.set(r.options),r.components&&Object.keys(r.components).forEach(function(e){r.components[e].rules&&t[e].ruler.enableOnly(r.components[e].rules),r.components[e].rules2&&t[e].ruler2.enableOnly(r.components[e].rules2)}),this},k.prototype.enable=function(r,e){var t=[];Array.isArray(r)||(r=[r]),["core","block","inline"].forEach(function(e){t=t.concat(this[e].ruler.enable(r,!0))},this),t=t.concat(this.inline.ruler2.enable(r,!0));var n=r.filter(function(e){return t.indexOf(e)<0});if(n.length&&!e)throw new Error("MarkdownIt. Failed to enable unknown rule(s): "+n);return this},k.prototype.disable=function(r,e){var t=[];Array.isArray(r)||(r=[r]),["core","block","inline"].forEach(function(e){t=t.concat(this[e].ruler.disable(r,!0))},this),t=t.concat(this.inline.ruler2.disable(r,!0));var n=r.filter(function(e){return t.indexOf(e)<0});if(n.length&&!e)throw new Error("MarkdownIt. Failed to disable unknown rule(s): "+n);return this},k.prototype.use=function(e){var r=[this].concat(Array.prototype.slice.call(arguments,1));return e.apply(e,r),this},k.prototype.parse=function(e,r){if("string"!=typeof e)throw new Error("Input data should be a String");var t=new this.core.State(e,this,r);return this.core.process(t),t.tokens},k.prototype.render=function(e,r){return r=r||{},this.renderer.render(this.parse(e,r),this.options,r)},k.prototype.parseInline=function(e,r){var t=new this.core.State(e,this,r);return t.inlineMode=!0,this.core.process(t),t.tokens},k.prototype.renderInline=function(e,r){return r=r||{},this.renderer.render(this.parseInline(e,r),this.options,r)},r.exports=k},{"./common/utils":4,"./helpers":5,"./parser_block":10,"./parser_core":11,"./parser_inline":12,"./presets/commonmark":13,"./presets/default":14,"./presets/zero":15,"./renderer":16,"linkify-it":53,mdurl:58,punycode:60}],10:[function(e,r,t){"use strict";var n=e("./ruler"),s=[["table",e("./rules_block/table"),["paragraph","reference"]],["code",e("./rules_block/code")],["fence",e("./rules_block/fence"),["paragraph","reference","blockquote","list"]],["blockquote",e("./rules_block/blockquote"),["paragraph","reference","blockquote","list"]],["hr",e("./rules_block/hr"),["paragraph","reference","blockquote","list"]],["list",e("./rules_block/list"),["paragraph","reference","blockquote"]],["reference",e("./rules_block/reference")],["heading",e("./rules_block/heading"),["paragraph","reference","blockquote"]],["lheading",e("./rules_block/lheading")],["html_block",e("./rules_block/html_block"),["paragraph","reference","blockquote"]],["paragraph",e("./rules_block/paragraph")]];function o(){this.ruler=new n;for(var e=0;e<s.length;e++)this.ruler.push(s[e][0],s[e][1],{alt:(s[e][2]||[]).slice()})}o.prototype.tokenize=function(e,r,t){for(var n,s=this.ruler.getRules(""),o=s.length,i=r,a=!1,c=e.md.options.maxNesting;i<t&&(e.line=i=e.skipEmptyLines(i),!(t<=i))&&!(e.sCount[i]<e.blkIndent);){if(e.level>=c){e.line=t;break}for(n=0;n<o&&!s[n](e,i,t,!1);n++);e.tight=!a,e.isEmpty(e.line-1)&&(a=!0),(i=e.line)<t&&e.isEmpty(i)&&(a=!0,i++,e.line=i)}},o.prototype.parse=function(e,r,t,n){var s;e&&(s=new this.State(e,r,t,n),this.tokenize(s,s.line,s.lineMax))},o.prototype.State=e("./rules_block/state_block"),r.exports=o},{"./ruler":17,"./rules_block/blockquote":18,"./rules_block/code":19,"./rules_block/fence":20,"./rules_block/heading":21,"./rules_block/hr":22,"./rules_block/html_block":23,"./rules_block/lheading":24,"./rules_block/list":25,"./rules_block/paragraph":26,"./rules_block/reference":27,"./rules_block/state_block":28,"./rules_block/table":29}],11:[function(e,r,t){"use strict";var n=e("./ruler"),s=[["normalize",e("./rules_core/normalize")],["block",e("./rules_core/block")],["inline",e("./rules_core/inline")],["linkify",e("./rules_core/linkify")],["replacements",e("./rules_core/replacements")],["smartquotes",e("./rules_core/smartquotes")]];function o(){this.ruler=new n;for(var e=0;e<s.length;e++)this.ruler.push(s[e][0],s[e][1])}o.prototype.process=function(e){var r,t,n;for(r=0,t=(n=this.ruler.getRules("")).length;r<t;r++)n[r](e)},o.prototype.State=e("./rules_core/state_core"),r.exports=o},{"./ruler":17,"./rules_core/block":30,"./rules_core/inline":31,"./rules_core/linkify":32,"./rules_core/normalize":33,"./rules_core/replacements":34,"./rules_core/smartquotes":35,"./rules_core/state_core":36}],12:[function(e,r,t){"use strict";var n=e("./ruler"),s=[["text",e("./rules_inline/text")],["newline",e("./rules_inline/newline")],["escape",e("./rules_inline/escape")],["backticks",e("./rules_inline/backticks")],["strikethrough",e("./rules_inline/strikethrough").tokenize],["emphasis",e("./rules_inline/emphasis").tokenize],["link",e("./rules_inline/link")],["image",e("./rules_inline/image")],["autolink",e("./rules_inline/autolink")],["html_inline",e("./rules_inline/html_inline")],["entity",e("./rules_inline/entity")]],o=[["balance_pairs",e("./rules_inline/balance_pairs")],["strikethrough",e("./rules_inline/strikethrough").postProcess],["emphasis",e("./rules_inline/emphasis").postProcess],["text_collapse",e("./rules_inline/text_collapse")]];function i(){var e;for(this.ruler=new n,e=0;e<s.length;e++)this.ruler.push(s[e][0],s[e][1]);for(this.ruler2=new n,e=0;e<o.length;e++)this.ruler2.push(o[e][0],o[e][1])}i.prototype.skipToken=function(e){var r,t,n=e.pos,s=this.ruler.getRules(""),o=s.length,i=e.md.options.maxNesting,a=e.cache;if(void 0===a[n]){if(e.level<i)for(t=0;t<o&&(e.level++,r=s[t](e,!0),e.level--,!r);t++);else e.pos=e.posMax;r||e.pos++,a[n]=e.pos}else e.pos=a[n]},i.prototype.tokenize=function(e){for(var r,t,n=this.ruler.getRules(""),s=n.length,o=e.posMax,i=e.md.options.maxNesting;e.pos<o;){if(e.level<i)for(t=0;t<s&&!(r=n[t](e,!1));t++);if(r){if(e.pos>=o)break}else e.pending+=e.src[e.pos++]}e.pending&&e.pushPending()},i.prototype.parse=function(e,r,t,n){var s,o,i,a=new this.State(e,r,t,n);for(this.tokenize(a),i=(o=this.ruler2.getRules("")).length,s=0;s<i;s++)o[s](a)},i.prototype.State=e("./rules_inline/state_inline"),r.exports=i},{"./ruler":17,"./rules_inline/autolink":37,"./rules_inline/backticks":38,"./rules_inline/balance_pairs":39,"./rules_inline/emphasis":40,"./rules_inline/entity":41,"./rules_inline/escape":42,"./rules_inline/html_inline":43,"./rules_inline/image":44,"./rules_inline/link":45,"./rules_inline/newline":46,"./rules_inline/state_inline":47,"./rules_inline/strikethrough":48,"./rules_inline/text":49,"./rules_inline/text_collapse":50}],13:[function(e,r,t){"use strict";r.exports={options:{html:!0,xhtmlOut:!0,breaks:!1,langPrefix:"language-",linkify:!1,typographer:!1,quotes:"\u201c\u201d\u2018\u2019",highlight:null,maxNesting:20},components:{core:{rules:["normalize","block","inline"]},block:{rules:["blockquote","code","fence","heading","hr","html_block","lheading","list","reference","paragraph"]},inline:{rules:["autolink","backticks","emphasis","entity","escape","html_inline","image","link","newline","text"],rules2:["balance_pairs","emphasis","text_collapse"]}}}},{}],14:[function(e,r,t){"use strict";r.exports={options:{html:!1,xhtmlOut:!1,breaks:!1,langPrefix:"language-",linkify:!1,typographer:!1,quotes:"\u201c\u201d\u2018\u2019",highlight:null,maxNesting:100},components:{core:{},block:{},inline:{}}}},{}],15:[function(e,r,t){"use strict";r.exports={options:{html:!1,xhtmlOut:!1,breaks:!1,langPrefix:"language-",linkify:!1,typographer:!1,quotes:"\u201c\u201d\u2018\u2019",highlight:null,maxNesting:20},components:{core:{rules:["normalize","block","inline"]},block:{rules:["paragraph"]},inline:{rules:["text"],rules2:["balance_pairs","text_collapse"]}}}},{}],16:[function(e,r,t){"use strict";var n=e("./common/utils").assign,h=e("./common/utils").unescapeAll,f=e("./common/utils").escapeHtml,s={};function o(){this.rules=n({},s)}s.code_inline=function(e,r,t,n,s){var o=e[r];return"<code"+s.renderAttrs(o)+">"+f(e[r].content)+"</code>"},s.code_block=function(e,r,t,n,s){var o=e[r];return"<pre"+s.renderAttrs(o)+"><code>"+f(e[r].content)+"</code></pre>\n"},s.fence=function(e,r,t,n,s){var o,i,a,c,l=e[r],u=l.info?h(l.info).trim():"",p="";return u&&(p=u.split(/\s+/g)[0]),0===(o=t.highlight&&t.highlight(l.content,p)||f(l.content)).indexOf("<pre")?o+"\n":u?(i=l.attrIndex("class"),a=l.attrs?l.attrs.slice():[],i<0?a.push(["class",t.langPrefix+p]):a[i][1]+=" "+t.langPrefix+p,c={attrs:a},"<pre><code"+s.renderAttrs(c)+">"+o+"</code></pre>\n"):"<pre><code"+s.renderAttrs(l)+">"+o+"</code></pre>\n"},s.image=function(e,r,t,n,s){var o=e[r];return o.attrs[o.attrIndex("alt")][1]=s.renderInlineAsText(o.children,t,n),s.renderToken(e,r,t)},s.hardbreak=function(e,r,t){return t.xhtmlOut?"<br />\n":"<br>\n"},s.softbreak=function(e,r,t){return t.breaks?t.xhtmlOut?"<br />\n":"<br>\n":"\n"},s.text=function(e,r){return f(e[r].content)},s.html_block=function(e,r){return e[r].content},s.html_inline=function(e,r){return e[r].content},o.prototype.renderAttrs=function(e){var r,t,n;if(!e.attrs)return"";for(n="",r=0,t=e.attrs.length;r<t;r++)n+=" "+f(e.attrs[r][0])+'="'+f(e.attrs[r][1])+'"';return n},o.prototype.renderToken=function(e,r,t){var n,s="",o=!1,i=e[r];return i.hidden?"":(i.block&&-1!==i.nesting&&r&&e[r-1].hidden&&(s+="\n"),s+=(-1===i.nesting?"</":"<")+i.tag,s+=this.renderAttrs(i),0===i.nesting&&t.xhtmlOut&&(s+=" /"),i.block&&(o=!0,1===i.nesting&&r+1<e.length&&("inline"===(n=e[r+1]).type||n.hidden?o=!1:-1===n.nesting&&n.tag===i.tag&&(o=!1))),s+=o?">\n":">")},o.prototype.renderInline=function(e,r,t){for(var n,s="",o=this.rules,i=0,a=e.length;i<a;i++)void 0!==o[n=e[i].type]?s+=o[n](e,i,r,t,this):s+=this.renderToken(e,i,r);return s},o.prototype.renderInlineAsText=function(e,r,t){for(var n="",s=0,o=e.length;s<o;s++)"text"===e[s].type?n+=e[s].content:"image"===e[s].type&&(n+=this.renderInlineAsText(e[s].children,r,t));return n},o.prototype.render=function(e,r,t){var n,s,o,i="",a=this.rules;for(n=0,s=e.length;n<s;n++)"inline"===(o=e[n].type)?i+=this.renderInline(e[n].children,r,t):void 0!==a[o]?i+=a[e[n].type](e,n,r,t,this):i+=this.renderToken(e,n,r,t);return i},r.exports=o},{"./common/utils":4}],17:[function(e,r,t){"use strict";function n(){this.__rules__=[],this.__cache__=null}n.prototype.__find__=function(e){for(var r=0;r<this.__rules__.length;r++)if(this.__rules__[r].name===e)return r;return-1},n.prototype.__compile__=function(){var t=this,r=[""];t.__rules__.forEach(function(e){e.enabled&&e.alt.forEach(function(e){r.indexOf(e)<0&&r.push(e)})}),t.__cache__={},r.forEach(function(r){t.__cache__[r]=[],t.__rules__.forEach(function(e){e.enabled&&(r&&e.alt.indexOf(r)<0||t.__cache__[r].push(e.fn))})})},n.prototype.at=function(e,r,t){var n=this.__find__(e),s=t||{};if(-1===n)throw new Error("Parser rule not found: "+e);this.__rules__[n].fn=r,this.__rules__[n].alt=s.alt||[],this.__cache__=null},n.prototype.before=function(e,r,t,n){var s=this.__find__(e),o=n||{};if(-1===s)throw new Error("Parser rule not found: "+e);this.__rules__.splice(s,0,{name:r,enabled:!0,fn:t,alt:o.alt||[]}),this.__cache__=null},n.prototype.after=function(e,r,t,n){var s=this.__find__(e),o=n||{};if(-1===s)throw new Error("Parser rule not found: "+e);this.__rules__.splice(s+1,0,{name:r,enabled:!0,fn:t,alt:o.alt||[]}),this.__cache__=null},n.prototype.push=function(e,r,t){var n=t||{};this.__rules__.push({name:e,enabled:!0,fn:r,alt:n.alt||[]}),this.__cache__=null},n.prototype.enable=function(e,t){Array.isArray(e)||(e=[e]);var n=[];return e.forEach(function(e){var r=this.__find__(e);if(r<0){if(t)return;throw new Error("Rules manager: invalid rule name "+e)}this.__rules__[r].enabled=!0,n.push(e)},this),this.__cache__=null,n},n.prototype.enableOnly=function(e,r){Array.isArray(e)||(e=[e]),this.__rules__.forEach(function(e){e.enabled=!1}),this.enable(e,r)},n.prototype.disable=function(e,t){Array.isArray(e)||(e=[e]);var n=[];return e.forEach(function(e){var r=this.__find__(e);if(r<0){if(t)return;throw new Error("Rules manager: invalid rule name "+e)}this.__rules__[r].enabled=!1,n.push(e)},this),this.__cache__=null,n},n.prototype.getRules=function(e){return null===this.__cache__&&this.__compile__(),this.__cache__[e]||[]},r.exports=n},{}],18:[function(e,r,t){"use strict";var E=e("../common/utils").isSpace;r.exports=function(e,r,t,n){var s,o,i,a,c,l,u,p,h,f,d,m,_,g,b,k,v,x,y,C,A=e.lineMax,w=e.bMarks[r]+e.tShift[r],D=e.eMarks[r];if(4<=e.sCount[r]-e.blkIndent)return!1;if(62!==e.src.charCodeAt(w++))return!1;if(n)return!0;for(a=h=e.sCount[r]+w-(e.bMarks[r]+e.tShift[r]),32===e.src.charCodeAt(w)?(w++,a++,h++,k=!(s=!1)):9===e.src.charCodeAt(w)?(k=!0,(e.bsCount[r]+h)%4==3?(w++,a++,h++,s=!1):s=!0):k=!1,f=[e.bMarks[r]],e.bMarks[r]=w;w<D&&(o=e.src.charCodeAt(w),E(o));)9===o?h+=4-(h+e.bsCount[r]+(s?1:0))%4:h++,w++;for(d=[e.bsCount[r]],e.bsCount[r]=e.sCount[r]+1+(k?1:0),l=D<=w,g=[e.sCount[r]],e.sCount[r]=h-a,b=[e.tShift[r]],e.tShift[r]=w-e.bMarks[r],x=e.md.block.ruler.getRules("blockquote"),_=e.parentType,C=!(e.parentType="blockquote"),p=r+1;p<t&&(e.sCount[p]<e.blkIndent&&(C=!0),w=e.bMarks[p]+e.tShift[p],!((D=e.eMarks[p])<=w));p++)if(62!==e.src.charCodeAt(w++)||C){if(l)break;for(v=!1,i=0,c=x.length;i<c;i++)if(x[i](e,p,t,!0)){v=!0;break}if(v){e.lineMax=p,0!==e.blkIndent&&(f.push(e.bMarks[p]),d.push(e.bsCount[p]),b.push(e.tShift[p]),g.push(e.sCount[p]),e.sCount[p]-=e.blkIndent);break}f.push(e.bMarks[p]),d.push(e.bsCount[p]),b.push(e.tShift[p]),g.push(e.sCount[p]),e.sCount[p]=-1}else{for(a=h=e.sCount[p]+w-(e.bMarks[p]+e.tShift[p]),32===e.src.charCodeAt(w)?(w++,a++,h++,k=!(s=!1)):9===e.src.charCodeAt(w)?(k=!0,(e.bsCount[p]+h)%4==3?(w++,a++,h++,s=!1):s=!0):k=!1,f.push(e.bMarks[p]),e.bMarks[p]=w;w<D&&(o=e.src.charCodeAt(w),E(o));)9===o?h+=4-(h+e.bsCount[p]+(s?1:0))%4:h++,w++;l=D<=w,d.push(e.bsCount[p]),e.bsCount[p]=e.sCount[p]+1+(k?1:0),g.push(e.sCount[p]),e.sCount[p]=h-a,b.push(e.tShift[p]),e.tShift[p]=w-e.bMarks[p]}for(m=e.blkIndent,e.blkIndent=0,(y=e.push("blockquote_open","blockquote",1)).markup=">",y.map=u=[r,0],e.md.block.tokenize(e,r,p),(y=e.push("blockquote_close","blockquote",-1)).markup=">",e.lineMax=A,e.parentType=_,u[1]=e.line,i=0;i<b.length;i++)e.bMarks[i+r]=f[i],e.tShift[i+r]=b[i],e.sCount[i+r]=g[i],e.bsCount[i+r]=d[i];return e.blkIndent=m,!0}},{"../common/utils":4}],19:[function(e,r,t){"use strict";r.exports=function(e,r,t){var n,s,o;if(e.sCount[r]-e.blkIndent<4)return!1;for(s=n=r+1;n<t;)if(e.isEmpty(n))n++;else{if(!(4<=e.sCount[n]-e.blkIndent))break;s=++n}return e.line=s,(o=e.push("code_block","code",0)).content=e.getLines(r,s,4+e.blkIndent,!0),o.map=[r,e.line],!0}},{}],20:[function(e,r,t){"use strict";r.exports=function(e,r,t,n){var s,o,i,a,c,l,u,p=!1,h=e.bMarks[r]+e.tShift[r],f=e.eMarks[r];if(4<=e.sCount[r]-e.blkIndent)return!1;if(f<h+3)return!1;if(126!==(s=e.src.charCodeAt(h))&&96!==s)return!1;if(c=h,(o=(h=e.skipChars(h,s))-c)<3)return!1;if(u=e.src.slice(c,h),0<=(i=e.src.slice(h,f)).indexOf(String.fromCharCode(s)))return!1;if(n)return!0;for(a=r;!(t<=++a)&&!((h=c=e.bMarks[a]+e.tShift[a])<(f=e.eMarks[a])&&e.sCount[a]<e.blkIndent);)if(e.src.charCodeAt(h)===s&&!(4<=e.sCount[a]-e.blkIndent||(h=e.skipChars(h,s))-c<o||(h=e.skipSpaces(h))<f)){p=!0;break}return o=e.sCount[r],e.line=a+(p?1:0),(l=e.push("fence","code",0)).info=i,l.content=e.getLines(r+1,a,o,!0),l.markup=u,l.map=[r,e.line],!0}},{}],21:[function(e,r,t){"use strict";var u=e("../common/utils").isSpace;r.exports=function(e,r,t,n){var s,o,i,a,c=e.bMarks[r]+e.tShift[r],l=e.eMarks[r];if(4<=e.sCount[r]-e.blkIndent)return!1;if(35!==(s=e.src.charCodeAt(c))||l<=c)return!1;for(o=1,s=e.src.charCodeAt(++c);35===s&&c<l&&o<=6;)o++,s=e.src.charCodeAt(++c);return!(6<o||c<l&&!u(s))&&(n||(l=e.skipSpacesBack(l,c),c<(i=e.skipCharsBack(l,35,c))&&u(e.src.charCodeAt(i-1))&&(l=i),e.line=r+1,(a=e.push("heading_open","h"+String(o),1)).markup="########".slice(0,o),a.map=[r,e.line],(a=e.push("inline","",0)).content=e.src.slice(c,l).trim(),a.map=[r,e.line],a.children=[],(a=e.push("heading_close","h"+String(o),-1)).markup="########".slice(0,o)),!0)}},{"../common/utils":4}],22:[function(e,r,t){"use strict";var u=e("../common/utils").isSpace;r.exports=function(e,r,t,n){var s,o,i,a,c=e.bMarks[r]+e.tShift[r],l=e.eMarks[r];if(4<=e.sCount[r]-e.blkIndent)return!1;if(42!==(s=e.src.charCodeAt(c++))&&45!==s&&95!==s)return!1;for(o=1;c<l;){if((i=e.src.charCodeAt(c++))!==s&&!u(i))return!1;i===s&&o++}return!(o<3)&&(n||(e.line=r+1,(a=e.push("hr","hr",0)).map=[r,e.line],a.markup=Array(o+1).join(String.fromCharCode(s))),!0)}},{"../common/utils":4}],23:[function(e,r,t){"use strict";var n=e("../common/html_blocks"),s=e("../common/html_re").HTML_OPEN_CLOSE_TAG_RE,u=[[/^<(script|pre|style)(?=(\s|>|$))/i,/<\/(script|pre|style)>/i,!0],[/^<!--/,/-->/,!0],[/^<\?/,/\?>/,!0],[/^<![A-Z]/,/>/,!0],[/^<!\[CDATA\[/,/\]\]>/,!0],[new RegExp("^</?("+n.join("|")+")(?=(\\s|/?>|$))","i"),/^$/,!0],[new RegExp(s.source+"\\s*$"),/^$/,!1]];r.exports=function(e,r,t,n){var s,o,i,a,c=e.bMarks[r]+e.tShift[r],l=e.eMarks[r];if(4<=e.sCount[r]-e.blkIndent)return!1;if(!e.md.options.html)return!1;if(60!==e.src.charCodeAt(c))return!1;for(a=e.src.slice(c,l),s=0;s<u.length&&!u[s][0].test(a);s++);if(s===u.length)return!1;if(n)return u[s][2];if(o=r+1,!u[s][1].test(a))for(;o<t&&!(e.sCount[o]<e.blkIndent);o++)if(c=e.bMarks[o]+e.tShift[o],l=e.eMarks[o],a=e.src.slice(c,l),u[s][1].test(a)){0!==a.length&&o++;break}return e.line=o,(i=e.push("html_block","",0)).map=[r,o],i.content=e.getLines(r,o,e.blkIndent,!0),!0}},{"../common/html_blocks":2,"../common/html_re":3}],24:[function(e,r,t){"use strict";r.exports=function(e,r,t){var n,s,o,i,a,c,l,u,p,h,f=r+1,d=e.md.block.ruler.getRules("paragraph");if(4<=e.sCount[r]-e.blkIndent)return!1;for(h=e.parentType,e.parentType="paragraph";f<t&&!e.isEmpty(f);f++)if(!(3<e.sCount[f]-e.blkIndent)){if(e.sCount[f]>=e.blkIndent&&(c=e.bMarks[f]+e.tShift[f])<(l=e.eMarks[f])&&(45===(p=e.src.charCodeAt(c))||61===p)&&(c=e.skipChars(c,p),l<=(c=e.skipSpaces(c)))){u=61===p?1:2;break}if(!(e.sCount[f]<0)){for(s=!1,o=0,i=d.length;o<i;o++)if(d[o](e,f,t,!0)){s=!0;break}if(s)break}}return!!u&&(n=e.getLines(r,f,e.blkIndent,!1).trim(),e.line=f+1,(a=e.push("heading_open","h"+String(u),1)).markup=String.fromCharCode(p),a.map=[r,e.line],(a=e.push("inline","",0)).content=n,a.map=[r,e.line-1],a.children=[],(a=e.push("heading_close","h"+String(u),-1)).markup=String.fromCharCode(p),e.parentType=h,!0)}},{}],25:[function(e,r,t){"use strict";var i=e("../common/utils").isSpace;function I(e,r){var t,n,s,o;return n=e.bMarks[r]+e.tShift[r],s=e.eMarks[r],42!==(t=e.src.charCodeAt(n++))&&45!==t&&43!==t?-1:n<s&&(o=e.src.charCodeAt(n),!i(o))?-1:n}function R(e,r){var t,n=e.bMarks[r]+e.tShift[r],s=n,o=e.eMarks[r];if(o<=s+1)return-1;if((t=e.src.charCodeAt(s++))<48||57<t)return-1;for(;;){if(o<=s)return-1;if(!(48<=(t=e.src.charCodeAt(s++))&&t<=57)){if(41===t||46===t)break;return-1}if(10<=s-n)return-1}return s<o&&(t=e.src.charCodeAt(s),!i(t))?-1:s}r.exports=function(e,r,t,n){var s,o,i,a,c,l,u,p,h,f,d,m,_,g,b,k,v,x,y,C,A,w,D,E,q,S,F,L,z=!1,T=!0;if(4<=e.sCount[r]-e.blkIndent)return!1;if(n&&"paragraph"===e.parentType&&e.tShift[r]>=e.blkIndent&&(z=!0),0<=(D=R(e,r))){if(u=!0,q=e.bMarks[r]+e.tShift[r],_=Number(e.src.substr(q,D-q-1)),z&&1!==_)return!1}else{if(!(0<=(D=I(e,r))))return!1;u=!1}if(z&&e.skipSpaces(D)>=e.eMarks[r])return!1;if(m=e.src.charCodeAt(D-1),n)return!0;for(d=e.tokens.length,u?(L=e.push("ordered_list_open","ol",1),1!==_&&(L.attrs=[["start",_]])):L=e.push("bullet_list_open","ul",1),L.map=f=[r,0],L.markup=String.fromCharCode(m),b=r,E=!1,F=e.md.block.ruler.getRules("list"),y=e.parentType,e.parentType="list";b<t;){for(w=D,g=e.eMarks[b],l=k=e.sCount[b]+D-(e.bMarks[r]+e.tShift[r]);w<g;){if(9===(s=e.src.charCodeAt(w)))k+=4-(k+e.bsCount[b])%4;else{if(32!==s)break;k++}w++}if(4<(c=g<=(o=w)?1:k-l)&&(c=1),a=l+c,(L=e.push("list_item_open","li",1)).markup=String.fromCharCode(m),L.map=p=[r,0],v=e.blkIndent,A=e.tight,C=e.tShift[r],x=e.sCount[r],e.blkIndent=a,e.tight=!0,e.tShift[r]=o-e.bMarks[r],e.sCount[r]=k,g<=o&&e.isEmpty(r+1)?e.line=Math.min(e.line+2,t):e.md.block.tokenize(e,r,t,!0),e.tight&&!E||(T=!1),E=1<e.line-r&&e.isEmpty(e.line-1),e.blkIndent=v,e.tShift[r]=C,e.sCount[r]=x,e.tight=A,(L=e.push("list_item_close","li",-1)).markup=String.fromCharCode(m),b=r=e.line,p[1]=b,o=e.bMarks[r],t<=b)break;if(e.sCount[b]<e.blkIndent)break;for(S=!1,i=0,h=F.length;i<h;i++)if(F[i](e,b,t,!0)){S=!0;break}if(S)break;if(u){if((D=R(e,b))<0)break}else if((D=I(e,b))<0)break;if(m!==e.src.charCodeAt(D-1))break}return(L=u?e.push("ordered_list_close","ol",-1):e.push("bullet_list_close","ul",-1)).markup=String.fromCharCode(m),f[1]=b,e.line=b,e.parentType=y,T&&function(e,r){var t,n,s=e.level+2;for(t=r+2,n=e.tokens.length-2;t<n;t++)e.tokens[t].level===s&&"paragraph_open"===e.tokens[t].type&&(e.tokens[t+2].hidden=!0,e.tokens[t].hidden=!0,t+=2)}(e,d),!0}},{"../common/utils":4}],26:[function(e,r,t){"use strict";r.exports=function(e,r){var t,n,s,o,i,a,c=r+1,l=e.md.block.ruler.getRules("paragraph"),u=e.lineMax;for(a=e.parentType,e.parentType="paragraph";c<u&&!e.isEmpty(c);c++)if(!(3<e.sCount[c]-e.blkIndent||e.sCount[c]<0)){for(n=!1,s=0,o=l.length;s<o;s++)if(l[s](e,c,u,!0)){n=!0;break}if(n)break}return t=e.getLines(r,c,e.blkIndent,!1).trim(),e.line=c,(i=e.push("paragraph_open","p",1)).map=[r,e.line],(i=e.push("inline","",0)).content=t,i.map=[r,e.line],i.children=[],i=e.push("paragraph_close","p",-1),e.parentType=a,!0}},{}],27:[function(e,r,t){"use strict";var A=e("../common/utils").normalizeReference,w=e("../common/utils").isSpace;r.exports=function(e,r,t,n){var s,o,i,a,c,l,u,p,h,f,d,m,_,g,b,k,v=0,x=e.bMarks[r]+e.tShift[r],y=e.eMarks[r],C=r+1;if(4<=e.sCount[r]-e.blkIndent)return!1;if(91!==e.src.charCodeAt(x))return!1;for(;++x<y;)if(93===e.src.charCodeAt(x)&&92!==e.src.charCodeAt(x-1)){if(x+1===y)return!1;if(58!==e.src.charCodeAt(x+1))return!1;break}for(a=e.lineMax,b=e.md.block.ruler.getRules("reference"),f=e.parentType,e.parentType="reference";C<a&&!e.isEmpty(C);C++)if(!(3<e.sCount[C]-e.blkIndent||e.sCount[C]<0)){for(g=!1,l=0,u=b.length;l<u;l++)if(b[l](e,C,a,!0)){g=!0;break}if(g)break}for(y=(_=e.getLines(r,C,e.blkIndent,!1).trim()).length,x=1;x<y;x++){if(91===(s=_.charCodeAt(x)))return!1;if(93===s){h=x;break}10===s?v++:92===s&&++x<y&&10===_.charCodeAt(x)&&v++}if(h<0||58!==_.charCodeAt(h+1))return!1;for(x=h+2;x<y;x++)if(10===(s=_.charCodeAt(x)))v++;else if(!w(s))break;if(!(d=e.md.helpers.parseLinkDestination(_,x,y)).ok)return!1;if(c=e.md.normalizeLink(d.str),!e.md.validateLink(c))return!1;for(x=d.pos,i=v+=d.lines,m=o=x;x<y;x++)if(10===(s=_.charCodeAt(x)))v++;else if(!w(s))break;for(d=e.md.helpers.parseLinkTitle(_,x,y),x<y&&m!==x&&d.ok?(k=d.str,x=d.pos,v+=d.lines):(k="",x=o,v=i);x<y&&(s=_.charCodeAt(x),w(s));)x++;if(x<y&&10!==_.charCodeAt(x)&&k)for(k="",x=o,v=i;x<y&&(s=_.charCodeAt(x),w(s));)x++;return!(x<y&&10!==_.charCodeAt(x))&&(!!(p=A(_.slice(1,h)))&&(n||(void 0===e.env.references&&(e.env.references={}),void 0===e.env.references[p]&&(e.env.references[p]={title:k,href:c}),e.parentType=f,e.line=r+v+1),!0))}},{"../common/utils":4}],28:[function(e,r,t){"use strict";var s=e("../token"),h=e("../common/utils").isSpace;function n(e,r,t,n){var s,o,i,a,c,l,u,p;for(this.src=e,this.md=r,this.env=t,this.tokens=n,this.bMarks=[],this.eMarks=[],this.tShift=[],this.sCount=[],this.bsCount=[],this.blkIndent=0,this.line=0,this.lineMax=0,this.tight=!1,this.ddIndent=-1,this.parentType="root",this.level=0,this.result="",p=!1,i=a=l=u=0,c=(o=this.src).length;a<c;a++){if(s=o.charCodeAt(a),!p){if(h(s)){l++,9===s?u+=4-u%4:u++;continue}p=!0}10!==s&&a!==c-1||(10!==s&&a++,this.bMarks.push(i),this.eMarks.push(a),this.tShift.push(l),this.sCount.push(u),this.bsCount.push(0),p=!1,u=l=0,i=a+1)}this.bMarks.push(o.length),this.eMarks.push(o.length),this.tShift.push(0),this.sCount.push(0),this.bsCount.push(0),this.lineMax=this.bMarks.length-1}n.prototype.push=function(e,r,t){var n=new s(e,r,t);return n.block=!0,t<0&&this.level--,n.level=this.level,0<t&&this.level++,this.tokens.push(n),n},n.prototype.isEmpty=function(e){return this.bMarks[e]+this.tShift[e]>=this.eMarks[e]},n.prototype.skipEmptyLines=function(e){for(var r=this.lineMax;e<r&&!(this.bMarks[e]+this.tShift[e]<this.eMarks[e]);e++);return e},n.prototype.skipSpaces=function(e){for(var r,t=this.src.length;e<t&&(r=this.src.charCodeAt(e),h(r));e++);return e},n.prototype.skipSpacesBack=function(e,r){if(e<=r)return e;for(;r<e;)if(!h(this.src.charCodeAt(--e)))return e+1;return e},n.prototype.skipChars=function(e,r){for(var t=this.src.length;e<t&&this.src.charCodeAt(e)===r;e++);return e},n.prototype.skipCharsBack=function(e,r,t){if(e<=t)return e;for(;t<e;)if(r!==this.src.charCodeAt(--e))return e+1;return e},n.prototype.getLines=function(e,r,t,n){var s,o,i,a,c,l,u,p=e;if(r<=e)return"";for(l=new Array(r-e),s=0;p<r;p++,s++){for(o=0,u=a=this.bMarks[p],c=p+1<r||n?this.eMarks[p]+1:this.eMarks[p];a<c&&o<t;){if(i=this.src.charCodeAt(a),h(i))9===i?o+=4-(o+this.bsCount[p])%4:o++;else{if(!(a-u<this.tShift[p]))break;o++}a++}l[s]=t<o?new Array(o-t+1).join(" ")+this.src.slice(a,c):this.src.slice(a,c)}return l.join("")},n.prototype.Token=s,r.exports=n},{"../common/utils":4,"../token":51}],29:[function(e,r,t){"use strict";var _=e("../common/utils").isSpace;function g(e,r){var t=e.bMarks[r]+e.blkIndent,n=e.eMarks[r];return e.src.substr(t,n-t)}function b(e){var r,t=[],n=0,s=e.length,o=0,i=0,a=!1,c=0;for(r=e.charCodeAt(n);n<s;)96===r?a?(a=!1,c=n):o%2==0&&(a=!0,c=n):124!==r||o%2!=0||a||(t.push(e.substring(i,n)),i=n+1),92===r?o++:o=0,++n===s&&a&&(a=!1,n=c+1),r=e.charCodeAt(n);return t.push(e.substring(i)),t}r.exports=function(e,r,t,n){var s,o,i,a,c,l,u,p,h,f,d,m;if(t<r+2)return!1;if(c=r+1,e.sCount[c]<e.blkIndent)return!1;if(4<=e.sCount[c]-e.blkIndent)return!1;if((i=e.bMarks[c]+e.tShift[c])>=e.eMarks[c])return!1;if(124!==(s=e.src.charCodeAt(i++))&&45!==s&&58!==s)return!1;for(;i<e.eMarks[c];){if(124!==(s=e.src.charCodeAt(i))&&45!==s&&58!==s&&!_(s))return!1;i++}for(l=(o=g(e,r+1)).split("|"),h=[],a=0;a<l.length;a++){if(!(f=l[a].trim())){if(0===a||a===l.length-1)continue;return!1}if(!/^:?-+:?$/.test(f))return!1;58===f.charCodeAt(f.length-1)?h.push(58===f.charCodeAt(0)?"center":"right"):58===f.charCodeAt(0)?h.push("left"):h.push("")}if(-1===(o=g(e,r).trim()).indexOf("|"))return!1;if(4<=e.sCount[r]-e.blkIndent)return!1;if((u=(l=b(o.replace(/^\||\|$/g,""))).length)>h.length)return!1;if(n)return!0;for((p=e.push("table_open","table",1)).map=d=[r,0],(p=e.push("thead_open","thead",1)).map=[r,r+1],(p=e.push("tr_open","tr",1)).map=[r,r+1],a=0;a<l.length;a++)(p=e.push("th_open","th",1)).map=[r,r+1],h[a]&&(p.attrs=[["style","text-align:"+h[a]]]),(p=e.push("inline","",0)).content=l[a].trim(),p.map=[r,r+1],p.children=[],p=e.push("th_close","th",-1);for(p=e.push("tr_close","tr",-1),p=e.push("thead_close","thead",-1),(p=e.push("tbody_open","tbody",1)).map=m=[r+2,0],c=r+2;c<t&&!(e.sCount[c]<e.blkIndent)&&-1!==(o=g(e,c).trim()).indexOf("|")&&!(4<=e.sCount[c]-e.blkIndent);c++){for(l=b(o.replace(/^\||\|$/g,"")),p=e.push("tr_open","tr",1),a=0;a<u;a++)p=e.push("td_open","td",1),h[a]&&(p.attrs=[["style","text-align:"+h[a]]]),(p=e.push("inline","",0)).content=l[a]?l[a].trim():"",p.children=[],p=e.push("td_close","td",-1);p=e.push("tr_close","tr",-1)}return p=e.push("tbody_close","tbody",-1),p=e.push("table_close","table",-1),d[1]=m[1]=c,e.line=c,!0}},{"../common/utils":4}],30:[function(e,r,t){"use strict";r.exports=function(e){var r;e.inlineMode?((r=new e.Token("inline","",0)).content=e.src,r.map=[0,1],r.children=[],e.tokens.push(r)):e.md.block.parse(e.src,e.md,e.env,e.tokens)}},{}],31:[function(e,r,t){"use strict";r.exports=function(e){var r,t,n,s=e.tokens;for(t=0,n=s.length;t<n;t++)"inline"===(r=s[t]).type&&e.md.inline.parse(r.content,e.md,e.env,r.children)}},{}],32:[function(e,r,t){"use strict";var x=e("../common/utils").arrayReplaceAt;r.exports=function(e){var r,t,n,s,o,i,a,c,l,u,p,h,f,d,m,_,g,b,k,v=e.tokens;if(e.md.options.linkify)for(t=0,n=v.length;t<n;t++)if("inline"===v[t].type&&e.md.linkify.pretest(v[t].content))for(f=0,r=(s=v[t].children).length-1;0<=r;r--)if("link_close"!==(i=s[r]).type){if("html_inline"===i.type&&(k=i.content,/^<a[>\s]/i.test(k)&&0<f&&f--,b=i.content,/^<\/a\s*>/i.test(b)&&f++),!(0<f)&&"text"===i.type&&e.md.linkify.test(i.content)){for(l=i.content,g=e.md.linkify.match(l),a=[],h=i.level,c=p=0;c<g.length;c++)d=g[c].url,m=e.md.normalizeLink(d),e.md.validateLink(m)&&(_=g[c].text,_=g[c].schema?"mailto:"!==g[c].schema||/^mailto:/i.test(_)?e.md.normalizeLinkText(_):e.md.normalizeLinkText("mailto:"+_).replace(/^mailto:/,""):e.md.normalizeLinkText("http://"+_).replace(/^http:\/\//,""),p<(u=g[c].index)&&((o=new e.Token("text","",0)).content=l.slice(p,u),o.level=h,a.push(o)),(o=new e.Token("link_open","a",1)).attrs=[["href",m]],o.level=h++,o.markup="linkify",o.info="auto",a.push(o),(o=new e.Token("text","",0)).content=_,o.level=h,a.push(o),(o=new e.Token("link_close","a",-1)).level=--h,o.markup="linkify",o.info="auto",a.push(o),p=g[c].lastIndex);p<l.length&&((o=new e.Token("text","",0)).content=l.slice(p),o.level=h,a.push(o)),v[t].children=s=x(s,r,a)}}else for(r--;s[r].level!==i.level&&"link_open"!==s[r].type;)r--}},{"../common/utils":4}],33:[function(e,r,t){"use strict";var n=/\r[\n\u0085]?|[\u2424\u2028\u0085]/g,s=/\u0000/g;r.exports=function(e){var r;r=(r=e.src.replace(n,"\n")).replace(s,"\ufffd"),e.src=r}},{}],34:[function(e,r,t){"use strict";var s=/\+-|\.\.|\?\?\?\?|!!!!|,,|--/,n=/\((c|tm|r|p)\)/i,o=/\((c|tm|r|p)\)/gi,i={c:"\xa9",r:"\xae",p:"\xa7",tm:"\u2122"};function a(e,r){return i[r.toLowerCase()]}function c(e){var r,t,n=0;for(r=e.length-1;0<=r;r--)"text"!==(t=e[r]).type||n||(t.content=t.content.replace(o,a)),"link_open"===t.type&&"auto"===t.info&&n--,"link_close"===t.type&&"auto"===t.info&&n++}function l(e){var r,t,n=0;for(r=e.length-1;0<=r;r--)"text"!==(t=e[r]).type||n||s.test(t.content)&&(t.content=t.content.replace(/\+-/g,"\xb1").replace(/\.{2,}/g,"\u2026").replace(/([?!])\u2026/g,"$1..").replace(/([?!]){4,}/g,"$1$1$1").replace(/,{2,}/g,",").replace(/(^|[^-])---([^-]|$)/gm,"$1\u2014$2").replace(/(^|\s)--(\s|$)/gm,"$1\u2013$2").replace(/(^|[^-\s])--([^-\s]|$)/gm,"$1\u2013$2")),"link_open"===t.type&&"auto"===t.info&&n--,"link_close"===t.type&&"auto"===t.info&&n++}r.exports=function(e){var r;if(e.md.options.typographer)for(r=e.tokens.length-1;0<=r;r--)"inline"===e.tokens[r].type&&(n.test(e.tokens[r].content)&&c(e.tokens[r].children),s.test(e.tokens[r].content)&&l(e.tokens[r].children))}},{}],35:[function(e,r,t){"use strict";var C=e("../common/utils").isWhiteSpace,A=e("../common/utils").isPunctChar,w=e("../common/utils").isMdAsciiPunct,n=/['"]/,D=/['"]/g,E="\u2019";function q(e,r,t){return e.substr(0,r)+t+e.substr(r+1)}function s(e,r){var t,n,s,o,i,a,c,l,u,p,h,f,d,m,_,g,b,k,v,x,y;for(v=[],t=0;t<e.length;t++){for(n=e[t],c=e[t].level,b=v.length-1;0<=b&&!(v[b].level<=c);b--);if(v.length=b+1,"text"===n.type){i=0,a=(s=n.content).length;e:for(;i<a&&(D.lastIndex=i,o=D.exec(s));){if(_=g=!0,i=o.index+1,k="'"===o[0],u=32,0<=o.index-1)u=s.charCodeAt(o.index-1);else for(b=t-1;0<=b&&("softbreak"!==e[b].type&&"hardbreak"!==e[b].type);b--)if("text"===e[b].type){u=e[b].content.charCodeAt(e[b].content.length-1);break}if(p=32,i<a)p=s.charCodeAt(i);else for(b=t+1;b<e.length&&("softbreak"!==e[b].type&&"hardbreak"!==e[b].type);b++)if("text"===e[b].type){p=e[b].content.charCodeAt(0);break}if(h=w(u)||A(String.fromCharCode(u)),f=w(p)||A(String.fromCharCode(p)),d=C(u),(m=C(p))?_=!1:f&&(d||h||(_=!1)),d?g=!1:h&&(m||f||(g=!1)),34===p&&'"'===o[0]&&48<=u&&u<=57&&(g=_=!1),_&&g&&(_=!1,g=f),_||g){if(g)for(b=v.length-1;0<=b&&(l=v[b],!(v[b].level<c));b--)if(l.single===k&&v[b].level===c){l=v[b],k?(x=r.md.options.quotes[2],y=r.md.options.quotes[3]):(x=r.md.options.quotes[0],y=r.md.options.quotes[1]),n.content=q(n.content,o.index,y),e[l.token].content=q(e[l.token].content,l.pos,x),i+=y.length-1,l.token===t&&(i+=x.length-1),a=(s=n.content).length,v.length=b;continue e}_?v.push({token:t,pos:o.index,single:k,level:c}):g&&k&&(n.content=q(n.content,o.index,E))}else k&&(n.content=q(n.content,o.index,E))}}}}r.exports=function(e){var r;if(e.md.options.typographer)for(r=e.tokens.length-1;0<=r;r--)"inline"===e.tokens[r].type&&n.test(e.tokens[r].content)&&s(e.tokens[r].children,e)}},{"../common/utils":4}],36:[function(e,r,t){"use strict";var n=e("../token");function s(e,r,t){this.src=e,this.env=t,this.tokens=[],this.inlineMode=!1,this.md=r}s.prototype.Token=n,r.exports=s},{"../token":51}],37:[function(e,r,t){"use strict";var l=/^<([a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*)>/,u=/^<([a-zA-Z][a-zA-Z0-9+.\-]{1,31}):([^<>\x00-\x20]*)>/;r.exports=function(e,r){var t,n,s,o,i,a,c=e.pos;return 60===e.src.charCodeAt(c)&&(!((t=e.src.slice(c)).indexOf(">")<0)&&(u.test(t)?(o=(n=t.match(u))[0].slice(1,-1),i=e.md.normalizeLink(o),!!e.md.validateLink(i)&&(r||((a=e.push("link_open","a",1)).attrs=[["href",i]],a.markup="autolink",a.info="auto",(a=e.push("text","",0)).content=e.md.normalizeLinkText(o),(a=e.push("link_close","a",-1)).markup="autolink",a.info="auto"),e.pos+=n[0].length,!0)):!!l.test(t)&&(o=(s=t.match(l))[0].slice(1,-1),i=e.md.normalizeLink("mailto:"+o),!!e.md.validateLink(i)&&(r||((a=e.push("link_open","a",1)).attrs=[["href",i]],a.markup="autolink",a.info="auto",(a=e.push("text","",0)).content=e.md.normalizeLinkText(o),(a=e.push("link_close","a",-1)).markup="autolink",a.info="auto"),e.pos+=s[0].length,!0))))}},{}],38:[function(e,r,t){"use strict";r.exports=function(e,r){var t,n,s,o,i,a,c=e.pos;if(96!==e.src.charCodeAt(c))return!1;for(t=c,c++,n=e.posMax;c<n&&96===e.src.charCodeAt(c);)c++;for(s=e.src.slice(t,c),o=i=c;-1!==(o=e.src.indexOf("`",i));){for(i=o+1;i<n&&96===e.src.charCodeAt(i);)i++;if(i-o===s.length)return r||((a=e.push("code_inline","code",0)).markup=s,a.content=e.src.slice(c,o).replace(/[ \n]+/g," ").trim()),e.pos=i,!0}return r||(e.pending+=s),e.pos+=s.length,!0}},{}],39:[function(e,r,t){"use strict";r.exports=function(e){var r,t,n,s,o=e.delimiters,i=e.delimiters.length;for(r=0;r<i;r++)if((n=o[r]).close)for(t=r-n.jump-1;0<=t;){if((s=o[t]).open&&s.marker===n.marker&&s.end<0&&s.level===n.level)if(!((s.close||n.open)&&void 0!==s.length&&void 0!==n.length&&(s.length+n.length)%3==0)){n.jump=r-t,n.open=!1,s.end=r,s.jump=0;break}t-=s.jump+1}}},{}],40:[function(e,r,t){"use strict";r.exports.tokenize=function(e,r){var t,n,s=e.pos,o=e.src.charCodeAt(s);if(r)return!1;if(95!==o&&42!==o)return!1;for(n=e.scanDelims(e.pos,42===o),t=0;t<n.length;t++)e.push("text","",0).content=String.fromCharCode(o),e.delimiters.push({marker:o,length:n.length,jump:t,token:e.tokens.length-1,level:e.level,end:-1,open:n.can_open,close:n.can_close});return e.pos+=n.length,!0},r.exports.postProcess=function(e){var r,t,n,s,o,i,a=e.delimiters;for(r=e.delimiters.length-1;0<=r;r--)95!==(t=a[r]).marker&&42!==t.marker||-1!==t.end&&(n=a[t.end],i=0<r&&a[r-1].end===t.end+1&&a[r-1].token===t.token-1&&a[t.end+1].token===n.token+1&&a[r-1].marker===t.marker,o=String.fromCharCode(t.marker),(s=e.tokens[t.token]).type=i?"strong_open":"em_open",s.tag=i?"strong":"em",s.nesting=1,s.markup=i?o+o:o,s.content="",(s=e.tokens[n.token]).type=i?"strong_close":"em_close",s.tag=i?"strong":"em",s.nesting=-1,s.markup=i?o+o:o,s.content="",i&&(e.tokens[a[r-1].token].content="",e.tokens[a[t.end+1].token].content="",r--))}},{}],41:[function(e,r,t){"use strict";var i=e("../common/entities"),a=e("../common/utils").has,c=e("../common/utils").isValidEntityCode,l=e("../common/utils").fromCodePoint,u=/^&#((?:x[a-f0-9]{1,8}|[0-9]{1,8}));/i,p=/^&([a-z][a-z0-9]{1,31});/i;r.exports=function(e,r){var t,n,s=e.pos,o=e.posMax;if(38!==e.src.charCodeAt(s))return!1;if(s+1<o)if(35===e.src.charCodeAt(s+1)){if(n=e.src.slice(s).match(u))return r||(t="x"===n[1][0].toLowerCase()?parseInt(n[1].slice(1),16):parseInt(n[1],10),e.pending+=c(t)?l(t):l(65533)),e.pos+=n[0].length,!0}else if((n=e.src.slice(s).match(p))&&a(i,n[1]))return r||(e.pending+=i[n[1]]),e.pos+=n[0].length,!0;return r||(e.pending+="&"),e.pos++,!0}},{"../common/entities":1,"../common/utils":4}],42:[function(e,r,t){"use strict";for(var o=e("../common/utils").isSpace,i=[],n=0;n<256;n++)i.push(0);"\\!\"#$%&'()*+,./:;<=>?@[]^_`{|}~-".split("").forEach(function(e){i[e.charCodeAt(0)]=1}),r.exports=function(e,r){var t,n=e.pos,s=e.posMax;if(92!==e.src.charCodeAt(n))return!1;if(++n<s){if((t=e.src.charCodeAt(n))<256&&0!==i[t])return r||(e.pending+=e.src[n]),e.pos+=2,!0;if(10===t){for(r||e.push("hardbreak","br",0),n++;n<s&&(t=e.src.charCodeAt(n),o(t));)n++;return e.pos=n,!0}}return r||(e.pending+="\\"),e.pos++,!0}},{"../common/utils":4}],43:[function(e,r,t){"use strict";var a=e("../common/html_re").HTML_TAG_RE;r.exports=function(e,r){var t,n,s,o,i=e.pos;return!!e.md.options.html&&(s=e.posMax,!(60!==e.src.charCodeAt(i)||s<=i+2)&&((33===(t=e.src.charCodeAt(i+1))||63===t||47===t||97<=(o=32|t)&&o<=122)&&(!!(n=e.src.slice(i).match(a))&&(r||(e.push("html_inline","",0).content=e.src.slice(i,i+n[0].length)),e.pos+=n[0].length,!0))))}},{"../common/html_re":3}],44:[function(e,r,t){"use strict";var b=e("../common/utils").normalizeReference,k=e("../common/utils").isSpace;r.exports=function(e,r){var t,n,s,o,i,a,c,l,u,p,h,f,d,m="",_=e.pos,g=e.posMax;if(33!==e.src.charCodeAt(e.pos))return!1;if(91!==e.src.charCodeAt(e.pos+1))return!1;if(a=e.pos+2,(i=e.md.helpers.parseLinkLabel(e,e.pos+1,!1))<0)return!1;if((c=i+1)<g&&40===e.src.charCodeAt(c)){for(c++;c<g&&(n=e.src.charCodeAt(c),k(n)||10===n);c++);if(g<=c)return!1;for(d=c,(u=e.md.helpers.parseLinkDestination(e.src,c,e.posMax)).ok&&(m=e.md.normalizeLink(u.str),e.md.validateLink(m)?c=u.pos:m=""),d=c;c<g&&(n=e.src.charCodeAt(c),k(n)||10===n);c++);if(u=e.md.helpers.parseLinkTitle(e.src,c,e.posMax),c<g&&d!==c&&u.ok)for(p=u.str,c=u.pos;c<g&&(n=e.src.charCodeAt(c),k(n)||10===n);c++);else p="";if(g<=c||41!==e.src.charCodeAt(c))return e.pos=_,!1;c++}else{if(void 0===e.env.references)return!1;if(c<g&&91===e.src.charCodeAt(c)?(d=c+1,0<=(c=e.md.helpers.parseLinkLabel(e,c))?o=e.src.slice(d,c++):c=i+1):c=i+1,o||(o=e.src.slice(a,i)),!(l=e.env.references[b(o)]))return e.pos=_,!1;m=l.href,p=l.title}return r||(s=e.src.slice(a,i),e.md.inline.parse(s,e.md,e.env,f=[]),(h=e.push("image","img",0)).attrs=t=[["src",m],["alt",""]],h.children=f,h.content=s,p&&t.push(["title",p])),e.pos=c,e.posMax=g,!0}},{"../common/utils":4}],45:[function(e,r,t){"use strict";var _=e("../common/utils").normalizeReference,g=e("../common/utils").isSpace;r.exports=function(e,r){var t,n,s,o,i,a,c,l,u,p="",h=e.pos,f=e.posMax,d=e.pos,m=!0;if(91!==e.src.charCodeAt(e.pos))return!1;if(i=e.pos+1,(o=e.md.helpers.parseLinkLabel(e,e.pos,!0))<0)return!1;if((a=o+1)<f&&40===e.src.charCodeAt(a)){for(m=!1,a++;a<f&&(n=e.src.charCodeAt(a),g(n)||10===n);a++);if(f<=a)return!1;for(d=a,(c=e.md.helpers.parseLinkDestination(e.src,a,e.posMax)).ok&&(p=e.md.normalizeLink(c.str),e.md.validateLink(p)?a=c.pos:p=""),d=a;a<f&&(n=e.src.charCodeAt(a),g(n)||10===n);a++);if(c=e.md.helpers.parseLinkTitle(e.src,a,e.posMax),a<f&&d!==a&&c.ok)for(u=c.str,a=c.pos;a<f&&(n=e.src.charCodeAt(a),g(n)||10===n);a++);else u="";(f<=a||41!==e.src.charCodeAt(a))&&(m=!0),a++}if(m){if(void 0===e.env.references)return!1;if(a<f&&91===e.src.charCodeAt(a)?(d=a+1,0<=(a=e.md.helpers.parseLinkLabel(e,a))?s=e.src.slice(d,a++):a=o+1):a=o+1,s||(s=e.src.slice(i,o)),!(l=e.env.references[_(s)]))return e.pos=h,!1;p=l.href,u=l.title}return r||(e.pos=i,e.posMax=o,e.push("link_open","a",1).attrs=t=[["href",p]],u&&t.push(["title",u]),e.md.inline.tokenize(e),e.push("link_close","a",-1)),e.pos=a,e.posMax=f,!0}},{"../common/utils":4}],46:[function(e,r,t){"use strict";var o=e("../common/utils").isSpace;r.exports=function(e,r){var t,n,s=e.pos;if(10!==e.src.charCodeAt(s))return!1;for(t=e.pending.length-1,n=e.posMax,r||(0<=t&&32===e.pending.charCodeAt(t)?1<=t&&32===e.pending.charCodeAt(t-1)?(e.pending=e.pending.replace(/ +$/,""),e.push("hardbreak","br",0)):(e.pending=e.pending.slice(0,-1),e.push("softbreak","br",0)):e.push("softbreak","br",0)),s++;s<n&&o(e.src.charCodeAt(s));)s++;return e.pos=s,!0}},{"../common/utils":4}],47:[function(e,r,t){"use strict";var s=e("../token"),_=e("../common/utils").isWhiteSpace,g=e("../common/utils").isPunctChar,b=e("../common/utils").isMdAsciiPunct;function n(e,r,t,n){this.src=e,this.env=t,this.md=r,this.tokens=n,this.pos=0,this.posMax=this.src.length,this.level=0,this.pending="",this.pendingLevel=0,this.cache={},this.delimiters=[]}n.prototype.pushPending=function(){var e=new s("text","",0);return e.content=this.pending,e.level=this.pendingLevel,this.tokens.push(e),this.pending="",e},n.prototype.push=function(e,r,t){this.pending&&this.pushPending();var n=new s(e,r,t);return t<0&&this.level--,n.level=this.level,0<t&&this.level++,this.pendingLevel=this.level,this.tokens.push(n),n},n.prototype.scanDelims=function(e,r){var t,n,s,o,i,a,c,l,u,p=e,h=!0,f=!0,d=this.posMax,m=this.src.charCodeAt(e);for(t=0<e?this.src.charCodeAt(e-1):32;p<d&&this.src.charCodeAt(p)===m;)p++;return s=p-e,n=p<d?this.src.charCodeAt(p):32,c=b(t)||g(String.fromCharCode(t)),u=b(n)||g(String.fromCharCode(n)),a=_(t),(l=_(n))?h=!1:u&&(a||c||(h=!1)),a?f=!1:c&&(l||u||(f=!1)),r?(o=h,i=f):(o=h&&(!f||c),i=f&&(!h||u)),{can_open:o,can_close:i,length:s}},n.prototype.Token=s,r.exports=n},{"../common/utils":4,"../token":51}],48:[function(e,r,t){"use strict";r.exports.tokenize=function(e,r){var t,n,s,o,i=e.pos,a=e.src.charCodeAt(i);if(r)return!1;if(126!==a)return!1;if(s=(n=e.scanDelims(e.pos,!0)).length,o=String.fromCharCode(a),s<2)return!1;for(s%2&&(e.push("text","",0).content=o,s--),t=0;t<s;t+=2)e.push("text","",0).content=o+o,e.delimiters.push({marker:a,jump:t,token:e.tokens.length-1,level:e.level,end:-1,open:n.can_open,close:n.can_close});return e.pos+=n.length,!0},r.exports.postProcess=function(e){var r,t,n,s,o,i=[],a=e.delimiters,c=e.delimiters.length;for(r=0;r<c;r++)126===(n=a[r]).marker&&-1!==n.end&&(s=a[n.end],(o=e.tokens[n.token]).type="s_open",o.tag="s",o.nesting=1,o.markup="~~",o.content="",(o=e.tokens[s.token]).type="s_close",o.tag="s",o.nesting=-1,o.markup="~~",o.content="","text"===e.tokens[s.token-1].type&&"~"===e.tokens[s.token-1].content&&i.push(s.token-1));for(;i.length;){for(t=(r=i.pop())+1;t<e.tokens.length&&"s_close"===e.tokens[t].type;)t++;r!==--t&&(o=e.tokens[t],e.tokens[t]=e.tokens[r],e.tokens[r]=o)}}},{}],49:[function(e,r,t){"use strict";function n(e){switch(e){case 10:case 33:case 35:case 36:case 37:case 38:case 42:case 43:case 45:case 58:case 60:case 61:case 62:case 64:case 91:case 92:case 93:case 94:case 95:case 96:case 123:case 125:case 126:return!0;default:return!1}}r.exports=function(e,r){for(var t=e.pos;t<e.posMax&&!n(e.src.charCodeAt(t));)t++;return t!==e.pos&&(r||(e.pending+=e.src.slice(e.pos,t)),e.pos=t,!0)}},{}],50:[function(e,r,t){"use strict";r.exports=function(e){var r,t,n=0,s=e.tokens,o=e.tokens.length;for(r=t=0;r<o;r++)n+=s[r].nesting,s[r].level=n,"text"===s[r].type&&r+1<o&&"text"===s[r+1].type?s[r+1].content=s[r].content+s[r+1].content:(r!==t&&(s[t]=s[r]),t++);r!==t&&(s.length=t)}},{}],51:[function(e,r,t){"use strict";function n(e,r,t){this.type=e,this.tag=r,this.attrs=null,this.map=null,this.nesting=t,this.level=0,this.children=null,this.content="",this.markup="",this.info="",this.meta=null,this.block=!1,this.hidden=!1}n.prototype.attrIndex=function(e){var r,t,n;if(!this.attrs)return-1;for(t=0,n=(r=this.attrs).length;t<n;t++)if(r[t][0]===e)return t;return-1},n.prototype.attrPush=function(e){this.attrs?this.attrs.push(e):this.attrs=[e]},n.prototype.attrSet=function(e,r){var t=this.attrIndex(e),n=[e,r];t<0?this.attrPush(n):this.attrs[t]=n},n.prototype.attrGet=function(e){var r=this.attrIndex(e),t=null;return 0<=r&&(t=this.attrs[r][1]),t},n.prototype.attrJoin=function(e,r){var t=this.attrIndex(e);t<0?this.attrPush([e,r]):this.attrs[t][1]=this.attrs[t][1]+" "+r},r.exports=n},{}],52:[function(e,r,t){r.exports={Aacute:"\xc1",aacute:"\xe1",Abreve:"\u0102",abreve:"\u0103",ac:"\u223e",acd:"\u223f",acE:"\u223e\u0333",Acirc:"\xc2",acirc:"\xe2",acute:"\xb4",Acy:"\u0410",acy:"\u0430",AElig:"\xc6",aelig:"\xe6",af:"\u2061",Afr:"\ud835\udd04",afr:"\ud835\udd1e",Agrave:"\xc0",agrave:"\xe0",alefsym:"\u2135",aleph:"\u2135",Alpha:"\u0391",alpha:"\u03b1",Amacr:"\u0100",amacr:"\u0101",amalg:"\u2a3f",amp:"&",AMP:"&",andand:"\u2a55",And:"\u2a53",and:"\u2227",andd:"\u2a5c",andslope:"\u2a58",andv:"\u2a5a",ang:"\u2220",ange:"\u29a4",angle:"\u2220",angmsdaa:"\u29a8",angmsdab:"\u29a9",angmsdac:"\u29aa",angmsdad:"\u29ab",angmsdae:"\u29ac",angmsdaf:"\u29ad",angmsdag:"\u29ae",angmsdah:"\u29af",angmsd:"\u2221",angrt:"\u221f",angrtvb:"\u22be",angrtvbd:"\u299d",angsph:"\u2222",angst:"\xc5",angzarr:"\u237c",Aogon:"\u0104",aogon:"\u0105",Aopf:"\ud835\udd38",aopf:"\ud835\udd52",apacir:"\u2a6f",ap:"\u2248",apE:"\u2a70",ape:"\u224a",apid:"\u224b",apos:"'",ApplyFunction:"\u2061",approx:"\u2248",approxeq:"\u224a",Aring:"\xc5",aring:"\xe5",Ascr:"\ud835\udc9c",ascr:"\ud835\udcb6",Assign:"\u2254",ast:"*",asymp:"\u2248",asympeq:"\u224d",Atilde:"\xc3",atilde:"\xe3",Auml:"\xc4",auml:"\xe4",awconint:"\u2233",awint:"\u2a11",backcong:"\u224c",backepsilon:"\u03f6",backprime:"\u2035",backsim:"\u223d",backsimeq:"\u22cd",Backslash:"\u2216",Barv:"\u2ae7",barvee:"\u22bd",barwed:"\u2305",Barwed:"\u2306",barwedge:"\u2305",bbrk:"\u23b5",bbrktbrk:"\u23b6",bcong:"\u224c",Bcy:"\u0411",bcy:"\u0431",bdquo:"\u201e",becaus:"\u2235",because:"\u2235",Because:"\u2235",bemptyv:"\u29b0",bepsi:"\u03f6",bernou:"\u212c",Bernoullis:"\u212c",Beta:"\u0392",beta:"\u03b2",beth:"\u2136",between:"\u226c",Bfr:"\ud835\udd05",bfr:"\ud835\udd1f",bigcap:"\u22c2",bigcirc:"\u25ef",bigcup:"\u22c3",bigodot:"\u2a00",bigoplus:"\u2a01",bigotimes:"\u2a02",bigsqcup:"\u2a06",bigstar:"\u2605",bigtriangledown:"\u25bd",bigtriangleup:"\u25b3",biguplus:"\u2a04",bigvee:"\u22c1",bigwedge:"\u22c0",bkarow:"\u290d",blacklozenge:"\u29eb",blacksquare:"\u25aa",blacktriangle:"\u25b4",blacktriangledown:"\u25be",blacktriangleleft:"\u25c2",blacktriangleright:"\u25b8",blank:"\u2423",blk12:"\u2592",blk14:"\u2591",blk34:"\u2593",block:"\u2588",bne:"=\u20e5",bnequiv:"\u2261\u20e5",bNot:"\u2aed",bnot:"\u2310",Bopf:"\ud835\udd39",bopf:"\ud835\udd53",bot:"\u22a5",bottom:"\u22a5",bowtie:"\u22c8",boxbox:"\u29c9",boxdl:"\u2510",boxdL:"\u2555",boxDl:"\u2556",boxDL:"\u2557",boxdr:"\u250c",boxdR:"\u2552",boxDr:"\u2553",boxDR:"\u2554",boxh:"\u2500",boxH:"\u2550",boxhd:"\u252c",boxHd:"\u2564",boxhD:"\u2565",boxHD:"\u2566",boxhu:"\u2534",boxHu:"\u2567",boxhU:"\u2568",boxHU:"\u2569",boxminus:"\u229f",boxplus:"\u229e",boxtimes:"\u22a0",boxul:"\u2518",boxuL:"\u255b",boxUl:"\u255c",boxUL:"\u255d",boxur:"\u2514",boxuR:"\u2558",boxUr:"\u2559",boxUR:"\u255a",boxv:"\u2502",boxV:"\u2551",boxvh:"\u253c",boxvH:"\u256a",boxVh:"\u256b",boxVH:"\u256c",boxvl:"\u2524",boxvL:"\u2561",boxVl:"\u2562",boxVL:"\u2563",boxvr:"\u251c",boxvR:"\u255e",boxVr:"\u255f",boxVR:"\u2560",bprime:"\u2035",breve:"\u02d8",Breve:"\u02d8",brvbar:"\xa6",bscr:"\ud835\udcb7",Bscr:"\u212c",bsemi:"\u204f",bsim:"\u223d",bsime:"\u22cd",bsolb:"\u29c5",bsol:"\\",bsolhsub:"\u27c8",bull:"\u2022",bullet:"\u2022",bump:"\u224e",bumpE:"\u2aae",bumpe:"\u224f",Bumpeq:"\u224e",bumpeq:"\u224f",Cacute:"\u0106",cacute:"\u0107",capand:"\u2a44",capbrcup:"\u2a49",capcap:"\u2a4b",cap:"\u2229",Cap:"\u22d2",capcup:"\u2a47",capdot:"\u2a40",CapitalDifferentialD:"\u2145",caps:"\u2229\ufe00",caret:"\u2041",caron:"\u02c7",Cayleys:"\u212d",ccaps:"\u2a4d",Ccaron:"\u010c",ccaron:"\u010d",Ccedil:"\xc7",ccedil:"\xe7",Ccirc:"\u0108",ccirc:"\u0109",Cconint:"\u2230",ccups:"\u2a4c",ccupssm:"\u2a50",Cdot:"\u010a",cdot:"\u010b",cedil:"\xb8",Cedilla:"\xb8",cemptyv:"\u29b2",cent:"\xa2",centerdot:"\xb7",CenterDot:"\xb7",cfr:"\ud835\udd20",Cfr:"\u212d",CHcy:"\u0427",chcy:"\u0447",check:"\u2713",checkmark:"\u2713",Chi:"\u03a7",chi:"\u03c7",circ:"\u02c6",circeq:"\u2257",circlearrowleft:"\u21ba",circlearrowright:"\u21bb",circledast:"\u229b",circledcirc:"\u229a",circleddash:"\u229d",CircleDot:"\u2299",circledR:"\xae",circledS:"\u24c8",CircleMinus:"\u2296",CirclePlus:"\u2295",CircleTimes:"\u2297",cir:"\u25cb",cirE:"\u29c3",cire:"\u2257",cirfnint:"\u2a10",cirmid:"\u2aef",cirscir:"\u29c2",ClockwiseContourIntegral:"\u2232",CloseCurlyDoubleQuote:"\u201d",CloseCurlyQuote:"\u2019",clubs:"\u2663",clubsuit:"\u2663",colon:":",Colon:"\u2237",Colone:"\u2a74",colone:"\u2254",coloneq:"\u2254",comma:",",commat:"@",comp:"\u2201",compfn:"\u2218",complement:"\u2201",complexes:"\u2102",cong:"\u2245",congdot:"\u2a6d",Congruent:"\u2261",conint:"\u222e",Conint:"\u222f",ContourIntegral:"\u222e",copf:"\ud835\udd54",Copf:"\u2102",coprod:"\u2210",Coproduct:"\u2210",copy:"\xa9",COPY:"\xa9",copysr:"\u2117",CounterClockwiseContourIntegral:"\u2233",crarr:"\u21b5",cross:"\u2717",Cross:"\u2a2f",Cscr:"\ud835\udc9e",cscr:"\ud835\udcb8",csub:"\u2acf",csube:"\u2ad1",csup:"\u2ad0",csupe:"\u2ad2",ctdot:"\u22ef",cudarrl:"\u2938",cudarrr:"\u2935",cuepr:"\u22de",cuesc:"\u22df",cularr:"\u21b6",cularrp:"\u293d",cupbrcap:"\u2a48",cupcap:"\u2a46",CupCap:"\u224d",cup:"\u222a",Cup:"\u22d3",cupcup:"\u2a4a",cupdot:"\u228d",cupor:"\u2a45",cups:"\u222a\ufe00",curarr:"\u21b7",curarrm:"\u293c",curlyeqprec:"\u22de",curlyeqsucc:"\u22df",curlyvee:"\u22ce",curlywedge:"\u22cf",curren:"\xa4",curvearrowleft:"\u21b6",curvearrowright:"\u21b7",cuvee:"\u22ce",cuwed:"\u22cf",cwconint:"\u2232",cwint:"\u2231",cylcty:"\u232d",dagger:"\u2020",Dagger:"\u2021",daleth:"\u2138",darr:"\u2193",Darr:"\u21a1",dArr:"\u21d3",dash:"\u2010",Dashv:"\u2ae4",dashv:"\u22a3",dbkarow:"\u290f",dblac:"\u02dd",Dcaron:"\u010e",dcaron:"\u010f",Dcy:"\u0414",dcy:"\u0434",ddagger:"\u2021",ddarr:"\u21ca",DD:"\u2145",dd:"\u2146",DDotrahd:"\u2911",ddotseq:"\u2a77",deg:"\xb0",Del:"\u2207",Delta:"\u0394",delta:"\u03b4",demptyv:"\u29b1",dfisht:"\u297f",Dfr:"\ud835\udd07",dfr:"\ud835\udd21",dHar:"\u2965",dharl:"\u21c3",dharr:"\u21c2",DiacriticalAcute:"\xb4",DiacriticalDot:"\u02d9",DiacriticalDoubleAcute:"\u02dd",DiacriticalGrave:"`",DiacriticalTilde:"\u02dc",diam:"\u22c4",diamond:"\u22c4",Diamond:"\u22c4",diamondsuit:"\u2666",diams:"\u2666",die:"\xa8",DifferentialD:"\u2146",digamma:"\u03dd",disin:"\u22f2",div:"\xf7",divide:"\xf7",divideontimes:"\u22c7",divonx:"\u22c7",DJcy:"\u0402",djcy:"\u0452",dlcorn:"\u231e",dlcrop:"\u230d",dollar:"$",Dopf:"\ud835\udd3b",dopf:"\ud835\udd55",Dot:"\xa8",dot:"\u02d9",DotDot:"\u20dc",doteq:"\u2250",doteqdot:"\u2251",DotEqual:"\u2250",dotminus:"\u2238",dotplus:"\u2214",dotsquare:"\u22a1",doublebarwedge:"\u2306",DoubleContourIntegral:"\u222f",DoubleDot:"\xa8",DoubleDownArrow:"\u21d3",DoubleLeftArrow:"\u21d0",DoubleLeftRightArrow:"\u21d4",DoubleLeftTee:"\u2ae4",DoubleLongLeftArrow:"\u27f8",DoubleLongLeftRightArrow:"\u27fa",DoubleLongRightArrow:"\u27f9",DoubleRightArrow:"\u21d2",DoubleRightTee:"\u22a8",DoubleUpArrow:"\u21d1",DoubleUpDownArrow:"\u21d5",DoubleVerticalBar:"\u2225",DownArrowBar:"\u2913",downarrow:"\u2193",DownArrow:"\u2193",Downarrow:"\u21d3",DownArrowUpArrow:"\u21f5",DownBreve:"\u0311",downdownarrows:"\u21ca",downharpoonleft:"\u21c3",downharpoonright:"\u21c2",DownLeftRightVector:"\u2950",DownLeftTeeVector:"\u295e",DownLeftVectorBar:"\u2956",DownLeftVector:"\u21bd",DownRightTeeVector:"\u295f",DownRightVectorBar:"\u2957",DownRightVector:"\u21c1",DownTeeArrow:"\u21a7",DownTee:"\u22a4",drbkarow:"\u2910",drcorn:"\u231f",drcrop:"\u230c",Dscr:"\ud835\udc9f",dscr:"\ud835\udcb9",DScy:"\u0405",dscy:"\u0455",dsol:"\u29f6",Dstrok:"\u0110",dstrok:"\u0111",dtdot:"\u22f1",dtri:"\u25bf",dtrif:"\u25be",duarr:"\u21f5",duhar:"\u296f",dwangle:"\u29a6",DZcy:"\u040f",dzcy:"\u045f",dzigrarr:"\u27ff",Eacute:"\xc9",eacute:"\xe9",easter:"\u2a6e",Ecaron:"\u011a",ecaron:"\u011b",Ecirc:"\xca",ecirc:"\xea",ecir:"\u2256",ecolon:"\u2255",Ecy:"\u042d",ecy:"\u044d",eDDot:"\u2a77",Edot:"\u0116",edot:"\u0117",eDot:"\u2251",ee:"\u2147",efDot:"\u2252",Efr:"\ud835\udd08",efr:"\ud835\udd22",eg:"\u2a9a",Egrave:"\xc8",egrave:"\xe8",egs:"\u2a96",egsdot:"\u2a98",el:"\u2a99",Element:"\u2208",elinters:"\u23e7",ell:"\u2113",els:"\u2a95",elsdot:"\u2a97",Emacr:"\u0112",emacr:"\u0113",empty:"\u2205",emptyset:"\u2205",EmptySmallSquare:"\u25fb",emptyv:"\u2205",EmptyVerySmallSquare:"\u25ab",emsp13:"\u2004",emsp14:"\u2005",emsp:"\u2003",ENG:"\u014a",eng:"\u014b",ensp:"\u2002",Eogon:"\u0118",eogon:"\u0119",Eopf:"\ud835\udd3c",eopf:"\ud835\udd56",epar:"\u22d5",eparsl:"\u29e3",eplus:"\u2a71",epsi:"\u03b5",Epsilon:"\u0395",epsilon:"\u03b5",epsiv:"\u03f5",eqcirc:"\u2256",eqcolon:"\u2255",eqsim:"\u2242",eqslantgtr:"\u2a96",eqslantless:"\u2a95",Equal:"\u2a75",equals:"=",EqualTilde:"\u2242",equest:"\u225f",Equilibrium:"\u21cc",equiv:"\u2261",equivDD:"\u2a78",eqvparsl:"\u29e5",erarr:"\u2971",erDot:"\u2253",escr:"\u212f",Escr:"\u2130",esdot:"\u2250",Esim:"\u2a73",esim:"\u2242",Eta:"\u0397",eta:"\u03b7",ETH:"\xd0",eth:"\xf0",Euml:"\xcb",euml:"\xeb",euro:"\u20ac",excl:"!",exist:"\u2203",Exists:"\u2203",expectation:"\u2130",exponentiale:"\u2147",ExponentialE:"\u2147",fallingdotseq:"\u2252",Fcy:"\u0424",fcy:"\u0444",female:"\u2640",ffilig:"\ufb03",fflig:"\ufb00",ffllig:"\ufb04",Ffr:"\ud835\udd09",ffr:"\ud835\udd23",filig:"\ufb01",FilledSmallSquare:"\u25fc",FilledVerySmallSquare:"\u25aa",fjlig:"fj",flat:"\u266d",fllig:"\ufb02",fltns:"\u25b1",fnof:"\u0192",Fopf:"\ud835\udd3d",fopf:"\ud835\udd57",forall:"\u2200",ForAll:"\u2200",fork:"\u22d4",forkv:"\u2ad9",Fouriertrf:"\u2131",fpartint:"\u2a0d",frac12:"\xbd",frac13:"\u2153",frac14:"\xbc",frac15:"\u2155",frac16:"\u2159",frac18:"\u215b",frac23:"\u2154",frac25:"\u2156",frac34:"\xbe",frac35:"\u2157",frac38:"\u215c",frac45:"\u2158",frac56:"\u215a",frac58:"\u215d",frac78:"\u215e",frasl:"\u2044",frown:"\u2322",fscr:"\ud835\udcbb",Fscr:"\u2131",gacute:"\u01f5",Gamma:"\u0393",gamma:"\u03b3",Gammad:"\u03dc",gammad:"\u03dd",gap:"\u2a86",Gbreve:"\u011e",gbreve:"\u011f",Gcedil:"\u0122",Gcirc:"\u011c",gcirc:"\u011d",Gcy:"\u0413",gcy:"\u0433",Gdot:"\u0120",gdot:"\u0121",ge:"\u2265",gE:"\u2267",gEl:"\u2a8c",gel:"\u22db",geq:"\u2265",geqq:"\u2267",geqslant:"\u2a7e",gescc:"\u2aa9",ges:"\u2a7e",gesdot:"\u2a80",gesdoto:"\u2a82",gesdotol:"\u2a84",gesl:"\u22db\ufe00",gesles:"\u2a94",Gfr:"\ud835\udd0a",gfr:"\ud835\udd24",gg:"\u226b",Gg:"\u22d9",ggg:"\u22d9",gimel:"\u2137",GJcy:"\u0403",gjcy:"\u0453",gla:"\u2aa5",gl:"\u2277",glE:"\u2a92",glj:"\u2aa4",gnap:"\u2a8a",gnapprox:"\u2a8a",gne:"\u2a88",gnE:"\u2269",gneq:"\u2a88",gneqq:"\u2269",gnsim:"\u22e7",Gopf:"\ud835\udd3e",gopf:"\ud835\udd58",grave:"`",GreaterEqual:"\u2265",GreaterEqualLess:"\u22db",GreaterFullEqual:"\u2267",GreaterGreater:"\u2aa2",GreaterLess:"\u2277",GreaterSlantEqual:"\u2a7e",GreaterTilde:"\u2273",Gscr:"\ud835\udca2",gscr:"\u210a",gsim:"\u2273",gsime:"\u2a8e",gsiml:"\u2a90",gtcc:"\u2aa7",gtcir:"\u2a7a",gt:">",GT:">",Gt:"\u226b",gtdot:"\u22d7",gtlPar:"\u2995",gtquest:"\u2a7c",gtrapprox:"\u2a86",gtrarr:"\u2978",gtrdot:"\u22d7",gtreqless:"\u22db",gtreqqless:"\u2a8c",gtrless:"\u2277",gtrsim:"\u2273",gvertneqq:"\u2269\ufe00",gvnE:"\u2269\ufe00",Hacek:"\u02c7",hairsp:"\u200a",half:"\xbd",hamilt:"\u210b",HARDcy:"\u042a",hardcy:"\u044a",harrcir:"\u2948",harr:"\u2194",hArr:"\u21d4",harrw:"\u21ad",Hat:"^",hbar:"\u210f",Hcirc:"\u0124",hcirc:"\u0125",hearts:"\u2665",heartsuit:"\u2665",hellip:"\u2026",hercon:"\u22b9",hfr:"\ud835\udd25",Hfr:"\u210c",HilbertSpace:"\u210b",hksearow:"\u2925",hkswarow:"\u2926",hoarr:"\u21ff",homtht:"\u223b",hookleftarrow:"\u21a9",hookrightarrow:"\u21aa",hopf:"\ud835\udd59",Hopf:"\u210d",horbar:"\u2015",HorizontalLine:"\u2500",hscr:"\ud835\udcbd",Hscr:"\u210b",hslash:"\u210f",Hstrok:"\u0126",hstrok:"\u0127",HumpDownHump:"\u224e",HumpEqual:"\u224f",hybull:"\u2043",hyphen:"\u2010",Iacute:"\xcd",iacute:"\xed",ic:"\u2063",Icirc:"\xce",icirc:"\xee",Icy:"\u0418",icy:"\u0438",Idot:"\u0130",IEcy:"\u0415",iecy:"\u0435",iexcl:"\xa1",iff:"\u21d4",ifr:"\ud835\udd26",Ifr:"\u2111",Igrave:"\xcc",igrave:"\xec",ii:"\u2148",iiiint:"\u2a0c",iiint:"\u222d",iinfin:"\u29dc",iiota:"\u2129",IJlig:"\u0132",ijlig:"\u0133",Imacr:"\u012a",imacr:"\u012b",image:"\u2111",ImaginaryI:"\u2148",imagline:"\u2110",imagpart:"\u2111",imath:"\u0131",Im:"\u2111",imof:"\u22b7",imped:"\u01b5",Implies:"\u21d2",incare:"\u2105",in:"\u2208",infin:"\u221e",infintie:"\u29dd",inodot:"\u0131",intcal:"\u22ba",int:"\u222b",Int:"\u222c",integers:"\u2124",Integral:"\u222b",intercal:"\u22ba",Intersection:"\u22c2",intlarhk:"\u2a17",intprod:"\u2a3c",InvisibleComma:"\u2063",InvisibleTimes:"\u2062",IOcy:"\u0401",iocy:"\u0451",Iogon:"\u012e",iogon:"\u012f",Iopf:"\ud835\udd40",iopf:"\ud835\udd5a",Iota:"\u0399",iota:"\u03b9",iprod:"\u2a3c",iquest:"\xbf",iscr:"\ud835\udcbe",Iscr:"\u2110",isin:"\u2208",isindot:"\u22f5",isinE:"\u22f9",isins:"\u22f4",isinsv:"\u22f3",isinv:"\u2208",it:"\u2062",Itilde:"\u0128",itilde:"\u0129",Iukcy:"\u0406",iukcy:"\u0456",Iuml:"\xcf",iuml:"\xef",Jcirc:"\u0134",jcirc:"\u0135",Jcy:"\u0419",jcy:"\u0439",Jfr:"\ud835\udd0d",jfr:"\ud835\udd27",jmath:"\u0237",Jopf:"\ud835\udd41",jopf:"\ud835\udd5b",Jscr:"\ud835\udca5",jscr:"\ud835\udcbf",Jsercy:"\u0408",jsercy:"\u0458",Jukcy:"\u0404",jukcy:"\u0454",Kappa:"\u039a",kappa:"\u03ba",kappav:"\u03f0",Kcedil:"\u0136",kcedil:"\u0137",Kcy:"\u041a",kcy:"\u043a",Kfr:"\ud835\udd0e",kfr:"\ud835\udd28",kgreen:"\u0138",KHcy:"\u0425",khcy:"\u0445",KJcy:"\u040c",kjcy:"\u045c",Kopf:"\ud835\udd42",kopf:"\ud835\udd5c",Kscr:"\ud835\udca6",kscr:"\ud835\udcc0",lAarr:"\u21da",Lacute:"\u0139",lacute:"\u013a",laemptyv:"\u29b4",lagran:"\u2112",Lambda:"\u039b",lambda:"\u03bb",lang:"\u27e8",Lang:"\u27ea",langd:"\u2991",langle:"\u27e8",lap:"\u2a85",Laplacetrf:"\u2112",laquo:"\xab",larrb:"\u21e4",larrbfs:"\u291f",larr:"\u2190",Larr:"\u219e",lArr:"\u21d0",larrfs:"\u291d",larrhk:"\u21a9",larrlp:"\u21ab",larrpl:"\u2939",larrsim:"\u2973",larrtl:"\u21a2",latail:"\u2919",lAtail:"\u291b",lat:"\u2aab",late:"\u2aad",lates:"\u2aad\ufe00",lbarr:"\u290c",lBarr:"\u290e",lbbrk:"\u2772",lbrace:"{",lbrack:"[",lbrke:"\u298b",lbrksld:"\u298f",lbrkslu:"\u298d",Lcaron:"\u013d",lcaron:"\u013e",Lcedil:"\u013b",lcedil:"\u013c",lceil:"\u2308",lcub:"{",Lcy:"\u041b",lcy:"\u043b",ldca:"\u2936",ldquo:"\u201c",ldquor:"\u201e",ldrdhar:"\u2967",ldrushar:"\u294b",ldsh:"\u21b2",le:"\u2264",lE:"\u2266",LeftAngleBracket:"\u27e8",LeftArrowBar:"\u21e4",leftarrow:"\u2190",LeftArrow:"\u2190",Leftarrow:"\u21d0",LeftArrowRightArrow:"\u21c6",leftarrowtail:"\u21a2",LeftCeiling:"\u2308",LeftDoubleBracket:"\u27e6",LeftDownTeeVector:"\u2961",LeftDownVectorBar:"\u2959",LeftDownVector:"\u21c3",LeftFloor:"\u230a",leftharpoondown:"\u21bd",leftharpoonup:"\u21bc",leftleftarrows:"\u21c7",leftrightarrow:"\u2194",LeftRightArrow:"\u2194",Leftrightarrow:"\u21d4",leftrightarrows:"\u21c6",leftrightharpoons:"\u21cb",leftrightsquigarrow:"\u21ad",LeftRightVector:"\u294e",LeftTeeArrow:"\u21a4",LeftTee:"\u22a3",LeftTeeVector:"\u295a",leftthreetimes:"\u22cb",LeftTriangleBar:"\u29cf",LeftTriangle:"\u22b2",LeftTriangleEqual:"\u22b4",LeftUpDownVector:"\u2951",LeftUpTeeVector:"\u2960",LeftUpVectorBar:"\u2958",LeftUpVector:"\u21bf",LeftVectorBar:"\u2952",LeftVector:"\u21bc",lEg:"\u2a8b",leg:"\u22da",leq:"\u2264",leqq:"\u2266",leqslant:"\u2a7d",lescc:"\u2aa8",les:"\u2a7d",lesdot:"\u2a7f",lesdoto:"\u2a81",lesdotor:"\u2a83",lesg:"\u22da\ufe00",lesges:"\u2a93",lessapprox:"\u2a85",lessdot:"\u22d6",lesseqgtr:"\u22da",lesseqqgtr:"\u2a8b",LessEqualGreater:"\u22da",LessFullEqual:"\u2266",LessGreater:"\u2276",lessgtr:"\u2276",LessLess:"\u2aa1",lesssim:"\u2272",LessSlantEqual:"\u2a7d",LessTilde:"\u2272",lfisht:"\u297c",lfloor:"\u230a",Lfr:"\ud835\udd0f",lfr:"\ud835\udd29",lg:"\u2276",lgE:"\u2a91",lHar:"\u2962",lhard:"\u21bd",lharu:"\u21bc",lharul:"\u296a",lhblk:"\u2584",LJcy:"\u0409",ljcy:"\u0459",llarr:"\u21c7",ll:"\u226a",Ll:"\u22d8",llcorner:"\u231e",Lleftarrow:"\u21da",llhard:"\u296b",lltri:"\u25fa",Lmidot:"\u013f",lmidot:"\u0140",lmoustache:"\u23b0",lmoust:"\u23b0",lnap:"\u2a89",lnapprox:"\u2a89",lne:"\u2a87",lnE:"\u2268",lneq:"\u2a87",lneqq:"\u2268",lnsim:"\u22e6",loang:"\u27ec",loarr:"\u21fd",lobrk:"\u27e6",longleftarrow:"\u27f5",LongLeftArrow:"\u27f5",Longleftarrow:"\u27f8",longleftrightarrow:"\u27f7",LongLeftRightArrow:"\u27f7",Longleftrightarrow:"\u27fa",longmapsto:"\u27fc",longrightarrow:"\u27f6",LongRightArrow:"\u27f6",Longrightarrow:"\u27f9",looparrowleft:"\u21ab",looparrowright:"\u21ac",lopar:"\u2985",Lopf:"\ud835\udd43",lopf:"\ud835\udd5d",loplus:"\u2a2d",lotimes:"\u2a34",lowast:"\u2217",lowbar:"_",LowerLeftArrow:"\u2199",LowerRightArrow:"\u2198",loz:"\u25ca",lozenge:"\u25ca",lozf:"\u29eb",lpar:"(",lparlt:"\u2993",lrarr:"\u21c6",lrcorner:"\u231f",lrhar:"\u21cb",lrhard:"\u296d",lrm:"\u200e",lrtri:"\u22bf",lsaquo:"\u2039",lscr:"\ud835\udcc1",Lscr:"\u2112",lsh:"\u21b0",Lsh:"\u21b0",lsim:"\u2272",lsime:"\u2a8d",lsimg:"\u2a8f",lsqb:"[",lsquo:"\u2018",lsquor:"\u201a",Lstrok:"\u0141",lstrok:"\u0142",ltcc:"\u2aa6",ltcir:"\u2a79",lt:"<",LT:"<",Lt:"\u226a",ltdot:"\u22d6",lthree:"\u22cb",ltimes:"\u22c9",ltlarr:"\u2976",ltquest:"\u2a7b",ltri:"\u25c3",ltrie:"\u22b4",ltrif:"\u25c2",ltrPar:"\u2996",lurdshar:"\u294a",luruhar:"\u2966",lvertneqq:"\u2268\ufe00",lvnE:"\u2268\ufe00",macr:"\xaf",male:"\u2642",malt:"\u2720",maltese:"\u2720",Map:"\u2905",map:"\u21a6",mapsto:"\u21a6",mapstodown:"\u21a7",mapstoleft:"\u21a4",mapstoup:"\u21a5",marker:"\u25ae",mcomma:"\u2a29",Mcy:"\u041c",mcy:"\u043c",mdash:"\u2014",mDDot:"\u223a",measuredangle:"\u2221",MediumSpace:"\u205f",Mellintrf:"\u2133",Mfr:"\ud835\udd10",mfr:"\ud835\udd2a",mho:"\u2127",micro:"\xb5",midast:"*",midcir:"\u2af0",mid:"\u2223",middot:"\xb7",minusb:"\u229f",minus:"\u2212",minusd:"\u2238",minusdu:"\u2a2a",MinusPlus:"\u2213",mlcp:"\u2adb",mldr:"\u2026",mnplus:"\u2213",models:"\u22a7",Mopf:"\ud835\udd44",mopf:"\ud835\udd5e",mp:"\u2213",mscr:"\ud835\udcc2",Mscr:"\u2133",mstpos:"\u223e",Mu:"\u039c",mu:"\u03bc",multimap:"\u22b8",mumap:"\u22b8",nabla:"\u2207",Nacute:"\u0143",nacute:"\u0144",nang:"\u2220\u20d2",nap:"\u2249",napE:"\u2a70\u0338",napid:"\u224b\u0338",napos:"\u0149",napprox:"\u2249",natural:"\u266e",naturals:"\u2115",natur:"\u266e",nbsp:"\xa0",nbump:"\u224e\u0338",nbumpe:"\u224f\u0338",ncap:"\u2a43",Ncaron:"\u0147",ncaron:"\u0148",Ncedil:"\u0145",ncedil:"\u0146",ncong:"\u2247",ncongdot:"\u2a6d\u0338",ncup:"\u2a42",Ncy:"\u041d",ncy:"\u043d",ndash:"\u2013",nearhk:"\u2924",nearr:"\u2197",neArr:"\u21d7",nearrow:"\u2197",ne:"\u2260",nedot:"\u2250\u0338",NegativeMediumSpace:"\u200b",NegativeThickSpace:"\u200b",NegativeThinSpace:"\u200b",NegativeVeryThinSpace:"\u200b",nequiv:"\u2262",nesear:"\u2928",nesim:"\u2242\u0338",NestedGreaterGreater:"\u226b",NestedLessLess:"\u226a",NewLine:"\n",nexist:"\u2204",nexists:"\u2204",Nfr:"\ud835\udd11",nfr:"\ud835\udd2b",ngE:"\u2267\u0338",nge:"\u2271",ngeq:"\u2271",ngeqq:"\u2267\u0338",ngeqslant:"\u2a7e\u0338",nges:"\u2a7e\u0338",nGg:"\u22d9\u0338",ngsim:"\u2275",nGt:"\u226b\u20d2",ngt:"\u226f",ngtr:"\u226f",nGtv:"\u226b\u0338",nharr:"\u21ae",nhArr:"\u21ce",nhpar:"\u2af2",ni:"\u220b",nis:"\u22fc",nisd:"\u22fa",niv:"\u220b",NJcy:"\u040a",njcy:"\u045a",nlarr:"\u219a",nlArr:"\u21cd",nldr:"\u2025",nlE:"\u2266\u0338",nle:"\u2270",nleftarrow:"\u219a",nLeftarrow:"\u21cd",nleftrightarrow:"\u21ae",nLeftrightarrow:"\u21ce",nleq:"\u2270",nleqq:"\u2266\u0338",nleqslant:"\u2a7d\u0338",nles:"\u2a7d\u0338",nless:"\u226e",nLl:"\u22d8\u0338",nlsim:"\u2274",nLt:"\u226a\u20d2",nlt:"\u226e",nltri:"\u22ea",nltrie:"\u22ec",nLtv:"\u226a\u0338",nmid:"\u2224",NoBreak:"\u2060",NonBreakingSpace:"\xa0",nopf:"\ud835\udd5f",Nopf:"\u2115",Not:"\u2aec",not:"\xac",NotCongruent:"\u2262",NotCupCap:"\u226d",NotDoubleVerticalBar:"\u2226",NotElement:"\u2209",NotEqual:"\u2260",NotEqualTilde:"\u2242\u0338",NotExists:"\u2204",NotGreater:"\u226f",NotGreaterEqual:"\u2271",NotGreaterFullEqual:"\u2267\u0338",NotGreaterGreater:"\u226b\u0338",NotGreaterLess:"\u2279",NotGreaterSlantEqual:"\u2a7e\u0338",NotGreaterTilde:"\u2275",NotHumpDownHump:"\u224e\u0338",NotHumpEqual:"\u224f\u0338",notin:"\u2209",notindot:"\u22f5\u0338",notinE:"\u22f9\u0338",notinva:"\u2209",notinvb:"\u22f7",notinvc:"\u22f6",NotLeftTriangleBar:"\u29cf\u0338",NotLeftTriangle:"\u22ea",NotLeftTriangleEqual:"\u22ec",NotLess:"\u226e",NotLessEqual:"\u2270",NotLessGreater:"\u2278",NotLessLess:"\u226a\u0338",NotLessSlantEqual:"\u2a7d\u0338",NotLessTilde:"\u2274",NotNestedGreaterGreater:"\u2aa2\u0338",NotNestedLessLess:"\u2aa1\u0338",notni:"\u220c",notniva:"\u220c",notnivb:"\u22fe",notnivc:"\u22fd",NotPrecedes:"\u2280",NotPrecedesEqual:"\u2aaf\u0338",NotPrecedesSlantEqual:"\u22e0",NotReverseElement:"\u220c",NotRightTriangleBar:"\u29d0\u0338",NotRightTriangle:"\u22eb",NotRightTriangleEqual:"\u22ed",NotSquareSubset:"\u228f\u0338",NotSquareSubsetEqual:"\u22e2",NotSquareSuperset:"\u2290\u0338",NotSquareSupersetEqual:"\u22e3",NotSubset:"\u2282\u20d2",NotSubsetEqual:"\u2288",NotSucceeds:"\u2281",NotSucceedsEqual:"\u2ab0\u0338",NotSucceedsSlantEqual:"\u22e1",NotSucceedsTilde:"\u227f\u0338",NotSuperset:"\u2283\u20d2",NotSupersetEqual:"\u2289",NotTilde:"\u2241",NotTildeEqual:"\u2244",NotTildeFullEqual:"\u2247",NotTildeTilde:"\u2249",NotVerticalBar:"\u2224",nparallel:"\u2226",npar:"\u2226",nparsl:"\u2afd\u20e5",npart:"\u2202\u0338",npolint:"\u2a14",npr:"\u2280",nprcue:"\u22e0",nprec:"\u2280",npreceq:"\u2aaf\u0338",npre:"\u2aaf\u0338",nrarrc:"\u2933\u0338",nrarr:"\u219b",nrArr:"\u21cf",nrarrw:"\u219d\u0338",nrightarrow:"\u219b",nRightarrow:"\u21cf",nrtri:"\u22eb",nrtrie:"\u22ed",nsc:"\u2281",nsccue:"\u22e1",nsce:"\u2ab0\u0338",Nscr:"\ud835\udca9",nscr:"\ud835\udcc3",nshortmid:"\u2224",nshortparallel:"\u2226",nsim:"\u2241",nsime:"\u2244",nsimeq:"\u2244",nsmid:"\u2224",nspar:"\u2226",nsqsube:"\u22e2",nsqsupe:"\u22e3",nsub:"\u2284",nsubE:"\u2ac5\u0338",nsube:"\u2288",nsubset:"\u2282\u20d2",nsubseteq:"\u2288",nsubseteqq:"\u2ac5\u0338",nsucc:"\u2281",nsucceq:"\u2ab0\u0338",nsup:"\u2285",nsupE:"\u2ac6\u0338",nsupe:"\u2289",nsupset:"\u2283\u20d2",nsupseteq:"\u2289",nsupseteqq:"\u2ac6\u0338",ntgl:"\u2279",Ntilde:"\xd1",ntilde:"\xf1",ntlg:"\u2278",ntriangleleft:"\u22ea",ntrianglelefteq:"\u22ec",ntriangleright:"\u22eb",ntrianglerighteq:"\u22ed",Nu:"\u039d",nu:"\u03bd",num:"#",numero:"\u2116",numsp:"\u2007",nvap:"\u224d\u20d2",nvdash:"\u22ac",nvDash:"\u22ad",nVdash:"\u22ae",nVDash:"\u22af",nvge:"\u2265\u20d2",nvgt:">\u20d2",nvHarr:"\u2904",nvinfin:"\u29de",nvlArr:"\u2902",nvle:"\u2264\u20d2",nvlt:"<\u20d2",nvltrie:"\u22b4\u20d2",nvrArr:"\u2903",nvrtrie:"\u22b5\u20d2",nvsim:"\u223c\u20d2",nwarhk:"\u2923",nwarr:"\u2196",nwArr:"\u21d6",nwarrow:"\u2196",nwnear:"\u2927",Oacute:"\xd3",oacute:"\xf3",oast:"\u229b",Ocirc:"\xd4",ocirc:"\xf4",ocir:"\u229a",Ocy:"\u041e",ocy:"\u043e",odash:"\u229d",Odblac:"\u0150",odblac:"\u0151",odiv:"\u2a38",odot:"\u2299",odsold:"\u29bc",OElig:"\u0152",oelig:"\u0153",ofcir:"\u29bf",Ofr:"\ud835\udd12",ofr:"\ud835\udd2c",ogon:"\u02db",Ograve:"\xd2",ograve:"\xf2",ogt:"\u29c1",ohbar:"\u29b5",ohm:"\u03a9",oint:"\u222e",olarr:"\u21ba",olcir:"\u29be",olcross:"\u29bb",oline:"\u203e",olt:"\u29c0",Omacr:"\u014c",omacr:"\u014d",Omega:"\u03a9",omega:"\u03c9",Omicron:"\u039f",omicron:"\u03bf",omid:"\u29b6",ominus:"\u2296",Oopf:"\ud835\udd46",oopf:"\ud835\udd60",opar:"\u29b7",OpenCurlyDoubleQuote:"\u201c",OpenCurlyQuote:"\u2018",operp:"\u29b9",oplus:"\u2295",orarr:"\u21bb",Or:"\u2a54",or:"\u2228",ord:"\u2a5d",order:"\u2134",orderof:"\u2134",ordf:"\xaa",ordm:"\xba",origof:"\u22b6",oror:"\u2a56",orslope:"\u2a57",orv:"\u2a5b",oS:"\u24c8",Oscr:"\ud835\udcaa",oscr:"\u2134",Oslash:"\xd8",oslash:"\xf8",osol:"\u2298",Otilde:"\xd5",otilde:"\xf5",otimesas:"\u2a36",Otimes:"\u2a37",otimes:"\u2297",Ouml:"\xd6",ouml:"\xf6",ovbar:"\u233d",OverBar:"\u203e",OverBrace:"\u23de",OverBracket:"\u23b4",OverParenthesis:"\u23dc",para:"\xb6",parallel:"\u2225",par:"\u2225",parsim:"\u2af3",parsl:"\u2afd",part:"\u2202",PartialD:"\u2202",Pcy:"\u041f",pcy:"\u043f",percnt:"%",period:".",permil:"\u2030",perp:"\u22a5",pertenk:"\u2031",Pfr:"\ud835\udd13",pfr:"\ud835\udd2d",Phi:"\u03a6",phi:"\u03c6",phiv:"\u03d5",phmmat:"\u2133",phone:"\u260e",Pi:"\u03a0",pi:"\u03c0",pitchfork:"\u22d4",piv:"\u03d6",planck:"\u210f",planckh:"\u210e",plankv:"\u210f",plusacir:"\u2a23",plusb:"\u229e",pluscir:"\u2a22",plus:"+",plusdo:"\u2214",plusdu:"\u2a25",pluse:"\u2a72",PlusMinus:"\xb1",plusmn:"\xb1",plussim:"\u2a26",plustwo:"\u2a27",pm:"\xb1",Poincareplane:"\u210c",pointint:"\u2a15",popf:"\ud835\udd61",Popf:"\u2119",pound:"\xa3",prap:"\u2ab7",Pr:"\u2abb",pr:"\u227a",prcue:"\u227c",precapprox:"\u2ab7",prec:"\u227a",preccurlyeq:"\u227c",Precedes:"\u227a",PrecedesEqual:"\u2aaf",PrecedesSlantEqual:"\u227c",PrecedesTilde:"\u227e",preceq:"\u2aaf",precnapprox:"\u2ab9",precneqq:"\u2ab5",precnsim:"\u22e8",pre:"\u2aaf",prE:"\u2ab3",precsim:"\u227e",prime:"\u2032",Prime:"\u2033",primes:"\u2119",prnap:"\u2ab9",prnE:"\u2ab5",prnsim:"\u22e8",prod:"\u220f",Product:"\u220f",profalar:"\u232e",profline:"\u2312",profsurf:"\u2313",prop:"\u221d",Proportional:"\u221d",Proportion:"\u2237",propto:"\u221d",prsim:"\u227e",prurel:"\u22b0",Pscr:"\ud835\udcab",pscr:"\ud835\udcc5",Psi:"\u03a8",psi:"\u03c8",puncsp:"\u2008",Qfr:"\ud835\udd14",qfr:"\ud835\udd2e",qint:"\u2a0c",qopf:"\ud835\udd62",Qopf:"\u211a",qprime:"\u2057",Qscr:"\ud835\udcac",qscr:"\ud835\udcc6",quaternions:"\u210d",quatint:"\u2a16",quest:"?",questeq:"\u225f",quot:'"',QUOT:'"',rAarr:"\u21db",race:"\u223d\u0331",Racute:"\u0154",racute:"\u0155",radic:"\u221a",raemptyv:"\u29b3",rang:"\u27e9",Rang:"\u27eb",rangd:"\u2992",range:"\u29a5",rangle:"\u27e9",raquo:"\xbb",rarrap:"\u2975",rarrb:"\u21e5",rarrbfs:"\u2920",rarrc:"\u2933",rarr:"\u2192",Rarr:"\u21a0",rArr:"\u21d2",rarrfs:"\u291e",rarrhk:"\u21aa",rarrlp:"\u21ac",rarrpl:"\u2945",rarrsim:"\u2974",Rarrtl:"\u2916",rarrtl:"\u21a3",rarrw:"\u219d",ratail:"\u291a",rAtail:"\u291c",ratio:"\u2236",rationals:"\u211a",rbarr:"\u290d",rBarr:"\u290f",RBarr:"\u2910",rbbrk:"\u2773",rbrace:"}",rbrack:"]",rbrke:"\u298c",rbrksld:"\u298e",rbrkslu:"\u2990",Rcaron:"\u0158",rcaron:"\u0159",Rcedil:"\u0156",rcedil:"\u0157",rceil:"\u2309",rcub:"}",Rcy:"\u0420",rcy:"\u0440",rdca:"\u2937",rdldhar:"\u2969",rdquo:"\u201d",rdquor:"\u201d",rdsh:"\u21b3",real:"\u211c",realine:"\u211b",realpart:"\u211c",reals:"\u211d",Re:"\u211c",rect:"\u25ad",reg:"\xae",REG:"\xae",ReverseElement:"\u220b",ReverseEquilibrium:"\u21cb",ReverseUpEquilibrium:"\u296f",rfisht:"\u297d",rfloor:"\u230b",rfr:"\ud835\udd2f",Rfr:"\u211c",rHar:"\u2964",rhard:"\u21c1",rharu:"\u21c0",rharul:"\u296c",Rho:"\u03a1",rho:"\u03c1",rhov:"\u03f1",RightAngleBracket:"\u27e9",RightArrowBar:"\u21e5",rightarrow:"\u2192",RightArrow:"\u2192",Rightarrow:"\u21d2",RightArrowLeftArrow:"\u21c4",rightarrowtail:"\u21a3",RightCeiling:"\u2309",RightDoubleBracket:"\u27e7",RightDownTeeVector:"\u295d",RightDownVectorBar:"\u2955",RightDownVector:"\u21c2",RightFloor:"\u230b",rightharpoondown:"\u21c1",rightharpoonup:"\u21c0",rightleftarrows:"\u21c4",rightleftharpoons:"\u21cc",rightrightarrows:"\u21c9",rightsquigarrow:"\u219d",RightTeeArrow:"\u21a6",RightTee:"\u22a2",RightTeeVector:"\u295b",rightthreetimes:"\u22cc",RightTriangleBar:"\u29d0",RightTriangle:"\u22b3",RightTriangleEqual:"\u22b5",RightUpDownVector:"\u294f",RightUpTeeVector:"\u295c",RightUpVectorBar:"\u2954",RightUpVector:"\u21be",RightVectorBar:"\u2953",RightVector:"\u21c0",ring:"\u02da",risingdotseq:"\u2253",rlarr:"\u21c4",rlhar:"\u21cc",rlm:"\u200f",rmoustache:"\u23b1",rmoust:"\u23b1",rnmid:"\u2aee",roang:"\u27ed",roarr:"\u21fe",robrk:"\u27e7",ropar:"\u2986",ropf:"\ud835\udd63",Ropf:"\u211d",roplus:"\u2a2e",rotimes:"\u2a35",RoundImplies:"\u2970",rpar:")",rpargt:"\u2994",rppolint:"\u2a12",rrarr:"\u21c9",Rrightarrow:"\u21db",rsaquo:"\u203a",rscr:"\ud835\udcc7",Rscr:"\u211b",rsh:"\u21b1",Rsh:"\u21b1",rsqb:"]",rsquo:"\u2019",rsquor:"\u2019",rthree:"\u22cc",rtimes:"\u22ca",rtri:"\u25b9",rtrie:"\u22b5",rtrif:"\u25b8",rtriltri:"\u29ce",RuleDelayed:"\u29f4",ruluhar:"\u2968",rx:"\u211e",Sacute:"\u015a",sacute:"\u015b",sbquo:"\u201a",scap:"\u2ab8",Scaron:"\u0160",scaron:"\u0161",Sc:"\u2abc",sc:"\u227b",sccue:"\u227d",sce:"\u2ab0",scE:"\u2ab4",Scedil:"\u015e",scedil:"\u015f",Scirc:"\u015c",scirc:"\u015d",scnap:"\u2aba",scnE:"\u2ab6",scnsim:"\u22e9",scpolint:"\u2a13",scsim:"\u227f",Scy:"\u0421",scy:"\u0441",sdotb:"\u22a1",sdot:"\u22c5",sdote:"\u2a66",searhk:"\u2925",searr:"\u2198",seArr:"\u21d8",searrow:"\u2198",sect:"\xa7",semi:";",seswar:"\u2929",setminus:"\u2216",setmn:"\u2216",sext:"\u2736",Sfr:"\ud835\udd16",sfr:"\ud835\udd30",sfrown:"\u2322",sharp:"\u266f",SHCHcy:"\u0429",shchcy:"\u0449",SHcy:"\u0428",shcy:"\u0448",ShortDownArrow:"\u2193",ShortLeftArrow:"\u2190",shortmid:"\u2223",shortparallel:"\u2225",ShortRightArrow:"\u2192",ShortUpArrow:"\u2191",shy:"\xad",Sigma:"\u03a3",sigma:"\u03c3",sigmaf:"\u03c2",sigmav:"\u03c2",sim:"\u223c",simdot:"\u2a6a",sime:"\u2243",simeq:"\u2243",simg:"\u2a9e",simgE:"\u2aa0",siml:"\u2a9d",simlE:"\u2a9f",simne:"\u2246",simplus:"\u2a24",simrarr:"\u2972",slarr:"\u2190",SmallCircle:"\u2218",smallsetminus:"\u2216",smashp:"\u2a33",smeparsl:"\u29e4",smid:"\u2223",smile:"\u2323",smt:"\u2aaa",smte:"\u2aac",smtes:"\u2aac\ufe00",SOFTcy:"\u042c",softcy:"\u044c",solbar:"\u233f",solb:"\u29c4",sol:"/",Sopf:"\ud835\udd4a",sopf:"\ud835\udd64",spades:"\u2660",spadesuit:"\u2660",spar:"\u2225",sqcap:"\u2293",sqcaps:"\u2293\ufe00",sqcup:"\u2294",sqcups:"\u2294\ufe00",Sqrt:"\u221a",sqsub:"\u228f",sqsube:"\u2291",sqsubset:"\u228f",sqsubseteq:"\u2291",sqsup:"\u2290",sqsupe:"\u2292",sqsupset:"\u2290",sqsupseteq:"\u2292",square:"\u25a1",Square:"\u25a1",SquareIntersection:"\u2293",SquareSubset:"\u228f",SquareSubsetEqual:"\u2291",SquareSuperset:"\u2290",SquareSupersetEqual:"\u2292",SquareUnion:"\u2294",squarf:"\u25aa",squ:"\u25a1",squf:"\u25aa",srarr:"\u2192",Sscr:"\ud835\udcae",sscr:"\ud835\udcc8",ssetmn:"\u2216",ssmile:"\u2323",sstarf:"\u22c6",Star:"\u22c6",star:"\u2606",starf:"\u2605",straightepsilon:"\u03f5",straightphi:"\u03d5",strns:"\xaf",sub:"\u2282",Sub:"\u22d0",subdot:"\u2abd",subE:"\u2ac5",sube:"\u2286",subedot:"\u2ac3",submult:"\u2ac1",subnE:"\u2acb",subne:"\u228a",subplus:"\u2abf",subrarr:"\u2979",subset:"\u2282",Subset:"\u22d0",subseteq:"\u2286",subseteqq:"\u2ac5",SubsetEqual:"\u2286",subsetneq:"\u228a",subsetneqq:"\u2acb",subsim:"\u2ac7",subsub:"\u2ad5",subsup:"\u2ad3",succapprox:"\u2ab8",succ:"\u227b",succcurlyeq:"\u227d",Succeeds:"\u227b",SucceedsEqual:"\u2ab0",SucceedsSlantEqual:"\u227d",SucceedsTilde:"\u227f",succeq:"\u2ab0",succnapprox:"\u2aba",succneqq:"\u2ab6",succnsim:"\u22e9",succsim:"\u227f",SuchThat:"\u220b",sum:"\u2211",Sum:"\u2211",sung:"\u266a",sup1:"\xb9",sup2:"\xb2",sup3:"\xb3",sup:"\u2283",Sup:"\u22d1",supdot:"\u2abe",supdsub:"\u2ad8",supE:"\u2ac6",supe:"\u2287",supedot:"\u2ac4",Superset:"\u2283",SupersetEqual:"\u2287",suphsol:"\u27c9",suphsub:"\u2ad7",suplarr:"\u297b",supmult:"\u2ac2",supnE:"\u2acc",supne:"\u228b",supplus:"\u2ac0",supset:"\u2283",Supset:"\u22d1",supseteq:"\u2287",supseteqq:"\u2ac6",supsetneq:"\u228b",supsetneqq:"\u2acc",supsim:"\u2ac8",supsub:"\u2ad4",supsup:"\u2ad6",swarhk:"\u2926",swarr:"\u2199",swArr:"\u21d9",swarrow:"\u2199",swnwar:"\u292a",szlig:"\xdf",Tab:"\t",target:"\u2316",Tau:"\u03a4",tau:"\u03c4",tbrk:"\u23b4",Tcaron:"\u0164",tcaron:"\u0165",Tcedil:"\u0162",tcedil:"\u0163",Tcy:"\u0422",tcy:"\u0442",tdot:"\u20db",telrec:"\u2315",Tfr:"\ud835\udd17",tfr:"\ud835\udd31",there4:"\u2234",therefore:"\u2234",Therefore:"\u2234",Theta:"\u0398",theta:"\u03b8",thetasym:"\u03d1",thetav:"\u03d1",thickapprox:"\u2248",thicksim:"\u223c",ThickSpace:"\u205f\u200a",ThinSpace:"\u2009",thinsp:"\u2009",thkap:"\u2248",thksim:"\u223c",THORN:"\xde",thorn:"\xfe",tilde:"\u02dc",Tilde:"\u223c",TildeEqual:"\u2243",TildeFullEqual:"\u2245",TildeTilde:"\u2248",timesbar:"\u2a31",timesb:"\u22a0",times:"\xd7",timesd:"\u2a30",tint:"\u222d",toea:"\u2928",topbot:"\u2336",topcir:"\u2af1",top:"\u22a4",Topf:"\ud835\udd4b",topf:"\ud835\udd65",topfork:"\u2ada",tosa:"\u2929",tprime:"\u2034",trade:"\u2122",TRADE:"\u2122",triangle:"\u25b5",triangledown:"\u25bf",triangleleft:"\u25c3",trianglelefteq:"\u22b4",triangleq:"\u225c",triangleright:"\u25b9",trianglerighteq:"\u22b5",tridot:"\u25ec",trie:"\u225c",triminus:"\u2a3a",TripleDot:"\u20db",triplus:"\u2a39",trisb:"\u29cd",tritime:"\u2a3b",trpezium:"\u23e2",Tscr:"\ud835\udcaf",tscr:"\ud835\udcc9",TScy:"\u0426",tscy:"\u0446",TSHcy:"\u040b",tshcy:"\u045b",Tstrok:"\u0166",tstrok:"\u0167",twixt:"\u226c",twoheadleftarrow:"\u219e",twoheadrightarrow:"\u21a0",Uacute:"\xda",uacute:"\xfa",uarr:"\u2191",Uarr:"\u219f",uArr:"\u21d1",Uarrocir:"\u2949",Ubrcy:"\u040e",ubrcy:"\u045e",Ubreve:"\u016c",ubreve:"\u016d",Ucirc:"\xdb",ucirc:"\xfb",Ucy:"\u0423",ucy:"\u0443",udarr:"\u21c5",Udblac:"\u0170",udblac:"\u0171",udhar:"\u296e",ufisht:"\u297e",Ufr:"\ud835\udd18",ufr:"\ud835\udd32",Ugrave:"\xd9",ugrave:"\xf9",uHar:"\u2963",uharl:"\u21bf",uharr:"\u21be",uhblk:"\u2580",ulcorn:"\u231c",ulcorner:"\u231c",ulcrop:"\u230f",ultri:"\u25f8",Umacr:"\u016a",umacr:"\u016b",uml:"\xa8",UnderBar:"_",UnderBrace:"\u23df",UnderBracket:"\u23b5",UnderParenthesis:"\u23dd",Union:"\u22c3",UnionPlus:"\u228e",Uogon:"\u0172",uogon:"\u0173",Uopf:"\ud835\udd4c",uopf:"\ud835\udd66",UpArrowBar:"\u2912",uparrow:"\u2191",UpArrow:"\u2191",Uparrow:"\u21d1",UpArrowDownArrow:"\u21c5",updownarrow:"\u2195",UpDownArrow:"\u2195",Updownarrow:"\u21d5",UpEquilibrium:"\u296e",upharpoonleft:"\u21bf",upharpoonright:"\u21be",uplus:"\u228e",UpperLeftArrow:"\u2196",UpperRightArrow:"\u2197",upsi:"\u03c5",Upsi:"\u03d2",upsih:"\u03d2",Upsilon:"\u03a5",upsilon:"\u03c5",UpTeeArrow:"\u21a5",UpTee:"\u22a5",upuparrows:"\u21c8",urcorn:"\u231d",urcorner:"\u231d",urcrop:"\u230e",Uring:"\u016e",uring:"\u016f",urtri:"\u25f9",Uscr:"\ud835\udcb0",uscr:"\ud835\udcca",utdot:"\u22f0",Utilde:"\u0168",utilde:"\u0169",utri:"\u25b5",utrif:"\u25b4",uuarr:"\u21c8",Uuml:"\xdc",uuml:"\xfc",uwangle:"\u29a7",vangrt:"\u299c",varepsilon:"\u03f5",varkappa:"\u03f0",varnothing:"\u2205",varphi:"\u03d5",varpi:"\u03d6",varpropto:"\u221d",varr:"\u2195",vArr:"\u21d5",varrho:"\u03f1",varsigma:"\u03c2",varsubsetneq:"\u228a\ufe00",varsubsetneqq:"\u2acb\ufe00",varsupsetneq:"\u228b\ufe00",varsupsetneqq:"\u2acc\ufe00",vartheta:"\u03d1",vartriangleleft:"\u22b2",vartriangleright:"\u22b3",vBar:"\u2ae8",Vbar:"\u2aeb",vBarv:"\u2ae9",Vcy:"\u0412",vcy:"\u0432",vdash:"\u22a2",vDash:"\u22a8",Vdash:"\u22a9",VDash:"\u22ab",Vdashl:"\u2ae6",veebar:"\u22bb",vee:"\u2228",Vee:"\u22c1",veeeq:"\u225a",vellip:"\u22ee",verbar:"|",Verbar:"\u2016",vert:"|",Vert:"\u2016",VerticalBar:"\u2223",VerticalLine:"|",VerticalSeparator:"\u2758",VerticalTilde:"\u2240",VeryThinSpace:"\u200a",Vfr:"\ud835\udd19",vfr:"\ud835\udd33",vltri:"\u22b2",vnsub:"\u2282\u20d2",vnsup:"\u2283\u20d2",Vopf:"\ud835\udd4d",vopf:"\ud835\udd67",vprop:"\u221d",vrtri:"\u22b3",Vscr:"\ud835\udcb1",vscr:"\ud835\udccb",vsubnE:"\u2acb\ufe00",vsubne:"\u228a\ufe00",vsupnE:"\u2acc\ufe00",vsupne:"\u228b\ufe00",Vvdash:"\u22aa",vzigzag:"\u299a",Wcirc:"\u0174",wcirc:"\u0175",wedbar:"\u2a5f",wedge:"\u2227",Wedge:"\u22c0",wedgeq:"\u2259",weierp:"\u2118",Wfr:"\ud835\udd1a",wfr:"\ud835\udd34",Wopf:"\ud835\udd4e",wopf:"\ud835\udd68",wp:"\u2118",wr:"\u2240",wreath:"\u2240",Wscr:"\ud835\udcb2",wscr:"\ud835\udccc",xcap:"\u22c2",xcirc:"\u25ef",xcup:"\u22c3",xdtri:"\u25bd",Xfr:"\ud835\udd1b",xfr:"\ud835\udd35",xharr:"\u27f7",xhArr:"\u27fa",Xi:"\u039e",xi:"\u03be",xlarr:"\u27f5",xlArr:"\u27f8",xmap:"\u27fc",xnis:"\u22fb",xodot:"\u2a00",Xopf:"\ud835\udd4f",xopf:"\ud835\udd69",xoplus:"\u2a01",xotime:"\u2a02",xrarr:"\u27f6",xrArr:"\u27f9",Xscr:"\ud835\udcb3",xscr:"\ud835\udccd",xsqcup:"\u2a06",xuplus:"\u2a04",xutri:"\u25b3",xvee:"\u22c1",xwedge:"\u22c0",Yacute:"\xdd",yacute:"\xfd",YAcy:"\u042f",yacy:"\u044f",Ycirc:"\u0176",ycirc:"\u0177",Ycy:"\u042b",ycy:"\u044b",yen:"\xa5",Yfr:"\ud835\udd1c",yfr:"\ud835\udd36",YIcy:"\u0407",yicy:"\u0457",Yopf:"\ud835\udd50",yopf:"\ud835\udd6a",Yscr:"\ud835\udcb4",yscr:"\ud835\udcce",YUcy:"\u042e",yucy:"\u044e",yuml:"\xff",Yuml:"\u0178",Zacute:"\u0179",zacute:"\u017a",Zcaron:"\u017d",zcaron:"\u017e",Zcy:"\u0417",zcy:"\u0437",Zdot:"\u017b",zdot:"\u017c",zeetrf:"\u2128",ZeroWidthSpace:"\u200b",Zeta:"\u0396",zeta:"\u03b6",zfr:"\ud835\udd37",Zfr:"\u2128",ZHcy:"\u0416",zhcy:"\u0436",zigrarr:"\u21dd",zopf:"\ud835\udd6b",Zopf:"\u2124",Zscr:"\ud835\udcb5",zscr:"\ud835\udccf",zwj:"\u200d",zwnj:"\u200c"}},{}],53:[function(c,e,r){"use strict";function n(t){return Array.prototype.slice.call(arguments,1).forEach(function(r){r&&Object.keys(r).forEach(function(e){t[e]=r[e]})}),t}function l(e){return Object.prototype.toString.call(e)}function u(e){return"[object Function]"===l(e)}function p(e){return e.replace(/[.?*+^$[\]\\(){}|-]/g,"\\$&")}var s={fuzzyLink:!0,fuzzyEmail:!0,fuzzyIP:!1};var o={"http:":{validate:function(e,r,t){var n=e.slice(r);return t.re.http||(t.re.http=new RegExp("^\\/\\/"+t.re.src_auth+t.re.src_host_port_strict+t.re.src_path,"i")),t.re.http.test(n)?n.match(t.re.http)[0].length:0}},"https:":"http:","ftp:":"http:","//":{validate:function(e,r,t){var n=e.slice(r);return t.re.no_http||(t.re.no_http=new RegExp("^"+t.re.src_auth+"(?:localhost|(?:(?:"+t.re.src_domain+")\\.)+"+t.re.src_domain_root+")"+t.re.src_port+t.re.src_host_terminator+t.re.src_path,"i")),t.re.no_http.test(n)?3<=r&&":"===e[r-3]?0:3<=r&&"/"===e[r-3]?0:n.match(t.re.no_http)[0].length:0}},"mailto:":{validate:function(e,r,t){var n=e.slice(r);return t.re.mailto||(t.re.mailto=new RegExp("^"+t.re.src_email_name+"@"+t.re.src_host_strict,"i")),t.re.mailto.test(n)?n.match(t.re.mailto)[0].length:0}}},h="a[cdefgilmnoqrstuwxz]|b[abdefghijmnorstvwyz]|c[acdfghiklmnoruvwxyz]|d[ejkmoz]|e[cegrstu]|f[ijkmor]|g[abdefghilmnpqrstuwy]|h[kmnrtu]|i[delmnoqrst]|j[emop]|k[eghimnprwyz]|l[abcikrstuvy]|m[acdeghklmnopqrstuvwxyz]|n[acefgilopruz]|om|p[aefghklmnrstwy]|qa|r[eosuw]|s[abcdeghijklmnortuvxyz]|t[cdfghjklmnortvwz]|u[agksyz]|v[aceginu]|w[fs]|y[et]|z[amw]",i="biz|com|edu|gov|net|org|pro|web|xxx|aero|asia|coop|info|museum|name|shop|\u0440\u0444".split("|");function a(s){var r=s.re=c("./lib/re")(s.__opts__),e=s.__tlds__.slice();function t(e){return e.replace("%TLDS%",r.src_tlds)}s.onCompile(),s.__tlds_replaced__||e.push(h),e.push(r.src_xn),r.src_tlds=e.join("|"),r.email_fuzzy=RegExp(t(r.tpl_email_fuzzy),"i"),r.link_fuzzy=RegExp(t(r.tpl_link_fuzzy),"i"),r.link_no_ip_fuzzy=RegExp(t(r.tpl_link_no_ip_fuzzy),"i"),r.host_fuzzy_test=RegExp(t(r.tpl_host_fuzzy_test),"i");var o=[];function i(e,r){throw new Error('(LinkifyIt) Invalid schema "'+e+'": '+r)}s.__compiled__={},Object.keys(s.__schemas__).forEach(function(e){var r=s.__schemas__[e];if(null!==r){var n,t={validate:null,link:null};if(s.__compiled__[e]=t,"[object Object]"===l(r))return"[object RegExp]"===l(r.validate)?t.validate=(n=r.validate,function(e,r){var t=e.slice(r);return n.test(t)?t.match(n)[0].length:0}):u(r.validate)?t.validate=r.validate:i(e,r),void(u(r.normalize)?t.normalize=r.normalize:r.normalize?i(e,r):t.normalize=function(e,r){r.normalize(e)});if("[object String]"!==l(r))i(e,r);else o.push(e)}}),o.forEach(function(e){s.__compiled__[s.__schemas__[e]]&&(s.__compiled__[e].validate=s.__compiled__[s.__schemas__[e]].validate,s.__compiled__[e].normalize=s.__compiled__[s.__schemas__[e]].normalize)}),s.__compiled__[""]={validate:null,normalize:function(e,r){r.normalize(e)}};var n,a=Object.keys(s.__compiled__).filter(function(e){return 0<e.length&&s.__compiled__[e]}).map(p).join("|");s.re.schema_test=RegExp("(^|(?!_)(?:[><\uff5c]|"+r.src_ZPCc+"))("+a+")","i"),s.re.schema_search=RegExp("(^|(?!_)(?:[><\uff5c]|"+r.src_ZPCc+"))("+a+")","ig"),s.re.pretest=RegExp("("+s.re.schema_test.source+")|("+s.re.host_fuzzy_test.source+")|@","i"),(n=s).__index__=-1,n.__text_cache__=""}function f(e,r){var t=new function(e,r){var t=e.__index__,n=e.__last_index__,s=e.__text_cache__.slice(t,n);this.schema=e.__schema__.toLowerCase(),this.index=t+r,this.lastIndex=n+r,this.raw=s,this.text=s,this.url=s}(e,r);return e.__compiled__[t.schema].normalize(t,e),t}function d(e,r){if(!(this instanceof d))return new d(e,r);var t;r||(t=e,Object.keys(t||{}).reduce(function(e,r){return e||s.hasOwnProperty(r)},!1)&&(r=e,e={})),this.__opts__=n({},s,r),this.__index__=-1,this.__last_index__=-1,this.__schema__="",this.__text_cache__="",this.__schemas__=n({},o,e),this.__compiled__={},this.__tlds__=i,this.__tlds_replaced__=!1,this.re={},a(this)}d.prototype.add=function(e,r){return this.__schemas__[e]=r,a(this),this},d.prototype.set=function(e){return this.__opts__=n(this.__opts__,e),this},d.prototype.test=function(e){if(this.__text_cache__=e,this.__index__=-1,!e.length)return!1;var r,t,n,s,o,i,a,c;if(this.re.schema_test.test(e))for((a=this.re.schema_search).lastIndex=0;null!==(r=a.exec(e));)if(s=this.testSchemaAt(e,r[2],a.lastIndex)){this.__schema__=r[2],this.__index__=r.index+r[1].length,this.__last_index__=r.index+r[0].length+s;break}return this.__opts__.fuzzyLink&&this.__compiled__["http:"]&&0<=(c=e.search(this.re.host_fuzzy_test))&&(this.__index__<0||c<this.__index__)&&null!==(t=e.match(this.__opts__.fuzzyIP?this.re.link_fuzzy:this.re.link_no_ip_fuzzy))&&(o=t.index+t[1].length,(this.__index__<0||o<this.__index__)&&(this.__schema__="",this.__index__=o,this.__last_index__=t.index+t[0].length)),this.__opts__.fuzzyEmail&&this.__compiled__["mailto:"]&&0<=e.indexOf("@")&&null!==(n=e.match(this.re.email_fuzzy))&&(o=n.index+n[1].length,i=n.index+n[0].length,(this.__index__<0||o<this.__index__||o===this.__index__&&i>this.__last_index__)&&(this.__schema__="mailto:",this.__index__=o,this.__last_index__=i)),0<=this.__index__},d.prototype.pretest=function(e){return this.re.pretest.test(e)},d.prototype.testSchemaAt=function(e,r,t){return this.__compiled__[r.toLowerCase()]?this.__compiled__[r.toLowerCase()].validate(e,t,this):0},d.prototype.match=function(e){var r=0,t=[];0<=this.__index__&&this.__text_cache__===e&&(t.push(f(this,r)),r=this.__last_index__);for(var n=r?e.slice(r):e;this.test(n);)t.push(f(this,r)),n=n.slice(this.__last_index__),r+=this.__last_index__;return t.length?t:null},d.prototype.tlds=function(e,r){return e=Array.isArray(e)?e:[e],r?this.__tlds__=this.__tlds__.concat(e).sort().filter(function(e,r,t){return e!==t[r-1]}).reverse():(this.__tlds__=e.slice(),this.__tlds_replaced__=!0),a(this),this},d.prototype.normalize=function(e){e.schema||(e.url="http://"+e.url),"mailto:"!==e.schema||/^mailto:/i.test(e.url)||(e.url="mailto:"+e.url)},d.prototype.onCompile=function(){},e.exports=d},{"./lib/re":54}],54:[function(n,e,r){"use strict";e.exports=function(e){var r={};r.src_Any=n("uc.micro/properties/Any/regex").source,r.src_Cc=n("uc.micro/categories/Cc/regex").source,r.src_Z=n("uc.micro/categories/Z/regex").source,r.src_P=n("uc.micro/categories/P/regex").source,r.src_ZPCc=[r.src_Z,r.src_P,r.src_Cc].join("|"),r.src_ZCc=[r.src_Z,r.src_Cc].join("|");var t="[><\uff5c]";return r.src_pseudo_letter="(?:(?![><\uff5c]|"+r.src_ZPCc+")"+r.src_Any+")",r.src_ip4="(?:(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)",r.src_auth="(?:(?:(?!"+r.src_ZCc+"|[@/\\[\\]()]).)+@)?",r.src_port="(?::(?:6(?:[0-4]\\d{3}|5(?:[0-4]\\d{2}|5(?:[0-2]\\d|3[0-5])))|[1-5]?\\d{1,4}))?",r.src_host_terminator="(?=$|[><\uff5c]|"+r.src_ZPCc+")(?!-|_|:\\d|\\.-|\\.(?!$|"+r.src_ZPCc+"))",r.src_path="(?:[/?#](?:(?!"+r.src_ZCc+"|"+t+"|[()[\\]{}.,\"'?!\\-]).|\\[(?:(?!"+r.src_ZCc+"|\\]).)*\\]|\\((?:(?!"+r.src_ZCc+"|[)]).)*\\)|\\{(?:(?!"+r.src_ZCc+'|[}]).)*\\}|\\"(?:(?!'+r.src_ZCc+'|["]).)+\\"|\\\'(?:(?!'+r.src_ZCc+"|[']).)+\\'|\\'(?="+r.src_pseudo_letter+"|[-]).|\\.{2,3}[a-zA-Z0-9%/]|\\.(?!"+r.src_ZCc+"|[.]).|"+(e&&e["---"]?"\\-(?!--(?:[^-]|$))(?:-*)|":"\\-+|")+"\\,(?!"+r.src_ZCc+").|\\!(?!"+r.src_ZCc+"|[!]).|\\?(?!"+r.src_ZCc+"|[?]).)+|\\/)?",r.src_email_name='[\\-;:&=\\+\\$,\\"\\.a-zA-Z0-9_]+',r.src_xn="xn--[a-z0-9\\-]{1,59}",r.src_domain_root="(?:"+r.src_xn+"|"+r.src_pseudo_letter+"{1,63})",r.src_domain="(?:"+r.src_xn+"|(?:"+r.src_pseudo_letter+")|(?:"+r.src_pseudo_letter+"(?:-(?!-)|"+r.src_pseudo_letter+"){0,61}"+r.src_pseudo_letter+"))",r.src_host="(?:(?:(?:(?:"+r.src_domain+")\\.)*"+r.src_domain+"))",r.tpl_host_fuzzy="(?:"+r.src_ip4+"|(?:(?:(?:"+r.src_domain+")\\.)+(?:%TLDS%)))",r.tpl_host_no_ip_fuzzy="(?:(?:(?:"+r.src_domain+")\\.)+(?:%TLDS%))",r.src_host_strict=r.src_host+r.src_host_terminator,r.tpl_host_fuzzy_strict=r.tpl_host_fuzzy+r.src_host_terminator,r.src_host_port_strict=r.src_host+r.src_port+r.src_host_terminator,r.tpl_host_port_fuzzy_strict=r.tpl_host_fuzzy+r.src_port+r.src_host_terminator,r.tpl_host_port_no_ip_fuzzy_strict=r.tpl_host_no_ip_fuzzy+r.src_port+r.src_host_terminator,r.tpl_host_fuzzy_test="localhost|www\\.|\\.\\d{1,3}\\.|(?:\\.(?:%TLDS%)(?:"+r.src_ZPCc+"|>|$))",r.tpl_email_fuzzy="(^|[><\uff5c]|\\(|"+r.src_ZCc+")("+r.src_email_name+"@"+r.tpl_host_fuzzy_strict+")",r.tpl_link_fuzzy="(^|(?![.:/\\-_@])(?:[$+<=>^`|\uff5c]|"+r.src_ZPCc+"))((?![$+<=>^`|\uff5c])"+r.tpl_host_port_fuzzy_strict+r.src_path+")",r.tpl_link_no_ip_fuzzy="(^|(?![.:/\\-_@])(?:[$+<=>^`|\uff5c]|"+r.src_ZPCc+"))((?![$+<=>^`|\uff5c])"+r.tpl_host_port_no_ip_fuzzy_strict+r.src_path+")",r}},{"uc.micro/categories/Cc/regex":61,"uc.micro/categories/P/regex":63,"uc.micro/categories/Z/regex":64,"uc.micro/properties/Any/regex":66}],55:[function(e,r,t){"use strict";var s={};function n(e,r){var l;return"string"!=typeof r&&(r=n.defaultChars),l=function(e){var r,t,n=s[e];if(n)return n;for(n=s[e]=[],r=0;r<128;r++)t=String.fromCharCode(r),n.push(t);for(r=0;r<e.length;r++)n[t=e.charCodeAt(r)]="%"+("0"+t.toString(16).toUpperCase()).slice(-2);return n}(r),e.replace(/(%[a-f0-9]{2})+/gi,function(e){var r,t,n,s,o,i,a,c="";for(r=0,t=e.length;r<t;r+=3)(n=parseInt(e.slice(r+1,r+3),16))<128?c+=l[n]:192==(224&n)&&r+3<t&&128==(192&(s=parseInt(e.slice(r+4,r+6),16)))?(c+=(a=n<<6&1984|63&s)<128?"\ufffd\ufffd":String.fromCharCode(a),r+=3):224==(240&n)&&r+6<t&&(s=parseInt(e.slice(r+4,r+6),16),o=parseInt(e.slice(r+7,r+9),16),128==(192&s)&&128==(192&o))?(c+=(a=n<<12&61440|s<<6&4032|63&o)<2048||55296<=a&&a<=57343?"\ufffd\ufffd\ufffd":String.fromCharCode(a),r+=6):240==(248&n)&&r+9<t&&(s=parseInt(e.slice(r+4,r+6),16),o=parseInt(e.slice(r+7,r+9),16),i=parseInt(e.slice(r+10,r+12),16),128==(192&s)&&128==(192&o)&&128==(192&i))?((a=n<<18&1835008|s<<12&258048|o<<6&4032|63&i)<65536||1114111<a?c+="\ufffd\ufffd\ufffd\ufffd":(a-=65536,c+=String.fromCharCode(55296+(a>>10),56320+(1023&a))),r+=9):c+="\ufffd";return c})}n.defaultChars=";/?:@&=+$,#",n.componentChars="",r.exports=n},{}],56:[function(e,r,t){"use strict";var l={};function u(e,r,t){var n,s,o,i,a,c="";for("string"!=typeof r&&(t=r,r=u.defaultChars),void 0===t&&(t=!0),a=function(e){var r,t,n=l[e];if(n)return n;for(n=l[e]=[],r=0;r<128;r++)t=String.fromCharCode(r),/^[0-9a-z]$/i.test(t)?n.push(t):n.push("%"+("0"+r.toString(16).toUpperCase()).slice(-2));for(r=0;r<e.length;r++)n[e.charCodeAt(r)]=e[r];return n}(r),n=0,s=e.length;n<s;n++)if(o=e.charCodeAt(n),t&&37===o&&n+2<s&&/^[0-9a-f]{2}$/i.test(e.slice(n+1,n+3)))c+=e.slice(n,n+3),n+=2;else if(o<128)c+=a[o];else if(55296<=o&&o<=57343){if(55296<=o&&o<=56319&&n+1<s&&56320<=(i=e.charCodeAt(n+1))&&i<=57343){c+=encodeURIComponent(e[n]+e[n+1]),n++;continue}c+="%EF%BF%BD"}else c+=encodeURIComponent(e[n]);return c}u.defaultChars=";/?:@&=+$,-_.!~*'()#",u.componentChars="-_.!~*'()",r.exports=u},{}],57:[function(e,r,t){"use strict";r.exports=function(e){var r="";return r+=e.protocol||"",r+=e.slashes?"//":"",r+=e.auth?e.auth+"@":"",e.hostname&&-1!==e.hostname.indexOf(":")?r+="["+e.hostname+"]":r+=e.hostname||"",r+=e.port?":"+e.port:"",r+=e.pathname||"",r+=e.search||"",r+=e.hash||""}},{}],58:[function(e,r,t){"use strict";r.exports.encode=e("./encode"),r.exports.decode=e("./decode"),r.exports.format=e("./format"),r.exports.parse=e("./parse")},{"./decode":55,"./encode":56,"./format":57,"./parse":59}],59:[function(e,r,t){"use strict";function n(){this.protocol=null,this.slashes=null,this.auth=null,this.port=null,this.hostname=null,this.hash=null,this.search=null,this.pathname=null}var w=/^([a-z0-9.+-]+:)/i,s=/:[0-9]*$/,D=/^(\/\/?(?!\/)[^\?\s]*)(\?[^\s]*)?$/,o=["{","}","|","\\","^","`"].concat(["<",">",'"',"`"," ","\r","\n","\t"]),i=["'"].concat(o),E=["%","/","?",";","#"].concat(i),q=["/","?","#"],S=/^[+a-z0-9A-Z_-]{0,63}$/,F=/^([+a-z0-9A-Z_-]{0,63})(.*)$/,L={javascript:!0,"javascript:":!0},z={http:!0,https:!0,ftp:!0,gopher:!0,file:!0,"http:":!0,"https:":!0,"ftp:":!0,"gopher:":!0,"file:":!0};n.prototype.parse=function(e,r){var t,n,s,o,i,a=e;if(a=a.trim(),!r&&1===e.split("#").length){var c=D.exec(a);if(c)return this.pathname=c[1],c[2]&&(this.search=c[2]),this}var l=w.exec(a);if(l&&(s=(l=l[0]).toLowerCase(),this.protocol=l,a=a.substr(l.length)),(r||l||a.match(/^\/\/[^@\/]+@[^@\/]+/))&&(!(i="//"===a.substr(0,2))||l&&L[l]||(a=a.substr(2),this.slashes=!0)),!L[l]&&(i||l&&!z[l])){var u,p,h=-1;for(t=0;t<q.length;t++)-1!==(o=a.indexOf(q[t]))&&(-1===h||o<h)&&(h=o);for(-1!==(p=-1===h?a.lastIndexOf("@"):a.lastIndexOf("@",h))&&(u=a.slice(0,p),a=a.slice(p+1),this.auth=u),h=-1,t=0;t<E.length;t++)-1!==(o=a.indexOf(E[t]))&&(-1===h||o<h)&&(h=o);-1===h&&(h=a.length),":"===a[h-1]&&h--;var f=a.slice(0,h);a=a.slice(h),this.parseHost(f),this.hostname=this.hostname||"";var d="["===this.hostname[0]&&"]"===this.hostname[this.hostname.length-1];if(!d){var m=this.hostname.split(/\./);for(t=0,n=m.length;t<n;t++){var _=m[t];if(_&&!_.match(S)){for(var g="",b=0,k=_.length;b<k;b++)127<_.charCodeAt(b)?g+="x":g+=_[b];if(!g.match(S)){var v=m.slice(0,t),x=m.slice(t+1),y=_.match(F);y&&(v.push(y[1]),x.unshift(y[2])),x.length&&(a=x.join(".")+a),this.hostname=v.join(".");break}}}}255<this.hostname.length&&(this.hostname=""),d&&(this.hostname=this.hostname.substr(1,this.hostname.length-2))}var C=a.indexOf("#");-1!==C&&(this.hash=a.substr(C),a=a.slice(0,C));var A=a.indexOf("?");return-1!==A&&(this.search=a.substr(A),a=a.slice(0,A)),a&&(this.pathname=a),z[s]&&this.hostname&&!this.pathname&&(this.pathname=""),this},n.prototype.parseHost=function(e){var r=s.exec(e);r&&(":"!==(r=r[0])&&(this.port=r.substr(1)),e=e.substr(0,e.length-r.length)),e&&(this.hostname=e)},r.exports=function(e,r){if(e&&e instanceof n)return e;var t=new n;return t.parse(e,r),t}},{}],60:[function(e,z,T){(function(L){!function(e){var r="object"==typeof T&&T&&!T.nodeType&&T,t="object"==typeof z&&z&&!z.nodeType&&z,n="object"==typeof L&&L;n.global!==n&&n.window!==n&&n.self!==n||(e=n);var s,o,g=2147483647,b=36,k=1,v=26,i=38,a=700,x=72,y=128,C="-",c=/^xn--/,l=/[^\x20-\x7E]/,u=/[\x2E\u3002\uFF0E\uFF61]/g,p={overflow:"Overflow: input needs wider integers to process","not-basic":"Illegal input >= 0x80 (not a basic code point)","invalid-input":"Invalid input"},h=b-k,A=Math.floor,w=String.fromCharCode;function D(e){throw new RangeError(p[e])}function f(e,r){for(var t=e.length,n=[];t--;)n[t]=r(e[t]);return n}function d(e,r){var t=e.split("@"),n="";return 1<t.length&&(n=t[0]+"@",e=t[1]),n+f((e=e.replace(u,".")).split("."),r).join(".")}function E(e){for(var r,t,n=[],s=0,o=e.length;s<o;)55296<=(r=e.charCodeAt(s++))&&r<=56319&&s<o?56320==(64512&(t=e.charCodeAt(s++)))?n.push(((1023&r)<<10)+(1023&t)+65536):(n.push(r),s--):n.push(r);return n}function q(e){return f(e,function(e){var r="";return 65535<e&&(r+=w((e-=65536)>>>10&1023|55296),e=56320|1023&e),r+=w(e)}).join("")}function S(e,r){return e+22+75*(e<26)-((0!=r)<<5)}function F(e,r,t){var n=0;for(e=t?A(e/a):e>>1,e+=A(e/r);h*v>>1<e;n+=b)e=A(e/h);return A(n+(h+1)*e/(e+i))}function m(e){var r,t,n,s,o,i,a,c,l,u,p,h=[],f=e.length,d=0,m=y,_=x;for((t=e.lastIndexOf(C))<0&&(t=0),n=0;n<t;++n)128<=e.charCodeAt(n)&&D("not-basic"),h.push(e.charCodeAt(n));for(s=0<t?t+1:0;s<f;){for(o=d,i=1,a=b;f<=s&&D("invalid-input"),p=e.charCodeAt(s++),(b<=(c=p-48<10?p-22:p-65<26?p-65:p-97<26?p-97:b)||c>A((g-d)/i))&&D("overflow"),d+=c*i,!(c<(l=a<=_?k:_+v<=a?v:a-_));a+=b)i>A(g/(u=b-l))&&D("overflow"),i*=u;_=F(d-o,r=h.length+1,0==o),A(d/r)>g-m&&D("overflow"),m+=A(d/r),d%=r,h.splice(d++,0,m)}return q(h)}function _(e){var r,t,n,s,o,i,a,c,l,u,p,h,f,d,m,_=[];for(h=(e=E(e)).length,r=y,o=x,i=t=0;i<h;++i)(p=e[i])<128&&_.push(w(p));for(n=s=_.length,s&&_.push(C);n<h;){for(a=g,i=0;i<h;++i)r<=(p=e[i])&&p<a&&(a=p);for(a-r>A((g-t)/(f=n+1))&&D("overflow"),t+=(a-r)*f,r=a,i=0;i<h;++i)if((p=e[i])<r&&++t>g&&D("overflow"),p==r){for(c=t,l=b;!(c<(u=l<=o?k:o+v<=l?v:l-o));l+=b)m=c-u,d=b-u,_.push(w(S(u+m%d,0))),c=A(m/d);_.push(w(S(c,0))),o=F(t,f,n==s),t=0,++n}++t,++r}return _.join("")}if(s={version:"1.4.1",ucs2:{decode:E,encode:q},decode:m,encode:_,toASCII:function(e){return d(e,function(e){return l.test(e)?"xn--"+_(e):e})},toUnicode:function(e){return d(e,function(e){return c.test(e)?m(e.slice(4).toLowerCase()):e})}},r&&t)if(z.exports==r)t.exports=s;else for(o in s)s.hasOwnProperty(o)&&(r[o]=s[o]);else e.punycode=s}(this)}).call(this,"undefined"!=typeof global?global:"undefined"!=typeof self?self:"undefined"!=typeof window?window:{})},{}],61:[function(e,r,t){r.exports=/[\0-\x1F\x7F-\x9F]/},{}],62:[function(e,r,t){r.exports=/[\xAD\u0600-\u0605\u061C\u06DD\u070F\u08E2\u180E\u200B-\u200F\u202A-\u202E\u2060-\u2064\u2066-\u206F\uFEFF\uFFF9-\uFFFB]|\uD804\uDCBD|\uD82F[\uDCA0-\uDCA3]|\uD834[\uDD73-\uDD7A]|\uDB40[\uDC01\uDC20-\uDC7F]/},{}],63:[function(e,r,t){r.exports=/[!-#%-\*,-/:;\?@\[-\]_\{\}\xA1\xA7\xAB\xB6\xB7\xBB\xBF\u037E\u0387\u055A-\u055F\u0589\u058A\u05BE\u05C0\u05C3\u05C6\u05F3\u05F4\u0609\u060A\u060C\u060D\u061B\u061E\u061F\u066A-\u066D\u06D4\u0700-\u070D\u07F7-\u07F9\u0830-\u083E\u085E\u0964\u0965\u0970\u09FD\u0AF0\u0DF4\u0E4F\u0E5A\u0E5B\u0F04-\u0F12\u0F14\u0F3A-\u0F3D\u0F85\u0FD0-\u0FD4\u0FD9\u0FDA\u104A-\u104F\u10FB\u1360-\u1368\u1400\u166D\u166E\u169B\u169C\u16EB-\u16ED\u1735\u1736\u17D4-\u17D6\u17D8-\u17DA\u1800-\u180A\u1944\u1945\u1A1E\u1A1F\u1AA0-\u1AA6\u1AA8-\u1AAD\u1B5A-\u1B60\u1BFC-\u1BFF\u1C3B-\u1C3F\u1C7E\u1C7F\u1CC0-\u1CC7\u1CD3\u2010-\u2027\u2030-\u2043\u2045-\u2051\u2053-\u205E\u207D\u207E\u208D\u208E\u2308-\u230B\u2329\u232A\u2768-\u2775\u27C5\u27C6\u27E6-\u27EF\u2983-\u2998\u29D8-\u29DB\u29FC\u29FD\u2CF9-\u2CFC\u2CFE\u2CFF\u2D70\u2E00-\u2E2E\u2E30-\u2E49\u3001-\u3003\u3008-\u3011\u3014-\u301F\u3030\u303D\u30A0\u30FB\uA4FE\uA4FF\uA60D-\uA60F\uA673\uA67E\uA6F2-\uA6F7\uA874-\uA877\uA8CE\uA8CF\uA8F8-\uA8FA\uA8FC\uA92E\uA92F\uA95F\uA9C1-\uA9CD\uA9DE\uA9DF\uAA5C-\uAA5F\uAADE\uAADF\uAAF0\uAAF1\uABEB\uFD3E\uFD3F\uFE10-\uFE19\uFE30-\uFE52\uFE54-\uFE61\uFE63\uFE68\uFE6A\uFE6B\uFF01-\uFF03\uFF05-\uFF0A\uFF0C-\uFF0F\uFF1A\uFF1B\uFF1F\uFF20\uFF3B-\uFF3D\uFF3F\uFF5B\uFF5D\uFF5F-\uFF65]|\uD800[\uDD00-\uDD02\uDF9F\uDFD0]|\uD801\uDD6F|\uD802[\uDC57\uDD1F\uDD3F\uDE50-\uDE58\uDE7F\uDEF0-\uDEF6\uDF39-\uDF3F\uDF99-\uDF9C]|\uD804[\uDC47-\uDC4D\uDCBB\uDCBC\uDCBE-\uDCC1\uDD40-\uDD43\uDD74\uDD75\uDDC5-\uDDC9\uDDCD\uDDDB\uDDDD-\uDDDF\uDE38-\uDE3D\uDEA9]|\uD805[\uDC4B-\uDC4F\uDC5B\uDC5D\uDCC6\uDDC1-\uDDD7\uDE41-\uDE43\uDE60-\uDE6C\uDF3C-\uDF3E]|\uD806[\uDE3F-\uDE46\uDE9A-\uDE9C\uDE9E-\uDEA2]|\uD807[\uDC41-\uDC45\uDC70\uDC71]|\uD809[\uDC70-\uDC74]|\uD81A[\uDE6E\uDE6F\uDEF5\uDF37-\uDF3B\uDF44]|\uD82F\uDC9F|\uD836[\uDE87-\uDE8B]|\uD83A[\uDD5E\uDD5F]/},{}],64:[function(e,r,t){r.exports=/[ \xA0\u1680\u2000-\u200A\u202F\u205F\u3000]/},{}],65:[function(e,r,t){"use strict";t.Any=e("./properties/Any/regex"),t.Cc=e("./categories/Cc/regex"),t.Cf=e("./categories/Cf/regex"),t.P=e("./categories/P/regex"),t.Z=e("./categories/Z/regex")},{"./categories/Cc/regex":61,"./categories/Cf/regex":62,"./categories/P/regex":63,"./categories/Z/regex":64,"./properties/Any/regex":66}],66:[function(e,r,t){r.exports=/[\0-\uD7FF\uE000-\uFFFF]|[\uD800-\uDBFF][\uDC00-\uDFFF]|[\uD800-\uDBFF](?![\uDC00-\uDFFF])|(?:[^\uD800-\uDBFF]|^)[\uDC00-\uDFFF]/},{}],67:[function(e,r,t){"use strict";r.exports=e("./lib/")},{"./lib/":9}]},{},[67])(67)});